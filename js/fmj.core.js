if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'fmj.core'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'fmj.core'.");
}
this['fmj.core'] = function (_, Kotlin) {
  'use strict';
  var $$importsForInline$$ = _.$$importsForInline$$ || (_.$$importsForInline$$ = {});
  var Unit = Kotlin.kotlin.Unit;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var Enum = Kotlin.kotlin.Enum;
  var throwISE = Kotlin.throwISE;
  var ensureNotNull = Kotlin.ensureNotNull;
  var equals = Kotlin.equals;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var throwUPAE = Kotlin.throwUPAE;
  var kotlin_js_internal_IntCompanionObject = Kotlin.kotlin.js.internal.IntCompanionObject;
  var throwCCE = Kotlin.throwCCE;
  var Error_0 = Kotlin.kotlin.Error;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var StringBuilder = Kotlin.kotlin.text.StringBuilder;
  var until = Kotlin.kotlin.ranges.until_dqglrj$;
  var downTo = Kotlin.kotlin.ranges.downTo_dqglrj$;
  var get_indices = Kotlin.kotlin.collections.get_indices_gzk92b$;
  var get_indices_0 = Kotlin.kotlin.collections.get_indices_tmsbgo$;
  var reversed = Kotlin.kotlin.ranges.reversed_zf1xzc$;
  var mutableListOf = Kotlin.kotlin.collections.mutableListOf_i5x0yv$;
  var get_indices_1 = Kotlin.kotlin.collections.get_indices_m7z4lg$;
  var numberToInt = Kotlin.numberToInt;
  var removeAll = Kotlin.kotlin.collections.removeAll_qafx1e$;
  var first = Kotlin.kotlin.collections.first_2p1efm$;
  var last = Kotlin.kotlin.collections.last_2p1efm$;
  var toMutableList = Kotlin.kotlin.collections.toMutableList_4c7yge$;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
  var toByte = Kotlin.toByte;
  var max = Kotlin.kotlin.collections.max_exjks8$;
  var toMutableList_0 = Kotlin.kotlin.collections.toMutableList_964n91$;
  var toByteArray = Kotlin.kotlin.collections.toByteArray_kdx1v$;
  var toString = Kotlin.toString;
  var PropertyMetadata = Kotlin.PropertyMetadata;
  var lazy = Kotlin.kotlin.lazy_klfg04$;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var get_indices_2 = Kotlin.kotlin.text.get_indices_gw00vp$;
  var HashMap_init = Kotlin.kotlin.collections.HashMap_init_xf5xz2$;
  var NotImplementedError = Kotlin.kotlin.NotImplementedError;
  var split = Kotlin.kotlin.text.split_o64adg$;
  var IllegalArgumentException = Kotlin.kotlin.IllegalArgumentException;
  var abs = Kotlin.kotlin.math.abs_za3lpa$;
  var sliceArray = Kotlin.kotlin.collections.sliceArray_dww5cs$;
  var toTypedArray = Kotlin.kotlin.collections.toTypedArray_964n91$;
  var indexOf = Kotlin.kotlin.collections.indexOf_jlnu8a$;
  var slice = Kotlin.kotlin.collections.slice_dww5cs$;
  var toByteArray_0 = Kotlin.kotlin.collections.toByteArray_vn5r1x$;
  var MutableList = Kotlin.kotlin.collections.MutableList;
  var CharRange = Kotlin.kotlin.ranges.CharRange;
  var joinToString = Kotlin.kotlin.collections.joinToString_s78119$;
  var defineInlineFunction = Kotlin.defineInlineFunction;
  var wrapFunction = Kotlin.wrapFunction;
  var toList = Kotlin.kotlin.collections.toList_964n91$;
  ScreenViewType.prototype = Object.create(Enum.prototype);
  ScreenViewType.prototype.constructor = ScreenViewType;
  Character$State.prototype = Object.create(Enum.prototype);
  Character$State.prototype.constructor = Character$State;
  Character.prototype = Object.create(ResBase.prototype);
  Character.prototype.constructor = Character;
  Direction.prototype = Object.create(Enum.prototype);
  Direction.prototype.constructor = Direction;
  FightingCharacter.prototype = Object.create(Character.prototype);
  FightingCharacter.prototype.constructor = FightingCharacter;
  Monster.prototype = Object.create(FightingCharacter.prototype);
  Monster.prototype.constructor = Monster;
  NPC.prototype = Object.create(Character.prototype);
  NPC.prototype.constructor = NPC;
  Player.prototype = Object.create(FightingCharacter.prototype);
  Player.prototype.constructor = Player;
  ResLevelupChain.prototype = Object.create(ResBase.prototype);
  ResLevelupChain.prototype.constructor = ResLevelupChain;
  SceneObj.prototype = Object.create(NPC.prototype);
  SceneObj.prototype.constructor = SceneObj;
  Combat$CombatState.prototype = Object.create(Enum.prototype);
  Combat$CombatState.prototype.constructor = Combat$CombatState;
  Combat.prototype = Object.create(BaseScreen.prototype);
  Combat.prototype.constructor = Combat;
  ActionCoopMagic.prototype = Object.create(Action.prototype);
  ActionCoopMagic.prototype.constructor = ActionCoopMagic;
  ActionSingleTarget.prototype = Object.create(Action.prototype);
  ActionSingleTarget.prototype.constructor = ActionSingleTarget;
  ActionDefend.prototype = Object.create(ActionSingleTarget.prototype);
  ActionDefend.prototype.constructor = ActionDefend;
  ActionFlee.prototype = Object.create(Action.prototype);
  ActionFlee.prototype.constructor = ActionFlee;
  ActionMultiTarget.prototype = Object.create(Action.prototype);
  ActionMultiTarget.prototype.constructor = ActionMultiTarget;
  ActionMagicAttackAll.prototype = Object.create(ActionMultiTarget.prototype);
  ActionMagicAttackAll.prototype.constructor = ActionMagicAttackAll;
  ActionMagicAttackOne.prototype = Object.create(ActionSingleTarget.prototype);
  ActionMagicAttackOne.prototype.constructor = ActionMagicAttackOne;
  ActionMagicHelpAll.prototype = Object.create(ActionMultiTarget.prototype);
  ActionMagicHelpAll.prototype.constructor = ActionMagicHelpAll;
  ActionMagicHelpOne.prototype = Object.create(ActionSingleTarget.prototype);
  ActionMagicHelpOne.prototype.constructor = ActionMagicHelpOne;
  ActionPhysicalAttackAll.prototype = Object.create(ActionMultiTarget.prototype);
  ActionPhysicalAttackAll.prototype.constructor = ActionPhysicalAttackAll;
  ActionPhysicalAttackOne.prototype = Object.create(ActionSingleTarget.prototype);
  ActionPhysicalAttackOne.prototype.constructor = ActionPhysicalAttackOne;
  ActionThrowItemAll.prototype = Object.create(ActionMultiTarget.prototype);
  ActionThrowItemAll.prototype.constructor = ActionThrowItemAll;
  ActionThrowItemOne.prototype = Object.create(ActionSingleTarget.prototype);
  ActionThrowItemOne.prototype.constructor = ActionThrowItemOne;
  ActionUseItemAll.prototype = Object.create(ActionMultiTarget.prototype);
  ActionUseItemAll.prototype.constructor = ActionUseItemAll;
  ActionUseItemOne.prototype = Object.create(ActionSingleTarget.prototype);
  ActionUseItemOne.prototype.constructor = ActionUseItemOne;
  CombatSuccess$MsgScreen.prototype = Object.create(BaseScreen.prototype);
  CombatSuccess$MsgScreen.prototype.constructor = CombatSuccess$MsgScreen;
  CombatSuccess$LevelupScreen.prototype = Object.create(BaseScreen.prototype);
  CombatSuccess$LevelupScreen.prototype.constructor = CombatSuccess$LevelupScreen;
  CombatSuccess$LearnMagicScreen.prototype = Object.create(BaseScreen.prototype);
  CombatSuccess$LearnMagicScreen.prototype.constructor = CombatSuccess$LearnMagicScreen;
  CombatUI$MainMenu.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MainMenu.prototype.constructor = CombatUI$MainMenu;
  CombatUI$MenuCharacterSelect.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MenuCharacterSelect.prototype.constructor = CombatUI$MenuCharacterSelect;
  CombatUI$MenuMisc$MenuState.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MenuMisc$MenuState.prototype.constructor = CombatUI$MenuMisc$MenuState;
  CombatUI$MenuMisc.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MenuMisc.prototype.constructor = CombatUI$MenuMisc;
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype.constructor = CombatUI$MenuGoods$equipSelected$ObjectLiteral;
  CombatUI$MenuGoods.prototype = Object.create(BaseScreen.prototype);
  CombatUI$MenuGoods.prototype.constructor = CombatUI$MenuGoods;
  CombatUI.prototype = Object.create(BaseScreen.prototype);
  CombatUI.prototype.constructor = CombatUI;
  ScreenActorState.prototype = Object.create(BaseScreen.prototype);
  ScreenActorState.prototype.constructor = ScreenActorState;
  ScreenActorWearing.prototype = Object.create(BaseScreen.prototype);
  ScreenActorWearing.prototype.constructor = ScreenActorWearing;
  ScreenChgEquipment.prototype = Object.create(BaseScreen.prototype);
  ScreenChgEquipment.prototype.constructor = ScreenChgEquipment;
  ScreenCommonMenu.prototype = Object.create(BaseScreen.prototype);
  ScreenCommonMenu.prototype.constructor = ScreenCommonMenu;
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype = Object.create(BaseScreen.prototype);
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.constructor = ScreenGameMainMenu$screenSelectActor$ObjectLiteral;
  ScreenGameMainMenu.prototype = Object.create(BaseScreen.prototype);
  ScreenGameMainMenu.prototype.constructor = ScreenGameMainMenu;
  ScreenGoodsList$Mode.prototype = Object.create(Enum.prototype);
  ScreenGoodsList$Mode.prototype.constructor = ScreenGoodsList$Mode;
  ScreenGoodsList.prototype = Object.create(BaseScreen.prototype);
  ScreenGoodsList.prototype.constructor = ScreenGoodsList;
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype = Object.create(BaseScreen.prototype);
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype.constructor = ScreenMenuGoods$equipSelected$ObjectLiteral;
  ScreenMenuGoods.prototype = Object.create(BaseScreen.prototype);
  ScreenMenuGoods.prototype.constructor = ScreenMenuGoods;
  ScreenMenuProperties.prototype = Object.create(BaseScreen.prototype);
  ScreenMenuProperties.prototype.constructor = ScreenMenuProperties;
  ScreenMenuSystem.prototype = Object.create(BaseScreen.prototype);
  ScreenMenuSystem.prototype.constructor = ScreenMenuSystem;
  ScreenTakeMedicine.prototype = Object.create(BaseScreen.prototype);
  ScreenTakeMedicine.prototype.constructor = ScreenTakeMedicine;
  ScreenUseMagic.prototype = Object.create(BaseScreen.prototype);
  ScreenUseMagic.prototype.constructor = ScreenUseMagic;
  BaseGoods.prototype = Object.create(ResBase.prototype);
  BaseGoods.prototype.constructor = BaseGoods;
  GoodsEquipment.prototype = Object.create(BaseGoods.prototype);
  GoodsEquipment.prototype.constructor = GoodsEquipment;
  GoodsDecorations.prototype = Object.create(GoodsEquipment.prototype);
  GoodsDecorations.prototype.constructor = GoodsDecorations;
  GoodsDrama.prototype = Object.create(BaseGoods.prototype);
  GoodsDrama.prototype.constructor = GoodsDrama;
  GoodsHiddenWeapon.prototype = Object.create(BaseGoods.prototype);
  GoodsHiddenWeapon.prototype.constructor = GoodsHiddenWeapon;
  GoodsMedicine.prototype = Object.create(BaseGoods.prototype);
  GoodsMedicine.prototype.constructor = GoodsMedicine;
  GoodsMedicineChg4Ever.prototype = Object.create(BaseGoods.prototype);
  GoodsMedicineChg4Ever.prototype.constructor = GoodsMedicineChg4Ever;
  GoodsMedicineLife.prototype = Object.create(BaseGoods.prototype);
  GoodsMedicineLife.prototype.constructor = GoodsMedicineLife;
  GoodsStimulant.prototype = Object.create(BaseGoods.prototype);
  GoodsStimulant.prototype.constructor = GoodsStimulant;
  GoodsTudun.prototype = Object.create(BaseGoods.prototype);
  GoodsTudun.prototype.constructor = GoodsTudun;
  GoodsWeapon.prototype = Object.create(GoodsEquipment.prototype);
  GoodsWeapon.prototype.constructor = GoodsWeapon;
  DatLib$ResType.prototype = Object.create(Enum.prototype);
  DatLib$ResType.prototype.constructor = DatLib$ResType;
  ResGut.prototype = Object.create(ResBase.prototype);
  ResGut.prototype.constructor = ResGut;
  ResImage.prototype = Object.create(ResBase.prototype);
  ResImage.prototype.constructor = ResImage;
  ResMap.prototype = Object.create(ResBase.prototype);
  ResMap.prototype.constructor = ResMap;
  ResSrs.prototype = Object.create(ResBase.prototype);
  ResSrs.prototype.constructor = ResSrs;
  BaseMagic.prototype = Object.create(ResBase.prototype);
  BaseMagic.prototype.constructor = BaseMagic;
  MagicAttack.prototype = Object.create(BaseMagic.prototype);
  MagicAttack.prototype.constructor = MagicAttack;
  MagicAuxiliary.prototype = Object.create(BaseMagic.prototype);
  MagicAuxiliary.prototype.constructor = MagicAuxiliary;
  MagicEnhance.prototype = Object.create(BaseMagic.prototype);
  MagicEnhance.prototype.constructor = MagicEnhance;
  MagicRestore.prototype = Object.create(BaseMagic.prototype);
  MagicRestore.prototype.constructor = MagicRestore;
  MagicSpecial.prototype = Object.create(BaseMagic.prototype);
  MagicSpecial.prototype.constructor = MagicSpecial;
  ResMagicChain.prototype = Object.create(ResBase.prototype);
  ResMagicChain.prototype.constructor = ResMagicChain;
  ScreenMagic.prototype = Object.create(BaseScreen.prototype);
  ScreenMagic.prototype.constructor = ScreenMagic;
  ScreenMainGame.prototype = Object.create(BaseScreen.prototype);
  ScreenMainGame.prototype.constructor = ScreenMainGame;
  OperateNop.prototype = Object.create(Operate.prototype);
  OperateNop.prototype.constructor = OperateNop;
  OperateAdapter.prototype = Object.create(Operate.prototype);
  OperateAdapter.prototype.constructor = OperateAdapter;
  OperateBuy$BuyGoodsScreen.prototype = Object.create(BaseScreen.prototype);
  OperateBuy$BuyGoodsScreen.prototype.constructor = OperateBuy$BuyGoodsScreen;
  OperateBuy.prototype = Object.create(Operate.prototype);
  OperateBuy.prototype.constructor = OperateBuy;
  OperateDrawOnce.prototype = Object.create(Operate.prototype);
  OperateDrawOnce.prototype.constructor = OperateDrawOnce;
  OperateSale$SaleGoodsScreen.prototype = Object.create(BaseScreen.prototype);
  OperateSale$SaleGoodsScreen.prototype.constructor = OperateSale$SaleGoodsScreen;
  OperateSale.prototype = Object.create(Operate.prototype);
  OperateSale.prototype.constructor = OperateSale;
  ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateDrawOnce.prototype);
  ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateDrawOnce.prototype);
  ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateDrawOnce.prototype);
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateDrawOnce.prototype);
  ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(Operate.prototype);
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral;
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype = Object.create(OperateAdapter.prototype);
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype.constructor = ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral;
  ScreenAnimation.prototype = Object.create(BaseScreen.prototype);
  ScreenAnimation.prototype.constructor = ScreenAnimation;
  ScreenMenu.prototype = Object.create(BaseScreen.prototype);
  ScreenMenu.prototype.constructor = ScreenMenu;
  ScreenMessageBox.prototype = Object.create(BaseScreen.prototype);
  ScreenMessageBox.prototype.constructor = ScreenMessageBox;
  ScreenSaveLoadGame$Operate.prototype = Object.create(Enum.prototype);
  ScreenSaveLoadGame$Operate.prototype.constructor = ScreenSaveLoadGame$Operate;
  ScreenSaveLoadGame.prototype = Object.create(BaseScreen.prototype);
  ScreenSaveLoadGame.prototype.constructor = ScreenSaveLoadGame;
  ScreenStack$showMessage$ObjectLiteral.prototype = Object.create(BaseScreen.prototype);
  ScreenStack$showMessage$ObjectLiteral.prototype.constructor = ScreenStack$showMessage$ObjectLiteral;
  Paint$Style.prototype = Object.create(Enum.prototype);
  Paint$Style.prototype.constructor = Paint$Style;
  function GameView() {
    this.screen_0 = new ScreenStack();
    this.canvas_8be2vx$ = new Canvas(Bitmap_init(Global_getInstance().SCREEN_WIDTH, Global_getInstance().SCREEN_HEIGHT));
    this.delta_0 = 40;
  }
  GameView.prototype.start = function () {
    ScriptProcess$Companion_getInstance().instance.delegate = this.screen_0;
    this.listenUIEvents_0();
    var scr = new ScreenAnimation(247);
    this.screen_0.pushScreen_2o7n0o$(scr);
  };
  GameView.prototype.draw = function () {
    this.screen_0.draw_9in0vv$(this.canvas_8be2vx$);
  };
  GameView.prototype.update_s8cxhz$ = function (delta) {
    this.screen_0.update_s8cxhz$(delta);
  };
  GameView.prototype.keyDown_0 = function (key) {
    this.screen_0.keyDown_za3lpa$(key);
  };
  GameView.prototype.keyUp_0 = function (key) {
    this.screen_0.keyUp_za3lpa$(key);
  };
  function GameView$listenUIEvents$lambda(this$GameView) {
    return function (it) {
      this$GameView.keyDown_0(it);
      return Unit;
    };
  }
  function GameView$listenUIEvents$lambda_0(this$GameView) {
    return function (it) {
      this$GameView.keyUp_0(it);
      return Unit;
    };
  }
  function GameView$listenUIEvents$lambda_1(this$GameView) {
    return function () {
      this$GameView.update_s8cxhz$(Kotlin.Long.fromInt(40));
      this$GameView.draw();
      sysDrawScreen(this$GameView.canvas_8be2vx$.buffer, this$GameView.canvas_8be2vx$.width, this$GameView.canvas_8be2vx$.height);
      return Unit;
    };
  }
  GameView.prototype.listenUIEvents_0 = function () {
    var delta = Kotlin.Long.fromInt(40);
    sysAddKeyDownListener(GameView$listenUIEvents$lambda(this));
    sysAddKeyUpListener(GameView$listenUIEvents$lambda_0(this));
    sysSetInterval(this.delta_0, GameView$listenUIEvents$lambda_1(this));
  };
  GameView.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GameView',
    interfaces: []
  };
  function main(args) {
    (new GameView()).start();
  }
  function Global() {
    Global_instance = this;
    this.COLOR_WHITE = Color$Companion_getInstance().WHITE;
    this.COLOR_BLACK = Color$Companion_getInstance().BLACK;
    this.COLOR_TRANSP = Color$Companion_getInstance().TRANSP;
    this.fgColor = Color$Companion_getInstance().BLACK;
    this.bgColor = Color$Companion_getInstance().WHITE;
    this.Scale = 3;
    this.SCREEN_WIDTH = 160;
    this.SCREEN_HEIGHT = 96;
    this.MAP_LEFT_OFFSET = 8;
    this.KEY_UP = 1;
    this.KEY_DOWN = 2;
    this.KEY_LEFT = 3;
    this.KEY_RIGHT = 4;
    this.KEY_PAGEUP = 5;
    this.KEY_PAGEDOWN = 6;
    this.KEY_ENTER = 7;
    this.KEY_CANCEL = 8;
    this.disableSave = false;
  }
  Global.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Global',
    interfaces: []
  };
  var Global_instance = null;
  function Global_getInstance() {
    if (Global_instance === null) {
      new Global();
    }
    return Global_instance;
  }
  function ScreenViewType(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ScreenViewType_initFields() {
    ScreenViewType_initFields = function () {
    };
    ScreenViewType$SCREEN_DEV_LOGO_instance = new ScreenViewType('SCREEN_DEV_LOGO', 0);
    ScreenViewType$SCREEN_GAME_LOGO_instance = new ScreenViewType('SCREEN_GAME_LOGO', 1);
    ScreenViewType$SCREEN_MENU_instance = new ScreenViewType('SCREEN_MENU', 2);
    ScreenViewType$SCREEN_MAIN_GAME_instance = new ScreenViewType('SCREEN_MAIN_GAME', 3);
    ScreenViewType$SCREEN_GAME_FAIL_instance = new ScreenViewType('SCREEN_GAME_FAIL', 4);
    ScreenViewType$SCREEN_SAVE_GAME_instance = new ScreenViewType('SCREEN_SAVE_GAME', 5);
    ScreenViewType$SCREEN_LOAD_GAME_instance = new ScreenViewType('SCREEN_LOAD_GAME', 6);
  }
  var ScreenViewType$SCREEN_DEV_LOGO_instance;
  function ScreenViewType$SCREEN_DEV_LOGO_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_DEV_LOGO_instance;
  }
  var ScreenViewType$SCREEN_GAME_LOGO_instance;
  function ScreenViewType$SCREEN_GAME_LOGO_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_GAME_LOGO_instance;
  }
  var ScreenViewType$SCREEN_MENU_instance;
  function ScreenViewType$SCREEN_MENU_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_MENU_instance;
  }
  var ScreenViewType$SCREEN_MAIN_GAME_instance;
  function ScreenViewType$SCREEN_MAIN_GAME_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_MAIN_GAME_instance;
  }
  var ScreenViewType$SCREEN_GAME_FAIL_instance;
  function ScreenViewType$SCREEN_GAME_FAIL_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_GAME_FAIL_instance;
  }
  var ScreenViewType$SCREEN_SAVE_GAME_instance;
  function ScreenViewType$SCREEN_SAVE_GAME_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_SAVE_GAME_instance;
  }
  var ScreenViewType$SCREEN_LOAD_GAME_instance;
  function ScreenViewType$SCREEN_LOAD_GAME_getInstance() {
    ScreenViewType_initFields();
    return ScreenViewType$SCREEN_LOAD_GAME_instance;
  }
  ScreenViewType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenViewType',
    interfaces: [Enum]
  };
  function ScreenViewType$values() {
    return [ScreenViewType$SCREEN_DEV_LOGO_getInstance(), ScreenViewType$SCREEN_GAME_LOGO_getInstance(), ScreenViewType$SCREEN_MENU_getInstance(), ScreenViewType$SCREEN_MAIN_GAME_getInstance(), ScreenViewType$SCREEN_GAME_FAIL_getInstance(), ScreenViewType$SCREEN_SAVE_GAME_getInstance(), ScreenViewType$SCREEN_LOAD_GAME_getInstance()];
  }
  ScreenViewType.values = ScreenViewType$values;
  function ScreenViewType$valueOf(name) {
    switch (name) {
      case 'SCREEN_DEV_LOGO':
        return ScreenViewType$SCREEN_DEV_LOGO_getInstance();
      case 'SCREEN_GAME_LOGO':
        return ScreenViewType$SCREEN_GAME_LOGO_getInstance();
      case 'SCREEN_MENU':
        return ScreenViewType$SCREEN_MENU_getInstance();
      case 'SCREEN_MAIN_GAME':
        return ScreenViewType$SCREEN_MAIN_GAME_getInstance();
      case 'SCREEN_GAME_FAIL':
        return ScreenViewType$SCREEN_GAME_FAIL_getInstance();
      case 'SCREEN_SAVE_GAME':
        return ScreenViewType$SCREEN_SAVE_GAME_getInstance();
      case 'SCREEN_LOAD_GAME':
        return ScreenViewType$SCREEN_LOAD_GAME_getInstance();
      default:throwISE('No enum constant fmj.ScreenViewType.' + name);
    }
  }
  ScreenViewType.valueOf_61zpoe$ = ScreenViewType$valueOf;
  function Character() {
    ResBase.call(this);
    this.name = '';
    this.state = Character$State$STOP_getInstance();
    this.posInMap = new Point();
    this.direction_eiels7$_0 = Direction$South_getInstance();
    this.mWalkingSprite_l2rv79$_0 = null;
  }
  Object.defineProperty(Character.prototype, 'direction', {
    get: function () {
      return this.direction_eiels7$_0;
    },
    set: function (d) {
      this.direction_eiels7$_0 = d;
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).setDirection_rtfsey$(d);
    }
  });
  Object.defineProperty(Character.prototype, 'walkingSpriteId', {
    get: function () {
      return ensureNotNull(this.mWalkingSprite_l2rv79$_0).id;
    }
  });
  Object.defineProperty(Character.prototype, 'step', {
    get: function () {
      return ensureNotNull(this.mWalkingSprite_l2rv79$_0).step;
    },
    set: function (step) {
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).step = step;
    }
  });
  Character.prototype.setPosInMap_vux9f0$ = function (x, y) {
    this.posInMap.set_vux9f0$(x, y);
  };
  Character.prototype.getPosOnScreen_wl9rgt$ = function (posMapScreen) {
    return new Point(this.posInMap.x - posMapScreen.x | 0, this.posInMap.y - posMapScreen.y | 0);
  };
  Character.prototype.setPosOnScreen_ejqt10$ = function (p, posMapScreen) {
    this.posInMap.set_vux9f0$(p.x + posMapScreen.x | 0, p.y + posMapScreen.y | 0);
  };
  Character.prototype.setPosOnScreen_av5i43$ = function (x, y, posMapScreen) {
    this.posInMap.set_vux9f0$(x + posMapScreen.x | 0, y + posMapScreen.y | 0);
  };
  Character.prototype.setWalkingSprite_arnj7f$ = function (sprite) {
    this.mWalkingSprite_l2rv79$_0 = sprite;
    ensureNotNull(this.mWalkingSprite_l2rv79$_0).setDirection_rtfsey$(this.direction);
  };
  Character.prototype.walk = function () {
    ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk();
    this.updatePosInMap_11qlre$_0(this.direction);
  };
  Character.prototype.walk_rtfsey$ = function (d) {
    if (d === this.direction) {
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk();
    }
     else {
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk_rtfsey$(d);
      this.direction = d;
    }
    this.updatePosInMap_11qlre$_0(d);
  };
  Character.prototype.updatePosInMap_11qlre$_0 = function (d) {
    if (equals(d, Direction$East_getInstance())) {
      var tmp$;
      tmp$ = this.posInMap;
      tmp$.x = tmp$.x + 1 | 0;
    }
     else if (equals(d, Direction$West_getInstance())) {
      var tmp$_0;
      tmp$_0 = this.posInMap;
      tmp$_0.x = tmp$_0.x - 1 | 0;
    }
     else if (equals(d, Direction$North_getInstance())) {
      var tmp$_1;
      tmp$_1 = this.posInMap;
      tmp$_1.y = tmp$_1.y - 1 | 0;
    }
     else if (equals(d, Direction$South_getInstance())) {
      var tmp$_2;
      tmp$_2 = this.posInMap;
      tmp$_2.y = tmp$_2.y + 1 | 0;
    }
     else
      Kotlin.noWhenBranchMatched();
  };
  Character.prototype.walkStay_rtfsey$ = function (d) {
    if (d === this.direction) {
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk();
    }
     else {
      ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk_rtfsey$(d);
      this.direction = d;
    }
  };
  Character.prototype.walkStay = function () {
    ensureNotNull(this.mWalkingSprite_l2rv79$_0).walk();
  };
  Character.prototype.drawWalkingSprite_hbb1nm$ = function (canvas, posMapScreen) {
    var p = this.getPosOnScreen_wl9rgt$(posMapScreen);
    ensureNotNull(this.mWalkingSprite_l2rv79$_0).draw_2g4tob$(canvas, p.x * 16 | 0, p.y * 16 | 0);
  };
  function Character$State(name, ordinal, v) {
    Enum.call(this);
    this.v = v;
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Character$State_initFields() {
    Character$State_initFields = function () {
    };
    Character$State$STOP_instance = new Character$State('STOP', 0, 0);
    Character$State$FORCE_MOVE_instance = new Character$State('FORCE_MOVE', 1, 1);
    Character$State$WALKING_instance = new Character$State('WALKING', 2, 2);
    Character$State$PAUSE_instance = new Character$State('PAUSE', 3, 3);
    Character$State$ACTIVE_instance = new Character$State('ACTIVE', 4, 4);
    Character$State$Companion_getInstance();
  }
  var Character$State$STOP_instance;
  function Character$State$STOP_getInstance() {
    Character$State_initFields();
    return Character$State$STOP_instance;
  }
  var Character$State$FORCE_MOVE_instance;
  function Character$State$FORCE_MOVE_getInstance() {
    Character$State_initFields();
    return Character$State$FORCE_MOVE_instance;
  }
  var Character$State$WALKING_instance;
  function Character$State$WALKING_getInstance() {
    Character$State_initFields();
    return Character$State$WALKING_instance;
  }
  var Character$State$PAUSE_instance;
  function Character$State$PAUSE_getInstance() {
    Character$State_initFields();
    return Character$State$PAUSE_instance;
  }
  var Character$State$ACTIVE_instance;
  function Character$State$ACTIVE_getInstance() {
    Character$State_initFields();
    return Character$State$ACTIVE_instance;
  }
  function Character$State$Companion() {
    Character$State$Companion_instance = this;
  }
  Character$State$Companion.prototype.fromInt_za3lpa$ = function (v) {
    var tmp$;
    if (v === 0)
      tmp$ = Character$State$STOP_getInstance();
    else if (v === 1)
      tmp$ = Character$State$FORCE_MOVE_getInstance();
    else if (v === 2)
      tmp$ = Character$State$WALKING_getInstance();
    else if (v === 3)
      tmp$ = Character$State$PAUSE_getInstance();
    else if (v === 4)
      tmp$ = Character$State$ACTIVE_getInstance();
    else {
      println('State.fromInt invalid v=' + v);
      tmp$ = Character$State$STOP_getInstance();
    }
    return tmp$;
  };
  Character$State$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Character$State$Companion_instance = null;
  function Character$State$Companion_getInstance() {
    Character$State_initFields();
    if (Character$State$Companion_instance === null) {
      new Character$State$Companion();
    }
    return Character$State$Companion_instance;
  }
  Character$State.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'State',
    interfaces: [Enum]
  };
  function Character$State$values() {
    return [Character$State$STOP_getInstance(), Character$State$FORCE_MOVE_getInstance(), Character$State$WALKING_getInstance(), Character$State$PAUSE_getInstance(), Character$State$ACTIVE_getInstance()];
  }
  Character$State.values = Character$State$values;
  function Character$State$valueOf(name) {
    switch (name) {
      case 'STOP':
        return Character$State$STOP_getInstance();
      case 'FORCE_MOVE':
        return Character$State$FORCE_MOVE_getInstance();
      case 'WALKING':
        return Character$State$WALKING_getInstance();
      case 'PAUSE':
        return Character$State$PAUSE_getInstance();
      case 'ACTIVE':
        return Character$State$ACTIVE_getInstance();
      default:throwISE('No enum constant fmj.characters.Character.State.' + name);
    }
  }
  Character$State.valueOf_61zpoe$ = Character$State$valueOf;
  Character.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Character',
    interfaces: [ResBase]
  };
  function Direction(name, ordinal, v) {
    Enum.call(this);
    this.v = v;
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Direction_initFields() {
    Direction_initFields = function () {
    };
    Direction$North_instance = new Direction('North', 0, 1);
    Direction$East_instance = new Direction('East', 1, 2);
    Direction$South_instance = new Direction('South', 2, 3);
    Direction$West_instance = new Direction('West', 3, 4);
    Direction$Companion_getInstance();
  }
  var Direction$North_instance;
  function Direction$North_getInstance() {
    Direction_initFields();
    return Direction$North_instance;
  }
  var Direction$East_instance;
  function Direction$East_getInstance() {
    Direction_initFields();
    return Direction$East_instance;
  }
  var Direction$South_instance;
  function Direction$South_getInstance() {
    Direction_initFields();
    return Direction$South_instance;
  }
  var Direction$West_instance;
  function Direction$West_getInstance() {
    Direction_initFields();
    return Direction$West_instance;
  }
  function Direction$Companion() {
    Direction$Companion_instance = this;
  }
  Direction$Companion.prototype.fromInt_za3lpa$ = function (v) {
    var tmp$;
    if (v === 1)
      tmp$ = Direction$North_getInstance();
    else if (v === 2)
      tmp$ = Direction$East_getInstance();
    else if (v === 3)
      tmp$ = Direction$South_getInstance();
    else if (v === 4)
      tmp$ = Direction$West_getInstance();
    else {
      println('Direction.fromInt invalid v=' + v);
      tmp$ = Direction$North_getInstance();
    }
    return tmp$;
  };
  Direction$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Direction$Companion_instance = null;
  function Direction$Companion_getInstance() {
    Direction_initFields();
    if (Direction$Companion_instance === null) {
      new Direction$Companion();
    }
    return Direction$Companion_instance;
  }
  Direction.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Direction',
    interfaces: [Enum]
  };
  function Direction$values() {
    return [Direction$North_getInstance(), Direction$East_getInstance(), Direction$South_getInstance(), Direction$West_getInstance()];
  }
  Direction.values = Direction$values;
  function Direction$valueOf(name) {
    switch (name) {
      case 'North':
        return Direction$North_getInstance();
      case 'East':
        return Direction$East_getInstance();
      case 'South':
        return Direction$South_getInstance();
      case 'West':
        return Direction$West_getInstance();
      default:throwISE('No enum constant fmj.characters.Direction.' + name);
    }
  }
  Direction.valueOf_61zpoe$ = Direction$valueOf;
  function FightingCharacter() {
    FightingCharacter$Companion_getInstance();
    Character.call(this);
    this.fightingSprite = null;
    this.magicChain_jxwzdi$_0 = this.magicChain_jxwzdi$_0;
    this.level = 0;
    this.maxHP_aqimg2$_0 = 0;
    this.maxMP_aqiiql$_0 = 0;
    this.hp_oo4bdu$_0 = 0;
    this.isVisiable = true;
    this.mp_oo4f3b$_0 = 0;
    this.attack_2swbku$_0 = 0;
    this.defend_xwzut0$_0 = 0;
    this.speed_7obhy5$_0 = 0;
    this.lingli_bak3kn$_0 = 0;
    this.luck_ge355z$_0 = 0;
    this.mBuff = new Int32Array(4);
    this.mBuffRound = new Int32Array(4);
    this.mDebuff = 0;
    this.mDebuffRound = new Int32Array(4);
    this.mAtbuff = 0;
    this.mAtbuffRound = new Int32Array(4);
  }
  Object.defineProperty(FightingCharacter.prototype, 'combatX', {
    get: function () {
      return ensureNotNull(this.fightingSprite).combatX;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'combatY', {
    get: function () {
      return ensureNotNull(this.fightingSprite).combatY;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'combatLeft', {
    get: function () {
      return ensureNotNull(this.fightingSprite).combatX - (ensureNotNull(this.fightingSprite).width / 2 | 0) | 0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'combatTop', {
    get: function () {
      return ensureNotNull(this.fightingSprite).combatY - (ensureNotNull(this.fightingSprite).height / 2 | 0) | 0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'magicChain', {
    get: function () {
      if (this.magicChain_jxwzdi$_0 == null)
        return throwUPAE('magicChain');
      return this.magicChain_jxwzdi$_0;
    },
    set: function (magicChain) {
      this.magicChain_jxwzdi$_0 = magicChain;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'maxHP', {
    get: function () {
      return this.maxHP_aqimg2$_0;
    },
    set: function (maxHP) {
      var maxHP_0 = maxHP;
      if (maxHP_0 > 999) {
        maxHP_0 = 999;
      }
      this.maxHP_aqimg2$_0 = maxHP_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'maxMP', {
    get: function () {
      return this.maxMP_aqiiql$_0;
    },
    set: function (maxMP) {
      var maxMP_0 = maxMP;
      if (maxMP_0 > 999) {
        maxMP_0 = 999;
      }
      this.maxMP_aqiiql$_0 = maxMP_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'hp', {
    get: function () {
      return this.hp_oo4bdu$_0;
    },
    set: function (hp) {
      var hp_0 = hp;
      if (hp_0 > this.maxHP) {
        hp_0 = this.maxHP;
      }
      this.hp_oo4bdu$_0 = hp_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'isAlive', {
    get: function () {
      return this.hp > 0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'mp', {
    get: function () {
      return this.mp_oo4f3b$_0;
    },
    set: function (mp) {
      var mp_0 = mp;
      if (mp_0 > this.maxMP) {
        mp_0 = this.maxMP;
      }
      this.mp_oo4f3b$_0 = mp_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'attack', {
    get: function () {
      return this.attack_2swbku$_0;
    },
    set: function (at) {
      var at_0 = at;
      if (at_0 > 999) {
        at_0 = 999;
      }
      this.attack_2swbku$_0 = at_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'defend', {
    get: function () {
      return this.defend_xwzut0$_0;
    },
    set: function (d) {
      var d_0 = d;
      if (d_0 > 999) {
        d_0 = 999;
      }
      this.defend_xwzut0$_0 = d_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'speed', {
    get: function () {
      return this.speed_7obhy5$_0;
    },
    set: function (s) {
      var s_0 = s;
      if (s_0 > 99) {
        s_0 = 99;
      }
      this.speed_7obhy5$_0 = s_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'lingli', {
    get: function () {
      return this.lingli_bak3kn$_0;
    },
    set: function (l) {
      var l_0 = l;
      if (l_0 > 99) {
        l_0 = 99;
      }
      this.lingli_bak3kn$_0 = l_0;
    }
  });
  Object.defineProperty(FightingCharacter.prototype, 'luck', {
    get: function () {
      return this.luck_ge355z$_0;
    },
    set: function (l) {
      var l_0 = l;
      if (l_0 > 99) {
        l_0 = 99;
      }
      this.luck_ge355z$_0 = l_0;
    }
  });
  FightingCharacter.prototype.setCombatPos_vux9f0$ = function (x, y) {
    ensureNotNull(this.fightingSprite).setCombatPos_vux9f0$(x, y);
  };
  FightingCharacter.prototype.hasBuff_za3lpa$ = function (mask) {
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU && this.mBuff[0] <= 0) {
      return false;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN && this.mBuff[1] <= 0) {
      return false;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG && this.mBuff[2] <= 0) {
      return false;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN && this.mBuff[3] <= 0) {
      return false;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_ALL) === FightingCharacter$Companion_getInstance().BUFF_MASK_ALL && this.mBuff[4] <= 0) {
      return false;
    }
    return true;
  };
  FightingCharacter.prototype.hasDebuff_za3lpa$ = function (mask) {
    return (this.mDebuff & mask) !== 0;
  };
  FightingCharacter.prototype.hasAtbuff_za3lpa$ = function (mask) {
    return (this.mAtbuff & mask) === mask;
  };
  FightingCharacter.prototype.addBuff_vux9f0$ = function (mask, rounds) {
    if (rounds === void 0)
      rounds = kotlin_js_internal_IntCompanionObject.MAX_VALUE;
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      var tmp$;
      tmp$ = this.mBuff;
      tmp$[0] = tmp$[0] + 1 | 0;
      tmp$[0];
      this.mBuffRound[0] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      var tmp$_0;
      tmp$_0 = this.mBuff;
      tmp$_0[1] = tmp$_0[1] + 1 | 0;
      tmp$_0[1];
      this.mBuffRound[1] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      var tmp$_1;
      tmp$_1 = this.mBuff;
      tmp$_1[2] = tmp$_1[2] + 1 | 0;
      tmp$_1[2];
      this.mBuffRound[2] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      var tmp$_2;
      tmp$_2 = this.mBuff;
      tmp$_2[3] = tmp$_2[3] + 1 | 0;
      tmp$_2[3];
      this.mBuffRound[3] = rounds;
    }
  };
  FightingCharacter.prototype.delBuff_za3lpa$ = function (mask) {
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      var tmp$;
      tmp$ = this.mBuff;
      tmp$[0] = tmp$[0] - 1 | 0;
      if (tmp$[0] < 0) {
        this.mBuff[0] = 0;
      }
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      var tmp$_0;
      tmp$_0 = this.mBuff;
      tmp$_0[1] = tmp$_0[1] - 1 | 0;
      if (tmp$_0[1] < 0) {
        this.mBuff[1] = 0;
      }
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      var tmp$_1;
      tmp$_1 = this.mBuff;
      tmp$_1[2] = tmp$_1[2] - 1 | 0;
      if (tmp$_1[2] < 0) {
        this.mBuff[2] = 0;
      }
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      var tmp$_2;
      tmp$_2 = this.mBuff;
      tmp$_2[3] = tmp$_2[3] - 1 | 0;
      if (tmp$_2[3] < 0) {
        this.mBuff[3] = 0;
      }
    }
  };
  FightingCharacter.prototype.getBuffRound_za3lpa$ = function (mask) {
    var tmp$;
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      return this.mBuffRound[0];
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      return this.mBuffRound[1];
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      return this.mBuffRound[2];
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      tmp$ = this.mBuffRound[3];
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  FightingCharacter.prototype.addDebuff_vux9f0$ = function (mask, rounds) {
    this.mDebuff = this.mDebuff | mask;
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      this.mDebuffRound[0] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      this.mDebuffRound[1] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      this.mDebuffRound[2] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      this.mDebuffRound[3] = rounds;
    }
  };
  FightingCharacter.prototype.delDebuff_za3lpa$ = function (mask) {
    this.mDebuff = this.mDebuff & ~mask;
  };
  FightingCharacter.prototype.addAtbuff_vux9f0$ = function (mask, rounds) {
    this.mAtbuff = this.mAtbuff | mask;
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      this.mAtbuffRound[0] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      this.mAtbuffRound[1] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      this.mAtbuffRound[2] = rounds;
    }
    if ((mask & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      this.mAtbuffRound[3] = rounds;
    }
  };
  FightingCharacter.prototype.delAtbuff_za3lpa$ = function (mask) {
    this.mAtbuff = this.mAtbuff & ~mask;
  };
  function FightingCharacter$Companion() {
    FightingCharacter$Companion_instance = this;
    this.BUFF_MASK_ALL = 16;
    this.BUFF_MASK_DU = 8;
    this.BUFF_MASK_LUAN = 4;
    this.BUFF_MASK_FENG = 2;
    this.BUFF_MASK_MIAN = 1;
    this.BUFF_MASK_GONG = 32;
    this.BUFF_MASK_FANG = 64;
    this.BUFF_MASK_SU = 128;
  }
  FightingCharacter$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var FightingCharacter$Companion_instance = null;
  function FightingCharacter$Companion_getInstance() {
    if (FightingCharacter$Companion_instance === null) {
      new FightingCharacter$Companion();
    }
    return FightingCharacter$Companion_instance;
  }
  FightingCharacter.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FightingCharacter',
    interfaces: [Character]
  };
  function FightingSprite(resType, index) {
    this.mImage_0 = null;
    this.currentFrame = 1;
    this.combatX_ti0xg4$_0 = 0;
    this.combatY_ti0xgz$_0 = 0;
    var tmp$, tmp$_0;
    if (resType === DatLib$ResType$ACP_getInstance()) {
      this.mImage_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ACP_getInstance(), 3, index), ResImage) ? tmp$ : throwCCE();
    }
     else if (resType === DatLib$ResType$PIC_getInstance()) {
      this.mImage_0 = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 3, index), ResImage) ? tmp$_0 : throwCCE();
    }
     else {
      throw new Error_0('resType Error. resType=' + resType);
    }
  }
  Object.defineProperty(FightingSprite.prototype, 'combatX', {
    get: function () {
      return this.combatX_ti0xg4$_0;
    },
    set: function (combatX) {
      this.combatX_ti0xg4$_0 = combatX;
    }
  });
  Object.defineProperty(FightingSprite.prototype, 'combatY', {
    get: function () {
      return this.combatY_ti0xgz$_0;
    },
    set: function (combatY) {
      this.combatY_ti0xgz$_0 = combatY;
    }
  });
  Object.defineProperty(FightingSprite.prototype, 'width', {
    get: function () {
      return ensureNotNull(this.mImage_0).width;
    }
  });
  Object.defineProperty(FightingSprite.prototype, 'height', {
    get: function () {
      return ensureNotNull(this.mImage_0).height;
    }
  });
  Object.defineProperty(FightingSprite.prototype, 'frameCnt', {
    get: function () {
      return ensureNotNull(this.mImage_0).number;
    }
  });
  FightingSprite.prototype.draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.mImage_0).draw_tj1hu5$(canvas, this.currentFrame, this.combatX - (ensureNotNull(this.mImage_0).width / 2 | 0) | 0, this.combatY - (ensureNotNull(this.mImage_0).height / 2 | 0) | 0);
  };
  FightingSprite.prototype.draw_2g4tob$ = function (canvas, x, y) {
    ensureNotNull(this.mImage_0).draw_tj1hu5$(canvas, this.currentFrame, x, y);
  };
  FightingSprite.prototype.setCombatPos_vux9f0$ = function (x, y) {
    this.combatX = x;
    this.combatY = y;
  };
  FightingSprite.prototype.move_vux9f0$ = function (dx, dy) {
    this.combatX = this.combatX + dx | 0;
    this.combatY = this.combatY + dy | 0;
  };
  FightingSprite.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FightingSprite',
    interfaces: []
  };
  function Monster() {
    Monster$Companion_getInstance();
    FightingCharacter.call(this);
    this.mLastRound_0 = 0;
    this.mIQ_0 = 0;
    this.money_mfcpdz$_0 = 0;
    this.exp_81bzzq$_0 = 0;
    this.mCarryGoods1_0 = new Int32Array(3);
    this.mCarryGoods2_0 = new Int32Array(3);
  }
  Object.defineProperty(Monster.prototype, 'money', {
    get: function () {
      return this.money_mfcpdz$_0;
    },
    set: function (money) {
      this.money_mfcpdz$_0 = money;
    }
  });
  Object.defineProperty(Monster.prototype, 'exp', {
    get: function () {
      return this.exp_81bzzq$_0;
    },
    set: function (exp) {
      this.exp_81bzzq$_0 = exp;
    }
  });
  Object.defineProperty(Monster.prototype, 'dropGoods', {
    get: function () {
      var tmp$;
      if (this.mCarryGoods2_0[0] === 0 || this.mCarryGoods2_0[1] === 0 || this.mCarryGoods2_0[2] === 0) {
        return null;
      }
      var g = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), this.mCarryGoods2_0[0], this.mCarryGoods2_0[1]), BaseGoods) ? tmp$ : throwCCE();
      g.goodsNum = this.mCarryGoods2_0[2];
      return g;
    }
  });
  Monster.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    var magicIndex = buf[offset + 47 | 0] & 255;
    this.magicChain = magicIndex > 0 ? Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MLR_getInstance(), 1, magicIndex), ResMagicChain) ? tmp$ : throwCCE() : new ResMagicChain();
    this.magicChain.learnNum = buf[offset + 2 | 0] & 255;
    this.addBuff_vux9f0$(buf[offset + 3 | 0] & 255);
    this.mAtbuff = buf[offset + 4 | 0] & 255;
    this.mLastRound_0 = buf[offset + 23 | 0] & 255;
    this.name = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 6 | 0);
    this.level = buf[offset + 18 | 0] & 255;
    this.maxHP = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 24 | 0);
    this.hp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 26 | 0);
    this.maxMP = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 28 | 0);
    this.mp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 30 | 0);
    this.attack = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 32 | 0);
    this.defend = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 34 | 0);
    this.speed = buf[offset + 19 | 0] & 255;
    this.lingli = buf[offset + 20 | 0] & 255;
    this.luck = buf[offset + 22 | 0] & 255;
    this.mIQ_0 = buf[offset + 21 | 0] & 255;
    this.money = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 36 | 0);
    this.exp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 38 | 0);
    this.mCarryGoods1_0[0] = buf[offset + 40 | 0] & 255;
    this.mCarryGoods1_0[1] = buf[offset + 41 | 0] & 255;
    this.mCarryGoods1_0[2] = buf[offset + 42 | 0] & 255;
    this.mCarryGoods2_0[0] = buf[offset + 43 | 0] & 255;
    this.mCarryGoods2_0[1] = buf[offset + 44 | 0] & 255;
    this.mCarryGoods2_0[2] = buf[offset + 45 | 0] & 255;
    this.fightingSprite = new FightingSprite(DatLib$ResType$ACP_getInstance(), buf[offset + 46 | 0] & 255);
  };
  Monster.prototype.setOriginalCombatPos_za3lpa$ = function (i) {
    var fs = this.fightingSprite;
    fs != null ? (fs.setCombatPos_vux9f0$(Monster$Companion_getInstance().arr_0[i][0] - (fs.width / 6 | 0) + (fs.width / 2 | 0) | 0, Monster$Companion_getInstance().arr_0[i][1] - (fs.height / 10 | 0) + (fs.height / 2 | 0) | 0), Unit) : null;
  };
  function Monster$Companion() {
    Monster$Companion_instance = this;
    this.arr_0 = [new Int32Array([12, 25]), new Int32Array([44, 14]), new Int32Array([82, 11])];
  }
  Monster$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Monster$Companion_instance = null;
  function Monster$Companion_getInstance() {
    if (Monster$Companion_instance === null) {
      new Monster$Companion();
    }
    return Monster$Companion_instance;
  }
  Monster.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Monster',
    interfaces: [FightingCharacter]
  };
  function NPC() {
    NPC$Companion_getInstance();
    Character.call(this);
    this.mDelay = 0;
    this.mCanWalk_lbchmg$_0 = null;
    this.mRandom_1t4gy8$_0 = new Random();
    this.mPauseCnt_9x88qo$_0 = Kotlin.Long.fromInt(this.mDelay * 100 | 0);
    this.mActiveCnt_bik1a6$_0 = Kotlin.Long.ZERO;
    this.mWalkingCnt_xj4ebn$_0 = Kotlin.Long.ZERO;
  }
  NPC.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    var st = buf[offset + 4 | 0] & 255;
    this.state = Character$State$Companion_getInstance().fromInt_za3lpa$(st);
    this.name = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 9 | 0);
    this.mDelay = buf[offset + 21 | 0] & 255;
    if (this.mDelay === 0) {
      this.state = Character$State$STOP_getInstance();
    }
    this.setWalkingSprite_arnj7f$(new WalkingSprite(2, buf[offset + 22 | 0] & 255));
    var faceto = buf[offset + 2 | 0] & 255;
    this.direction = Direction$Companion_getInstance().fromInt_za3lpa$(faceto);
    this.step = buf[offset + 3 | 0] & 255;
  };
  NPC.prototype.encode_vcd9jg$ = function (out) {
    out.writeInt_za3lpa$(this.type);
    out.writeInt_za3lpa$(this.index);
    out.writeInt_za3lpa$(this.state.v);
    out.writeString_61zpoe$(this.name);
    out.writeInt_za3lpa$(this.mDelay);
    out.writeInt_za3lpa$(this.walkingSpriteId);
    out.writeInt_za3lpa$(this.direction.v);
    out.writeInt_za3lpa$(this.step);
    out.writeLong_s8cxhz$(this.mPauseCnt_9x88qo$_0);
    out.writeLong_s8cxhz$(this.mActiveCnt_bik1a6$_0);
    out.writeLong_s8cxhz$(this.mWalkingCnt_xj4ebn$_0);
    out.writeInt_za3lpa$(this.posInMap.x);
    out.writeInt_za3lpa$(this.posInMap.y);
  };
  NPC.prototype.decode_setnfj$ = function (coder) {
    this.type = coder.readInt();
    this.index = coder.readInt();
    this.state = Character$State$Companion_getInstance().fromInt_za3lpa$(coder.readInt());
    this.name = coder.readString();
    this.mDelay = coder.readInt();
    this.setWalkingSprite_arnj7f$(new WalkingSprite(2, coder.readInt()));
    this.direction = Direction$Companion_getInstance().fromInt_za3lpa$(coder.readInt());
    this.step = coder.readInt();
    this.mPauseCnt_9x88qo$_0 = coder.readLong();
    this.mActiveCnt_bik1a6$_0 = coder.readLong();
    this.mWalkingCnt_xj4ebn$_0 = coder.readLong();
    this.setPosInMap_vux9f0$(coder.readInt(), coder.readInt());
  };
  function NPC$ICanWalk() {
  }
  NPC$ICanWalk.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ICanWalk',
    interfaces: []
  };
  NPC.prototype.setICanWalk_mkhjrq$ = function (arg) {
    this.mCanWalk_lbchmg$_0 = arg;
  };
  NPC.prototype.update_s8cxhz$ = function (delta) {
    var tmp$;
    tmp$ = this.state;
    if (equals(tmp$, Character$State$PAUSE_getInstance())) {
      this.mPauseCnt_9x88qo$_0 = this.mPauseCnt_9x88qo$_0.subtract(delta);
      if (this.mPauseCnt_9x88qo$_0.compareTo_11rb$(Kotlin.Long.fromInt(0)) < 0) {
        this.state = Character$State$WALKING_getInstance();
      }
    }
     else if (equals(tmp$, Character$State$FORCE_MOVE_getInstance()) || equals(tmp$, Character$State$WALKING_getInstance())) {
      this.mWalkingCnt_xj4ebn$_0 = this.mWalkingCnt_xj4ebn$_0.add(delta);
      if (this.mWalkingCnt_xj4ebn$_0.compareTo_11rb$(Kotlin.Long.fromInt(500)) < 0)
        return;
      this.mWalkingCnt_xj4ebn$_0 = Kotlin.Long.ZERO;
      if (this.mRandom_1t4gy8$_0.nextInt_za3lpa$(5) === 0) {
        this.mPauseCnt_9x88qo$_0 = Kotlin.Long.fromInt(this.mDelay * 100 | 0);
        this.state = Character$State$PAUSE_getInstance();
      }
       else if (this.mRandom_1t4gy8$_0.nextInt_za3lpa$(5) === 0) {
        var i = this.mRandom_1t4gy8$_0.nextInt_za3lpa$(4);
        var d = Direction$North_getInstance();
        if (i === 0)
          d = Direction$North_getInstance();
        else if (i === 1)
          d = Direction$East_getInstance();
        else if (i === 2)
          d = Direction$South_getInstance();
        else if (i === 3)
          d = Direction$West_getInstance();
        this.direction = d;
        this.walk();
      }
       else {
        this.walk();
      }
    }
     else if (!equals(tmp$, Character$State$STOP_getInstance()))
      if (equals(tmp$, Character$State$ACTIVE_getInstance())) {
        this.mActiveCnt_bik1a6$_0 = this.mActiveCnt_bik1a6$_0.add(delta);
        if (this.mActiveCnt_bik1a6$_0.compareTo_11rb$(Kotlin.Long.fromInt(100)) > 0) {
          this.mActiveCnt_bik1a6$_0 = Kotlin.Long.ZERO;
          this.walkStay();
        }
      }
  };
  NPC.prototype.walk = function () {
    var tmp$;
    var x = this.posInMap.x;
    var y = this.posInMap.y;
    tmp$ = this.direction;
    if (equals(tmp$, Direction$North_getInstance()))
      y = y - 1 | 0;
    else if (equals(tmp$, Direction$East_getInstance()))
      x = x + 1 | 0;
    else if (equals(tmp$, Direction$South_getInstance()))
      y = y + 1 | 0;
    else if (equals(tmp$, Direction$West_getInstance()))
      x = x - 1 | 0;
    else
      Kotlin.noWhenBranchMatched();
    if (ensureNotNull(this.mCanWalk_lbchmg$_0).canWalk_vux9f0$(x, y)) {
      Character.prototype.walk.call(this);
    }
  };
  Object.defineProperty(NPC.prototype, 'isEmpty', {
    get: function () {
      return this.type === 0;
    }
  });
  function NPC$Companion() {
    NPC$Companion_instance = this;
    this.empty = new NPC();
  }
  NPC$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var NPC$Companion_instance = null;
  function NPC$Companion_getInstance() {
    if (NPC$Companion_instance === null) {
      new NPC$Companion();
    }
    return NPC$Companion_instance;
  }
  NPC.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'NPC',
    interfaces: [Coder, Character]
  };
  function Player() {
    Player$Companion_getInstance();
    FightingCharacter.call(this);
    this.mImgHead_0 = null;
    this.levelupChain_1ejzlo$_0 = this.levelupChain_1ejzlo$_0;
    this.currentExp = 0;
    this.equipmentsArray = Kotlin.newArray(8, null);
  }
  Object.defineProperty(Player.prototype, 'levelupChain', {
    get: function () {
      if (this.levelupChain_1ejzlo$_0 == null)
        return throwUPAE('levelupChain');
      return this.levelupChain_1ejzlo$_0;
    },
    set: function (levelupChain) {
      this.levelupChain_1ejzlo$_0 = levelupChain;
    }
  });
  Player.prototype.drawHead_2g4tob$ = function (canvas, x, y) {
    if (this.mImgHead_0 != null) {
      ensureNotNull(this.mImgHead_0).draw_tj1hu5$(canvas, 1, x, y);
    }
  };
  Player.prototype.setFrameByState = function () {
    if (this.isAlive) {
      if (this.hasDebuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) || this.hp < (this.maxHP / 10 | 0)) {
        ensureNotNull(this.fightingSprite).currentFrame = 11;
      }
       else {
        ensureNotNull(this.fightingSprite).currentFrame = 1;
      }
    }
     else {
      ensureNotNull(this.fightingSprite).currentFrame = 12;
    }
  };
  Player.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8;
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    if (this.index > 0)
      this.mImgHead_0 = (tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, this.index)) == null || Kotlin.isType(tmp$, ResImage) ? tmp$ : throwCCE();
    this.setWalkingSprite_arnj7f$(new WalkingSprite(this.type, buf[offset + 22 | 0] & 255));
    this.fightingSprite = new FightingSprite(DatLib$ResType$PIC_getInstance(), this.index);
    this.direction = Direction$Companion_getInstance().fromInt_za3lpa$(buf[offset + 2 | 0] & 255);
    this.step = buf[offset + 3 | 0] & 255;
    this.setPosInMap_vux9f0$(buf[offset + 5 | 0] & 255, buf[offset + 6 | 0] & 255);
    var magicChainId = buf[offset + 23 | 0] & 255;
    this.magicChain = DatLib$Companion_getInstance().getMlr_ydzd23$(1, magicChainId, true);
    this.magicChain.learnNum = buf[offset + 9 | 0] & 255;
    this.name = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 10 | 0);
    this.level = buf[offset + 32 | 0] & 255;
    this.maxHP = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 38 | 0);
    this.hp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 40 | 0);
    this.maxMP = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 42 | 0);
    this.mp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 44 | 0);
    this.attack = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 46 | 0);
    this.defend = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 48 | 0);
    this.speed = buf[offset + 54 | 0] & 255;
    this.lingli = buf[offset + 55 | 0] & 255;
    this.luck = buf[offset + 56 | 0] & 255;
    this.currentExp = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 50 | 0);
    this.levelupChain = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MLR_getInstance(), 2, this.index), ResLevelupChain) ? tmp$_0 : throwCCE();
    var tmp = buf[offset + 30 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[0] = (tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 6, tmp)) == null || Kotlin.isType(tmp$_1, GoodsEquipment) ? tmp$_1 : throwCCE();
    }
    tmp = buf[offset + 31 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[1] = (tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 6, tmp)) == null || Kotlin.isType(tmp$_2, GoodsEquipment) ? tmp$_2 : throwCCE();
    }
    tmp = buf[offset + 27 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[2] = (tmp$_3 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 5, tmp)) == null || Kotlin.isType(tmp$_3, GoodsEquipment) ? tmp$_3 : throwCCE();
    }
    tmp = buf[offset + 29 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[3] = (tmp$_4 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 3, tmp)) == null || Kotlin.isType(tmp$_4, GoodsEquipment) ? tmp$_4 : throwCCE();
    }
    tmp = buf[offset + 28 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[4] = (tmp$_5 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 7, tmp)) == null || Kotlin.isType(tmp$_5, GoodsEquipment) ? tmp$_5 : throwCCE();
    }
    tmp = buf[offset + 25 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[5] = (tmp$_6 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 2, tmp)) == null || Kotlin.isType(tmp$_6, GoodsEquipment) ? tmp$_6 : throwCCE();
    }
    tmp = buf[offset + 26 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[6] = (tmp$_7 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 4, tmp)) == null || Kotlin.isType(tmp$_7, GoodsEquipment) ? tmp$_7 : throwCCE();
    }
    tmp = buf[offset + 24 | 0] & 255;
    if (tmp !== 0) {
      this.equipmentsArray[7] = (tmp$_8 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), 1, tmp)) == null || Kotlin.isType(tmp$_8, GoodsEquipment) ? tmp$_8 : throwCCE();
    }
  };
  Player.prototype.getCurrentEquipment_za3lpa$ = function (type) {
    for (var i = 0; i <= 7; i++) {
      if (Player$Companion_getInstance().sEquipTypes[i] === type) {
        return this.equipmentsArray[i];
      }
    }
    return null;
  };
  Player.prototype.hasEquipt_vux9f0$ = function (type, id) {
    var tmp$;
    if (type === 6) {
      var $receiver = [this.equipmentsArray[0], this.equipmentsArray[1]];
      var tmp$_0;
      for (tmp$_0 = 0; tmp$_0 !== $receiver.length; ++tmp$_0) {
        var element = $receiver[tmp$_0];
        var tmp$_1;
        return (tmp$_1 = element != null ? element.type === type && element.index === id : null) != null ? tmp$_1 : false;
      }
      return true;
    }
    for (var i = 2; i <= 7; i++) {
      if ((tmp$ = this.equipmentsArray[i]) != null) {
        if (tmp$.type === type && tmp$.index === id) {
          return true;
        }
      }
    }
    return false;
  };
  Player.prototype.putOn_uyhnfp$ = function (goods) {
    for (var i = 0; i <= 7; i++) {
      if (goods.type === Player$Companion_getInstance().sEquipTypes[i]) {
        if (this.equipmentsArray[i] == null) {
          goods.putOn_xa4yhy$(this);
          this.equipmentsArray[i] = goods;
          break;
        }
      }
    }
  };
  Player.prototype.takeOff_za3lpa$ = function (type) {
    var tmp$;
    for (var i = 0; i <= 7; i++) {
      if (type === Player$Companion_getInstance().sEquipTypes[i]) {
        if (this.equipmentsArray[i] != null) {
          (tmp$ = this.equipmentsArray[i]) != null ? (tmp$.takeOff_xa4yhy$(this), Unit) : null;
          this.equipmentsArray[i] = null;
          break;
        }
      }
    }
  };
  Player.prototype.hasSpace_za3lpa$ = function (type) {
    if (type === 6) {
      return this.equipmentsArray[0] == null || this.equipmentsArray[1] == null;
    }
     else {
      for (var i = 0; i <= 7; i++) {
        if (Player$Companion_getInstance().sEquipTypes[i] === type && this.equipmentsArray[i] == null) {
          return true;
        }
      }
    }
    return false;
  };
  Player.prototype.drawState_86va19$ = function (canvas, page) {
    canvas.drawLine_x3aj6j$(37, 10, 37, 87, Util_getInstance().sBlackPaint);
    if (page === 0) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u7B49\u7EA7   ' + this.level, 41, 4);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u751F\u547D   ' + this.hp + '/' + this.maxHP, 41, 23);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u771F\u6C14   ' + this.mp + '/' + this.maxMP, 41, 41);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u653B\u51FB\u529B ' + this.attack, 41, 59);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u9632\u5FA1\u529B ' + this.defend, 41, 77);
    }
     else if (page === 1) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u7ECF\u9A8C\u503C', 41, 4);
      var w = Util_getInstance().drawSmallNum_tj1hu5$(canvas, this.currentExp, 97, 4);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '/', 97 + w + 2 | 0, 4);
      Util_getInstance().drawSmallNum_tj1hu5$(canvas, this.levelupChain.getNextLevelExp_za3lpa$(this.level), 97 + w + 9 | 0, 10);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u8EAB\u6CD5   ' + this.speed, 41, 23);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u7075\u529B   ' + this.lingli, 41, 41);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u5E78\u8FD0   ' + this.luck, 41, 59);
      var sb = new StringBuilder('\u514D\u75AB   ');
      var tmp = new StringBuilder();
      if (this.hasBuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_DU)) {
        tmp.append_s8itvh$(27602);
      }
      if (this.hasBuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN)) {
        tmp.append_s8itvh$(20081);
      }
      if (this.hasBuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_FENG)) {
        tmp.append_s8itvh$(23553);
      }
      if (this.hasBuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN)) {
        tmp.append_s8itvh$(30496);
      }
      if (tmp.length > 0) {
        sb.append_gw00v9$(tmp);
      }
       else {
        sb.append_s8itvh$(26080);
      }
      TextRender_getInstance().drawText_kkuqvh$(canvas, sb.toString(), 41, 77);
    }
  };
  Player.prototype.decode_setnfj$ = function (coder) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    this.type = coder.readInt();
    this.index = coder.readInt();
    if (this.index > 0)
      this.mImgHead_0 = (tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, this.index)) == null || Kotlin.isType(tmp$, ResImage) ? tmp$ : throwCCE();
    this.levelupChain = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MLR_getInstance(), 2, this.index), ResLevelupChain) ? tmp$_0 : throwCCE();
    this.setWalkingSprite_arnj7f$(new WalkingSprite(this.type, coder.readInt()));
    this.fightingSprite = new FightingSprite(DatLib$ResType$PIC_getInstance(), this.index);
    this.direction = Direction$Companion_getInstance().fromInt_za3lpa$(coder.readInt());
    this.step = coder.readInt();
    this.setPosInMap_vux9f0$(coder.readInt(), coder.readInt());
    this.magicChain = Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MLR_getInstance(), 1, coder.readInt()), ResMagicChain) ? tmp$_1 : throwCCE();
    this.magicChain.learnNum = coder.readInt();
    this.name = coder.readString();
    this.level = coder.readInt();
    this.maxHP = coder.readInt();
    this.hp = coder.readInt();
    this.maxMP = coder.readInt();
    this.mp = coder.readInt();
    this.attack = coder.readInt();
    this.defend = coder.readInt();
    this.speed = coder.readInt();
    this.lingli = coder.readInt();
    this.luck = coder.readInt();
    this.currentExp = coder.readInt();
    for (var i = 0; i <= 7; i++) {
      var type = coder.readInt();
      var index = coder.readInt();
      if (type !== 0 && index !== 0) {
        this.equipmentsArray[i] = (tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), type, index)) == null || Kotlin.isType(tmp$_2, GoodsEquipment) ? tmp$_2 : throwCCE();
      }
    }
  };
  Player.prototype.encode_vcd9jg$ = function (out) {
    var tmp$;
    out.writeInt_za3lpa$(this.type);
    out.writeInt_za3lpa$(this.index);
    out.writeInt_za3lpa$(this.walkingSpriteId);
    out.writeInt_za3lpa$(this.direction.v);
    out.writeInt_za3lpa$(this.step);
    out.writeInt_za3lpa$(this.posInMap.x);
    out.writeInt_za3lpa$(this.posInMap.y);
    out.writeInt_za3lpa$(this.magicChain.index);
    out.writeInt_za3lpa$(this.magicChain.learnNum);
    out.writeString_61zpoe$(this.name);
    out.writeInt_za3lpa$(this.level);
    out.writeInt_za3lpa$(this.maxHP);
    out.writeInt_za3lpa$(this.hp);
    out.writeInt_za3lpa$(this.maxMP);
    out.writeInt_za3lpa$(this.mp);
    out.writeInt_za3lpa$(this.attack);
    out.writeInt_za3lpa$(this.defend);
    out.writeInt_za3lpa$(this.speed);
    out.writeInt_za3lpa$(this.lingli);
    out.writeInt_za3lpa$(this.luck);
    out.writeInt_za3lpa$(this.currentExp);
    for (var i = 0; i <= 7; i++) {
      var tmp$_0;
      if ((tmp$ = this.equipmentsArray[i]) != null) {
        out.writeInt_za3lpa$(tmp$.type);
        out.writeInt_za3lpa$(tmp$.index);
        tmp$_0 = Unit;
      }
       else
        tmp$_0 = null;
      if (tmp$_0 == null) {
        out.writeInt_za3lpa$(0);
        out.writeInt_za3lpa$(0);
      }
    }
  };
  var Math_0 = Math;
  Player.prototype.setLevel_za3lpa$ = function (level) {
    var b = this.levelupChain.maxLevel;
    this.level = Math_0.min(level, b);
  };
  function Player$Companion() {
    Player$Companion_instance = this;
    this.sEquipTypes = new Int32Array([6, 6, 5, 3, 7, 2, 4, 1]);
    this.sGoodsList = new GoodsManage();
    this.sMoney = 0;
  }
  Player$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Player$Companion_instance = null;
  function Player$Companion_getInstance() {
    if (Player$Companion_instance === null) {
      new Player$Companion();
    }
    return Player$Companion_instance;
  }
  Player.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Player',
    interfaces: [Coder, FightingCharacter]
  };
  function ResLevelupChain() {
    ResLevelupChain$Companion_getInstance();
    ResBase.call(this);
    this.maxLevel_vhqkyz$_0 = 0;
    this.mLevelData_0 = new Int8Array(Kotlin.imul(this.maxLevel, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0));
  }
  Object.defineProperty(ResLevelupChain.prototype, 'maxLevel', {
    get: function () {
      return this.maxLevel_vhqkyz$_0;
    },
    set: function (maxLevel) {
      this.maxLevel_vhqkyz$_0 = maxLevel;
    }
  });
  ResLevelupChain.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.maxLevel = buf[offset + 2 | 0] & 255;
    this.mLevelData_0 = new Int8Array(Kotlin.imul(this.maxLevel, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0));
    System_getInstance().arraycopy_nlwz52$(buf, offset + 4 | 0, this.mLevelData_0, 0, this.mLevelData_0.length);
  };
  ResLevelupChain.prototype.getMaxHP_za3lpa$ = function (level) {
    var tmp$;
    if (level <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, Kotlin.imul(level, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getHP_za3lpa$ = function (level) {
    var tmp$;
    if (level <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 2 + Kotlin.imul(level, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getMaxMP_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 4 + Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getMP_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 6 + Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getAttack_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 8 + Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getDefend_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 10 + Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getNextLevelExp_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(this.mLevelData_0, 14 + Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 | 0);
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getSpeed_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = this.mLevelData_0[Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 + 16 | 0] & 255;
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getLingli_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = this.mLevelData_0[Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 + 17 | 0] & 255;
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getLuck_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = this.mLevelData_0[Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 + 18 | 0] & 255;
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  ResLevelupChain.prototype.getLearnMagicNum_za3lpa$ = function (l) {
    var tmp$;
    if (l <= this.maxLevel) {
      tmp$ = this.mLevelData_0[Kotlin.imul(l, ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0) - ResLevelupChain$Companion_getInstance().LEVEL_BYTES_0 + 19 | 0] & 255;
    }
     else
      tmp$ = 0;
    return tmp$;
  };
  function ResLevelupChain$Companion() {
    ResLevelupChain$Companion_instance = this;
    this.LEVEL_BYTES_0 = 20;
  }
  ResLevelupChain$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ResLevelupChain$Companion_instance = null;
  function ResLevelupChain$Companion_getInstance() {
    if (ResLevelupChain$Companion_instance === null) {
      new ResLevelupChain$Companion();
    }
    return ResLevelupChain$Companion_instance;
  }
  ResLevelupChain.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResLevelupChain',
    interfaces: [ResBase]
  };
  function SceneObj() {
    NPC.call(this);
  }
  SceneObj.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.state = Character$State$Companion_getInstance().fromInt_za3lpa$(buf[offset + 4 | 0] & 255);
    this.name = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 9 | 0);
    this.mDelay = buf[offset + 21 | 0] & 255;
    this.setWalkingSprite_arnj7f$(new WalkingSprite(4, buf[offset + 22 | 0] & 255));
    this.direction = Direction$North_getInstance();
    this.step = buf[offset + 3 | 0] & 255;
  };
  SceneObj.prototype.encode_vcd9jg$ = function (out) {
    out.writeInt_za3lpa$(this.type);
    out.writeInt_za3lpa$(this.index);
    out.writeInt_za3lpa$(this.state.v);
    out.writeString_61zpoe$(this.name);
    out.writeInt_za3lpa$(this.mDelay);
    out.writeInt_za3lpa$(this.walkingSpriteId);
    out.writeInt_za3lpa$(this.direction.v);
    out.writeInt_za3lpa$(this.step);
    out.writeInt_za3lpa$(this.posInMap.x);
    out.writeInt_za3lpa$(this.posInMap.y);
  };
  SceneObj.prototype.decode_setnfj$ = function (coder) {
    this.type = coder.readInt();
    this.index = coder.readInt();
    this.state = Character$State$Companion_getInstance().fromInt_za3lpa$(coder.readInt());
    this.name = coder.readString();
    this.mDelay = coder.readInt();
    this.setWalkingSprite_arnj7f$(new WalkingSprite(4, coder.readInt()));
    this.direction = Direction$Companion_getInstance().fromInt_za3lpa$(coder.readInt());
    this.step = coder.readInt();
    this.setPosInMap_vux9f0$(coder.readInt(), coder.readInt());
  };
  SceneObj.prototype.walk = function () {
  };
  SceneObj.prototype.walk_rtfsey$ = function (d) {
  };
  SceneObj.prototype.walkStay_rtfsey$ = function (d) {
  };
  SceneObj.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'SceneObj',
    interfaces: [NPC]
  };
  function WalkingSprite(type, id) {
    WalkingSprite$Companion_getInstance();
    this.mResImage_0 = null;
    this.mOffset_0 = 1;
    this.mI_0 = 0;
    var tmp$;
    this.mResImage_0 = Kotlin.isType(tmp$ = ensureNotNull(DatLib$Companion_getInstance().instance).getRes_2et8c9$(DatLib$ResType$ACP_getInstance(), type, id), ResImage) ? tmp$ : throwCCE();
  }
  Object.defineProperty(WalkingSprite.prototype, 'id', {
    get: function () {
      return this.mResImage_0.index;
    }
  });
  Object.defineProperty(WalkingSprite.prototype, 'step', {
    get: function () {
      return this.mI_0;
    },
    set: function (step) {
      this.mI_0 = step % 4;
    }
  });
  WalkingSprite.prototype.setDirection_rtfsey$ = function (d) {
    var tmp$;
    if (equals(d, Direction$North_getInstance()))
      tmp$ = 1;
    else if (equals(d, Direction$East_getInstance()))
      tmp$ = 4;
    else if (equals(d, Direction$South_getInstance()))
      tmp$ = 7;
    else if (equals(d, Direction$West_getInstance()))
      tmp$ = 10;
    else
      tmp$ = Kotlin.noWhenBranchMatched();
    this.mOffset_0 = tmp$;
  };
  WalkingSprite.prototype.walk_rtfsey$ = function (d) {
    this.setDirection_rtfsey$(d);
    this.walk();
  };
  WalkingSprite.prototype.walk = function () {
    this.mI_0 = this.mI_0 + 1 | 0;
    this.mI_0 = this.mI_0 % 4;
  };
  WalkingSprite.prototype.draw_2g4tob$ = function (canvas, x, y) {
    var y_0 = y;
    y_0 = y_0 + 16 - this.mResImage_0.height | 0;
    if ((x + this.mResImage_0.width | 0) > 0 && x < (160 - 16 | 0) && (y_0 + this.mResImage_0.height | 0) > 0 && y_0 < 96) {
      this.mResImage_0.draw_tj1hu5$(canvas, this.mOffset_0 + WalkingSprite$Companion_getInstance().OFFSET_0[this.mI_0] | 0, x + Global_getInstance().MAP_LEFT_OFFSET | 0, y_0);
    }
  };
  function WalkingSprite$Companion() {
    WalkingSprite$Companion_instance = this;
    this.OFFSET_0 = new Int32Array([0, 1, 2, 1]);
  }
  WalkingSprite$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var WalkingSprite$Companion_instance = null;
  function WalkingSprite$Companion_getInstance() {
    if (WalkingSprite$Companion_instance === null) {
      new WalkingSprite$Companion();
    }
    return WalkingSprite$Companion_instance;
  }
  WalkingSprite.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'WalkingSprite',
    interfaces: []
  };
  function ActionExecutor(mActionQueue, mCombat) {
    this.mActionQueue_0 = mActionQueue;
    this.mCombat_0 = mCombat;
    this.mCurrentAction_0 = null;
    this.mIsNewAction_0 = true;
  }
  ActionExecutor.prototype.reset = function () {
    this.mCurrentAction_0 = null;
    this.mIsNewAction_0 = true;
  };
  ActionExecutor.prototype.update_s8cxhz$ = function (delta) {
    if (this.mCurrentAction_0 == null) {
      this.mCurrentAction_0 = this.mActionQueue_0.pop();
      if (this.mCurrentAction_0 == null) {
        return false;
      }
      ensureNotNull(this.mCurrentAction_0).preproccess();
      this.mIsNewAction_0 = false;
    }
    if (this.mIsNewAction_0) {
      if (!this.fixAction_0()) {
        return false;
      }
      ensureNotNull(this.mCurrentAction_0).preproccess();
      this.mIsNewAction_0 = false;
    }
    if (!ensureNotNull(this.mCurrentAction_0).update_s8cxhz$(delta)) {
      ensureNotNull(this.mCurrentAction_0).postExecute();
      this.mCurrentAction_0 = this.mActionQueue_0.pop();
      if (this.mCurrentAction_0 == null) {
        return false;
      }
      this.mIsNewAction_0 = true;
    }
    return true;
  };
  ActionExecutor.prototype.fixAction_0 = function () {
    var tmp$, tmp$_0;
    while (!ensureNotNull(this.mCurrentAction_0).isAttackerAlive) {
      this.mCurrentAction_0 = this.mActionQueue_0.pop();
      if (this.mCurrentAction_0 == null) {
        return false;
      }
    }
    if (!ensureNotNull(this.mCurrentAction_0).isTargetAlive) {
      if (ensureNotNull(this.mCurrentAction_0).isSingleTarget) {
        return false;
      }
       else {
        if (ensureNotNull(this.mCurrentAction_0).targetIsMonster()) {
          tmp$ = this.mCombat_0.firstAliveMonster;
        }
         else {
          tmp$ = this.mCombat_0.randomAlivePlayer;
        }
        var newTarget = tmp$;
        if (newTarget == null) {
          return false;
        }
         else if (!Kotlin.isType(this.mCurrentAction_0, ActionFlee)) {
          (Kotlin.isType(tmp$_0 = this.mCurrentAction_0, ActionSingleTarget) ? tmp$_0 : throwCCE()).setTarget_qpjxya$(newTarget);
        }
      }
    }
    return true;
  };
  ActionExecutor.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$;
    (tmp$ = this.mCurrentAction_0) != null ? (tmp$.draw_9in0vv$(canvas), Unit) : null;
  };
  ActionExecutor.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionExecutor',
    interfaces: []
  };
  var emptyList = Kotlin.kotlin.collections.emptyList_287e2$;
  function Combat() {
    Combat$Companion_getInstance();
    BaseScreen.call(this);
    this.mScrb_0 = 0;
    this.mScrl_0 = 0;
    this.mScrR_0 = 0;
    this.mCombatState_0 = Combat$CombatState$SelectAction_getInstance();
    this.mIsAutoAttack_0 = false;
    this.mActionQueue_0 = ArrayQueue$Companion_getInstance().create_287e2$();
    this.mActionExecutor_0 = new ActionExecutor(this.mActionQueue_0, this);
    this.mCombatUI_0 = new CombatUI(this, 0);
    this.mMonsterType_0 = null;
    this.mMonsterList_0 = ArrayList_init();
    this.mPlayerList_0 = emptyList();
    this.mCurSelActionPlayerIndex_0 = 0;
    this.mRoundCnt_0 = 0;
    this.mHasEventExed_0 = false;
    this.mMaxRound_0 = 0;
    this.mEventRound_0 = null;
    this.mEventNum_0 = null;
    this.mLossAddr_0 = 0;
    this.mWinAddr_0 = 0;
    var tmp$;
    this.mFlyPeach_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 249), ResSrs) ? tmp$ : throwCCE();
    this.mIsWin_0 = false;
    this.mBackground_8be2vx$ = Bitmap_init(0, 0);
    this.mWinMoney_0 = 0;
    this.mWinExp_0 = 0;
    this.mCombatSuccess_0 = null;
    this.mTimeCnt_0 = Kotlin.Long.ZERO;
  }
  var Collection = Kotlin.kotlin.collections.Collection;
  Object.defineProperty(Combat.prototype, 'isAnyPlayerAlive_0', {
    get: function () {
      var $receiver = this.mPlayerList_0;
      var any$result;
      any$break: do {
        var tmp$;
        if (Kotlin.isType($receiver, Collection) && $receiver.isEmpty()) {
          any$result = false;
          break any$break;
        }
        tmp$ = $receiver.iterator();
        while (tmp$.hasNext()) {
          var element = tmp$.next();
          if (element.hp > 0) {
            any$result = true;
            break any$break;
          }
        }
        any$result = false;
      }
       while (false);
      return any$result;
    }
  });
  Object.defineProperty(Combat.prototype, 'isAllMonsterDead_0', {
    get: function () {
      return this.firstAliveMonster == null;
    }
  });
  Object.defineProperty(Combat.prototype, 'nextAlivePlayerIndex_0', {
    get: function () {
      var tmp$;
      var $receiver = until(this.mCurSelActionPlayerIndex_0 + 1 | 0, this.mPlayerList_0.size);
      var firstOrNull$result;
      firstOrNull$break: do {
        var tmp$_0;
        tmp$_0 = $receiver.iterator();
        while (tmp$_0.hasNext()) {
          var element = tmp$_0.next();
          if (this.mPlayerList_0.get_za3lpa$(element).isAlive) {
            firstOrNull$result = element;
            break firstOrNull$break;
          }
        }
        firstOrNull$result = null;
      }
       while (false);
      return (tmp$ = firstOrNull$result) != null ? tmp$ : -1;
    }
  });
  Object.defineProperty(Combat.prototype, 'preAlivePlayerIndex_0', {
    get: function () {
      var tmp$;
      var $receiver = downTo(this.mCurSelActionPlayerIndex_0 - 1 | 0, 0);
      var firstOrNull$result;
      firstOrNull$break: do {
        var tmp$_0;
        tmp$_0 = $receiver.iterator();
        while (tmp$_0.hasNext()) {
          var element = tmp$_0.next();
          if (this.mPlayerList_0.get_za3lpa$(element).isAlive) {
            firstOrNull$result = element;
            break firstOrNull$break;
          }
        }
        firstOrNull$result = null;
      }
       while (false);
      return (tmp$ = firstOrNull$result) != null ? tmp$ : -1;
    }
  });
  Object.defineProperty(Combat.prototype, 'firstAlivePlayerIndex_0', {
    get: function () {
      var tmp$;
      var $receiver = get_indices(this.mPlayerList_0);
      var firstOrNull$result;
      firstOrNull$break: do {
        var tmp$_0;
        tmp$_0 = $receiver.iterator();
        while (tmp$_0.hasNext()) {
          var element = tmp$_0.next();
          if (this.mPlayerList_0.get_za3lpa$(element).isAlive) {
            firstOrNull$result = element;
            break firstOrNull$break;
          }
        }
        firstOrNull$result = null;
      }
       while (false);
      return (tmp$ = firstOrNull$result) != null ? tmp$ : -1;
    }
  });
  Object.defineProperty(Combat.prototype, 'firstAliveMonster', {
    get: function () {
      var $receiver = this.mMonsterList_0;
      var firstOrNull$result;
      firstOrNull$break: do {
        var tmp$;
        tmp$ = $receiver.iterator();
        while (tmp$.hasNext()) {
          var element = tmp$.next();
          if (element.isAlive) {
            firstOrNull$result = element;
            break firstOrNull$break;
          }
        }
        firstOrNull$result = null;
      }
       while (false);
      return firstOrNull$result;
    }
  });
  Object.defineProperty(Combat.prototype, 'randomAlivePlayer', {
    get: function () {
      var tmp$, tmp$_0, tmp$_1;
      var cnt = 0;
      tmp$ = this.mPlayerList_0.iterator();
      while (tmp$.hasNext()) {
        var p = tmp$.next();
        if (p.isAlive) {
          cnt = cnt + 1 | 0;
        }
      }
      if (cnt === 0)
        return null;
      var arr = Kotlin.newArray(cnt, null);
      var i = 0;
      tmp$_0 = this.mPlayerList_0.iterator();
      while (tmp$_0.hasNext()) {
        var p_0 = tmp$_0.next();
        if (p_0.isAlive) {
          arr[tmp$_1 = i, i = tmp$_1 + 1 | 0, tmp$_1] = p_0;
        }
      }
      return arr[Combat$Companion_getInstance().sRandom_0.nextInt_za3lpa$(cnt)];
    }
  });
  function Combat$CombatState(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Combat$CombatState_initFields() {
    Combat$CombatState_initFields = function () {
    };
    Combat$CombatState$SelectAction_instance = new Combat$CombatState('SelectAction', 0);
    Combat$CombatState$PerformAction_instance = new Combat$CombatState('PerformAction', 1);
    Combat$CombatState$Win_instance = new Combat$CombatState('Win', 2);
    Combat$CombatState$Loss_instance = new Combat$CombatState('Loss', 3);
    Combat$CombatState$Exit_instance = new Combat$CombatState('Exit', 4);
  }
  var Combat$CombatState$SelectAction_instance;
  function Combat$CombatState$SelectAction_getInstance() {
    Combat$CombatState_initFields();
    return Combat$CombatState$SelectAction_instance;
  }
  var Combat$CombatState$PerformAction_instance;
  function Combat$CombatState$PerformAction_getInstance() {
    Combat$CombatState_initFields();
    return Combat$CombatState$PerformAction_instance;
  }
  var Combat$CombatState$Win_instance;
  function Combat$CombatState$Win_getInstance() {
    Combat$CombatState_initFields();
    return Combat$CombatState$Win_instance;
  }
  var Combat$CombatState$Loss_instance;
  function Combat$CombatState$Loss_getInstance() {
    Combat$CombatState_initFields();
    return Combat$CombatState$Loss_instance;
  }
  var Combat$CombatState$Exit_instance;
  function Combat$CombatState$Exit_getInstance() {
    Combat$CombatState_initFields();
    return Combat$CombatState$Exit_instance;
  }
  Combat$CombatState.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CombatState',
    interfaces: [Enum]
  };
  function Combat$CombatState$values() {
    return [Combat$CombatState$SelectAction_getInstance(), Combat$CombatState$PerformAction_getInstance(), Combat$CombatState$Win_getInstance(), Combat$CombatState$Loss_getInstance(), Combat$CombatState$Exit_getInstance()];
  }
  Combat$CombatState.values = Combat$CombatState$values;
  function Combat$CombatState$valueOf(name) {
    switch (name) {
      case 'SelectAction':
        return Combat$CombatState$SelectAction_getInstance();
      case 'PerformAction':
        return Combat$CombatState$PerformAction_getInstance();
      case 'Win':
        return Combat$CombatState$Win_getInstance();
      case 'Loss':
        return Combat$CombatState$Loss_getInstance();
      case 'Exit':
        return Combat$CombatState$Exit_getInstance();
      default:throwISE('No enum constant fmj.combat.Combat.CombatState.' + name);
    }
  }
  Combat$CombatState.valueOf_61zpoe$ = Combat$CombatState$valueOf;
  Combat.prototype.createBackgroundBitmap_0 = function (scrb, scrl, scrr) {
    var tmp$, tmp$_0, tmp$_1;
    this.mBackground_8be2vx$ = Bitmap$Companion_getInstance().createBitmap_vux9f0$(160, 96);
    var canvas = new Canvas(this.mBackground_8be2vx$);
    var img;
    img = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 4, scrb), ResImage) ? tmp$ : throwCCE();
    img.draw_tj1hu5$(canvas, 1, 0, 0);
    img = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 4, scrl), ResImage) ? tmp$_0 : throwCCE();
    img.draw_tj1hu5$(canvas, 1, 0, 96 - img.height | 0);
    img = Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 4, scrr), ResImage) ? tmp$_1 : throwCCE();
    img.draw_tj1hu5$(canvas, 1, 160 - img.width | 0, 0);
    this.mScrb_0 = scrb;
    this.mScrl_0 = scrl;
    this.mScrR_0 = scrr;
  };
  Combat.prototype.prepareForNewCombat_0 = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    this.mActionQueue_0.clear();
    this.mIsAutoAttack_0 = false;
    this.mCombatState_0 = Combat$CombatState$SelectAction_getInstance();
    this.mCurSelActionPlayerIndex_0 = 0;
    this.mPlayerList_0 = ScreenMainGame$Companion_getInstance().sPlayerList;
    this.mCombatUI_0.reset();
    this.mCombatUI_0.setCurrentPlayerIndex_za3lpa$(0);
    this.mCombatUI_0.setMonsterList_kg2ssq$(this.mMonsterList_0);
    this.mCombatUI_0.setPlayerList_51hka1$(this.mPlayerList_0);
    this.setOriginalPlayerPos_0();
    this.setOriginalMonsterPos_0();
    this.mRoundCnt_0 = 0;
    this.mHasEventExed_0 = false;
    tmp$ = this.mPlayerList_0.iterator();
    while (tmp$.hasNext()) {
      var p = tmp$.next();
      if (p.hp <= 0) {
        p.hp = 1;
      }
      p.setFrameByState();
    }
    tmp$_0 = this.mMonsterList_0.iterator();
    while (tmp$_0.hasNext()) {
      var m = tmp$_0.next();
      m.hp = m.maxHP;
    }
    this.mWinMoney_0 = 0;
    this.mWinExp_0 = 0;
    tmp$_1 = this.mMonsterList_0.iterator();
    while (tmp$_1.hasNext()) {
      var m_0 = tmp$_1.next();
      this.mWinMoney_0 = this.mWinMoney_0 + m_0.money | 0;
      this.mWinExp_0 = this.mWinExp_0 + m_0.exp | 0;
    }
    if (!Combat$Companion_getInstance().sIsRandomFight_0 && this.mMonsterList_0.size === 1) {
      var m_1 = this.mMonsterList_0.get_za3lpa$(0);
      var n = Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), m_1.type, m_1.index), Monster) ? tmp$_2 : throwCCE();
      n.hp = -1;
      n.isVisiable = false;
      this.mMonsterList_0.add_wxm5ur$(0, n);
      this.setOriginalMonsterPos_0();
    }
    this.mFlyPeach_0.startAni();
    this.mFlyPeach_0.setIteratorNum_za3lpa$(5);
  };
  Combat.prototype.exitCurrentCombat_0 = function () {
    var tmp$;
    if (!Combat$Companion_getInstance().sIsRandomFight_0) {
      ScreenMainGame$Companion_getInstance().instance.gotoAddress_za3lpa$(this.mIsWin_0 ? this.mWinAddr_0 : this.mLossAddr_0);
      ScriptExecutor$Companion_getInstance().goonExecute = true;
      Combat$Companion_getInstance().sIsRandomFight_0 = true;
      Combat$Companion_getInstance().sInstance_0 = Combat$Companion_getInstance().sInstanceBk_0;
      Combat$Companion_getInstance().sInstanceBk_0 = null;
    }
     else {
      if (!this.mIsWin_0) {
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
      }
    }
    Combat$Companion_getInstance().sIsFighting_0 = false;
    this.mActionQueue_0.clear();
    this.mActionExecutor_0.reset();
    this.mCombatUI_0.reset();
    this.mIsAutoAttack_0 = false;
    tmp$ = this.mPlayerList_0.iterator();
    while (tmp$.hasNext()) {
      var p = tmp$.next();
      if (p.hp <= 0) {
        p.hp = 1;
      }
      if (p.mp <= 0) {
        p.mp = 1;
      }
      p.hp = p.hp + ((p.maxHP - p.hp | 0) / 10 | 0) | 0;
      p.mp = p.mp + (p.maxMP / 5 | 0) | 0;
      if (p.mp > p.maxMP) {
        p.mp = p.maxMP;
      }
    }
  };
  Combat.prototype.setOriginalPlayerPos_0 = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    tmp$ = get_indices(this.mPlayerList_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      this.mPlayerList_0.get_za3lpa$(i).setCombatPos_vux9f0$(Combat$Companion_getInstance().sPlayerPos[i].x, Combat$Companion_getInstance().sPlayerPos[i].y);
    }
  };
  Combat.prototype.setOriginalMonsterPos_0 = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    tmp$ = get_indices(this.mMonsterList_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      this.mMonsterList_0.get_za3lpa$(i).setOriginalCombatPos_za3lpa$(i);
    }
  };
  Combat.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9, tmp$_10;
    this.mTimeCnt_0 = this.mTimeCnt_0.add(delta);
    tmp$ = this.mCombatState_0;
    if (equals(tmp$, Combat$CombatState$SelectAction_getInstance())) {
      if (!this.mHasEventExed_0 && !Combat$Companion_getInstance().sIsRandomFight_0) {
        this.mHasEventExed_0 = true;
        tmp$_0 = get_indices_0(ensureNotNull(this.mEventRound_0));
        tmp$_1 = tmp$_0.first;
        tmp$_2 = tmp$_0.last;
        tmp$_3 = tmp$_0.step;
        for (var i = tmp$_1; i <= tmp$_2; i += tmp$_3) {
          if (this.mRoundCnt_0 === ensureNotNull(this.mEventRound_0)[i] && ensureNotNull(this.mEventNum_0)[i] !== 0) {
            ScreenMainGame$Companion_getInstance().instance.triggerEvent_za3lpa$(ensureNotNull(this.mEventNum_0)[i]);
          }
        }
      }
      if (this.mIsAutoAttack_0) {
        this.generateAutoActionQueue_0();
        this.mCombatState_0 = Combat$CombatState$PerformAction_getInstance();
      }
       else {
        this.mCombatUI_0.update_s8cxhz$(delta);
      }
    }
     else if (equals(tmp$, Combat$CombatState$PerformAction_getInstance())) {
      if (!this.mActionExecutor_0.update_s8cxhz$(delta)) {
        if (this.isAllMonsterDead_0) {
          this.mTimeCnt_0 = Kotlin.Long.ZERO;
          this.mCombatState_0 = Combat$CombatState$Win_getInstance();
          tmp$_4 = Player$Companion_getInstance();
          tmp$_4.sMoney = tmp$_4.sMoney + this.mWinMoney_0 | 0;
          var lvuplist = ArrayList_init();
          tmp$_5 = this.mPlayerList_0.iterator();
          while (tmp$_5.hasNext()) {
            var p = tmp$_5.next();
            if (p.isAlive) {
              if (p.level >= p.levelupChain.maxLevel)
                break;
              var nextExp = p.levelupChain.getNextLevelExp_za3lpa$(p.level);
              var exp = this.mWinExp_0 + p.currentExp | 0;
              if (exp < nextExp) {
                p.currentExp = exp;
              }
               else {
                var cl = p.level;
                var c = p.levelupChain;
                p.currentExp = exp - nextExp | 0;
                p.level = cl + 1 | 0;
                p.maxHP = p.maxHP + c.getMaxHP_za3lpa$(cl + 1 | 0) - c.getMaxHP_za3lpa$(cl) | 0;
                p.hp = p.maxHP;
                p.maxMP = p.maxMP + c.getMaxMP_za3lpa$(cl + 1 | 0) - c.getMaxMP_za3lpa$(cl) | 0;
                p.mp = p.maxMP;
                p.attack = p.attack + c.getAttack_za3lpa$(cl + 1 | 0) - c.getAttack_za3lpa$(cl) | 0;
                p.defend = p.defend + c.getDefend_za3lpa$(cl + 1 | 0) - c.getDefend_za3lpa$(cl) | 0;
                p.magicChain.learnNum = c.getLearnMagicNum_za3lpa$(cl + 1 | 0);
                p.speed = p.speed + c.getSpeed_za3lpa$(cl + 1 | 0) - c.getSpeed_za3lpa$(cl) | 0;
                p.lingli = p.lingli + c.getLingli_za3lpa$(cl + 1 | 0) - c.getLingli_za3lpa$(cl) | 0;
                p.luck = p.luck + c.getLuck_za3lpa$(cl + 1 | 0) - c.getLuck_za3lpa$(cl) | 0;
                lvuplist.add_11rb$(p);
              }
            }
          }
          var ppt = 10;
          tmp$_6 = this.mPlayerList_0.iterator();
          while (tmp$_6.hasNext()) {
            var p_0 = tmp$_6.next();
            if (p_0.luck > ppt) {
              ppt = p_0.luck;
            }
          }
          ppt = ppt - 10 | 0;
          if (ppt > 100) {
            ppt = 100;
          }
           else if (ppt < 0) {
            ppt = 10;
          }
          var gm = new GoodsManage();
          var gl = ArrayList_init();
          tmp$_7 = this.mMonsterList_0.iterator();
          while (tmp$_7.hasNext()) {
            var m = tmp$_7.next();
            var g = m.dropGoods;
            if (g != null && Combat$Companion_getInstance().sRandom_0.nextInt_za3lpa$(101) < ppt) {
              gm.addGoods_qt1dr2$(g.type, g.index, g.goodsNum);
              Player$Companion_getInstance().sGoodsList.addGoods_qt1dr2$(g.type, g.index, g.goodsNum);
            }
          }
          gl.addAll_brywnq$(gm.goodsList);
          gl.addAll_brywnq$(gm.equipList);
          this.mCombatSuccess_0 = new CombatSuccess(this.mWinExp_0, this.mWinMoney_0, gl, lvuplist);
        }
         else {
          if (this.isAnyPlayerAlive_0) {
            this.mRoundCnt_0 = this.mRoundCnt_0 + 1 | 0;
            this.updateFighterState_0();
            this.mCombatState_0 = Combat$CombatState$SelectAction_getInstance();
            this.mCurSelActionPlayerIndex_0 = this.firstAlivePlayerIndex_0;
            this.mCombatUI_0.setCurrentPlayerIndex_za3lpa$(this.mCurSelActionPlayerIndex_0);
            tmp$_8 = this.mPlayerList_0.iterator();
            while (tmp$_8.hasNext()) {
              var p_1 = tmp$_8.next();
              p_1.setFrameByState();
            }
          }
           else {
            this.mTimeCnt_0 = Kotlin.Long.ZERO;
            this.mCombatState_0 = Combat$CombatState$Loss_getInstance();
          }
        }
      }
    }
     else if (equals(tmp$, Combat$CombatState$Win_getInstance())) {
      this.mIsWin_0 = true;
      if ((tmp$_10 = (tmp$_9 = this.mCombatSuccess_0) != null ? tmp$_9.update_s8cxhz$(delta) : null) != null ? tmp$_10 : true) {
        this.mCombatState_0 = Combat$CombatState$Exit_getInstance();
      }
    }
     else if (equals(tmp$, Combat$CombatState$Loss_getInstance())) {
      if (!Combat$Companion_getInstance().sIsRandomFight_0 || !this.mFlyPeach_0.update_s8cxhz$(delta)) {
        this.mIsWin_0 = false;
        this.mCombatState_0 = Combat$CombatState$Exit_getInstance();
      }
    }
     else if (equals(tmp$, Combat$CombatState$Exit_getInstance()))
      this.exitCurrentCombat_0();
  };
  Combat.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    canvas.drawBitmap_t8cslu$(this.mBackground_8be2vx$, 0, 0);
    tmp$ = get_indices(this.mMonsterList_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      var fc = this.mMonsterList_0.get_za3lpa$(i);
      if (fc.isVisiable) {
        ensureNotNull(fc.fightingSprite).draw_9in0vv$(canvas);
      }
    }
    tmp$_3 = reversed(get_indices(this.mPlayerList_0)).iterator();
    while (tmp$_3.hasNext()) {
      var i_0 = tmp$_3.next();
      var f = ensureNotNull(this.mPlayerList_0.get_za3lpa$(i_0).fightingSprite);
      f.draw_9in0vv$(canvas);
    }
    if (this.mCombatState_0 === Combat$CombatState$SelectAction_getInstance() && !this.mIsAutoAttack_0) {
      this.mCombatUI_0.draw_9in0vv$(canvas);
    }
     else if (this.mCombatState_0 === Combat$CombatState$PerformAction_getInstance()) {
      this.mActionExecutor_0.draw_9in0vv$(canvas);
    }
     else if (this.mCombatState_0 === Combat$CombatState$Win_getInstance()) {
      (tmp$_4 = this.mCombatSuccess_0) != null ? (tmp$_4.draw_9in0vv$(canvas), Unit) : null;
    }
     else if (this.mCombatState_0 === Combat$CombatState$Loss_getInstance() && Combat$Companion_getInstance().sIsRandomFight_0) {
      this.mFlyPeach_0.draw_2g4tob$(canvas, 0, 0);
    }
  };
  Combat.prototype.onKeyDown_za3lpa$ = function (key) {
    var tmp$;
    if (this.mCombatState_0 === Combat$CombatState$SelectAction_getInstance()) {
      if (!this.mIsAutoAttack_0) {
        this.mCombatUI_0.onKeyDown_za3lpa$(key);
      }
    }
     else if (this.mCombatState_0 === Combat$CombatState$Win_getInstance()) {
      (tmp$ = this.mCombatSuccess_0) != null ? (tmp$.onKeyDown_za3lpa$(key), Unit) : null;
    }
  };
  Combat.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (this.mCombatState_0 === Combat$CombatState$SelectAction_getInstance()) {
      if (!this.mIsAutoAttack_0) {
        this.mCombatUI_0.onKeyUp_za3lpa$(key);
      }
    }
     else if (this.mCombatState_0 === Combat$CombatState$Win_getInstance()) {
      (tmp$ = this.mCombatSuccess_0) != null ? (tmp$.onKeyUp_za3lpa$(key), Unit) : null;
    }
    if (this.mIsAutoAttack_0 && key === Global_getInstance().KEY_CANCEL) {
      this.mIsAutoAttack_0 = false;
    }
  };
  Combat.prototype.generateAutoActionQueue_0 = function () {
    var tmp$;
    var monster = this.firstAliveMonster;
    this.mActionQueue_0.clear();
    tmp$ = this.mPlayerList_0.iterator();
    while (tmp$.hasNext()) {
      var p = tmp$.next();
      if (p.isAlive) {
        this.mActionQueue_0.add_11rb$(p.hasAtbuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_ALL) ? new ActionPhysicalAttackAll(p, this.mMonsterList_0) : new ActionPhysicalAttackOne(p, ensureNotNull(monster)));
      }
    }
    this.generateMonstersActions_0();
    this.sortActionQueue_0();
  };
  Combat.prototype.generateMonstersActions_0 = function () {
    var tmp$;
    tmp$ = this.mMonsterList_0.iterator();
    while (tmp$.hasNext()) {
      var m = tmp$.next();
      if (m.isAlive) {
        var p = this.randomAlivePlayer;
        if (p != null) {
          this.mActionQueue_0.add_11rb$(m.hasAtbuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_ALL) ? new ActionPhysicalAttackAll(m, this.mPlayerList_0) : new ActionPhysicalAttackOne(m, p));
        }
      }
    }
  };
  function Combat$sortActionQueue$lambda(it) {
    return it.priority;
  }
  var sortWith = Kotlin.kotlin.collections.sortWith_nqfjgj$;
  var compareByDescending$lambda = wrapFunction(function () {
    var compareValues = Kotlin.kotlin.comparisons.compareValues_s00gnj$;
    return function (closure$selector) {
      return function (a, b) {
        var selector = closure$selector;
        return compareValues(selector(b), selector(a));
      };
    };
  });
  var Comparator = Kotlin.kotlin.Comparator;
  function Comparator$ObjectLiteral(closure$comparison) {
    this.closure$comparison = closure$comparison;
  }
  Comparator$ObjectLiteral.prototype.compare = function (a, b) {
    return this.closure$comparison(a, b);
  };
  Comparator$ObjectLiteral.$metadata$ = {kind: Kind_CLASS, interfaces: [Comparator]};
  Combat.prototype.sortActionQueue_0 = function () {
    var $receiver = this.mActionQueue_0;
    if ($receiver.size > 1) {
      sortWith($receiver, new Comparator$ObjectLiteral(compareByDescending$lambda(Combat$sortActionQueue$lambda)));
    }
  };
  Combat.prototype.isPlayerBehindDead_0 = function (index) {
    var $receiver = until(index + 1 | 0, this.mPlayerList_0.size);
    var none$result;
    none$break: do {
      var tmp$;
      if (Kotlin.isType($receiver, Collection) && $receiver.isEmpty()) {
        none$result = true;
        break none$break;
      }
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        if (this.mPlayerList_0.get_za3lpa$(element).isAlive) {
          none$result = false;
          break none$break;
        }
      }
      none$result = true;
    }
     while (false);
    return none$result;
  };
  Combat.prototype.updateFighterState_0 = function () {
  };
  Combat.prototype.onActionSelected_wwcj9m$ = function (action) {
    this.mActionQueue_0.add_11rb$(action);
    this.mCombatUI_0.reset();
    if (Kotlin.isType(action, ActionCoopMagic)) {
      this.mActionQueue_0.clear();
      this.mActionQueue_0.add_11rb$(action);
      this.generateMonstersActions_0();
      this.sortActionQueue_0();
      this.mCombatState_0 = Combat$CombatState$PerformAction_getInstance();
    }
     else if (this.mCurSelActionPlayerIndex_0 >= (this.mPlayerList_0.size - 1 | 0) || this.isPlayerBehindDead_0(this.mCurSelActionPlayerIndex_0)) {
      this.generateMonstersActions_0();
      this.sortActionQueue_0();
      this.mCombatState_0 = Combat$CombatState$PerformAction_getInstance();
    }
     else {
      this.mCurSelActionPlayerIndex_0 = this.nextAlivePlayerIndex_0;
      this.mCombatUI_0.setCurrentPlayerIndex_za3lpa$(this.mCurSelActionPlayerIndex_0);
    }
  };
  Combat.prototype.onAutoAttack = function () {
    this.mCombatUI_0.reset();
    this.mActionQueue_0.clear();
    this.mIsAutoAttack_0 = true;
    this.mCombatState_0 = Combat$CombatState$SelectAction_getInstance();
  };
  function Combat$onFlee$ObjectLiteral(this$Combat) {
    this.this$Combat = this$Combat;
  }
  Combat$onFlee$ObjectLiteral.prototype.run = function () {
    this.this$Combat.mIsWin_0 = true;
    this.this$Combat.mCombatState_0 = Combat$CombatState$Exit_getInstance();
  };
  Combat$onFlee$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Runnable]
  };
  Combat.prototype.onFlee = function () {
    var tmp$, tmp$_0;
    this.mCombatUI_0.reset();
    tmp$ = this.mCurSelActionPlayerIndex_0;
    tmp$_0 = this.mPlayerList_0.size;
    for (var i = tmp$; i < tmp$_0; i++) {
      if (this.mPlayerList_0.get_za3lpa$(i).isAlive && Combat$Companion_getInstance().sRandom_0.nextBoolean() && Combat$Companion_getInstance().sIsRandomFight_0) {
        this.mActionQueue_0.add_11rb$(new ActionFlee(this.mPlayerList_0.get_za3lpa$(i), true, new Combat$onFlee$ObjectLiteral(this)));
        break;
      }
       else {
        this.mActionQueue_0.add_11rb$(new ActionFlee(this.mPlayerList_0.get_za3lpa$(i), false, null));
      }
    }
    this.generateMonstersActions_0();
    this.sortActionQueue_0();
    this.mCombatState_0 = Combat$CombatState$PerformAction_getInstance();
  };
  Combat.prototype.onCancel = function () {
    var i = this.preAlivePlayerIndex_0;
    if (i >= 0) {
      this.mActionQueue_0.removeAt_za3lpa$(this.mActionQueue_0.size - 1 | 0);
      this.mCurSelActionPlayerIndex_0 = i;
      this.mCombatUI_0.setCurrentPlayerIndex_za3lpa$(this.mCurSelActionPlayerIndex_0);
      this.mCombatUI_0.reset();
    }
  };
  function Combat$Companion() {
    Combat$Companion_instance = this;
    this.sIsEnable_0 = false;
    this.globalDisableFighting_0 = false;
    this.sIsFighting_0 = false;
    this.sInstance_0 = null;
    this.sInstanceBk_0 = null;
    this.sIsRandomFight_0 = false;
    this.COMBAT_PROBABILITY_0 = 20;
    this.sRandom_0 = new Random();
    this.sPlayerPos = [new Point(64 + 12 | 0, 52 + 18 | 0), new Point(96 + 12 | 0, 48 + 18 | 0), new Point(128 + 12 | 0, 40 + 18 | 0)];
  }
  Combat$Companion.prototype.IsActive = function () {
    return this.sIsEnable_0 && this.sInstance_0 != null && this.sIsFighting_0;
  };
  Combat$Companion.prototype.FightEnable = function () {
    this.sIsEnable_0 = true;
  };
  Combat$Companion.prototype.FightDisable = function () {
    this.sIsEnable_0 = false;
    if (this.sInstance_0 != null) {
      this.sInstance_0 = null;
    }
  };
  Combat$Companion.prototype.InitFight_7lcbvb$ = function (monstersType, scrb, scrl, scrr) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    this.sIsEnable_0 = true;
    this.sIsRandomFight_0 = true;
    this.sIsFighting_0 = false;
    this.sInstance_0 = new Combat();
    this.sInstanceBk_0 = null;
    var cnt = 0;
    tmp$ = get_indices_0(monstersType);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      if (monstersType[i] > 0) {
        cnt = cnt + 1 | 0;
      }
    }
    ensureNotNull(this.sInstance_0).mMonsterType_0 = new Int32Array(cnt);
    var i_0 = 0;
    var j = 0;
    while (i_0 < monstersType.length) {
      if (monstersType[i_0] > 0) {
        ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[tmp$_3 = j, j = tmp$_3 + 1 | 0, tmp$_3] = monstersType[i_0];
      }
      i_0 = i_0 + 1 | 0;
    }
    ensureNotNull(this.sInstance_0).mRoundCnt_0 = 0;
    ensureNotNull(this.sInstance_0).mMaxRound_0 = 0;
    ensureNotNull(this.sInstance_0).createBackgroundBitmap_0(scrb, scrl, scrr);
  };
  Combat$Companion.prototype.SetDelegate_wle2mc$ = function (delegate) {
    var tmp$;
    if ((tmp$ = this.sInstance_0) != null) {
      tmp$.delegate = delegate;
      tmp$.mCombatUI_0.delegate = delegate;
    }
  };
  Combat$Companion.prototype.write_vcd9jg$ = function (out) {
    out.writeBoolean_6taknv$(this.IsActive());
    if (this.IsActive()) {
      out.writeIntArray_q5rwfd$(ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0));
      out.writeInt_za3lpa$(ensureNotNull(this.sInstance_0).mScrb_0);
      out.writeInt_za3lpa$(ensureNotNull(this.sInstance_0).mScrl_0);
      out.writeInt_za3lpa$(ensureNotNull(this.sInstance_0).mScrR_0);
    }
  };
  Combat$Companion.prototype.read_setnfj$ = function (coder) {
    this.sIsEnable_0 = coder.readBoolean();
    if (this.sIsEnable_0) {
      var monsterType = coder.readIntArray();
      var scrb = coder.readInt();
      var scrl = coder.readInt();
      var scrr = coder.readInt();
      this.InitFight_7lcbvb$(monsterType, scrb, scrl, scrr);
    }
  };
  var collectionSizeOrDefault = Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$;
  Combat$Companion.prototype.EnterFight_hkogy6$ = function (roundMax, monstersType, scr, evtRnds, evts, lossto, winto) {
    this.sIsRandomFight_0 = false;
    this.sInstanceBk_0 = this.sInstance_0;
    this.sInstance_0 = new Combat();
    ensureNotNull(this.sInstance_0).mMonsterList_0 = ArrayList_init();
    var $receiver = get_indices_0(monstersType);
    var destination = ArrayList_init();
    var tmp$;
    tmp$ = $receiver.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      if (monstersType[element] > 0)
        destination.add_11rb$(element);
    }
    var destination_0 = ArrayList_init(collectionSizeOrDefault(destination, 10));
    var tmp$_0;
    tmp$_0 = destination.iterator();
    while (tmp$_0.hasNext()) {
      var item = tmp$_0.next();
      var tmp$_1;
      destination_0.add_11rb$(Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), 3, monstersType[item]), Monster) ? tmp$_1 : throwCCE());
    }
    var tmp$_2;
    tmp$_2 = destination_0.iterator();
    while (tmp$_2.hasNext()) {
      var element_0 = tmp$_2.next();
      ensureNotNull(this.sInstance_0).mMonsterList_0.add_11rb$(element_0);
    }
    ensureNotNull(this.sInstance_0).mMaxRound_0 = roundMax;
    ensureNotNull(this.sInstance_0).mRoundCnt_0 = 0;
    this.PrepareForNewCombat_0();
    ensureNotNull(this.sInstance_0).createBackgroundBitmap_0(scr[0], scr[1], scr[2]);
    ensureNotNull(this.sInstance_0).mEventRound_0 = evtRnds;
    ensureNotNull(this.sInstance_0).mEventNum_0 = evts;
    ensureNotNull(this.sInstance_0).mLossAddr_0 = lossto;
    ensureNotNull(this.sInstance_0).mWinAddr_0 = winto;
  };
  Combat$Companion.prototype.PrepareForNewCombat_0 = function () {
    this.sIsEnable_0 = true;
    this.sIsFighting_0 = true;
    ensureNotNull(this.sInstance_0).prepareForNewCombat_0();
  };
  Combat$Companion.prototype.StartNewRandomCombat = function () {
    var tmp$, tmp$_0;
    if (this.globalDisableFighting_0 || !this.sIsEnable_0 || this.sInstance_0 == null || this.sRandom_0.nextInt_za3lpa$(this.COMBAT_PROBABILITY_0) !== 0) {
      this.sIsFighting_0 = false;
      return false;
    }
    for (var i = ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0).length - 1 | 0; i >= 2; i--) {
      var r = this.sRandom_0.nextInt_za3lpa$(i);
      var t = ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[i];
      ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[i] = ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[r];
      ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[r] = t;
    }
    ensureNotNull(this.sInstance_0).mMonsterList_0.clear();
    var i_0 = this.sRandom_0.nextInt_za3lpa$(3);
    var j = 0;
    while (i_0 >= 0) {
      var m = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), 3, ensureNotNull(ensureNotNull(this.sInstance_0).mMonsterType_0)[tmp$ = j, j = tmp$ + 1 | 0, tmp$]), Monster) ? tmp$_0 : throwCCE();
      ensureNotNull(this.sInstance_0).mMonsterList_0.add_11rb$(m);
      i_0 = i_0 - 1 | 0;
    }
    ensureNotNull(this.sInstance_0).mRoundCnt_0 = 0;
    ensureNotNull(this.sInstance_0).mMaxRound_0 = 0;
    this.PrepareForNewCombat_0();
    return true;
  };
  Combat$Companion.prototype.Update_s8cxhz$ = function (delta) {
    ensureNotNull(this.sInstance_0).update_s8cxhz$(delta);
  };
  Combat$Companion.prototype.Draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.sInstance_0).draw_9in0vv$(canvas);
  };
  Combat$Companion.prototype.KeyDown_za3lpa$ = function (key) {
    ensureNotNull(this.sInstance_0).onKeyDown_za3lpa$(key);
  };
  Combat$Companion.prototype.KeyUp_za3lpa$ = function (key) {
    ensureNotNull(this.sInstance_0).onKeyUp_za3lpa$(key);
  };
  Combat$Companion.prototype.ForceWin = function () {
    var tmp$;
    (tmp$ = this.sInstance_0) != null ? (tmp$.mCombatState_0 = Combat$CombatState$Win_getInstance()) : null;
  };
  Combat$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Combat$Companion_instance = null;
  function Combat$Companion_getInstance() {
    if (Combat$Companion_instance === null) {
      new Combat$Companion();
    }
    return Combat$Companion_instance;
  }
  Combat.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Combat',
    interfaces: [CombatUI$CallBack, BaseScreen]
  };
  function Action() {
    Action$Companion_getInstance();
    this.mAttacker = null;
    this.mTimeCnt_13rtyr$_0 = Kotlin.Long.ZERO;
    this.mCurrentFrame = 0;
  }
  Object.defineProperty(Action.prototype, 'priority', {
    get: function () {
      return ensureNotNull(this.mAttacker).speed;
    }
  });
  Object.defineProperty(Action.prototype, 'isAttackerAlive', {
    get: function () {
      return ensureNotNull(this.mAttacker).isAlive;
    }
  });
  Action.prototype.preproccess = function () {
  };
  Action.prototype.update_s8cxhz$ = function (delta) {
    this.mTimeCnt_13rtyr$_0 = this.mTimeCnt_13rtyr$_0.add(delta);
    if (this.mTimeCnt_13rtyr$_0.compareTo_11rb$(Kotlin.Long.fromInt(Action$Companion_getInstance().DELTA_0)) >= 0) {
      this.mCurrentFrame = this.mCurrentFrame + 1 | 0;
      this.mTimeCnt_13rtyr$_0 = Kotlin.Long.ZERO;
    }
    return true;
  };
  Action.prototype.draw_9in0vv$ = function (canvas) {
  };
  function Action$Companion() {
    Action$Companion_instance = this;
    this.DELTA_0 = 1000 / 20 | 0;
  }
  Action$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Action$Companion_instance = null;
  function Action$Companion_getInstance() {
    if (Action$Companion_instance === null) {
      new Action$Companion();
    }
    return Action$Companion_instance;
  }
  Action.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Action',
    interfaces: []
  };
  function ActionCoopMagic() {
    ActionCoopMagic$Companion_getInstance();
    this.mState_0 = ActionCoopMagic$Companion_getInstance().STATE_MOV_0;
    this.mActors_0 = null;
    this.mMonsters_0 = ArrayList_init();
    this.isSingleTarget_2ttc7h$_0 = false;
    this.magic_8be2vx$ = null;
    this.mAni_0 = new ResSrs();
    this.mRaiseAni_0 = null;
    this.mRaiseAnis_0 = null;
    this.dxy_0 = null;
    this.oxy_0 = null;
    this.mAniX_0 = 0;
    this.mAniY_0 = 0;
  }
  Object.defineProperty(ActionCoopMagic.prototype, 'mMonster_0', {
    get: function () {
      return this.mMonsters_0.get_za3lpa$(0);
    },
    set: function (value) {
      this.mMonsters_0 = mutableListOf([value]);
    }
  });
  Object.defineProperty(ActionCoopMagic.prototype, 'isSingleTarget', {
    get: function () {
      return this.isSingleTarget_2ttc7h$_0;
    },
    set: function (isSingleTarget) {
      this.isSingleTarget_2ttc7h$_0 = isSingleTarget;
    }
  });
  Object.defineProperty(ActionCoopMagic.prototype, 'coopMagic_0', {
    get: function () {
      var tmp$;
      var firstPlayer = this.mActors_0.get_za3lpa$(0);
      var dc = (tmp$ = firstPlayer.equipmentsArray[0]) == null || Kotlin.isType(tmp$, GoodsDecorations) ? tmp$ : throwCCE();
      return dc != null ? dc.coopMagic : null;
    }
  });
  Object.defineProperty(ActionCoopMagic.prototype, 'priority', {
    get: function () {
      return this.mActors_0.get_za3lpa$(0).speed;
    }
  });
  Object.defineProperty(ActionCoopMagic.prototype, 'isAttackerAlive', {
    get: function () {
      return this.mActors_0.get_za3lpa$(0).isAlive;
    }
  });
  Object.defineProperty(ActionCoopMagic.prototype, 'isTargetAlive', {
    get: function () {
      if (this.isSingleTarget) {
        return this.mMonster_0.isAlive;
      }
       else {
        var $receiver = this.mMonsters_0;
        var any$result;
        any$break: do {
          var tmp$;
          if (Kotlin.isType($receiver, Collection) && $receiver.isEmpty()) {
            any$result = false;
            break any$break;
          }
          tmp$ = $receiver.iterator();
          while (tmp$.hasNext()) {
            var element = tmp$.next();
            if (element.isAlive) {
              any$result = true;
              break any$break;
            }
          }
          any$result = false;
        }
         while (false);
        return any$result;
      }
    }
  });
  var Array_0 = Array;
  ActionCoopMagic.prototype.preproccess = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8;
    var midpos = [new Int32Array([92, 52]), new Int32Array([109, 63]), new Int32Array([126, 74])];
    var array = Array_0(this.mActors_0.size);
    var tmp$_9;
    tmp$_9 = array.length - 1 | 0;
    for (var i = 0; i <= tmp$_9; i++) {
      array[i] = new Float32Array(2);
    }
    this.dxy_0 = array;
    var array_0 = Array_0(this.mActors_0.size);
    var tmp$_10;
    tmp$_10 = array_0.length - 1 | 0;
    for (var i_0 = 0; i_0 <= tmp$_10; i_0++) {
      array_0[i_0] = new Int32Array(2);
    }
    this.oxy_0 = array_0;
    tmp$ = get_indices(this.mActors_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i_1 = tmp$_0; i_1 <= tmp$_1; i_1 += tmp$_2) {
      ensureNotNull(this.oxy_0)[i_1][0] = this.mActors_0.get_za3lpa$(i_1).combatX;
      ensureNotNull(this.oxy_0)[i_1][1] = this.mActors_0.get_za3lpa$(i_1).combatY;
    }
    tmp$_3 = get_indices_1(ensureNotNull(this.dxy_0));
    tmp$_4 = tmp$_3.first;
    tmp$_5 = tmp$_3.last;
    tmp$_6 = tmp$_3.step;
    for (var i_2 = tmp$_4; i_2 <= tmp$_5; i_2 += tmp$_6) {
      ensureNotNull(this.dxy_0)[i_2][0] = midpos[i_2][0] - ensureNotNull(this.oxy_0)[i_2][0] | 0;
      ensureNotNull(this.dxy_0)[i_2][0] = ensureNotNull(this.dxy_0)[i_2][0] / ActionCoopMagic$Companion_getInstance().MOV_FRAME_0;
      ensureNotNull(this.dxy_0)[i_2][1] = midpos[i_2][1] - ensureNotNull(this.oxy_0)[i_2][1] | 0;
      ensureNotNull(this.dxy_0)[i_2][1] = ensureNotNull(this.dxy_0)[i_2][1] / ActionCoopMagic$Companion_getInstance().MOV_FRAME_0;
    }
    if (this.isSingleTarget) {
      this.mAniX_0 = this.mMonster_0.combatX;
      this.mAniY_0 = this.mMonster_0.combatY - (ensureNotNull(this.mMonster_0.fightingSprite).height / 2 | 0) | 0;
    }
     else {
      this.mAniY_0 = 0;
      this.mAniX_0 = this.mAniY_0;
    }
    if (this.magic_8be2vx$ == null) {
      tmp$_8 = Kotlin.isType(tmp$_7 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 2, 240), ResSrs) ? tmp$_7 : throwCCE();
    }
     else {
      tmp$_8 = ensureNotNull(ensureNotNull(this.magic_8be2vx$).magicAni);
    }
    this.mAni_0 = tmp$_8;
    this.mAni_0.startAni();
  };
  ActionCoopMagic.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9, tmp$_10, tmp$_11;
    Action.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionCoopMagic$Companion_getInstance().STATE_MOV_0)
      if (this.mCurrentFrame < ActionCoopMagic$Companion_getInstance().MOV_FRAME_0) {
        tmp$_0 = get_indices(this.mActors_0);
        tmp$_1 = tmp$_0.first;
        tmp$_2 = tmp$_0.last;
        tmp$_3 = tmp$_0.step;
        for (var i = tmp$_1; i <= tmp$_2; i += tmp$_3) {
          var x = ensureNotNull(this.oxy_0)[i][0] + ensureNotNull(this.dxy_0)[i][0] * this.mCurrentFrame;
          var y = ensureNotNull(this.oxy_0)[i][1] + ensureNotNull(this.dxy_0)[i][1] * this.mCurrentFrame;
          this.mActors_0.get_za3lpa$(i).setCombatPos_vux9f0$(numberToInt(x), numberToInt(y));
        }
      }
       else {
        this.mState_0 = ActionCoopMagic$Companion_getInstance().STATE_PRE_0;
      }
     else if (tmp$ === ActionCoopMagic$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < (10 + ActionCoopMagic$Companion_getInstance().MOV_FRAME_0 | 0)) {
        tmp$_4 = get_indices(this.mActors_0);
        tmp$_5 = tmp$_4.first;
        tmp$_6 = tmp$_4.last;
        tmp$_7 = tmp$_4.step;
        for (var i_0 = tmp$_5; i_0 <= tmp$_6; i_0 += tmp$_7) {
          ensureNotNull(this.mActors_0.get_za3lpa$(i_0).fightingSprite).currentFrame = (((this.mCurrentFrame - ActionCoopMagic$Companion_getInstance().MOV_FRAME_0 | 0) * 3 | 0) / 10 | 0) + 6 | 0;
        }
      }
       else {
        this.mState_0 = ActionCoopMagic$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionCoopMagic$Companion_getInstance().STATE_ANI_0) {
      if (!this.mAni_0.update_s8cxhz$(delta)) {
        this.mState_0 = ActionCoopMagic$Companion_getInstance().STATE_AFT_0;
        tmp$_8 = get_indices(this.mActors_0);
        tmp$_9 = tmp$_8.first;
        tmp$_10 = tmp$_8.last;
        tmp$_11 = tmp$_8.step;
        for (var i_1 = tmp$_9; i_1 <= tmp$_10; i_1 += tmp$_11) {
          this.mActors_0.get_za3lpa$(i_1).setFrameByState();
          this.mActors_0.get_za3lpa$(i_1).setCombatPos_vux9f0$(ensureNotNull(this.oxy_0)[i_1][0], ensureNotNull(this.oxy_0)[i_1][1]);
        }
      }
    }
     else if (tmp$ === ActionCoopMagic$Companion_getInstance().STATE_AFT_0) {
      this.isSingleTarget;
      return false;
    }
    return true;
  };
  ActionCoopMagic.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionCoopMagic$Companion_getInstance().STATE_ANI_0) {
      this.mAni_0.drawAbsolutely_2g4tob$(canvas, this.mAniX_0, this.mAniY_0);
    }
  };
  ActionCoopMagic.prototype.postExecute = function () {
  };
  function ActionCoopMagic$updateRaiseAnimation$lambda$lambda(closure$delta) {
    return function (it) {
      return !it.update_s8cxhz$(closure$delta);
    };
  }
  ActionCoopMagic.prototype.updateRaiseAnimation_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1;
    if (this.isSingleTarget) {
      return (tmp$_0 = (tmp$ = this.mRaiseAni_0) != null ? tmp$.update_s8cxhz$(delta) : null) != null ? tmp$_0 : false;
    }
    if ((tmp$_1 = this.mRaiseAnis_0) != null) {
      removeAll(tmp$_1, ActionCoopMagic$updateRaiseAnimation$lambda$lambda(delta));
      return !tmp$_1.isEmpty();
    }
    return false;
  };
  ActionCoopMagic.prototype.drawRaiseAnimation_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0;
    if (this.isSingleTarget) {
      (tmp$ = this.mRaiseAni_0) != null ? (tmp$.draw_9in0vv$(canvas), Unit) : null;
    }
     else {
      if (this.mRaiseAnis_0 != null) {
        tmp$_0 = this.mRaiseAnis_0.iterator();
        while (tmp$_0.hasNext()) {
          var ani = tmp$_0.next();
          ani.draw_9in0vv$(canvas);
        }
      }
    }
  };
  ActionCoopMagic.prototype.targetIsMonster = function () {
    return true;
  };
  function ActionCoopMagic$Companion() {
    ActionCoopMagic$Companion_instance = this;
    this.STATE_MOV_0 = 0;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
    this.MOV_FRAME_0 = 5;
  }
  ActionCoopMagic$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionCoopMagic$Companion_instance = null;
  function ActionCoopMagic$Companion_getInstance() {
    if (ActionCoopMagic$Companion_instance === null) {
      new ActionCoopMagic$Companion();
    }
    return ActionCoopMagic$Companion_instance;
  }
  ActionCoopMagic.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionCoopMagic',
    interfaces: [Action]
  };
  function ActionCoopMagic_init(actors, monster, $this) {
    $this = $this || Object.create(ActionCoopMagic.prototype);
    Action.call($this);
    ActionCoopMagic.call($this);
    $this.mActors_0 = actors;
    $this.mMonster_0 = monster;
    $this.isSingleTarget = true;
    $this.magic_8be2vx$ = $this.coopMagic_0;
    return $this;
  }
  function ActionCoopMagic_init_0(actors, monsters, $this) {
    $this = $this || Object.create(ActionCoopMagic.prototype);
    Action.call($this);
    ActionCoopMagic.call($this);
    $this.mActors_0 = actors;
    $this.mMonsters_0 = ArrayList_init();
    $this.mMonsters_0.addAll_brywnq$(monsters);
    $this.isSingleTarget = false;
    $this.magic_8be2vx$ = $this.coopMagic_0;
    return $this;
  }
  function ActionDefend(fc) {
    ActionSingleTarget.call(this, fc, fc);
  }
  Object.defineProperty(ActionDefend.prototype, 'isTargetAlive', {
    get: function () {
      return true;
    }
  });
  Object.defineProperty(ActionDefend.prototype, 'isSingleTarget', {
    get: function () {
      return false;
    }
  });
  ActionDefend.prototype.preproccess = function () {
  };
  ActionDefend.prototype.postExecute = function () {
  };
  ActionDefend.prototype.targetIsMonster = function () {
    return true;
  };
  ActionDefend.prototype.update_s8cxhz$ = function (delta) {
    return false;
  };
  ActionDefend.prototype.draw_9in0vv$ = function (canvas) {
  };
  ActionDefend.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionDefend',
    interfaces: [ActionSingleTarget]
  };
  function ActionFlee(player, fleeSucceed, runAfterFlee) {
    Action.call(this);
    this.player_0 = player;
    this.fleeSucceed_0 = fleeSucceed;
    this.runAfterFlee_0 = runAfterFlee;
    this.FRAME_CNT_0 = 5;
    this.ox_0 = 0;
    this.oy_0 = 0;
    this.dy_0 = 0;
  }
  Object.defineProperty(ActionFlee.prototype, 'priority', {
    get: function () {
      return this.player_0.speed * 100 | 0;
    }
  });
  Object.defineProperty(ActionFlee.prototype, 'isAttackerAlive', {
    get: function () {
      return true;
    }
  });
  Object.defineProperty(ActionFlee.prototype, 'isTargetAlive', {
    get: function () {
      return false;
    }
  });
  Object.defineProperty(ActionFlee.prototype, 'isSingleTarget', {
    get: function () {
      return false;
    }
  });
  ActionFlee.prototype.preproccess = function () {
    this.ox_0 = this.player_0.combatX;
    this.oy_0 = this.player_0.combatY;
    this.dy_0 = (96 - this.oy_0 | 0) / this.FRAME_CNT_0 | 0;
    ensureNotNull(this.player_0.fightingSprite).currentFrame = 1;
  };
  ActionFlee.prototype.update_s8cxhz$ = function (delta) {
    Action.prototype.update_s8cxhz$.call(this, delta);
    if (this.mCurrentFrame < this.FRAME_CNT_0) {
      this.player_0.setCombatPos_vux9f0$(this.ox_0, this.oy_0 + Kotlin.imul(this.dy_0, this.mCurrentFrame) | 0);
      return true;
    }
     else if (!this.fleeSucceed_0 && this.mCurrentFrame < (this.FRAME_CNT_0 + 2 | 0)) {
      this.player_0.setCombatPos_vux9f0$(this.ox_0, this.oy_0);
      ensureNotNull(this.player_0.fightingSprite).currentFrame = 11;
    }
     else if (!this.fleeSucceed_0) {
      this.player_0.setFrameByState();
    }
    return false;
  };
  ActionFlee.prototype.postExecute = function () {
    if (this.fleeSucceed_0 && this.runAfterFlee_0 != null) {
      ensureNotNull(this.runAfterFlee_0).run();
    }
     else {
      this.player_0.setCombatPos_vux9f0$(this.ox_0, this.oy_0);
    }
  };
  ActionFlee.prototype.updateRaiseAnimation_s8cxhz$ = function (delta) {
    return false;
  };
  ActionFlee.prototype.drawRaiseAnimation_9in0vv$ = function (canvas) {
  };
  ActionFlee.prototype.targetIsMonster = function () {
    return false;
  };
  ActionFlee.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionFlee',
    interfaces: [Action]
  };
  function ActionMagicAttackAll(attacker, targets, magic) {
    ActionMagicAttackAll$Companion_getInstance();
    ActionMultiTarget.call(this, attacker, targets);
    this.magic_0 = magic;
    this.mState_0 = 1;
    this.mAni_0 = null;
    this.ox_0 = 0;
    this.oy_0 = 0;
  }
  ActionMagicAttackAll.prototype.preproccess = function () {
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.mAni_0 = this.magic_0.magicAni;
    ensureNotNull(this.mAni_0).startAni();
    ensureNotNull(this.mAni_0).setIteratorNum_za3lpa$(2);
    this.magic_0.use_h32lzv$(ensureNotNull(this.mAttacker), this.mTargets);
    this.mRaiseAnis.add_11rb$(new RaiseAnimation(10, 10, 10, 0));
    this.mRaiseAnis.add_11rb$(new RaiseAnimation(30, 10, 20, 0));
  };
  ActionMagicAttackAll.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5;
    ActionMultiTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionMagicAttackAll$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0 + 2 | 0, this.oy_0 + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionMagicAttackAll$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionMagicAttackAll$Companion_getInstance().STATE_ANI_0) {
      if (!ensureNotNull(this.mAni_0).update_s8cxhz$(delta)) {
        this.mState_0 = ActionMagicAttackAll$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
        if (Kotlin.isType(this.mTargets.get_za3lpa$(0), Player)) {
          tmp$_1 = this.mTargets.iterator();
          while (tmp$_1.hasNext()) {
            var fc = tmp$_1.next();
            ensureNotNull(fc.fightingSprite).currentFrame = 10;
          }
        }
         else {
          tmp$_2 = this.mTargets.iterator();
          while (tmp$_2.hasNext()) {
            var fc_0 = tmp$_2.next();
            ensureNotNull(fc_0.fightingSprite).move_vux9f0$(2, 2);
          }
        }
      }
    }
     else if (tmp$ === ActionMagicAttackAll$Companion_getInstance().STATE_AFT_0)
      if (!this.updateRaiseAnimation_s8cxhz$(delta)) {
        if (Kotlin.isType(this.mTargets.get_za3lpa$(0), Player)) {
          tmp$_3 = this.mTargets.iterator();
          while (tmp$_3.hasNext()) {
            var fc_1 = tmp$_3.next();
            (Kotlin.isType(tmp$_4 = fc_1, Player) ? tmp$_4 : throwCCE()).setFrameByState();
          }
        }
         else {
          tmp$_5 = this.mTargets.iterator();
          while (tmp$_5.hasNext()) {
            var fc_2 = tmp$_5.next();
            ensureNotNull(fc_2.fightingSprite).move_vux9f0$(-2, -2);
          }
        }
        return false;
      }
    return true;
  };
  ActionMagicAttackAll.prototype.draw_9in0vv$ = function (canvas) {
    ActionMultiTarget.prototype.draw_9in0vv$.call(this, canvas);
    if (this.mState_0 === ActionMagicAttackAll$Companion_getInstance().STATE_ANI_0) {
      ensureNotNull(this.mAni_0).draw_2g4tob$(canvas, 0, 0);
    }
     else if (this.mState_0 === ActionMagicAttackAll$Companion_getInstance().STATE_AFT_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  function ActionMagicAttackAll$Companion() {
    ActionMagicAttackAll$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionMagicAttackAll$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionMagicAttackAll$Companion_instance = null;
  function ActionMagicAttackAll$Companion_getInstance() {
    if (ActionMagicAttackAll$Companion_instance === null) {
      new ActionMagicAttackAll$Companion();
    }
    return ActionMagicAttackAll$Companion_instance;
  }
  ActionMagicAttackAll.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionMagicAttackAll',
    interfaces: [ActionMultiTarget]
  };
  function ActionMagicAttackOne(attacker, target, magic) {
    ActionMagicAttackOne$Companion_getInstance();
    ActionSingleTarget.call(this, attacker, target);
    this.magic_0 = magic;
    this.mState_0 = 1;
    this.mAni_0 = null;
    this.mAniX_0 = 0;
    this.mAniY_0 = 0;
    this.ox_0 = 0;
    this.oy_0 = 0;
  }
  ActionMagicAttackOne.prototype.preproccess = function () {
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.mAni_0 = this.magic_0.magicAni;
    ensureNotNull(this.mAni_0).startAni();
    ensureNotNull(this.mAni_0).setIteratorNum_za3lpa$(2);
    var ohp = this.mTarget.hp;
    if (Kotlin.isType(this.magic_0, MagicAttack)) {
      this.magic_0.use_qwqr58$(ensureNotNull(this.mAttacker), this.mTarget);
    }
    this.mAniX_0 = this.mTarget.combatX;
    this.mAniY_0 = this.mTarget.combatY - (ensureNotNull(this.mTarget.fightingSprite).height / 2 | 0) | 0;
    this.mRaiseAni = new RaiseAnimation(this.mTarget.combatX, this.mTarget.combatY, this.mTarget.hp - ohp | 0, 0);
  };
  ActionMagicAttackOne.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1;
    ActionSingleTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionMagicAttackOne$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0 + 2 | 0, this.oy_0 + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionMagicAttackOne$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionMagicAttackOne$Companion_getInstance().STATE_ANI_0) {
      if (!ensureNotNull(this.mAni_0).update_s8cxhz$(delta)) {
        this.mState_0 = ActionMagicAttackOne$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
        if (Kotlin.isType(this.mTarget, Player)) {
          ensureNotNull(this.mTarget.fightingSprite).currentFrame = 10;
        }
         else {
          ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(2, 2);
        }
      }
    }
     else if (tmp$ === ActionMagicAttackOne$Companion_getInstance().STATE_AFT_0)
      if (!ensureNotNull(this.mRaiseAni).update_s8cxhz$(delta)) {
        if (Kotlin.isType(this.mTarget, Player)) {
          (Kotlin.isType(tmp$_1 = this.mTarget, Player) ? tmp$_1 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(-2, -2);
        }
        return false;
      }
    return true;
  };
  ActionMagicAttackOne.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionMagicAttackOne$Companion_getInstance().STATE_ANI_0) {
      ensureNotNull(this.mAni_0).drawAbsolutely_2g4tob$(canvas, this.mAniX_0, this.mAniY_0);
    }
     else if (this.mState_0 === ActionMagicAttackOne$Companion_getInstance().STATE_AFT_0) {
      ensureNotNull(this.mRaiseAni).draw_9in0vv$(canvas);
    }
  };
  function ActionMagicAttackOne$Companion() {
    ActionMagicAttackOne$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionMagicAttackOne$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionMagicAttackOne$Companion_instance = null;
  function ActionMagicAttackOne$Companion_getInstance() {
    if (ActionMagicAttackOne$Companion_instance === null) {
      new ActionMagicAttackOne$Companion();
    }
    return ActionMagicAttackOne$Companion_instance;
  }
  ActionMagicAttackOne.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionMagicAttackOne',
    interfaces: [ActionSingleTarget]
  };
  function ActionMagicHelpAll(attacker, targets, magic) {
    ActionMagicHelpAll$Companion_getInstance();
    ActionMultiTarget.call(this, attacker, targets);
    this.magic_8be2vx$ = magic;
    this.mState_0 = 1;
    this.mAni_8be2vx$ = new ResSrs();
    this.ox_8be2vx$ = 0;
    this.oy_8be2vx$ = 0;
  }
  ActionMagicHelpAll.prototype.preproccess = function () {
    this.ox_8be2vx$ = ensureNotNull(this.mAttacker).combatX;
    this.oy_8be2vx$ = ensureNotNull(this.mAttacker).combatY;
    this.mAni_8be2vx$ = ensureNotNull(this.magic_8be2vx$.magicAni);
    this.mAni_8be2vx$.startAni();
    this.mAni_8be2vx$.setIteratorNum_za3lpa$(2);
    ensureNotNull(this.mRaiseAnis).add_11rb$(new RaiseAnimation(10, 20, 10, 0));
    ensureNotNull(this.mRaiseAnis).add_11rb$(new RaiseAnimation(30, 10, 10, 0));
  };
  ActionMagicHelpAll.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0;
    ActionMultiTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionMagicHelpAll$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_8be2vx$ + 2 | 0, this.oy_8be2vx$ + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionMagicHelpAll$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionMagicHelpAll$Companion_getInstance().STATE_ANI_0) {
      if (!this.mAni_8be2vx$.update_s8cxhz$(delta)) {
        this.mState_0 = ActionMagicHelpAll$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
      }
    }
     else if (tmp$ === ActionMagicHelpAll$Companion_getInstance().STATE_AFT_0)
      return this.updateRaiseAnimation_s8cxhz$(delta);
    return true;
  };
  ActionMagicHelpAll.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionMagicHelpAll$Companion_getInstance().STATE_ANI_0) {
      this.mAni_8be2vx$.draw_2g4tob$(canvas, 0, 0);
    }
     else if (this.mState_0 === ActionMagicHelpAll$Companion_getInstance().STATE_AFT_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  function ActionMagicHelpAll$Companion() {
    ActionMagicHelpAll$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionMagicHelpAll$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionMagicHelpAll$Companion_instance = null;
  function ActionMagicHelpAll$Companion_getInstance() {
    if (ActionMagicHelpAll$Companion_instance === null) {
      new ActionMagicHelpAll$Companion();
    }
    return ActionMagicHelpAll$Companion_instance;
  }
  ActionMagicHelpAll.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionMagicHelpAll',
    interfaces: [ActionMultiTarget]
  };
  function ActionMagicHelpOne(attacker, target, magic) {
    ActionMagicHelpOne$Companion_getInstance();
    ActionSingleTarget.call(this, attacker, target);
    this.magic_8be2vx$ = magic;
    this.mState_0 = 1;
    this.mAni_8be2vx$ = new ResSrs();
    this.mAnix_8be2vx$ = 0;
    this.mAniy_8be2vx$ = 0;
    this.ox_8be2vx$ = 0;
    this.oy_8be2vx$ = 0;
  }
  ActionMagicHelpOne.prototype.preproccess = function () {
    this.mAni_8be2vx$ = ensureNotNull(this.magic_8be2vx$.magicAni);
    this.mAni_8be2vx$.startAni();
    this.mAni_8be2vx$.setIteratorNum_za3lpa$(2);
    this.mAnix_8be2vx$ = this.mTarget.combatX;
    this.mAniy_8be2vx$ = this.mTarget.combatY;
    this.mRaiseAni = new RaiseAnimation(this.mTarget.combatX, this.mTarget.combatTop, 10, 0);
  };
  ActionMagicHelpOne.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0;
    ActionSingleTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionMagicHelpOne$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_8be2vx$ + 2 | 0, this.oy_8be2vx$ + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionMagicHelpOne$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionMagicHelpOne$Companion_getInstance().STATE_ANI_0) {
      if (!this.mAni_8be2vx$.update_s8cxhz$(delta)) {
        this.mState_0 = ActionMagicHelpOne$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
      }
    }
     else if (tmp$ === ActionMagicHelpOne$Companion_getInstance().STATE_AFT_0)
      return ensureNotNull(this.mRaiseAni).update_s8cxhz$(delta);
    return true;
  };
  ActionMagicHelpOne.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionMagicHelpOne$Companion_getInstance().STATE_ANI_0) {
      this.mAni_8be2vx$.drawAbsolutely_2g4tob$(canvas, this.mAnix_8be2vx$, this.mAniy_8be2vx$);
    }
     else if (this.mState_0 === ActionMagicHelpOne$Companion_getInstance().STATE_AFT_0) {
      ensureNotNull(this.mRaiseAni).draw_9in0vv$(canvas);
    }
  };
  function ActionMagicHelpOne$Companion() {
    ActionMagicHelpOne$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionMagicHelpOne$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionMagicHelpOne$Companion_instance = null;
  function ActionMagicHelpOne$Companion_getInstance() {
    if (ActionMagicHelpOne$Companion_instance === null) {
      new ActionMagicHelpOne$Companion();
    }
    return ActionMagicHelpOne$Companion_instance;
  }
  ActionMagicHelpOne.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionMagicHelpOne',
    interfaces: [ActionSingleTarget]
  };
  function ActionMultiTarget(attacker, targets) {
    Action.call(this);
    this.mTargets = ArrayList_init();
    this.mRaiseAnis = ArrayList_init();
    this.mAttacker = attacker;
    this.mTargets.addAll_brywnq$(targets);
  }
  Object.defineProperty(ActionMultiTarget.prototype, 'isTargetAlive', {
    get: function () {
      var $receiver = this.mTargets;
      var any$result;
      any$break: do {
        var tmp$;
        if (Kotlin.isType($receiver, Collection) && $receiver.isEmpty()) {
          any$result = false;
          break any$break;
        }
        tmp$ = $receiver.iterator();
        while (tmp$.hasNext()) {
          var element = tmp$.next();
          if (element.isAlive) {
            any$result = true;
            break any$break;
          }
        }
        any$result = false;
      }
       while (false);
      return any$result;
    }
  });
  Object.defineProperty(ActionMultiTarget.prototype, 'isSingleTarget', {
    get: function () {
      return false;
    }
  });
  ActionMultiTarget.prototype.postExecute = function () {
    var tmp$;
    tmp$ = this.mTargets.iterator();
    while (tmp$.hasNext()) {
      var fc = tmp$.next();
      fc.isVisiable = fc.isAlive;
    }
  };
  function ActionMultiTarget$updateRaiseAnimation$lambda(closure$delta) {
    return function (it) {
      return !it.update_s8cxhz$(closure$delta);
    };
  }
  ActionMultiTarget.prototype.updateRaiseAnimation_s8cxhz$ = function (delta) {
    removeAll(this.mRaiseAnis, ActionMultiTarget$updateRaiseAnimation$lambda(delta));
    return !this.mRaiseAnis.isEmpty();
  };
  ActionMultiTarget.prototype.drawRaiseAnimation_9in0vv$ = function (canvas) {
    var tmp$;
    tmp$ = this.mRaiseAnis.iterator();
    while (tmp$.hasNext()) {
      var ani = tmp$.next();
      ani.draw_9in0vv$(canvas);
    }
  };
  ActionMultiTarget.prototype.draw_9in0vv$ = function (canvas) {
  };
  ActionMultiTarget.prototype.targetIsMonster = function () {
    return Kotlin.isType(this.mTargets.get_za3lpa$(0), Monster);
  };
  ActionMultiTarget.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionMultiTarget',
    interfaces: [Action]
  };
  function ActionPhysicalAttackAll(attacker, targets) {
    ActionMultiTarget.call(this, attacker, targets);
    this.TOTAL_FRAME_0 = 5;
    this.dx_0 = 0;
    this.dy_0 = 0;
    this.ox_0 = 0;
    this.oy_0 = 0;
  }
  ActionPhysicalAttackAll.prototype.preproccess = function () {
    var tmp$;
    var damage;
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.dx_0 = (44.0 - ensureNotNull(this.mAttacker).combatX) / this.TOTAL_FRAME_0;
    this.dy_0 = (14.0 - ensureNotNull(this.mAttacker).combatY) / this.TOTAL_FRAME_0;
    tmp$ = this.mTargets.size;
    for (var i = 0; i < tmp$; i++) {
      var fc = this.mTargets.get_za3lpa$(i);
      if (!fc.isAlive) {
        continue;
      }
      damage = ensureNotNull(this.mAttacker).attack - fc.defend | 0;
      if (damage <= 0) {
        damage = 1;
      }
      damage = damage + numberToInt(random() * 3) | 0;
      fc.hp = fc.hp - damage | 0;
      this.mRaiseAnis.add_11rb$(new RaiseAnimation(this.mTargets.get_za3lpa$(i).combatX, this.mTargets.get_za3lpa$(i).combatY, -damage, 0));
    }
  };
  ActionPhysicalAttackAll.prototype.update_s8cxhz$ = function (delta) {
    ActionMultiTarget.prototype.update_s8cxhz$.call(this, delta);
    if (this.mCurrentFrame < this.TOTAL_FRAME_0) {
      ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(numberToInt(this.ox_0 + this.dx_0 * this.mCurrentFrame), numberToInt(this.oy_0 + this.dy_0 * this.mCurrentFrame));
      if (Kotlin.isType(this.mAttacker, Monster)) {
        var fs = ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite);
        fs.currentFrame = (Kotlin.imul(fs.frameCnt, this.mCurrentFrame) / this.TOTAL_FRAME_0 | 0) + 1 | 0;
      }
       else if (Kotlin.isType(this.mAttacker, Player)) {
        var fs_0 = ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite);
        fs_0.currentFrame = ((5 * this.mCurrentFrame | 0) / this.TOTAL_FRAME_0 | 0) + 1 | 0;
      }
    }
     else if (this.mCurrentFrame > this.TOTAL_FRAME_0) {
      return this.updateRaiseAnimation_s8cxhz$(delta);
    }
     else {
      ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0, this.oy_0);
      if (Kotlin.isType(this.mAttacker, Monster)) {
        ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).currentFrame = 1;
      }
       else if (Kotlin.isType(this.mAttacker, Player)) {
        var fs_1 = ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite);
        fs_1.currentFrame = 1;
      }
    }
    return true;
  };
  ActionPhysicalAttackAll.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mCurrentFrame >= this.TOTAL_FRAME_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  ActionPhysicalAttackAll.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionPhysicalAttackAll',
    interfaces: [ActionMultiTarget]
  };
  function ActionPhysicalAttackOne(attacker, target) {
    ActionSingleTarget.call(this, attacker, target);
    this.TOTAL_FRAME_0 = 5;
    this.dx_0 = 0;
    this.dy_0 = 0;
    this.ox_0 = 0;
    this.oy_0 = 0;
    this.mTotalMark_0 = true;
  }
  ActionPhysicalAttackOne.prototype.preproccess = function () {
    var damage;
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.dx_0 = (this.mTarget.combatX - ensureNotNull(this.mAttacker).combatX | 0) / this.TOTAL_FRAME_0;
    this.dy_0 = (this.mTarget.combatY - ensureNotNull(this.mAttacker).combatY | 0) / this.TOTAL_FRAME_0;
    damage = ensureNotNull(this.mAttacker).attack - this.mTarget.defend | 0;
    if (damage <= 0) {
      damage = 1;
    }
    if (Kotlin.isType(this.mAttacker, Player)) {
      damage = damage * 10 | 0;
    }
    damage = damage + numberToInt(random() * 10) | 0;
    this.mTarget.hp = this.mTarget.hp - damage | 0;
    this.mRaiseAni = new RaiseAnimation(this.mTarget.combatLeft, this.mTarget.combatTop, -damage, 0);
  };
  ActionPhysicalAttackOne.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    ActionSingleTarget.prototype.update_s8cxhz$.call(this, delta);
    if (this.mCurrentFrame < this.TOTAL_FRAME_0) {
      ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(numberToInt(this.ox_0 + this.dx_0 * this.mCurrentFrame), numberToInt(this.oy_0 + this.dy_0 * this.mCurrentFrame));
      if (Kotlin.isType(this.mAttacker, Monster)) {
        var fs = ensureNotNull((Kotlin.isType(tmp$ = this.mAttacker, Monster) ? tmp$ : throwCCE()).fightingSprite);
        fs.currentFrame = (Kotlin.imul(fs.frameCnt, this.mCurrentFrame) / this.TOTAL_FRAME_0 | 0) + 1 | 0;
      }
       else if (Kotlin.isType(this.mAttacker, Player)) {
        var fs_0 = ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite);
        fs_0.currentFrame = ((5 * this.mCurrentFrame | 0) / this.TOTAL_FRAME_0 | 0) + 1 | 0;
      }
    }
     else if (this.mCurrentFrame > this.TOTAL_FRAME_0) {
      if (!this.updateRaiseAnimation_s8cxhz$(delta)) {
        if (Kotlin.isType(this.mTarget, Player)) {
          (Kotlin.isType(tmp$_0 = this.mTarget, Player) ? tmp$_0 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(-2, -2);
        }
        return false;
      }
    }
     else if (this.mTotalMark_0) {
      this.mTotalMark_0 = false;
      ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0, this.oy_0);
      if (Kotlin.isType(this.mAttacker, Monster)) {
        var fs_1 = ensureNotNull((Kotlin.isType(tmp$_1 = this.mAttacker, Monster) ? tmp$_1 : throwCCE()).fightingSprite);
        fs_1.currentFrame = 1;
      }
       else if (Kotlin.isType(this.mAttacker, Player)) {
        (Kotlin.isType(tmp$_2 = this.mAttacker, Player) ? tmp$_2 : throwCCE()).setFrameByState();
      }
      if (Kotlin.isType(this.mTarget, Player)) {
        ensureNotNull(this.mTarget.fightingSprite).currentFrame = 10;
      }
       else {
        ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(2, 2);
      }
    }
    return true;
  };
  ActionPhysicalAttackOne.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mCurrentFrame >= this.TOTAL_FRAME_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  ActionPhysicalAttackOne.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionPhysicalAttackOne',
    interfaces: [ActionSingleTarget]
  };
  function ActionSingleTarget(attacker, mTarget) {
    Action.call(this);
    this.mTarget = mTarget;
    this.mRaiseAni = null;
    this.mAttacker = attacker;
  }
  Object.defineProperty(ActionSingleTarget.prototype, 'isTargetAlive', {
    get: function () {
      return this.mTarget.isAlive;
    }
  });
  Object.defineProperty(ActionSingleTarget.prototype, 'isSingleTarget', {
    get: function () {
      return false;
    }
  });
  ActionSingleTarget.prototype.postExecute = function () {
    this.mTarget.isVisiable = this.mTarget.isAlive;
  };
  ActionSingleTarget.prototype.updateRaiseAnimation_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0;
    return (tmp$_0 = (tmp$ = this.mRaiseAni) != null ? tmp$.update_s8cxhz$(delta) : null) != null ? tmp$_0 : false;
  };
  ActionSingleTarget.prototype.drawRaiseAnimation_9in0vv$ = function (canvas) {
    var tmp$;
    (tmp$ = this.mRaiseAni) != null ? (tmp$.draw_9in0vv$(canvas), Unit) : null;
  };
  ActionSingleTarget.prototype.targetIsMonster = function () {
    return Kotlin.isType(this.mTarget, Monster);
  };
  ActionSingleTarget.prototype.setTarget_qpjxya$ = function (fc) {
    this.mTarget = fc;
  };
  ActionSingleTarget.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionSingleTarget',
    interfaces: [Action]
  };
  function ActionThrowItemAll(attacker, targets, hiddenWeapon) {
    ActionThrowItemAll$Companion_getInstance();
    ActionMultiTarget.call(this, attacker, targets);
    this.hiddenWeapon_8be2vx$ = hiddenWeapon;
    this.mState_0 = 1;
    this.mAni_0 = null;
    this.ox_0 = 0;
    this.oy_0 = 0;
  }
  ActionThrowItemAll.prototype.preproccess = function () {
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.mAni_0 = this.hiddenWeapon_8be2vx$.ani;
    ensureNotNull(this.mAni_0).startAni();
    ensureNotNull(this.mAni_0).setIteratorNum_za3lpa$(2);
    this.mRaiseAnis.add_11rb$(new RaiseAnimation(10, 20, 10, 0));
    this.mRaiseAnis.add_11rb$(new RaiseAnimation(30, 10, 10, 0));
  };
  ActionThrowItemAll.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6;
    ActionMultiTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionThrowItemAll$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull((Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0 + 2 | 0, this.oy_0 + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionThrowItemAll$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionThrowItemAll$Companion_getInstance().STATE_ANI_0) {
      if (!ensureNotNull(this.mAni_0).update_s8cxhz$(delta)) {
        this.mState_0 = ActionThrowItemAll$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_1 = this.mAttacker, Player) ? tmp$_1 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
        if (!this.targetIsMonster()) {
          tmp$_2 = this.mTargets.iterator();
          while (tmp$_2.hasNext()) {
            var fc = tmp$_2.next();
            ensureNotNull(fc.fightingSprite).currentFrame = 10;
          }
        }
         else {
          tmp$_3 = this.mTargets.iterator();
          while (tmp$_3.hasNext()) {
            var fc_0 = tmp$_3.next();
            ensureNotNull(fc_0.fightingSprite).move_vux9f0$(2, 2);
          }
        }
      }
    }
     else if (tmp$ === ActionThrowItemAll$Companion_getInstance().STATE_AFT_0)
      if (!this.updateRaiseAnimation_s8cxhz$(delta)) {
        if (this.targetIsMonster()) {
          tmp$_4 = this.mTargets.iterator();
          while (tmp$_4.hasNext()) {
            var fc_1 = tmp$_4.next();
            ensureNotNull(fc_1.fightingSprite).move_vux9f0$(-2, -2);
          }
        }
         else {
          tmp$_5 = this.mTargets.iterator();
          while (tmp$_5.hasNext()) {
            var fc_2 = tmp$_5.next();
            (Kotlin.isType(tmp$_6 = fc_2, Player) ? tmp$_6 : throwCCE()).setFrameByState();
          }
        }
        return false;
      }
    return true;
  };
  ActionThrowItemAll.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionThrowItemAll$Companion_getInstance().STATE_ANI_0) {
      ensureNotNull(this.mAni_0).draw_2g4tob$(canvas, 0, 0);
    }
     else if (this.mState_0 === ActionThrowItemAll$Companion_getInstance().STATE_AFT_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  function ActionThrowItemAll$Companion() {
    ActionThrowItemAll$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionThrowItemAll$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionThrowItemAll$Companion_instance = null;
  function ActionThrowItemAll$Companion_getInstance() {
    if (ActionThrowItemAll$Companion_instance === null) {
      new ActionThrowItemAll$Companion();
    }
    return ActionThrowItemAll$Companion_instance;
  }
  ActionThrowItemAll.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionThrowItemAll',
    interfaces: [ActionMultiTarget]
  };
  function ActionThrowItemOne(attacker, target, hiddenWeapon) {
    ActionThrowItemOne$Companion_getInstance();
    ActionSingleTarget.call(this, attacker, target);
    this.hiddenWeapon_8be2vx$ = hiddenWeapon;
    this.mState_0 = 1;
    this.mAni_0 = null;
    this.mAniX_0 = 0;
    this.mAniY_0 = 0;
    this.ox_0 = 0;
    this.oy_0 = 0;
  }
  ActionThrowItemOne.prototype.preproccess = function () {
    this.ox_0 = ensureNotNull(this.mAttacker).combatX;
    this.oy_0 = ensureNotNull(this.mAttacker).combatY;
    this.mAni_0 = this.hiddenWeapon_8be2vx$.ani;
    ensureNotNull(this.mAni_0).startAni();
    ensureNotNull(this.mAni_0).setIteratorNum_za3lpa$(2);
    this.mAniX_0 = this.mTarget.combatX;
    this.mAniY_0 = this.mTarget.combatY;
    this.mRaiseAni = new RaiseAnimation(this.mAniX_0, this.mTarget.combatTop, 10, 0);
  };
  ActionThrowItemOne.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    ActionSingleTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionThrowItemOne$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull((Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_0 + 2 | 0, this.oy_0 + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionThrowItemOne$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionThrowItemOne$Companion_getInstance().STATE_ANI_0) {
      if (!ensureNotNull(this.mAni_0).update_s8cxhz$(delta)) {
        this.mState_0 = ActionThrowItemOne$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_1 = this.mAttacker, Player) ? tmp$_1 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
        if (Kotlin.isType(this.mTarget, Player)) {
          ensureNotNull(this.mTarget.fightingSprite).currentFrame = 10;
        }
         else {
          ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(2, 2);
        }
      }
    }
     else if (tmp$ === ActionThrowItemOne$Companion_getInstance().STATE_AFT_0)
      if (!ensureNotNull(this.mRaiseAni).update_s8cxhz$(delta)) {
        if (Kotlin.isType(this.mTarget, Player)) {
          (Kotlin.isType(tmp$_2 = this.mTarget, Player) ? tmp$_2 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(this.mTarget.fightingSprite).move_vux9f0$(-2, -2);
        }
        return false;
      }
    return true;
  };
  ActionThrowItemOne.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionThrowItemOne$Companion_getInstance().STATE_ANI_0) {
      ensureNotNull(this.mAni_0).drawAbsolutely_2g4tob$(canvas, this.mAniX_0, this.mAniY_0);
    }
     else if (this.mState_0 === ActionThrowItemOne$Companion_getInstance().STATE_AFT_0) {
      ensureNotNull(this.mRaiseAni).draw_9in0vv$(canvas);
    }
  };
  function ActionThrowItemOne$Companion() {
    ActionThrowItemOne$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionThrowItemOne$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionThrowItemOne$Companion_instance = null;
  function ActionThrowItemOne$Companion_getInstance() {
    if (ActionThrowItemOne$Companion_instance === null) {
      new ActionThrowItemOne$Companion();
    }
    return ActionThrowItemOne$Companion_instance;
  }
  ActionThrowItemOne.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionThrowItemOne',
    interfaces: [ActionSingleTarget]
  };
  function ActionUseItemAll(attacker, targets, goods) {
    ActionUseItemAll$Companion_getInstance();
    ActionMultiTarget.call(this, attacker, targets);
    this.goods_8be2vx$ = goods;
    this.mState_0 = 1;
    this.mAni_8be2vx$ = null;
    this.ox_8be2vx$ = 0;
    this.oy_8be2vx$ = 0;
  }
  ActionUseItemAll.prototype.preproccess = function () {
    this.ox_8be2vx$ = ensureNotNull(this.mAttacker).combatX;
    this.oy_8be2vx$ = ensureNotNull(this.mAttacker).combatY;
    ensureNotNull(this.mAni_8be2vx$).startAni();
    ensureNotNull(this.mAni_8be2vx$).setIteratorNum_za3lpa$(2);
    ensureNotNull(this.mRaiseAnis).add_11rb$(new RaiseAnimation(10, 20, 10, 0));
    ensureNotNull(this.mRaiseAnis).add_11rb$(new RaiseAnimation(30, 10, 10, 0));
  };
  ActionUseItemAll.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1;
    ActionMultiTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionUseItemAll$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull((Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_8be2vx$ + 2 | 0, this.oy_8be2vx$ + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionUseItemAll$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionUseItemAll$Companion_getInstance().STATE_ANI_0) {
      if (!ensureNotNull(this.mAni_8be2vx$).update_s8cxhz$(delta)) {
        this.mState_0 = ActionUseItemAll$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_1 = this.mAttacker, Player) ? tmp$_1 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
      }
    }
     else if (tmp$ === ActionUseItemAll$Companion_getInstance().STATE_AFT_0)
      return this.updateRaiseAnimation_s8cxhz$(delta);
    return true;
  };
  ActionUseItemAll.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionUseItemAll$Companion_getInstance().STATE_ANI_0) {
      ensureNotNull(this.mAni_8be2vx$).draw_2g4tob$(canvas, 0, 0);
    }
     else if (this.mState_0 === ActionUseItemAll$Companion_getInstance().STATE_AFT_0) {
      this.drawRaiseAnimation_9in0vv$(canvas);
    }
  };
  function ActionUseItemAll$Companion() {
    ActionUseItemAll$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionUseItemAll$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionUseItemAll$Companion_instance = null;
  function ActionUseItemAll$Companion_getInstance() {
    if (ActionUseItemAll$Companion_instance === null) {
      new ActionUseItemAll$Companion();
    }
    return ActionUseItemAll$Companion_instance;
  }
  ActionUseItemAll.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionUseItemAll',
    interfaces: [ActionMultiTarget]
  };
  function ActionUseItemOne(attacker, target, goods) {
    ActionUseItemOne$Companion_getInstance();
    ActionSingleTarget.call(this, attacker, target);
    this.goods_8be2vx$ = goods;
    this.mState_0 = 1;
    this.mAni_8be2vx$ = new ResSrs();
    this.mAnix_8be2vx$ = 0;
    this.mAniy_8be2vx$ = 0;
    this.ox_8be2vx$ = 0;
    this.oy_8be2vx$ = 0;
  }
  ActionUseItemOne.prototype.preproccess = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var hp = 0;
    if (Kotlin.isType(this.goods_8be2vx$, GoodsMedicine)) {
      this.mAni_8be2vx$ = ensureNotNull((Kotlin.isType(tmp$ = this.goods_8be2vx$, GoodsMedicine) ? tmp$ : throwCCE()).ani);
      hp = this.mTarget.hp;
      (Kotlin.isType(tmp$_0 = this.goods_8be2vx$, GoodsMedicine) ? tmp$_0 : throwCCE()).eat_xa4yhy$(Kotlin.isType(tmp$_1 = this.mTarget, Player) ? tmp$_1 : throwCCE());
      hp = this.mTarget.hp - hp | 0;
    }
     else {
      this.mAni_8be2vx$ = Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 2, 1), ResSrs) ? tmp$_2 : throwCCE();
    }
    this.mAni_8be2vx$.startAni();
    this.mAni_8be2vx$.setIteratorNum_za3lpa$(2);
    this.mAnix_8be2vx$ = this.mTarget.combatX;
    this.mAniy_8be2vx$ = this.mTarget.combatY;
    this.mRaiseAni = new RaiseAnimation(this.mTarget.combatX, this.mTarget.combatTop, hp, 0);
  };
  ActionUseItemOne.prototype.update_s8cxhz$ = function (delta) {
    var tmp$, tmp$_0, tmp$_1;
    ActionSingleTarget.prototype.update_s8cxhz$.call(this, delta);
    tmp$ = this.mState_0;
    if (tmp$ === ActionUseItemOne$Companion_getInstance().STATE_PRE_0)
      if (this.mCurrentFrame < 10) {
        if (Kotlin.isType(this.mAttacker, Player)) {
          ensureNotNull((Kotlin.isType(tmp$_0 = this.mAttacker, Player) ? tmp$_0 : throwCCE()).fightingSprite).currentFrame = ((this.mCurrentFrame * 3 | 0) / 10 | 0) + 6 | 0;
        }
         else {
          ensureNotNull(this.mAttacker).setCombatPos_vux9f0$(this.ox_8be2vx$ + 2 | 0, this.oy_8be2vx$ + 2 | 0);
        }
      }
       else {
        this.mState_0 = ActionUseItemOne$Companion_getInstance().STATE_ANI_0;
      }
     else if (tmp$ === ActionUseItemOne$Companion_getInstance().STATE_ANI_0) {
      if (!this.mAni_8be2vx$.update_s8cxhz$(delta)) {
        this.mState_0 = ActionUseItemOne$Companion_getInstance().STATE_AFT_0;
        if (Kotlin.isType(this.mAttacker, Player)) {
          (Kotlin.isType(tmp$_1 = this.mAttacker, Player) ? tmp$_1 : throwCCE()).setFrameByState();
        }
         else {
          ensureNotNull(ensureNotNull(this.mAttacker).fightingSprite).move_vux9f0$(-2, -2);
        }
      }
    }
     else if (tmp$ === ActionUseItemOne$Companion_getInstance().STATE_AFT_0)
      return ensureNotNull(this.mRaiseAni).update_s8cxhz$(delta);
    return true;
  };
  ActionUseItemOne.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mState_0 === ActionUseItemOne$Companion_getInstance().STATE_ANI_0) {
      this.mAni_8be2vx$.drawAbsolutely_2g4tob$(canvas, this.mAnix_8be2vx$, this.mAniy_8be2vx$);
    }
     else if (this.mState_0 === ActionUseItemOne$Companion_getInstance().STATE_AFT_0) {
      ensureNotNull(this.mRaiseAni).draw_9in0vv$(canvas);
    }
  };
  function ActionUseItemOne$Companion() {
    ActionUseItemOne$Companion_instance = this;
    this.STATE_PRE_0 = 1;
    this.STATE_ANI_0 = 2;
    this.STATE_AFT_0 = 3;
  }
  ActionUseItemOne$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ActionUseItemOne$Companion_instance = null;
  function ActionUseItemOne$Companion_getInstance() {
    if (ActionUseItemOne$Companion_instance === null) {
      new ActionUseItemOne$Companion();
    }
    return ActionUseItemOne$Companion_instance;
  }
  ActionUseItemOne.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ActionUseItemOne',
    interfaces: [ActionSingleTarget]
  };
  function CalcDamage() {
    CalcDamage_instance = this;
  }
  CalcDamage.prototype.calcBaseDamage_vux9f0$ = function (attack, defense) {
    var damage;
    if (attack > defense) {
      damage = numberToInt((attack * 2 | 0) - defense * 1.6 + 0.5);
    }
     else if (attack > defense * 0.6) {
      damage = numberToInt(attack - defense * 0.6 + 0.5);
    }
     else {
      damage = 0;
    }
    return damage;
  };
  CalcDamage.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'CalcDamage',
    interfaces: []
  };
  var CalcDamage_instance = null;
  function CalcDamage_getInstance() {
    if (CalcDamage_instance === null) {
      new CalcDamage();
    }
    return CalcDamage_instance;
  }
  function FrameAnimation(mImage, mStartFrame, mEndFrame) {
    if (mStartFrame === void 0)
      mStartFrame = 1;
    if (mEndFrame === void 0)
      mEndFrame = mImage.number;
    this.mImage_0 = mImage;
    this.mStartFrame_0 = mStartFrame;
    this.mEndFrame_0 = mEndFrame;
    this.DELTA_0 = 1000 / 5 | 0;
    this.mCurFrame_0 = 0;
    this.mTimeCnt_0 = Kotlin.Long.ZERO;
    this.mCurFrame_0 = this.mStartFrame_0;
  }
  FrameAnimation.prototype.setFPS_za3lpa$ = function (fps) {
    this.DELTA_0 = 1000 / fps | 0;
  };
  FrameAnimation.prototype.update_s8cxhz$ = function (delta) {
    this.mTimeCnt_0 = this.mTimeCnt_0.add(delta);
    if (this.mTimeCnt_0.compareTo_11rb$(Kotlin.Long.fromInt(this.DELTA_0)) >= 0) {
      this.mTimeCnt_0 = Kotlin.Long.ZERO;
      if ((this.mCurFrame_0 = this.mCurFrame_0 + 1 | 0, this.mCurFrame_0) > this.mEndFrame_0) {
        this.mCurFrame_0 = this.mStartFrame_0;
      }
    }
  };
  FrameAnimation.prototype.draw_2g4tob$ = function (canvas, x, y) {
    this.mImage_0.draw_tj1hu5$(canvas, this.mCurFrame_0, x, y);
  };
  FrameAnimation.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'FrameAnimation',
    interfaces: []
  };
  function RaiseAnimation(x, y, hitpoint, buff) {
    this.x_0 = x;
    this.y_0 = y;
    this.dy_0 = 0;
    this.dt_0 = 0;
    this.raiseNum_0 = Util_getInstance().getSmallSignedNumBitmap_za3lpa$(hitpoint);
    this.srsList_0 = null;
    this.bShowNum_0 = false;
    this.cnt_0 = Kotlin.Long.ZERO;
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5;
    this.bShowNum_0 = hitpoint !== 0;
    this.srsList_0 = ArrayList_init();
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_DU) === FightingCharacter$Companion_getInstance().BUFF_MASK_DU) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 243), ResSrs) ? tmp$ : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_LUAN) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 244), ResSrs) ? tmp$_0 : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FENG) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 245), ResSrs) ? tmp$_1 : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) === FightingCharacter$Companion_getInstance().BUFF_MASK_MIAN) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 246), ResSrs) ? tmp$_2 : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_GONG) === FightingCharacter$Companion_getInstance().BUFF_MASK_GONG) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_3 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 240), ResSrs) ? tmp$_3 : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_FANG) === FightingCharacter$Companion_getInstance().BUFF_MASK_FANG) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_4 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 241), ResSrs) ? tmp$_4 : throwCCE());
      last(this.srsList_0).startAni();
    }
    if ((buff & FightingCharacter$Companion_getInstance().BUFF_MASK_SU) === FightingCharacter$Companion_getInstance().BUFF_MASK_SU) {
      this.srsList_0.add_11rb$(Kotlin.isType(tmp$_5 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 242), ResSrs) ? tmp$_5 : throwCCE());
      last(this.srsList_0).startAni();
    }
  }
  RaiseAnimation.prototype.update_s8cxhz$ = function (delta) {
    if (this.bShowNum_0) {
      this.cnt_0 = this.cnt_0.add(delta);
      if (this.cnt_0.compareTo_11rb$(Kotlin.Long.fromInt(50)) > 0) {
        this.cnt_0 = Kotlin.Long.ZERO;
        this.dt_0 = this.dt_0 + 1 | 0;
        this.dy_0 = this.dy_0 - this.dt_0 | 0;
        if (this.dt_0 > 4) {
          this.bShowNum_0 = false;
        }
      }
    }
     else {
      if (this.srsList_0.isEmpty()) {
        return false;
      }
       else {
        if (!first(this.srsList_0).update_s8cxhz$(delta)) {
          this.srsList_0.removeAt_za3lpa$(0);
          return !this.srsList_0.isEmpty();
        }
      }
    }
    return true;
  };
  RaiseAnimation.prototype.draw_9in0vv$ = function (canvas) {
    if (this.bShowNum_0) {
      canvas.drawBitmap_t8cslu$(this.raiseNum_0, this.x_0, this.y_0 + this.dy_0 | 0);
    }
     else {
      if (this.srsList_0.size > 0) {
        first(this.srsList_0).drawAbsolutely_2g4tob$(canvas, this.x_0, this.y_0);
      }
    }
  };
  RaiseAnimation.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RaiseAnimation',
    interfaces: []
  };
  function CombatSuccess(exp, money, mGoodsList, lvuplist) {
    this.mGoodsList_0 = mGoodsList;
    this.mMsgList_0 = ArrayList_init();
    this.mLvupList_0 = null;
    this.mCnt_0 = Kotlin.Long.ZERO;
    this.mIsAnyKeyPressed_0 = false;
    var tmp$;
    var e = exp.toString();
    var tmp$_0 = this.mMsgList_0;
    var $receiver = '\u83B7\u5F97\u7ECF\u9A8C     ';
    var endIndex = 9 - e.length | 0;
    tmp$_0.add_11rb$(new CombatSuccess$MsgScreen(this, 18, $receiver.substring(0, endIndex) + e));
    var m = money.toString();
    var tmp$_1 = this.mMsgList_0;
    var $receiver_0 = '\u6218\u6597\u83B7\u5F97        ';
    var endIndex_0 = 10 - m.length | 0;
    tmp$_1.add_11rb$(new CombatSuccess$MsgScreen(this, 46, $receiver_0.substring(0, endIndex_0) + m + '\u94B1'));
    this.mLvupList_0 = ArrayList_init();
    tmp$ = lvuplist.iterator();
    while (tmp$.hasNext()) {
      var p = tmp$.next();
      this.mLvupList_0.add_11rb$(CombatSuccess$CombatSuccess$MsgScreen_init(this, p.name + '\u4FEE\u884C\u63D0\u5347'));
      this.mLvupList_0.add_11rb$(new CombatSuccess$LevelupScreen(this, p));
      if (p.levelupChain.getLearnMagicNum_za3lpa$(p.level) > p.levelupChain.getLearnMagicNum_za3lpa$(p.level - 1 | 0)) {
        this.mLvupList_0.add_11rb$(new CombatSuccess$LearnMagicScreen(this, p.name, p.magicChain.getMagic_za3lpa$(p.levelupChain.getLearnMagicNum_za3lpa$(p.level) - 1 | 0).magicName));
      }
    }
  }
  CombatSuccess.prototype.update_s8cxhz$ = function (delta) {
    this.mCnt_0 = this.mCnt_0.add(delta);
    if (this.mCnt_0.compareTo_11rb$(Kotlin.Long.fromInt(1000)) > 0 || this.mIsAnyKeyPressed_0) {
      this.mCnt_0 = Kotlin.Long.ZERO;
      this.mIsAnyKeyPressed_0 = false;
      if (this.mGoodsList_0.size === 0) {
        if (this.mLvupList_0.size === 0) {
          return true;
        }
         else {
          this.mMsgList_0.add_11rb$(this.mLvupList_0.removeAt_za3lpa$(0));
        }
      }
       else {
        var g = this.mGoodsList_0.removeAt_za3lpa$(0);
        this.mMsgList_0.add_11rb$(CombatSuccess$CombatSuccess$MsgScreen_init(this, '\u5F97\u5230 ' + g.name + ' x' + g.goodsNum));
      }
    }
    return false;
  };
  CombatSuccess.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$;
    tmp$ = this.mMsgList_0.iterator();
    while (tmp$.hasNext()) {
      var s = tmp$.next();
      s.draw_9in0vv$(canvas);
    }
  };
  CombatSuccess.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  CombatSuccess.prototype.onKeyUp_za3lpa$ = function (key) {
    this.mIsAnyKeyPressed_0 = true;
  };
  function CombatSuccess$MsgScreen($outer, mY, _msg) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.mY_0 = mY;
    this.mMsg_0 = null;
    this.mX_0 = 0;
    var tmp$;
    var msg = gbkBytes(_msg);
    var side = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 8), ResImage) ? tmp$ : throwCCE();
    this.mMsg_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$((msg.length * 8 | 0) + 8 | 0, 24);
    var c = new Canvas(this.mMsg_0);
    c.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    side.draw_tj1hu5$(c, 1, 0, 0);
    side.draw_tj1hu5$(c, 2, this.mMsg_0.width - 3 | 0, 0);
    var p = new Paint();
    p.color = Global_getInstance().COLOR_BLACK;
    p.style = Paint$Style$FILL_AND_STROKE_getInstance();
    c.drawLine_x3aj6j$(0, 1, this.mMsg_0.width, 1, p);
    c.drawLine_x3aj6j$(0, 22, this.mMsg_0.width, 22, p);
    TextRender_getInstance().drawText_pbrmiz$(c, msg, 4, 4);
    this.mX_0 = (160 - this.mMsg_0.width | 0) / 2 | 0;
  }
  CombatSuccess$MsgScreen.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatSuccess$MsgScreen.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mMsg_0, this.mX_0, this.mY_0);
  };
  CombatSuccess$MsgScreen.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  CombatSuccess$MsgScreen.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  CombatSuccess$MsgScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MsgScreen',
    interfaces: [BaseScreen]
  };
  function CombatSuccess$CombatSuccess$MsgScreen_init($outer, msg, $this) {
    $this = $this || Object.create(CombatSuccess$MsgScreen.prototype);
    CombatSuccess$MsgScreen.call($this, $outer, (96 - 24 | 0) / 2 | 0, msg);
    return $this;
  }
  function CombatSuccess$LevelupScreen($outer, p) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.mInfo_0 = null;
    var tmp$;
    var ri = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 9), ResImage) ? tmp$ : throwCCE();
    this.mInfo_0 = ensureNotNull(ri.getBitmap_za3lpa$(0));
    var canvas = new Canvas(this.mInfo_0);
    var lc = p.levelupChain;
    var curl = p.level;
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.hp, 37, 9);
    p.hp = p.maxHP;
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxHP - (lc.getMaxHP_za3lpa$(curl) - lc.getMaxHP_za3lpa$(curl - 1 | 0)) | 0, 56, 9);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxHP, 86, 9);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxHP, 105, 9);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.mp, 37, 21);
    p.mp = p.maxMP;
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxMP - (lc.getMaxMP_za3lpa$(curl) - lc.getMaxMP_za3lpa$(curl - 1 | 0)) | 0, 56, 21);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxMP, 86, 21);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxMP, 105, 21);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.attack - (lc.getAttack_za3lpa$(curl) - lc.getAttack_za3lpa$(curl - 1 | 0)) | 0, 47, 33);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.attack, 96, 33);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.defend - (lc.getDefend_za3lpa$(curl) - lc.getDefend_za3lpa$(curl - 1 | 0)) | 0, 47, 45);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.defend, 96, 45);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.speed - (lc.getSpeed_za3lpa$(curl) - lc.getSpeed_za3lpa$(curl - 1 | 0)) | 0, 47, 57);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.speed, 96, 57);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.lingli - (lc.getLingli_za3lpa$(curl) - lc.getLingli_za3lpa$(curl - 1 | 0)) | 0, 47, 69);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.lingli, 96, 69);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.luck - (lc.getLuck_za3lpa$(curl) - lc.getLuck_za3lpa$(curl - 1 | 0)) | 0, 47, 81);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.luck, 96, 81);
  }
  CombatSuccess$LevelupScreen.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatSuccess$LevelupScreen.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mInfo_0, (160 - this.mInfo_0.width | 0) / 2 | 0, (96 - this.mInfo_0.height | 0) / 2 | 0);
  };
  CombatSuccess$LevelupScreen.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  CombatSuccess$LevelupScreen.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  CombatSuccess$LevelupScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'LevelupScreen',
    interfaces: [BaseScreen]
  };
  function CombatSuccess$LearnMagicScreen($outer, playerName, magicName) {
    this.$outer = $outer;
    BaseScreen.call(this);
    var tmp$;
    this.mInfo_0 = ensureNotNull((Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 10), ResImage) ? tmp$ : throwCCE()).getBitmap_za3lpa$(0));
    var pn;
    var mn;
    try {
      pn = gbkBytes(playerName);
      mn = gbkBytes(magicName);
    }
     catch (e) {
      if (Kotlin.isType(e, Error_0)) {
        pn = new Int8Array(0);
        mn = new Int8Array(0);
      }
       else
        throw e;
    }
    var canvas = new Canvas(this.mInfo_0);
    TextRender_getInstance().drawText_pbrmiz$(canvas, pn, (this.mInfo_0.width - (pn.length * 8 | 0) | 0) / 2 | 0, 8);
    TextRender_getInstance().drawText_pbrmiz$(canvas, mn, (this.mInfo_0.width - (mn.length * 8 | 0) | 0) / 2 | 0, 42);
  }
  CombatSuccess$LearnMagicScreen.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatSuccess$LearnMagicScreen.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mInfo_0, (160 - this.mInfo_0.width | 0) / 2 | 0, (96 - this.mInfo_0.height | 0) / 2 | 0);
  };
  CombatSuccess$LearnMagicScreen.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  CombatSuccess$LearnMagicScreen.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  CombatSuccess$LearnMagicScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'LearnMagicScreen',
    interfaces: [BaseScreen]
  };
  CombatSuccess.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CombatSuccess',
    interfaces: []
  };
  function CombatUI(mCallBack, mCurPlayerIndex) {
    CombatUI$Companion_getInstance();
    BaseScreen.call(this);
    this.mCallBack_0 = mCallBack;
    this.mCurPlayerIndex_0 = mCurPlayerIndex;
    this.mScreenStack_0 = new ScreenStack();
    this.mPlayerList_0 = emptyList();
    this.mMonsterList_0 = emptyList();
    this.mPlayerIndicator_0 = null;
    this.mTargetIndicator_0 = null;
    this.mMonsterIndicator_0 = null;
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    this.mHeadsImg_0 = [Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, 1), ResImage) ? tmp$ : throwCCE(), Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, 2), ResImage) ? tmp$_0 : throwCCE(), Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, 3), ResImage) ? tmp$_1 : throwCCE()];
    this.mScreenStack_0.pushScreen_2o7n0o$(new CombatUI$MainMenu(this));
    var tmpImg = Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 4), ResImage) ? tmp$_2 : throwCCE();
    this.mPlayerIndicator_0 = new FrameAnimation(tmpImg, 1, 2);
    this.mTargetIndicator_0 = new FrameAnimation(tmpImg, 3, 4);
    tmpImg = Kotlin.isType(tmp$_3 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 3), ResImage) ? tmp$_3 : throwCCE();
    this.mMonsterIndicator_0 = new FrameAnimation(tmpImg);
  }
  function CombatUI$CallBack() {
  }
  CombatUI$CallBack.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'CallBack',
    interfaces: []
  };
  CombatUI.prototype.update_s8cxhz$ = function (delta) {
    this.mScreenStack_0.update_s8cxhz$(delta);
  };
  CombatUI.prototype.draw_9in0vv$ = function (canvas) {
    this.mScreenStack_0.draw_9in0vv$(canvas);
  };
  CombatUI.prototype.onKeyDown_za3lpa$ = function (key) {
    this.mScreenStack_0.keyDown_za3lpa$(key);
  };
  CombatUI.prototype.onKeyUp_za3lpa$ = function (key) {
    this.mScreenStack_0.keyUp_za3lpa$(key);
  };
  CombatUI.prototype.reset = function () {
    this.mScreenStack_0.clear();
    this.mScreenStack_0.pushScreen_2o7n0o$(new CombatUI$MainMenu(this));
  };
  CombatUI.prototype.setPlayerList_51hka1$ = function (list) {
    this.mPlayerList_0 = list;
  };
  CombatUI.prototype.setMonsterList_kg2ssq$ = function (list) {
    this.mMonsterList_0 = list;
  };
  CombatUI.prototype.setCurrentPlayerIndex_za3lpa$ = function (i) {
    this.mCurPlayerIndex_0 = i;
  };
  CombatUI.prototype.onActionSelected_0 = function (action) {
    var tmp$;
    (tmp$ = this.mCallBack_0) != null ? (tmp$.onActionSelected_wwcj9m$(action), Unit) : null;
  };
  CombatUI.prototype.onCancel_0 = function () {
    var tmp$;
    (tmp$ = this.mCallBack_0) != null ? (tmp$.onCancel(), Unit) : null;
  };
  CombatUI.prototype.getGBKBytes_61zpoe$ = function (s) {
    return gbkBytes(s);
  };
  function CombatUI$MainMenu($outer) {
    this.$outer = $outer;
    BaseScreen.call(this);
    var tmp$, tmp$_0;
    this.mMenuIcon_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 1), ResImage) ? tmp$ : throwCCE();
    this.mPlayerInfoBg_0 = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 2), ResImage) ? tmp$_0 : throwCCE();
    this.mCurIconIndex_0 = 1;
  }
  CombatUI$MainMenu.prototype.update_s8cxhz$ = function (delta) {
    this.$outer.mPlayerIndicator_0.update_s8cxhz$(delta);
  };
  CombatUI$MainMenu.prototype.draw_9in0vv$ = function (canvas) {
    this.mMenuIcon_0.draw_tj1hu5$(canvas, this.mCurIconIndex_0, 7, 96 - this.mMenuIcon_0.height | 0);
    this.mPlayerInfoBg_0.draw_tj1hu5$(canvas, 1, 49, 66);
    var p = this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0);
    this.$outer.mHeadsImg_0[p.index - 1 | 0].draw_tj1hu5$(canvas, 1, 50, 63);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.hp, 79, 72);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxHP, 104, 72);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.mp, 79, 83);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxMP, 104, 83);
    this.$outer.mPlayerIndicator_0.draw_2g4tob$(canvas, CombatUI$Companion_getInstance().sPlayerIndicatorPos_0[this.$outer.mCurPlayerIndex_0].x, CombatUI$Companion_getInstance().sPlayerIndicatorPos_0[this.$outer.mCurPlayerIndex_0].y);
  };
  CombatUI$MainMenu.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_LEFT) {
      if (this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0).hasDebuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_FENG)) {
        return;
      }
      this.mCurIconIndex_0 = 2;
    }
     else if (key === Global_getInstance().KEY_DOWN)
      this.mCurIconIndex_0 = 3;
    else if (key === Global_getInstance().KEY_RIGHT) {
      if (this.$outer.mPlayerList_0.size <= 1) {
        return;
      }
      this.mCurIconIndex_0 = 4;
    }
     else if (key === Global_getInstance().KEY_UP)
      this.mCurIconIndex_0 = 1;
  };
  function CombatUI$MainMenu$onKeyUp$ObjectLiteral(this$CombatUI) {
    this.this$CombatUI = this$CombatUI;
  }
  CombatUI$MainMenu$onKeyUp$ObjectLiteral.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    this.this$CombatUI.onActionSelected_0(new ActionPhysicalAttackOne(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), fc));
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  function CombatUI$MainMenu$onKeyUp$ObjectLiteral_0(this$MainMenu, this$CombatUI) {
    this.this$MainMenu = this$MainMenu;
    this.this$CombatUI = this$CombatUI;
  }
  function CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral(this$CombatUI, closure$magic) {
    this.this$CombatUI = this$CombatUI;
    this.closure$magic = closure$magic;
  }
  CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    this.this$CombatUI.onActionSelected_0(new ActionMagicAttackOne(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), fc, this.closure$magic));
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  function CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0(this$CombatUI, closure$magic) {
    this.this$CombatUI = this$CombatUI;
    this.closure$magic = closure$magic;
  }
  CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    this.this$CombatUI.onActionSelected_0(new ActionMagicHelpOne(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), fc, this.closure$magic));
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral_0.prototype.onItemSelected_3fncnk$ = function (magic) {
    var tmp$, tmp$_0;
    this.this$MainMenu.delegate.popScreen();
    if (Kotlin.isType(magic, MagicAttack) || Kotlin.isType(magic, MagicSpecial)) {
      if (magic.isForAll) {
        tmp$_0 = new ActionMagicAttackAll(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), this.this$CombatUI.mMonsterList_0, Kotlin.isType(tmp$ = magic, MagicAttack) ? tmp$ : throwCCE());
        this.this$CombatUI.onActionSelected_0(tmp$_0);
      }
       else {
        this.this$MainMenu.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.this$CombatUI, this.this$CombatUI.mMonsterIndicator_0, CombatUI$Companion_getInstance().sMonsterIndicatorPos_0, this.this$CombatUI.mMonsterList_0, new CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral(this.this$CombatUI, magic), true));
      }
    }
     else {
      if (magic.isForAll) {
        this.this$CombatUI.onActionSelected_0(new ActionMagicHelpAll(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), this.this$CombatUI.mPlayerList_0, magic));
      }
       else {
        this.this$MainMenu.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.this$CombatUI, this.this$CombatUI.mTargetIndicator_0, CombatUI$Companion_getInstance().sPlayerIndicatorPos_0, this.this$CombatUI.mPlayerList_0, new CombatUI$MainMenu$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0(this.this$CombatUI, magic), false));
      }
    }
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral_0.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenMagic$OnItemSelectedListener]
  };
  function CombatUI$MainMenu$onKeyUp$ObjectLiteral_1(this$CombatUI) {
    this.this$CombatUI = this$CombatUI;
  }
  CombatUI$MainMenu$onKeyUp$ObjectLiteral_1.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    this.this$CombatUI.onActionSelected_0(ActionCoopMagic_init(this.this$CombatUI.mPlayerList_0, fc));
  };
  CombatUI$MainMenu$onKeyUp$ObjectLiteral_1.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  CombatUI$MainMenu.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_ENTER) {
      tmp$ = this.mCurIconIndex_0;
      if (tmp$ === 1) {
        if (this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0).hasAtbuff_za3lpa$(FightingCharacter$Companion_getInstance().BUFF_MASK_ALL)) {
          this.$outer.onActionSelected_0(new ActionPhysicalAttackAll(this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0), this.$outer.mMonsterList_0));
          return;
        }
        this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.$outer, this.$outer.mMonsterIndicator_0, CombatUI$Companion_getInstance().sMonsterIndicatorPos_0, this.$outer.mMonsterList_0, new CombatUI$MainMenu$onKeyUp$ObjectLiteral(this.$outer), true));
      }
       else if (tmp$ === 2)
        this.delegate.pushScreen_2o7n0o$(new ScreenMagic(this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0).magicChain, new CombatUI$MainMenu$onKeyUp$ObjectLiteral_0(this, this.$outer)));
      else if (tmp$ === 3)
        this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuMisc(this.$outer));
      else if (tmp$ === 4)
        this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.$outer, this.$outer.mMonsterIndicator_0, CombatUI$Companion_getInstance().sMonsterIndicatorPos_0, this.$outer.mMonsterList_0, new CombatUI$MainMenu$onKeyUp$ObjectLiteral_1(this.$outer), true));
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.$outer.onCancel_0();
    }
  };
  CombatUI$MainMenu.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MainMenu',
    interfaces: [BaseScreen]
  };
  function CombatUI$OnCharacterSelectedListener() {
  }
  CombatUI$OnCharacterSelectedListener.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'OnCharacterSelectedListener',
    interfaces: []
  };
  function CombatUI$MenuCharacterSelect($outer, mIndicator, mIndicatorPos, mList, mOnCharacterSelectedListener, mIgnoreDead) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.mIndicator_0 = mIndicator;
    this.mIndicatorPos_0 = mIndicatorPos;
    this.mList_0 = mList;
    this.mOnCharacterSelectedListener_0 = mOnCharacterSelectedListener;
    this.mIgnoreDead_0 = mIgnoreDead;
    this.mCurSel_0 = 0;
    var tmp$;
    tmp$ = this.mList_0.size;
    for (var i = 0; i < tmp$; i++) {
      if (this.mList_0.get_za3lpa$(i).isAlive) {
        this.mCurSel_0 = i;
        break;
      }
    }
  }
  CombatUI$MenuCharacterSelect.prototype.update_s8cxhz$ = function (delta) {
    this.mIndicator_0.update_s8cxhz$(delta);
  };
  CombatUI$MenuCharacterSelect.prototype.draw_9in0vv$ = function (canvas) {
    this.mIndicator_0.draw_2g4tob$(canvas, this.mIndicatorPos_0[this.mCurSel_0].x, this.mIndicatorPos_0[this.mCurSel_0].y);
    if (equals(this.mIndicator_0, this.$outer.mTargetIndicator_0)) {
      var p = this.$outer.mPlayerList_0.get_za3lpa$(this.mCurSel_0);
      this.$outer.mHeadsImg_0[p.index - 1 | 0].draw_tj1hu5$(canvas, 1, 50, 63);
      Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.hp, 79, 72);
      Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxHP, 104, 72);
      Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.mp, 79, 83);
      Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.maxMP, 104, 83);
    }
  };
  CombatUI$MenuCharacterSelect.prototype.selectNextTarget_0 = function () {
    do {
      this.mCurSel_0 = this.mCurSel_0 + 1 | 0;
      this.mCurSel_0 = this.mCurSel_0 % this.mList_0.size;
    }
     while (this.mIgnoreDead_0 && !this.mList_0.get_za3lpa$(this.mCurSel_0).isAlive);
  };
  CombatUI$MenuCharacterSelect.prototype.selectPreTarget_0 = function () {
    do {
      this.mCurSel_0 = this.mCurSel_0 - 1 | 0;
      this.mCurSel_0 = (this.mCurSel_0 + this.mList_0.size | 0) % this.mList_0.size;
    }
     while (this.mIgnoreDead_0 && !this.mList_0.get_za3lpa$(this.mCurSel_0).isAlive);
  };
  CombatUI$MenuCharacterSelect.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_RIGHT) {
      this.selectNextTarget_0();
    }
     else if (key === Global_getInstance().KEY_LEFT) {
      this.selectPreTarget_0();
    }
  };
  CombatUI$MenuCharacterSelect.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      (tmp$ = this.mOnCharacterSelectedListener_0) != null ? (tmp$.onCharacterSelected_qpjxya$(this.mList_0.get_za3lpa$(this.mCurSel_0)), Unit) : null;
    }
  };
  CombatUI$MenuCharacterSelect.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MenuCharacterSelect',
    interfaces: [BaseScreen]
  };
  function CombatUI$MenuMisc($outer) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.mBg_0 = Util_getInstance().getFrameBitmap_vux9f0$((2 * 16 | 0) + 6 | 0, (5 * 16 | 0) + 6 | 0);
    this.mText_0 = this.$outer.getGBKBytes_61zpoe$('\u56F4\u653B\u9053\u5177\u9632\u5FA1\u9003\u8DD1\u72B6\u6001');
    this.mItemText_0 = [this.$outer.getGBKBytes_61zpoe$('\u56F4\u653B'), this.$outer.getGBKBytes_61zpoe$('\u9053\u5177'), this.$outer.getGBKBytes_61zpoe$('\u9632\u5FA1'), this.$outer.getGBKBytes_61zpoe$('\u9003\u8DD1'), this.$outer.getGBKBytes_61zpoe$('\u72B6\u6001')];
    this.mTextRect_0 = new Rect(9 + 3 | 0, 4 + 3 | 0, 9 + 4 + (16 * 2 | 0) | 0, 4 + 3 + (16 * 5 | 0) | 0);
    this.mCurSelIndex_0 = 0;
  }
  CombatUI$MenuMisc.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatUI$MenuMisc.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mBg_0, 9, 4);
    TextRender_getInstance().drawText_tz7kd0$(canvas, this.mText_0, 0, this.mTextRect_0);
    TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.mItemText_0[this.mCurSelIndex_0], this.mTextRect_0.left, this.mTextRect_0.top + (this.mCurSelIndex_0 * 16 | 0) | 0);
  };
  CombatUI$MenuMisc.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP) {
      this.mCurSelIndex_0 = this.mCurSelIndex_0 - 1 | 0;
      this.mCurSelIndex_0 = (this.mItemText_0.length + this.mCurSelIndex_0 | 0) % this.mItemText_0.length;
    }
     else if (key === Global_getInstance().KEY_DOWN) {
      this.mCurSelIndex_0 = this.mCurSelIndex_0 + 1 | 0;
      this.mCurSelIndex_0 = this.mCurSelIndex_0 % this.mItemText_0.length;
    }
  };
  CombatUI$MenuMisc.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$, tmp$_0, tmp$_1;
    if (key === Global_getInstance().KEY_ENTER) {
      tmp$ = this.mCurSelIndex_0;
      if (tmp$ === 0)
        (tmp$_0 = this.$outer.mCallBack_0) != null ? (tmp$_0.onAutoAttack(), Unit) : null;
      else if (tmp$ === 1)
        this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuGoods(this.$outer));
      else if (tmp$ === 2) {
        var p = this.$outer.mPlayerList_0.get_za3lpa$(this.$outer.mCurPlayerIndex_0);
        ensureNotNull(p.fightingSprite).currentFrame = 9;
        this.$outer.onActionSelected_0(new ActionDefend(p));
      }
       else if (tmp$ === 3)
        (tmp$_1 = this.$outer.mCallBack_0) != null ? (tmp$_1.onFlee(), Unit) : null;
      else if (tmp$ === 4) {
        this.delegate.popScreen();
        this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuMisc$MenuState(this));
      }
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  function CombatUI$MenuMisc$MenuState($outer) {
    this.$outer = $outer;
    BaseScreen.call(this);
    var tmp$, tmp$_0;
    this.mBg_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 11), ResImage) ? tmp$ : throwCCE();
    this.mMarker_0 = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 12), ResImage) ? tmp$_0 : throwCCE();
    this.mCurPlayer_0 = 0;
    this.mCurPlayer_0 = this.$outer.$outer.mCurPlayerIndex_0;
  }
  CombatUI$MenuMisc$MenuState.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatUI$MenuMisc$MenuState.prototype.draw_9in0vv$ = function (canvas) {
    var x = (160 - this.mBg_0.width | 0) / 2 | 0;
    var y = (96 - this.mBg_0.height | 0) / 2 | 0;
    this.mBg_0.draw_tj1hu5$(canvas, 1, x, y);
    var p = this.$outer.$outer.mPlayerList_0.get_za3lpa$(this.mCurPlayer_0);
    p.drawHead_2g4tob$(canvas, x + 7 | 0, y + 4 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.hp, x + 50 | 0, y + 9 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.attack, x + 50 | 0, y + 21 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.luck, x + 87 | 0, y + 9 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, p.speed, x + 87 | 0, y + 21 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 1, x + 9 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 2, x + 25 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 5, x + 41 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 3, x + 57 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 4, x + 73 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 3, x + 88 | 0, y + 48 | 0);
    this.mMarker_0.draw_tj1hu5$(canvas, 4, x + 104 | 0, y + 48 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 10 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 26 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 42 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 58 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 74 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 90 | 0, y + 57 | 0);
    Util_getInstance().drawSmallNum_tj1hu5$(canvas, 5, x + 106 | 0, y + 57 | 0);
  };
  CombatUI$MenuMisc$MenuState.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_RIGHT || key === Global_getInstance().KEY_DOWN || key === Global_getInstance().KEY_PAGEDOWN || key === Global_getInstance().KEY_ENTER) {
      this.mCurPlayer_0 = this.mCurPlayer_0 + 1 | 0;
      this.mCurPlayer_0 = this.mCurPlayer_0 % this.$outer.$outer.mPlayerList_0.size;
    }
     else if (key === Global_getInstance().KEY_LEFT || key === Global_getInstance().KEY_UP || key === Global_getInstance().KEY_PAGEUP) {
      this.mCurPlayer_0 = this.mCurPlayer_0 - 1 | 0;
      this.mCurPlayer_0 = (this.mCurPlayer_0 + this.$outer.$outer.mPlayerList_0.size | 0) % this.$outer.$outer.mPlayerList_0.size;
    }
  };
  CombatUI$MenuMisc$MenuState.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
      this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuMisc(this.$outer.$outer));
    }
  };
  CombatUI$MenuMisc$MenuState.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MenuState',
    interfaces: [BaseScreen]
  };
  CombatUI$MenuMisc.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MenuMisc',
    interfaces: [BaseScreen]
  };
  function CombatUI$MenuGoods($outer) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.mBg_0 = Util_getInstance().getFrameBitmap_vux9f0$((16 * 2 | 0) + 6 | 0, (16 * 3 | 0) + 6 | 0);
    this.mText_0 = this.$outer.getGBKBytes_61zpoe$('\u88C5\u5907\u6295\u63B7\u4F7F\u7528');
    this.mItemText_0 = [this.$outer.getGBKBytes_61zpoe$('\u88C5\u5907'), this.$outer.getGBKBytes_61zpoe$('\u6295\u63B7'), this.$outer.getGBKBytes_61zpoe$('\u4F7F\u7528')];
    this.mTextRect_0 = new Rect(29 + 3 | 0, 14 + 3 | 0, 29 + 3 + this.mBg_0.width | 0, 14 + 3 + this.mBg_0.height | 0);
    this.mSelIndex_0 = 0;
  }
  Object.defineProperty(CombatUI$MenuGoods.prototype, 'useableGoodsList_0', {
    get: function () {
      var tmp$, tmp$_0;
      var rlt = ArrayList_init();
      tmp$ = Player$Companion_getInstance().sGoodsList.goodsList.iterator();
      while (tmp$.hasNext()) {
        var g = tmp$.next();
        tmp$_0 = g.type;
        if (tmp$_0 === 9 || tmp$_0 === 10 || tmp$_0 === 11 || tmp$_0 === 12)
          rlt.add_11rb$(g);
      }
      return rlt;
    }
  });
  Object.defineProperty(CombatUI$MenuGoods.prototype, 'throwableGoodsList_0', {
    get: function () {
      var $receiver = Player$Companion_getInstance().sGoodsList.goodsList;
      var destination = ArrayList_init();
      var tmp$;
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        if (element.type === 8)
          destination.add_11rb$(element);
      }
      return toMutableList(destination);
    }
  });
  CombatUI$MenuGoods.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatUI$MenuGoods.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mBg_0, 29, 14);
    TextRender_getInstance().drawText_tz7kd0$(canvas, this.mText_0, 0, this.mTextRect_0);
    TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.mItemText_0[this.mSelIndex_0], this.mTextRect_0.left, this.mTextRect_0.top + (16 * this.mSelIndex_0 | 0) | 0);
  };
  CombatUI$MenuGoods.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN) {
      this.mSelIndex_0 = this.mSelIndex_0 + 1 | 0;
      this.mSelIndex_0 = this.mSelIndex_0 % this.mItemText_0.length;
    }
     else if (key === Global_getInstance().KEY_UP) {
      this.mSelIndex_0 = this.mSelIndex_0 - 1 | 0;
      this.mSelIndex_0 = (this.mSelIndex_0 + this.mItemText_0.length | 0) % this.mItemText_0.length;
    }
  };
  function CombatUI$MenuGoods$onKeyUp$ObjectLiteral(this$MenuGoods) {
    this.this$MenuGoods = this$MenuGoods;
  }
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral.prototype.onItemSelected_6xxg66$ = function (goods) {
    this.this$MenuGoods.equipSelected_0(goods);
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenGoodsList$OnItemSelectedListener]
  };
  function CombatUI$MenuGoods$onKeyUp$ObjectLiteral_0(this$MenuGoods, this$CombatUI) {
    this.this$MenuGoods = this$MenuGoods;
    this.this$CombatUI = this$CombatUI;
  }
  function CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral(this$CombatUI, closure$goods) {
    this.this$CombatUI = this$CombatUI;
    this.closure$goods = closure$goods;
  }
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    var tmp$, tmp$_0;
    tmp$_0 = new ActionThrowItemOne(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), fc, Kotlin.isType(tmp$ = this.closure$goods, GoodsHiddenWeapon) ? tmp$ : throwCCE());
    this.this$CombatUI.onActionSelected_0(tmp$_0);
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral_0.prototype.onItemSelected_6xxg66$ = function (goods) {
    var tmp$, tmp$_0;
    this.this$MenuGoods.delegate.popScreen();
    this.this$MenuGoods.delegate.popScreen();
    if (goods.effectAll()) {
      tmp$_0 = new ActionThrowItemAll(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), this.this$CombatUI.mMonsterList_0, Kotlin.isType(tmp$ = goods, GoodsHiddenWeapon) ? tmp$ : throwCCE());
      this.this$CombatUI.onActionSelected_0(tmp$_0);
    }
     else {
      this.this$MenuGoods.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.this$CombatUI, this.this$CombatUI.mMonsterIndicator_0, CombatUI$Companion_getInstance().sMonsterIndicatorPos_0, this.this$CombatUI.mMonsterList_0, new CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral(this.this$CombatUI, goods), true));
    }
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral_0.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenGoodsList$OnItemSelectedListener]
  };
  function CombatUI$MenuGoods$onKeyUp$ObjectLiteral_1(this$MenuGoods, this$CombatUI) {
    this.this$MenuGoods = this$MenuGoods;
    this.this$CombatUI = this$CombatUI;
  }
  function CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0(this$CombatUI, closure$goods) {
    this.this$CombatUI = this$CombatUI;
    this.closure$goods = closure$goods;
  }
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0.prototype.onCharacterSelected_qpjxya$ = function (fc) {
    this.this$CombatUI.onActionSelected_0(new ActionUseItemOne(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), fc, this.closure$goods));
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [CombatUI$OnCharacterSelectedListener]
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral_1.prototype.onItemSelected_6xxg66$ = function (goods) {
    this.this$MenuGoods.delegate.popScreen();
    this.this$MenuGoods.delegate.popScreen();
    if (goods.effectAll()) {
      this.this$CombatUI.onActionSelected_0(new ActionUseItemAll(this.this$CombatUI.mPlayerList_0.get_za3lpa$(this.this$CombatUI.mCurPlayerIndex_0), this.this$CombatUI.mMonsterList_0, goods));
    }
     else {
      this.this$MenuGoods.delegate.pushScreen_2o7n0o$(new CombatUI$MenuCharacterSelect(this.this$CombatUI, this.this$CombatUI.mTargetIndicator_0, CombatUI$Companion_getInstance().sPlayerIndicatorPos_0, this.this$CombatUI.mPlayerList_0, new CombatUI$MenuGoods$onKeyUp$ObjectLiteral$onItemSelected$ObjectLiteral_0(this.this$CombatUI, goods), false));
    }
  };
  CombatUI$MenuGoods$onKeyUp$ObjectLiteral_1.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenGoodsList$OnItemSelectedListener]
  };
  CombatUI$MenuGoods.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      tmp$ = this.mSelIndex_0;
      if (tmp$ === 0)
        this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(Player$Companion_getInstance().sGoodsList.equipList, new CombatUI$MenuGoods$onKeyUp$ObjectLiteral(this), ScreenGoodsList$Mode$Use_getInstance()));
      else if (tmp$ === 1)
        this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(this.throwableGoodsList_0, new CombatUI$MenuGoods$onKeyUp$ObjectLiteral_0(this, this.$outer), ScreenGoodsList$Mode$Use_getInstance()));
      else if (tmp$ === 2)
        this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(this.useableGoodsList_0, new CombatUI$MenuGoods$onKeyUp$ObjectLiteral_1(this, this.$outer), ScreenGoodsList$Mode$Use_getInstance()));
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  function CombatUI$MenuGoods$equipSelected$ObjectLiteral(closure$list, closure$goods) {
    this.closure$list = closure$list;
    this.closure$goods = closure$goods;
    BaseScreen.call(this);
    this.bg_8be2vx$ = Util_getInstance().getFrameBitmap_vux9f0$((16 * 5 | 0) + 6 | 0, 6 + (16 * closure$list.size | 0) | 0);
    this.curSel_8be2vx$ = 0;
    var array = Array_0(closure$list.size);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = new Int8Array(11);
    }
    this.itemsText_8be2vx$ = array;
    var tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    tmp$_0 = get_indices_1(this.itemsText_8be2vx$);
    tmp$_1 = tmp$_0.first;
    tmp$_2 = tmp$_0.last;
    tmp$_3 = tmp$_0.step;
    for (var i_0 = tmp$_1; i_0 <= tmp$_2; i_0 += tmp$_3) {
      for (var j = 0; j <= 9; j++) {
        this.itemsText_8be2vx$[i_0][j] = toByte(32 | 0);
      }
      var tmp = gbkBytes(closure$list.get_za3lpa$(i_0).name);
      System_getInstance().arraycopy_nlwz52$(tmp, 0, this.itemsText_8be2vx$[i_0], 0, tmp.length);
    }
  }
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
  };
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_ENTER) {
      if (this.closure$list.get_za3lpa$(this.curSel_8be2vx$).hasEquipt_vux9f0$(this.closure$goods.type, this.closure$goods.index)) {
        this.msgDelegate.showMessage_4wgjuj$('\u5DF2\u88C5\u5907!', Kotlin.Long.fromInt(1000));
      }
       else {
        this.delegate.popScreen();
        this.delegate.pushScreen_2o7n0o$(new ScreenChgEquipment(this.closure$list.get_za3lpa$(this.curSel_8be2vx$), Kotlin.isType(tmp$ = this.closure$goods, GoodsEquipment) ? tmp$ : throwCCE()));
      }
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN) {
      this.curSel_8be2vx$ = this.curSel_8be2vx$ + 1 | 0;
      this.curSel_8be2vx$ = this.curSel_8be2vx$ % this.itemsText_8be2vx$.length;
    }
     else if (key === Global_getInstance().KEY_UP) {
      this.curSel_8be2vx$ = this.curSel_8be2vx$ - 1 | 0;
      this.curSel_8be2vx$ = (this.curSel_8be2vx$ + this.itemsText_8be2vx$.length | 0) % this.itemsText_8be2vx$.length;
    }
  };
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    canvas.drawBitmap_t8cslu$(this.bg_8be2vx$, 50, 14);
    tmp$ = get_indices_1(this.itemsText_8be2vx$);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      if (i !== this.curSel_8be2vx$) {
        TextRender_getInstance().drawText_pbrmiz$(canvas, this.itemsText_8be2vx$[i], 50 + 3 | 0, 14 + 3 + (16 * i | 0) | 0);
      }
       else {
        TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.itemsText_8be2vx$[i], 50 + 3 | 0, 14 + 3 + (16 * i | 0) | 0);
      }
    }
  };
  CombatUI$MenuGoods$equipSelected$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [BaseScreen]
  };
  CombatUI$MenuGoods.prototype.equipSelected_0 = function (goods) {
    var tmp$;
    var list = ArrayList_init();
    var tmp$_0;
    tmp$_0 = this.$outer.mPlayerList_0.iterator();
    while (tmp$_0.hasNext()) {
      var element = tmp$_0.next();
      if (goods.canPlayerUse_za3lpa$(element.index))
        list.add_11rb$(element);
    }
    if (list.size === 0) {
      this.msgDelegate.showMessage_4wgjuj$('\u4E0D\u80FD\u88C5\u5907!', Kotlin.Long.fromInt(1000));
    }
     else if (list.size === 1) {
      if (list.get_za3lpa$(0).hasEquipt_vux9f0$(goods.type, goods.index)) {
        this.msgDelegate.showMessage_4wgjuj$('\u5DF2\u88C5\u5907!', Kotlin.Long.fromInt(1000));
      }
       else {
        this.delegate.pushScreen_2o7n0o$(new ScreenChgEquipment(list.get_za3lpa$(0), Kotlin.isType(tmp$ = goods, GoodsEquipment) ? tmp$ : throwCCE()));
      }
    }
     else {
      this.delegate.pushScreen_2o7n0o$(new CombatUI$MenuGoods$equipSelected$ObjectLiteral(list, goods));
    }
  };
  CombatUI$MenuGoods.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MenuGoods',
    interfaces: [BaseScreen]
  };
  function CombatUI$Companion() {
    CombatUI$Companion_instance = this;
    this.sPlayerIndicatorPos_0 = [new Point(69, 45), new Point(101, 41), new Point(133, 33)];
    this.sMonsterIndicatorPos_0 = [new Point(16, 14), new Point(48, 3), new Point(86, 0)];
  }
  CombatUI$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var CombatUI$Companion_instance = null;
  function CombatUI$Companion_getInstance() {
    if (CombatUI$Companion_instance === null) {
      new CombatUI$Companion();
    }
    return CombatUI$Companion_instance;
  }
  CombatUI.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'CombatUI',
    interfaces: [BaseScreen]
  };
  function ScreenActorState() {
    BaseScreen.call(this);
    this.mPage_0 = 0;
    this.mPlayerList_0 = ScreenMainGame$Companion_getInstance().instance.playerList;
    this.mCurPlayer_0 = 0;
  }
  ScreenActorState.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenActorState.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    var i = 0;
    while (i < this.mPlayerList_0.size) {
      this.mPlayerList_0.get_za3lpa$(i).drawHead_2g4tob$(canvas, 10, 2 + (32 * i | 0) | 0);
      i = i + 1 | 0;
    }
    if (!this.mPlayerList_0.isEmpty()) {
      this.mPlayerList_0.get_za3lpa$(this.mCurPlayer_0).drawState_86va19$(canvas, this.mPage_0);
      Util_getInstance().drawTriangleCursor_2g4tob$(canvas, 3, 10 + (32 * this.mCurPlayer_0 | 0) | 0);
    }
  };
  ScreenActorState.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_PAGEDOWN || key === Global_getInstance().KEY_PAGEUP) {
      this.mPage_0 = 1 - this.mPage_0 | 0;
    }
     else if (key === Global_getInstance().KEY_DOWN) {
      this.mCurPlayer_0 = this.mCurPlayer_0 + 1 | 0;
      if (this.mCurPlayer_0 >= this.mPlayerList_0.size) {
        this.mCurPlayer_0 = 0;
      }
    }
     else if (key === Global_getInstance().KEY_UP) {
      this.mCurPlayer_0 = this.mCurPlayer_0 - 1 | 0;
      if (this.mCurPlayer_0 < 0) {
        this.mCurPlayer_0 = this.mPlayerList_0.size - 1 | 0;
      }
    }
  };
  ScreenActorState.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  ScreenActorState.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenActorState',
    interfaces: [BaseScreen]
  };
  function ScreenActorWearing() {
    ScreenActorWearing$Companion_getInstance();
    BaseScreen.call(this);
    this.mPos_0 = null;
    this.mEquipments_0 = null;
    this.mItemName_0 = ['\u88C5\u9970', '\u88C5\u9970', '\u62A4\u8155', '\u811A\u8E6C', '\u624B\u6301', '\u8EAB\u7A7F', '\u80A9\u62AB', '\u5934\u6234'];
    this.mCurItem_0 = 0;
    this.mActorIndex_0 = -1;
    this.showingDesc_0 = false;
    this.bmpName_0 = Util_getInstance().getFrameBitmap_vux9f0$(92 - 9 + 1 | 0, 29 - 10 + 1 | 0);
    this.bmpDesc_0 = Util_getInstance().getFrameBitmap_vux9f0$(151 - 9 + 1 | 0, 65 - 28 + 1 | 0);
    this.mTextName_0 = new Int8Array([]);
    this.mTextDesc_0 = new Int8Array([]);
    this.mToDraw_0 = 0;
    this.mNextToDraw_0 = 0;
    this.mStackLastToDraw_0 = Stack$Companion_getInstance().create_287e2$();
    this.mEquipments_0 = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(0).equipmentsArray;
    this.mActorIndex_0 = 0;
    this.mPos_0 = [new Point(4, 3), new Point(4, 30), new Point(21, 59), new Point(51, 65), new Point(80, 61), new Point(109, 46), new Point(107, 9), new Point(79, 2)];
  }
  ScreenActorWearing.prototype.getGBKBytes_0 = function (s) {
    return gbkBytes(s);
  };
  ScreenActorWearing.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenActorWearing.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$;
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    canvas.drawBitmap_t8cslu$(Util_getInstance().bmpChuandai, 160 - Util_getInstance().bmpChuandai.width | 0, 0);
    for (var i = 0; i <= 7; i++) {
      (tmp$ = this.mEquipments_0[i]) != null ? (tmp$.draw_2g4tob$(canvas, this.mPos_0[i].x + 1 | 0, this.mPos_0[i].y + 1 | 0), Unit) : null;
    }
    canvas.drawRect_x3aj6j$(this.mPos_0[this.mCurItem_0].x, this.mPos_0[this.mCurItem_0].y, this.mPos_0[this.mCurItem_0].x + 25 | 0, this.mPos_0[this.mCurItem_0].y + 25 | 0, Util_getInstance().sBlackPaint);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mItemName_0[this.mCurItem_0], 120, 80);
    if (this.mActorIndex_0 >= 0) {
      var p = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0);
      p.drawHead_2g4tob$(canvas, 44, 12);
      TextRender_getInstance().drawText_kkuqvh$(canvas, p.name, 30, 40);
    }
    if (this.showingDesc_0) {
      canvas.drawBitmap_t8cslu$(this.bmpName_0, 9, 10);
      canvas.drawBitmap_t8cslu$(this.bmpDesc_0, 9, 28);
      TextRender_getInstance().drawText_pbrmiz$(canvas, this.mTextName_0, 9 + 3 | 0, 10 + 3 | 0);
      this.mNextToDraw_0 = TextRender_getInstance().drawText_tz7kd0$(canvas, this.mTextDesc_0, this.mToDraw_0, ScreenActorWearing$Companion_getInstance().sRectDesc_0);
    }
  };
  ScreenActorWearing.prototype.resetDesc_0 = function () {
    if (this.showingDesc_0) {
      this.showingDesc_0 = false;
      this.mNextToDraw_0 = 0;
      this.mToDraw_0 = this.mNextToDraw_0;
      this.mStackLastToDraw_0.clear();
    }
  };
  ScreenActorWearing.prototype.onKeyDown_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_DOWN && this.mCurItem_0 < (8 - 1 | 0)) {
      this.mCurItem_0 = this.mCurItem_0 + 1 | 0;
      this.resetDesc_0();
    }
     else if (key === Global_getInstance().KEY_UP && this.mCurItem_0 > 0) {
      this.mCurItem_0 = this.mCurItem_0 - 1 | 0;
      this.resetDesc_0();
    }
     else if (key === Global_getInstance().KEY_RIGHT && this.mActorIndex_0 < (ScreenMainGame$Companion_getInstance().sPlayerList.size - 1 | 0)) {
      this.mActorIndex_0 = this.mActorIndex_0 + 1 | 0;
      this.mEquipments_0 = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0).equipmentsArray;
      this.resetDesc_0();
    }
     else if (key === Global_getInstance().KEY_LEFT && this.mActorIndex_0 > 0) {
      this.mActorIndex_0 = this.mActorIndex_0 - 1 | 0;
      this.mEquipments_0 = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0).equipmentsArray;
      this.resetDesc_0();
    }
     else if (this.showingDesc_0) {
      if (key === Global_getInstance().KEY_PAGEDOWN) {
        if (this.mNextToDraw_0 < this.mTextDesc_0.length) {
          this.mStackLastToDraw_0.push_11rb$(this.mToDraw_0);
          this.mToDraw_0 = this.mNextToDraw_0;
        }
      }
       else if (key === Global_getInstance().KEY_PAGEUP && this.mToDraw_0 !== 0) {
        if ((tmp$ = this.mStackLastToDraw_0.pop()) != null) {
          this.mToDraw_0 = tmp$;
        }
      }
    }
  };
  function ScreenActorWearing$onKeyUp$ObjectLiteral(this$ScreenActorWearing) {
    this.this$ScreenActorWearing = this$ScreenActorWearing;
  }
  ScreenActorWearing$onKeyUp$ObjectLiteral.prototype.onItemSelected_6xxg66$ = function (goods) {
    var tmp$;
    var actor = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.this$ScreenActorWearing.mActorIndex_0);
    if (goods.canPlayerUse_za3lpa$(actor.index)) {
      this.this$ScreenActorWearing.delegate.popScreen();
      this.this$ScreenActorWearing.delegate.pushScreen_2o7n0o$(new ScreenChgEquipment(actor, Kotlin.isType(tmp$ = goods, GoodsEquipment) ? tmp$ : throwCCE()));
    }
     else {
      this.this$ScreenActorWearing.msgDelegate.showMessage_4wgjuj$('\u4E0D\u80FD\u88C5\u5907!', Kotlin.Long.fromInt(1000));
    }
  };
  ScreenActorWearing$onKeyUp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenGoodsList$OnItemSelectedListener]
  };
  ScreenActorWearing.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      if (!this.showingDesc_0 && this.mEquipments_0[this.mCurItem_0] != null) {
        this.showingDesc_0 = true;
        this.mTextName_0 = this.getGBKBytes_0((tmp$_0 = (tmp$ = this.mEquipments_0[this.mCurItem_0]) != null ? tmp$.name : null) != null ? tmp$_0 : '');
        this.mTextDesc_0 = this.getGBKBytes_0((tmp$_2 = (tmp$_1 = this.mEquipments_0[this.mCurItem_0]) != null ? tmp$_1.description : null) != null ? tmp$_2 : '');
      }
       else {
        this.resetDesc_0();
        this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(this.getTheEquipList_0(Player$Companion_getInstance().sEquipTypes[this.mCurItem_0]), new ScreenActorWearing$onKeyUp$ObjectLiteral(this), ScreenGoodsList$Mode$Use_getInstance()));
      }
    }
  };
  ScreenActorWearing.prototype.getTheEquipList_0 = function (type) {
    var $receiver = Player$Companion_getInstance().sGoodsList.equipList;
    var destination = ArrayList_init();
    var tmp$;
    tmp$ = $receiver.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      if (element.type === type)
        destination.add_11rb$(element);
    }
    return destination;
  };
  function ScreenActorWearing$Companion() {
    ScreenActorWearing$Companion_instance = this;
    this.sRectDesc_0 = new Rect(9 + 3 | 0, 28 + 3 | 0, 151, 65);
  }
  ScreenActorWearing$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenActorWearing$Companion_instance = null;
  function ScreenActorWearing$Companion_getInstance() {
    if (ScreenActorWearing$Companion_instance === null) {
      new ScreenActorWearing$Companion();
    }
    return ScreenActorWearing$Companion_instance;
  }
  ScreenActorWearing.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenActorWearing',
    interfaces: [BaseScreen]
  };
  function ScreenChgEquipment(mActor, goods) {
    BaseScreen.call(this);
    this.mActor_0 = mActor;
    this.mGoods_0 = null;
    this.mSelIndex_0 = 0;
    this.mPage_0 = 0;
    var currentEquip = this.mActor_0.getCurrentEquipment_za3lpa$(goods.type);
    if (currentEquip == null || this.mActor_0.hasSpace_za3lpa$(goods.type)) {
      this.mGoods_0 = [goods];
      this.mSelIndex_0 = 0;
    }
     else {
      this.mGoods_0 = [currentEquip, goods];
      this.mSelIndex_0 = 1;
      this.mActor_0.takeOff_za3lpa$(goods.type);
    }
    this.mActor_0.putOn_uyhnfp$(goods);
  }
  ScreenChgEquipment.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenChgEquipment.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    this.mActor_0.drawState_86va19$(canvas, this.mPage_0);
    this.mActor_0.drawHead_2g4tob$(canvas, 5, 60);
    var $receiver = this.mGoods_0;
    var tmp$, tmp$_0;
    var index = 0;
    for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
      var item = $receiver[tmp$];
      item.draw_2g4tob$(canvas, 8, 2 + (32 * (tmp$_0 = index, index = tmp$_0 + 1 | 0, tmp$_0) | 0) | 0);
    }
    Util_getInstance().drawTriangleCursor_2g4tob$(canvas, 1, 10 + (32 * this.mSelIndex_0 | 0) | 0);
  };
  ScreenChgEquipment.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP && this.mSelIndex_0 > 0) {
      this.mActor_0.takeOff_za3lpa$(this.mGoods_0[this.mSelIndex_0].type);
      this.mSelIndex_0 = this.mSelIndex_0 - 1 | 0;
      this.mActor_0.putOn_uyhnfp$(this.mGoods_0[this.mSelIndex_0]);
    }
     else if (key === Global_getInstance().KEY_DOWN && this.mSelIndex_0 < (this.mGoods_0.length - 1 | 0)) {
      this.mActor_0.takeOff_za3lpa$(this.mGoods_0[this.mSelIndex_0].type);
      this.mSelIndex_0 = this.mSelIndex_0 + 1 | 0;
      this.mActor_0.putOn_uyhnfp$(this.mGoods_0[this.mSelIndex_0]);
    }
     else if (key === Global_getInstance().KEY_PAGEDOWN || key === Global_getInstance().KEY_PAGEUP) {
      this.mPage_0 = 1 - this.mPage_0 | 0;
    }
  };
  ScreenChgEquipment.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.mActor_0.takeOff_za3lpa$(this.mGoods_0[0].type);
      if (this.mGoods_0.length > 1) {
        this.mActor_0.putOn_uyhnfp$(this.mGoods_0[0]);
      }
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      if (this.mSelIndex_0 === (this.mGoods_0.length - 1 | 0)) {
        Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(this.mGoods_0[this.mGoods_0.length - 1 | 0].type, this.mGoods_0[this.mGoods_0.length - 1 | 0].index);
        if (this.mGoods_0.length > 1) {
          Player$Companion_getInstance().sGoodsList.addGoods_vux9f0$(this.mGoods_0[0].type, this.mGoods_0[0].index);
        }
      }
      this.delegate.popScreen();
    }
  };
  ScreenChgEquipment.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenChgEquipment',
    interfaces: [BaseScreen]
  };
  function ScreenCommonMenu(items, callback) {
    BaseScreen.call(this);
    this.callback_0 = callback;
    this.curSel_0 = 0;
    this.paddedItems_0 = null;
    this.top_0 = 0;
    this.left_0 = 0;
    this.bg_0 = null;
    this.padx_0 = 3;
    this.pady_0 = 3;
    var tmp$;
    var destination = ArrayList_init(items.length);
    var tmp$_0;
    for (tmp$_0 = 0; tmp$_0 !== items.length; ++tmp$_0) {
      var item = items[tmp$_0];
      destination.add_11rb$(gbkBytes(item));
    }
    var byteItems = destination;
    var destination_0 = ArrayList_init(collectionSizeOrDefault(byteItems, 10));
    var tmp$_1;
    tmp$_1 = byteItems.iterator();
    while (tmp$_1.hasNext()) {
      var item_0 = tmp$_1.next();
      destination_0.add_11rb$(item_0.length);
    }
    var colCount = (tmp$ = max(destination_0)) != null ? tmp$ : 2;
    var width = 8 * colCount | 0;
    var height = 16 * items.length | 0;
    this.bg_0 = Util_getInstance().getFrameBitmap_vux9f0$(width + (this.padx_0 * 2 | 0) | 0, height + (this.pady_0 * 2 | 0) | 0);
    this.left_0 = (Global_getInstance().SCREEN_WIDTH - width | 0) / 2 | 0;
    this.top_0 = (Global_getInstance().SCREEN_HEIGHT - height | 0) / 2 | 0;
    var destination_1 = ArrayList_init(collectionSizeOrDefault(byteItems, 10));
    var tmp$_2;
    tmp$_2 = byteItems.iterator();
    while (tmp$_2.hasNext()) {
      var item_1 = tmp$_2.next();
      var tmp$_3 = destination_1.add_11rb$;
      var s = toMutableList_0(item_1);
      while (s.size < colCount)
        s.add_11rb$(toByte(32 | 0));
      tmp$_3.call(destination_1, toByteArray(s));
    }
    this.paddedItems_0 = destination_1;
  }
  Object.defineProperty(ScreenCommonMenu.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenCommonMenu.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenCommonMenu.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      this.callback_0(this.curSel_0 + 1 | 0);
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
      this.callback_0(0);
    }
  };
  ScreenCommonMenu.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN && this.curSel_0 < (this.paddedItems_0.size - 1 | 0)) {
      this.curSel_0 = this.curSel_0 + 1 | 0;
    }
     else if (key === Global_getInstance().KEY_UP && this.curSel_0 > 0) {
      this.curSel_0 = this.curSel_0 - 1 | 0;
    }
  };
  ScreenCommonMenu.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    canvas.drawBitmap_t8cslu$(this.bg_0, this.left_0 - this.padx_0 | 0, this.top_0 - this.pady_0 | 0);
    tmp$ = get_indices(this.paddedItems_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      if (i !== this.curSel_0) {
        TextRender_getInstance().drawText_pbrmiz$(canvas, this.paddedItems_0.get_za3lpa$(i), this.left_0, this.top_0 + (16 * i | 0) | 0);
      }
       else {
        TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.paddedItems_0.get_za3lpa$(i), this.left_0, this.top_0 + (16 * i | 0) | 0);
      }
    }
  };
  ScreenCommonMenu.prototype.reset = function () {
    this.curSel_0 = 0;
  };
  ScreenCommonMenu.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenCommonMenu',
    interfaces: [BaseScreen]
  };
  function ScreenGameMainMenu() {
    BaseScreen.call(this);
    this.bmpFrame1_0 = Util_getInstance().getFrameBitmap_vux9f0$(93, 16 + 6 | 0);
    this.bmpFrame2_0 = Util_getInstance().getFrameBitmap_vux9f0$(32 + 6 | 0, 64 + 6 | 0);
    this.menuItemsRect_0 = null;
    this.menuItems_0 = gbkBytes('\u5C5E\u6027\u9B54\u6CD5\u7269\u54C1\u7CFB\u7EDF');
    this.menuItemsS_0 = ['\u5C5E\u6027', '\u9B54\u6CD5', '\u7269\u54C1', '\u7CFB\u7EDF'];
    this.mSelIndex_0 = 0;
    this.screenSelectActor_8be2vx$ = new ScreenGameMainMenu$screenSelectActor$ObjectLiteral(this);
    var l = 9 + 3 | 0;
    var t = 3 + 16 + 6 - 1 + 3 | 0;
    var r = l + 32 | 0;
    var b = t + 64 | 0;
    this.menuItemsRect_0 = new Rect(l, t, r, b);
  }
  Object.defineProperty(ScreenGameMainMenu.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenGameMainMenu.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenGameMainMenu.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.bmpFrame1_0, 9, 3);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u91D1\u94B1:' + toString(Player$Companion_getInstance().sMoney), 9 + 3 | 0, 3 + 3 | 0);
    canvas.drawBitmap_t8cslu$(this.bmpFrame2_0, 9, 3 + 16 + 6 - 1 | 0);
    TextRender_getInstance().drawText_tz7kd0$(canvas, this.menuItems_0, 0, this.menuItemsRect_0);
    TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.menuItemsS_0[this.mSelIndex_0], this.menuItemsRect_0.left, this.menuItemsRect_0.top + (this.mSelIndex_0 * 16 | 0) | 0);
  };
  ScreenGameMainMenu.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP) {
      if ((this.mSelIndex_0 = this.mSelIndex_0 - 1 | 0, this.mSelIndex_0) < 0) {
        this.mSelIndex_0 = 3;
      }
    }
     else if (key === Global_getInstance().KEY_DOWN) {
      if ((this.mSelIndex_0 = this.mSelIndex_0 + 1 | 0, this.mSelIndex_0) > 3) {
        this.mSelIndex_0 = 0;
      }
    }
  };
  ScreenGameMainMenu.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$, tmp$_0;
    if (key === Global_getInstance().KEY_ENTER) {
      tmp$ = this.mSelIndex_0;
      if (tmp$ === 0)
        tmp$_0 = new ScreenMenuProperties();
      else if (tmp$ === 1)
        tmp$_0 = ScreenMainGame$Companion_getInstance().instance.playerList.size > 1 ? this.screenSelectActor_8be2vx$ : this.getScreenMagic_0(0);
      else if (tmp$ === 2)
        tmp$_0 = new ScreenMenuGoods();
      else if (tmp$ === 3)
        tmp$_0 = new ScreenMenuSystem();
      else
        tmp$_0 = null;
      var screen = tmp$_0;
      if (screen != null) {
        this.delegate.pushScreen_2o7n0o$(screen);
      }
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  function ScreenGameMainMenu$getScreenMagic$ObjectLiteral(this$ScreenGameMainMenu, closure$id) {
    this.this$ScreenGameMainMenu = this$ScreenGameMainMenu;
    this.closure$id = closure$id;
  }
  ScreenGameMainMenu$getScreenMagic$ObjectLiteral.prototype.onItemSelected_3fncnk$ = function (magic) {
    if (Kotlin.isType(magic, MagicRestore)) {
      this.this$ScreenGameMainMenu.delegate.pushScreen_2o7n0o$(new ScreenUseMagic(magic, ScreenMainGame$Companion_getInstance().instance.playerList.get_za3lpa$(this.closure$id)));
    }
     else {
      this.this$ScreenGameMainMenu.msgDelegate.showMessage_4wgjuj$('\u6B64\u5904\u65E0\u6CD5\u4F7F\u7528!', Kotlin.Long.fromInt(1000));
    }
  };
  ScreenGameMainMenu$getScreenMagic$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenMagic$OnItemSelectedListener]
  };
  ScreenGameMainMenu.prototype.getScreenMagic_0 = function (id) {
    var magicChain = ScreenMainGame$Companion_getInstance().instance.playerList.get_za3lpa$(id).magicChain;
    if (magicChain.learnNum === 0)
      return null;
    return new ScreenMagic(magicChain, new ScreenGameMainMenu$getScreenMagic$ObjectLiteral(this, id));
  };
  var copyToArray = Kotlin.kotlin.collections.copyToArray;
  function ScreenGameMainMenu$screenSelectActor$ObjectLiteral(this$ScreenGameMainMenu) {
    this.this$ScreenGameMainMenu = this$ScreenGameMainMenu;
    BaseScreen.call(this);
    this.index_0 = 0;
    this.mFrameRect_0 = new Rect(39, 29, 125, 67 - 32 + (ScreenMainGame$Companion_getInstance().instance.playerList.size * 16 | 0) | 0);
    this.bmpFrame_0 = Util_getInstance().getFrameBitmap_vux9f0$(this.mFrameRect_0.width(), this.mFrameRect_0.height());
    this.mNames_0 = null;
    this.mSum_0 = 0;
    var list = ScreenMainGame$Companion_getInstance().instance.playerList;
    this.mSum_0 = list.size;
    var destination = ArrayList_init(collectionSizeOrDefault(list, 10));
    var tmp$;
    tmp$ = list.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      destination.add_11rb$(item.name);
    }
    this.mNames_0 = copyToArray(destination);
  }
  Object.defineProperty(ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.format_0 = function (s) {
    var s_0 = s;
    while (gbkBytes(s_0).length < 10)
      s_0 += ' ';
    return s_0;
  };
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$;
    canvas.drawBitmap_t8cslu$(this.bmpFrame_0, this.mFrameRect_0.left, this.mFrameRect_0.top);
    tmp$ = this.mSum_0;
    for (var i = 0; i < tmp$; i++) {
      if (i === this.index_0) {
        TextRender_getInstance().drawSelText_kkuqvh$(canvas, ensureNotNull(this.mNames_0)[i], this.mFrameRect_0.left + 3 | 0, this.mFrameRect_0.top + 3 + (16 * i | 0) | 0);
      }
       else {
        TextRender_getInstance().drawText_kkuqvh$(canvas, ensureNotNull(this.mNames_0)[i], this.mFrameRect_0.left + 3 | 0, this.mFrameRect_0.top + 3 + (16 * i | 0) | 0);
      }
    }
  };
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN) {
      this.index_0 = this.index_0 + 1 | 0;
      if (this.index_0 >= this.mSum_0) {
        this.index_0 = 0;
      }
    }
     else if (key === Global_getInstance().KEY_UP) {
      this.index_0 = this.index_0 - 1 | 0;
      if (this.index_0 < 0) {
        this.index_0 = this.mSum_0 - 1 | 0;
      }
    }
  };
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      if ((tmp$ = this.this$ScreenGameMainMenu.getScreenMagic_0(this.index_0)) != null) {
        this.delegate.pushScreen_2o7n0o$(tmp$);
      }
    }
  };
  ScreenGameMainMenu$screenSelectActor$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [BaseScreen]
  };
  ScreenGameMainMenu.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenGameMainMenu',
    interfaces: [BaseScreen]
  };
  function ScreenGoodsList(goodsList, itemSelectedListener, mode) {
    ScreenGoodsList$Companion_getInstance();
    BaseScreen.call(this);
    this.goodsList_0 = goodsList;
    this.itemSelectedListener_0 = itemSelectedListener;
    this.mode_0 = mode;
    this.description_0 = gbkBytes('');
    this.toDraw_0 = 0;
    this.nextToDraw_0 = 0;
    this.stackLastToDraw_0 = Stack$Companion_getInstance().create_287e2$();
    this.firstDisplayItemIndex_0 = 0;
    this.curItemIndex_0 = 0;
    this.lastDownKey_0 = -1;
  }
  function ScreenGoodsList$Mode(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ScreenGoodsList$Mode_initFields() {
    ScreenGoodsList$Mode_initFields = function () {
    };
    ScreenGoodsList$Mode$Sale_instance = new ScreenGoodsList$Mode('Sale', 0);
    ScreenGoodsList$Mode$Buy_instance = new ScreenGoodsList$Mode('Buy', 1);
    ScreenGoodsList$Mode$Use_instance = new ScreenGoodsList$Mode('Use', 2);
  }
  var ScreenGoodsList$Mode$Sale_instance;
  function ScreenGoodsList$Mode$Sale_getInstance() {
    ScreenGoodsList$Mode_initFields();
    return ScreenGoodsList$Mode$Sale_instance;
  }
  var ScreenGoodsList$Mode$Buy_instance;
  function ScreenGoodsList$Mode$Buy_getInstance() {
    ScreenGoodsList$Mode_initFields();
    return ScreenGoodsList$Mode$Buy_instance;
  }
  var ScreenGoodsList$Mode$Use_instance;
  function ScreenGoodsList$Mode$Use_getInstance() {
    ScreenGoodsList$Mode_initFields();
    return ScreenGoodsList$Mode$Use_instance;
  }
  ScreenGoodsList$Mode.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Mode',
    interfaces: [Enum]
  };
  function ScreenGoodsList$Mode$values() {
    return [ScreenGoodsList$Mode$Sale_getInstance(), ScreenGoodsList$Mode$Buy_getInstance(), ScreenGoodsList$Mode$Use_getInstance()];
  }
  ScreenGoodsList$Mode.values = ScreenGoodsList$Mode$values;
  function ScreenGoodsList$Mode$valueOf(name) {
    switch (name) {
      case 'Sale':
        return ScreenGoodsList$Mode$Sale_getInstance();
      case 'Buy':
        return ScreenGoodsList$Mode$Buy_getInstance();
      case 'Use':
        return ScreenGoodsList$Mode$Use_getInstance();
      default:throwISE('No enum constant fmj.gamemenu.ScreenGoodsList.Mode.' + name);
    }
  }
  ScreenGoodsList$Mode.valueOf_61zpoe$ = ScreenGoodsList$Mode$valueOf;
  function ScreenGoodsList$OnItemSelectedListener() {
  }
  ScreenGoodsList$OnItemSelectedListener.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'OnItemSelectedListener',
    interfaces: []
  };
  ScreenGoodsList.prototype.resetDescription_0 = function () {
    var tmp$;
    if (!this.goodsList_0.isEmpty()) {
      tmp$ = gbkBytes(this.goodsList_0.get_za3lpa$(this.curItemIndex_0).description);
    }
     else {
      tmp$ = gbkBytes('');
    }
    this.description_0 = tmp$;
    this.nextToDraw_0 = 0;
    this.toDraw_0 = this.nextToDraw_0;
    this.stackLastToDraw_0.clear();
  };
  ScreenGoodsList.prototype.willAppear = function () {
    if (this.goodsList_0.isEmpty()) {
      this.curItemIndex_0 = 0;
    }
     else if (this.curItemIndex_0 >= this.goodsList_0.size) {
      this.curItemIndex_0 = this.goodsList_0.size - 1 | 0;
    }
    this.resetDescription_0();
  };
  ScreenGoodsList.prototype.update_s8cxhz$ = function (delta) {
    if (this.goodsList_0.isEmpty()) {
      this.delegate.popScreen();
    }
  };
  ScreenGoodsList.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(ScreenGoodsList$Companion_getInstance().bgImage_0, 0, 0);
    if (this.goodsList_0.isEmpty())
      return;
    while (this.curItemIndex_0 >= this.goodsList_0.size)
      this.showPreItem_0();
    var g = this.goodsList_0.get_za3lpa$(this.curItemIndex_0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mode_0 === ScreenGoodsList$Mode$Buy_getInstance() ? '\u91D1\u94B1:' + toString(Player$Companion_getInstance().sMoney) : '\u6570\u91CF:' + toString(g.goodsNum), 60, 2);
    TextRender_getInstance().drawText_kkuqvh$(canvas, g.name, 69, 23);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '' + toString(this.mode_0 === ScreenGoodsList$Mode$Buy_getInstance() ? g.buyPrice : g.sellPrice), 69, 40);
    Util_getInstance().drawTriangleCursor_2g4tob$(canvas, 4, 8 + (23 * (this.curItemIndex_0 - this.firstDisplayItemIndex_0 | 0) | 0) | 0);
    var i = this.firstDisplayItemIndex_0;
    while (i < (this.firstDisplayItemIndex_0 + ScreenGoodsList$Companion_getInstance().itemNumberPerPage_0 | 0) && i < this.goodsList_0.size) {
      this.goodsList_0.get_za3lpa$(i).draw_2g4tob$(canvas, 14, 2 + (23 * (i - this.firstDisplayItemIndex_0 | 0) | 0) | 0);
      i = i + 1 | 0;
    }
    this.nextToDraw_0 = TextRender_getInstance().drawText_tz7kd0$(canvas, this.description_0, this.toDraw_0, ScreenGoodsList$Companion_getInstance().displayRect_0);
  };
  ScreenGoodsList.prototype.showNextItem_0 = function () {
    this.curItemIndex_0 = this.curItemIndex_0 + 1 | 0;
    if (this.curItemIndex_0 >= (this.firstDisplayItemIndex_0 + ScreenGoodsList$Companion_getInstance().itemNumberPerPage_0 | 0)) {
      this.firstDisplayItemIndex_0 = this.firstDisplayItemIndex_0 + 1 | 0;
    }
    this.resetDescription_0();
  };
  ScreenGoodsList.prototype.showPreItem_0 = function () {
    this.curItemIndex_0 = this.curItemIndex_0 - 1 | 0;
    if (this.curItemIndex_0 < this.firstDisplayItemIndex_0) {
      this.firstDisplayItemIndex_0 = this.firstDisplayItemIndex_0 - 1 | 0;
    }
    this.resetDescription_0();
  };
  ScreenGoodsList.prototype.onKeyDown_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_UP && this.curItemIndex_0 > 0) {
      this.showPreItem_0();
    }
     else if (key === Global_getInstance().KEY_DOWN && (this.curItemIndex_0 + 1 | 0) < this.goodsList_0.size) {
      this.showNextItem_0();
    }
     else if (key === Global_getInstance().KEY_PAGEDOWN) {
      var len = this.description_0.length;
      if (this.nextToDraw_0 < len) {
        this.stackLastToDraw_0.push_11rb$(this.toDraw_0);
        this.toDraw_0 = this.nextToDraw_0;
      }
    }
     else if (key === Global_getInstance().KEY_PAGEUP && this.toDraw_0 !== 0) {
      if ((tmp$ = this.stackLastToDraw_0.pop()) != null) {
        this.toDraw_0 = tmp$;
      }
    }
    this.lastDownKey_0 = key;
  };
  ScreenGoodsList.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER && this.lastDownKey_0 === Global_getInstance().KEY_ENTER) {
      this.itemSelectedListener_0.onItemSelected_6xxg66$(this.goodsList_0.get_za3lpa$(this.curItemIndex_0));
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  function ScreenGoodsList$Companion() {
    ScreenGoodsList$Companion_instance = this;
    this.bgImage_nrj872$_0 = lazy(ScreenGoodsList$Companion$bgImage$lambda);
    this.displayRect_0 = new Rect(44, 61, 156, 94);
    this.itemNumberPerPage_0 = 4;
  }
  Object.defineProperty(ScreenGoodsList$Companion.prototype, 'bgImage_0', {
    get: function () {
      var $receiver = this.bgImage_nrj872$_0;
      new PropertyMetadata('bgImage');
      return $receiver.value;
    }
  });
  function ScreenGoodsList$Companion$bgImage$lambda() {
    var bmp = Bitmap$Companion_getInstance().createBitmap_vux9f0$(160, 96);
    var pts = new Float32Array([40.0, 21.0, 40.0, 95.0, 40.0, 95.0, 0.0, 95.0, 0.0, 95.0, 0.0, 5.0, 0.0, 5.0, 5.0, 0.0, 5.0, 0.0, 39.0, 0.0, 39.0, 0.0, 58.0, 19.0, 38.0, 0.0, 57.0, 19.0, 57.0, 19.0, 140.0, 19.0, 41.0, 20.0, 140.0, 20.0, 41.0, 21.0, 159.0, 21.0, 54.0, 0.0, 140.0, 0.0, 40.0, 95.0, 159.0, 95.0, 40.0, 57.0, 160.0, 57.0, 40.0, 58.0, 140.0, 58.0, 40.0, 59.0, 159.0, 59.0, 41.0, 20.0, 41.0, 95.0, 42.0, 20.0, 42.0, 95.0, 159.0, 21.0, 159.0, 57.0, 159.0, 59.0, 159.0, 96.0]);
    var c = new Canvas(bmp);
    c.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    c.drawLines_ffeagz$(pts, Util_getInstance().sBlackPaint);
    TextRender_getInstance().drawText_kkuqvh$(c, '\u540D:', 45, 23);
    TextRender_getInstance().drawText_kkuqvh$(c, '\u4EF7:', 45, 40);
    return bmp;
  }
  ScreenGoodsList$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenGoodsList$Companion_instance = null;
  function ScreenGoodsList$Companion_getInstance() {
    if (ScreenGoodsList$Companion_instance === null) {
      new ScreenGoodsList$Companion();
    }
    return ScreenGoodsList$Companion_instance;
  }
  ScreenGoodsList.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenGoodsList',
    interfaces: [BaseScreen]
  };
  function ScreenMenuGoods() {
    BaseScreen.call(this);
    this.mFrameBmp_0 = Util_getInstance().getFrameBitmap_vux9f0$(77 - 39 + 1 | 0, 77 - 39 + 1 | 0);
    this.strs_0 = ['\u4F7F\u7528', '\u88C5\u5907'];
    this.mSelId_0 = 0;
  }
  Object.defineProperty(ScreenMenuGoods.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenMenuGoods.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMenuGoods.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mFrameBmp_0, 39, 39);
    if (this.mSelId_0 === 0) {
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.strs_0[0], 39 + 3 | 0, 39 + 3 | 0);
      TextRender_getInstance().drawText_kkuqvh$(canvas, this.strs_0[1], 39 + 3 | 0, 39 + 3 + 16 | 0);
    }
     else if (this.mSelId_0 === 1) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, this.strs_0[0], 39 + 3 | 0, 39 + 3 | 0);
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.strs_0[1], 39 + 3 | 0, 39 + 3 + 16 | 0);
    }
  };
  ScreenMenuGoods.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP || key === Global_getInstance().KEY_DOWN) {
      this.mSelId_0 = 1 - this.mSelId_0 | 0;
    }
  };
  ScreenMenuGoods.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(this.mSelId_0 === 0 ? Player$Companion_getInstance().sGoodsList.goodsList : Player$Companion_getInstance().sGoodsList.equipList, this, ScreenGoodsList$Mode$Use_getInstance()));
    }
  };
  ScreenMenuGoods.prototype.onItemSelected_6xxg66$ = function (goods) {
    if (this.mSelId_0 === 0) {
      this.goodsSelected_0(goods);
    }
     else if (this.mSelId_0 === 1) {
      this.equipSelected_0(goods);
    }
  };
  ScreenMenuGoods.prototype.goodsSelected_0 = function (goods) {
    var tmp$;
    tmp$ = goods.type;
    if (tmp$ === 8 || tmp$ === 12)
      this.msgDelegate.showMessage_4wgjuj$('\u6218\u6597\u4E2D\u624D\u80FD\u4F7F\u7528!', Kotlin.Long.fromInt(1000));
    else if (tmp$ === 13) {
      ScreenMainGame$Companion_getInstance().instance.triggerEvent_za3lpa$(255);
      while (!Kotlin.isType(this.delegate.getCurScreen(), ScreenMainGame)) {
        this.delegate.popScreen();
      }
    }
     else if (tmp$ === 14)
      this.msgDelegate.showMessage_4wgjuj$('\u5F53\u524D\u65E0\u6CD5\u4F7F\u7528!', Kotlin.Long.fromInt(1000));
    else if (tmp$ === 9 || tmp$ === 10 || tmp$ === 11)
      this.delegate.pushScreen_2o7n0o$(new ScreenTakeMedicine(goods));
  };
  function ScreenMenuGoods$equipSelected$ObjectLiteral(closure$list, closure$goods) {
    this.closure$list = closure$list;
    this.closure$goods = closure$goods;
    BaseScreen.call(this);
    this.bg_8be2vx$ = Util_getInstance().getFrameBitmap_vux9f0$((16 * 5 | 0) + 6 | 0, 6 + (16 * closure$list.size | 0) | 0);
    this.curSel_8be2vx$ = 0;
    var array = Array_0(closure$list.size);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = new Int8Array(11);
    }
    this.itemsText_8be2vx$ = array;
    var tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    tmp$_0 = get_indices_1(this.itemsText_8be2vx$);
    tmp$_1 = tmp$_0.first;
    tmp$_2 = tmp$_0.last;
    tmp$_3 = tmp$_0.step;
    for (var i_0 = tmp$_1; i_0 <= tmp$_2; i_0 += tmp$_3) {
      for (var j = 0; j <= 9; j++) {
        this.itemsText_8be2vx$[i_0][j] = toByte(32 | 0);
      }
      var tmp = gbkBytes(closure$list.get_za3lpa$(i_0).name);
      System_getInstance().arraycopy_nlwz52$(tmp, 0, this.itemsText_8be2vx$[i_0], 0, tmp.length);
    }
  }
  Object.defineProperty(ScreenMenuGoods$equipSelected$ObjectLiteral.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_ENTER) {
      if (this.closure$list.get_za3lpa$(this.curSel_8be2vx$).hasEquipt_vux9f0$(this.closure$goods.type, this.closure$goods.index)) {
        this.msgDelegate.showMessage_4wgjuj$('\u5DF2\u88C5\u5907!', Kotlin.Long.fromInt(1000));
      }
       else {
        this.delegate.popScreen();
        this.delegate.pushScreen_2o7n0o$(new ScreenChgEquipment(this.closure$list.get_za3lpa$(this.curSel_8be2vx$), Kotlin.isType(tmp$ = this.closure$goods, GoodsEquipment) ? tmp$ : throwCCE()));
      }
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN && this.curSel_8be2vx$ < (this.itemsText_8be2vx$.length - 1 | 0)) {
      this.curSel_8be2vx$ = this.curSel_8be2vx$ + 1 | 0;
    }
     else if (key === Global_getInstance().KEY_UP && this.curSel_8be2vx$ > 0) {
      this.curSel_8be2vx$ = this.curSel_8be2vx$ - 1 | 0;
    }
  };
  ScreenMenuGoods$equipSelected$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    canvas.drawBitmap_t8cslu$(this.bg_8be2vx$, 50, 14);
    tmp$ = get_indices_1(this.itemsText_8be2vx$);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      if (i !== this.curSel_8be2vx$) {
        TextRender_getInstance().drawText_pbrmiz$(canvas, this.itemsText_8be2vx$[i], 50 + 3 | 0, 14 + 3 + (16 * i | 0) | 0);
      }
       else {
        TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.itemsText_8be2vx$[i], 50 + 3 | 0, 14 + 3 + (16 * i | 0) | 0);
      }
    }
  };
  ScreenMenuGoods$equipSelected$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [BaseScreen]
  };
  ScreenMenuGoods.prototype.equipSelected_0 = function (goods) {
    var tmp$;
    var $receiver = ScreenMainGame$Companion_getInstance().sPlayerList;
    var destination = ArrayList_init();
    var tmp$_0;
    tmp$_0 = $receiver.iterator();
    while (tmp$_0.hasNext()) {
      var element = tmp$_0.next();
      if (goods.canPlayerUse_za3lpa$(element.index))
        destination.add_11rb$(element);
    }
    var list = destination;
    if (list.isEmpty()) {
      this.msgDelegate.showMessage_4wgjuj$('\u4E0D\u80FD\u88C5\u5907!', Kotlin.Long.fromInt(1000));
    }
     else if (list.size === 1) {
      if (list.get_za3lpa$(0).hasEquipt_vux9f0$(goods.type, goods.index)) {
        this.msgDelegate.showMessage_4wgjuj$('\u5DF2\u88C5\u5907!', Kotlin.Long.fromInt(1000));
      }
       else {
        this.delegate.pushScreen_2o7n0o$(new ScreenChgEquipment(list.get_za3lpa$(0), Kotlin.isType(tmp$ = goods, GoodsEquipment) ? tmp$ : throwCCE()));
      }
    }
     else {
      this.delegate.pushScreen_2o7n0o$(new ScreenMenuGoods$equipSelected$ObjectLiteral(list, goods));
    }
  };
  ScreenMenuGoods.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMenuGoods',
    interfaces: [ScreenGoodsList$OnItemSelectedListener, BaseScreen]
  };
  function ScreenMenuProperties() {
    BaseScreen.call(this);
    this.mFrameBmp_0 = Util_getInstance().getFrameBitmap_vux9f0$(77 - 39 + 1 | 0, 54 - 16 + 1 | 0);
    this.strs_0 = ['\u72B6\u6001', '\u7A7F\u6234'];
    this.mSelId_0 = 0;
  }
  Object.defineProperty(ScreenMenuProperties.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenMenuProperties.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMenuProperties.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.mFrameBmp_0, 39, 16);
    if (this.mSelId_0 === 0) {
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.strs_0[0], 39 + 3 | 0, 16 + 3 | 0);
      TextRender_getInstance().drawText_kkuqvh$(canvas, this.strs_0[1], 39 + 3 | 0, 16 + 3 + 16 | 0);
    }
     else if (this.mSelId_0 === 1) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, this.strs_0[0], 39 + 3 | 0, 16 + 3 | 0);
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.strs_0[1], 39 + 3 | 0, 16 + 3 + 16 | 0);
    }
  };
  ScreenMenuProperties.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP || key === Global_getInstance().KEY_DOWN) {
      this.mSelId_0 = 1 - this.mSelId_0 | 0;
    }
  };
  ScreenMenuProperties.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      this.delegate.popScreen();
      if (this.mSelId_0 === 0) {
        this.delegate.pushScreen_2o7n0o$(new ScreenActorState());
      }
       else {
        this.delegate.pushScreen_2o7n0o$(new ScreenActorWearing());
      }
    }
  };
  ScreenMenuProperties.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMenuProperties',
    interfaces: [BaseScreen]
  };
  function ScreenMenuSystem() {
    BaseScreen.call(this);
    this.first_0 = 0;
    this.index_0 = 0;
    this.str_0 = ['\u8BFB\u5165\u8FDB\u5EA6', '\u5B58\u50A8\u8FDB\u5EA6', '\u6E38\u620F\u8BBE\u7F6E', '\u7ED3\u675F\u6E38\u620F'];
    this.strX_0 = 42;
    this.strY_0 = 32;
    this.selY_0 = this.strY_0;
    this.bmpFrame_0 = Util_getInstance().getFrameBitmap_vux9f0$(109 - 39 + 1 | 0, 91 - 29 + 1 | 0);
    this.bmpArrowUp_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(7, 4);
    this.bmpArrowDown_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(7, 4);
    this.bmpArr_0 = [this.bmpArrowDown_0, this.bmpArrowUp_0];
    this.arrowX_0 = 70;
    this.arrowY_0 = 82;
    this.bmpi_0 = 0;
    var p = new Paint();
    p.color = Global_getInstance().COLOR_BLACK;
    var c = new Canvas(this.bmpArrowUp_0);
    c.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    c.drawLine_x3aj6j$(3, 0, 4, 0, p);
    c.drawLine_x3aj6j$(2, 1, 5, 1, p);
    c.drawLine_x3aj6j$(1, 2, 6, 2, p);
    c.drawLine_x3aj6j$(0, 3, 7, 3, p);
    c.setBitmap_963ehe$(this.bmpArrowDown_0);
    c.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    c.drawLine_x3aj6j$(0, 0, 7, 0, p);
    c.drawLine_x3aj6j$(1, 1, 6, 1, p);
    c.drawLine_x3aj6j$(2, 2, 5, 2, p);
    c.drawLine_x3aj6j$(3, 3, 4, 3, p);
  }
  Object.defineProperty(ScreenMenuSystem.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenMenuSystem.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMenuSystem.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.bmpFrame_0, 39, 29);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.str_0[this.first_0], this.strX_0, this.strY_0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.str_0[this.first_0 + 1 | 0], this.strX_0, this.strY_0 + 16 | 0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.str_0[this.first_0 + 2 | 0], this.strX_0, this.strY_0 + 32 | 0);
    TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.str_0[this.index_0], this.strX_0, this.selY_0);
    canvas.drawBitmap_t8cslu$(this.bmpArr_0[this.bmpi_0], this.arrowX_0, this.arrowY_0);
  };
  ScreenMenuSystem.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP) {
      this.index_0 = this.index_0 - 1 | 0;
      this.selY_0 = this.selY_0 - 16 | 0;
    }
     else if (key === Global_getInstance().KEY_DOWN) {
      this.index_0 = this.index_0 + 1 | 0;
      this.selY_0 = this.selY_0 + 16 | 0;
    }
    if (this.index_0 === 0 || this.index_0 === 4) {
      this.index_0 = 0;
      this.selY_0 = 32;
      this.arrowY_0 = 82;
      this.bmpi_0 = 0;
      this.first_0 = 0;
      this.strY_0 = 32;
    }
     else if (this.index_0 === 3 || this.index_0 === -1) {
      this.index_0 = 3;
      this.selY_0 = 72;
      this.arrowY_0 = 34;
      this.bmpi_0 = 1;
      this.first_0 = 1;
      this.strY_0 = 40;
    }
  };
  ScreenMenuSystem.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      tmp$ = this.index_0;
      if (tmp$ === 0)
        this.delegate.pushScreen_2o7n0o$(new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$LOAD_getInstance()));
      else if (tmp$ === 1)
        if (Global_getInstance().disableSave)
          this.delegate.showMessage_61zpoe$('\u5F53\u524D\u4E0D\u80FD\u5B58\u6863');
        else
          this.delegate.pushScreen_2o7n0o$(new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$SAVE_getInstance()));
      else if (tmp$ !== 2)
        if (tmp$ === 3)
          this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
    }
  };
  ScreenMenuSystem.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMenuSystem',
    interfaces: [BaseScreen]
  };
  function ScreenTakeMedicine(mMedicine) {
    BaseScreen.call(this);
    this.mMedicine_0 = mMedicine;
    this.mStatePageIndex_0 = 0;
    this.mActorIndex_0 = 0;
  }
  ScreenTakeMedicine.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenTakeMedicine.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0).drawState_86va19$(canvas, this.mStatePageIndex_0);
    ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0).drawHead_2g4tob$(canvas, 5, 60);
    if (this.mMedicine_0.goodsNum > 0) {
      this.mMedicine_0.draw_2g4tob$(canvas, 5, 10);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '' + toString(this.mMedicine_0.goodsNum), 13, 35);
    }
  };
  ScreenTakeMedicine.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_PAGEDOWN) {
      this.mStatePageIndex_0 = 1;
    }
     else if (key === Global_getInstance().KEY_PAGEUP) {
      this.mStatePageIndex_0 = 0;
    }
     else if (key === Global_getInstance().KEY_LEFT && this.mActorIndex_0 > 0) {
      this.mActorIndex_0 = this.mActorIndex_0 - 1 | 0;
    }
     else if (key === Global_getInstance().KEY_RIGHT && this.mActorIndex_0 < (ScreenMainGame$Companion_getInstance().sPlayerList.size - 1 | 0)) {
      this.mActorIndex_0 = this.mActorIndex_0 + 1 | 0;
    }
  };
  ScreenTakeMedicine.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      if (this.mMedicine_0.goodsNum > 0) {
        if (this.mMedicine_0.type === 9 && (Kotlin.isType(tmp$ = this.mMedicine_0, GoodsMedicine) ? tmp$ : throwCCE()).effectAll()) {
          tmp$_0 = reversed(get_indices(ScreenMainGame$Companion_getInstance().sPlayerList)).iterator();
          while (tmp$_0.hasNext()) {
            var i = tmp$_0.next();
            (Kotlin.isType(tmp$_1 = this.mMedicine_0, IEatMedicine) ? tmp$_1 : throwCCE()).eat_xa4yhy$(ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(i));
          }
        }
         else {
          (Kotlin.isType(tmp$_2 = this.mMedicine_0, IEatMedicine) ? tmp$_2 : throwCCE()).eat_xa4yhy$(ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mActorIndex_0));
        }
      }
       else {
        this.delegate.popScreen();
      }
    }
  };
  ScreenTakeMedicine.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenTakeMedicine',
    interfaces: [BaseScreen]
  };
  function ScreenUseMagic(mMagic, mScr) {
    ScreenUseMagic$Companion_getInstance();
    BaseScreen.call(this);
    this.mMagic_0 = mMagic;
    this.mScr_0 = mScr;
    this.mCurPage_0 = 0;
    this.mCurActor_0 = 0;
  }
  ScreenUseMagic.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenUseMagic.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    TextRender_getInstance().drawText_8xt01w$(canvas, this.mMagic_0.magicName, 0, ScreenUseMagic$Companion_getInstance().sNameRect_0);
    var actor = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mCurActor_0);
    actor.drawState_86va19$(canvas, this.mCurPage_0);
    actor.drawHead_2g4tob$(canvas, 5, 60);
  };
  ScreenUseMagic.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_RIGHT && this.mCurActor_0 < (ScreenMainGame$Companion_getInstance().sPlayerList.size - 1 | 0)) {
      this.mCurActor_0 = this.mCurActor_0 + 1 | 0;
    }
     else if (key === Global_getInstance().KEY_LEFT && this.mCurActor_0 > 0) {
      this.mCurActor_0 = this.mCurActor_0 - 1 | 0;
    }
     else if (key === Global_getInstance().KEY_PAGEDOWN || key === Global_getInstance().KEY_PAGEUP) {
      this.mCurPage_0 = 1 - this.mCurPage_0 | 0;
    }
  };
  ScreenUseMagic.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      this.mMagic_0.use_wd4e0$(this.mScr_0, ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(this.mCurActor_0));
      this.delegate.popScreen();
    }
  };
  function ScreenUseMagic$Companion() {
    ScreenUseMagic$Companion_instance = this;
    this.sNameRect_0 = new Rect(4, 4, 37, 96);
  }
  ScreenUseMagic$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenUseMagic$Companion_instance = null;
  function ScreenUseMagic$Companion_getInstance() {
    if (ScreenUseMagic$Companion_instance === null) {
      new ScreenUseMagic$Companion();
    }
    return ScreenUseMagic$Companion_instance;
  }
  ScreenUseMagic.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenUseMagic',
    interfaces: [BaseScreen]
  };
  function BaseGoods() {
    ResBase.call(this);
    this.mEnable_799bpm$_0 = 0;
    this.sumRound_y71hmz$_0 = 0;
    this.mImage_na5mo$_0 = null;
    this.name_dx74sj$_0 = '';
    this.buyPrice_79wyzv$_0 = 0;
    this.sellPrice_tlp66n$_0 = 0;
    this.description_yw9j1y$_0 = '';
    this.eventId_jwk35b$_0 = 0;
    this.goodsNum = 0;
  }
  Object.defineProperty(BaseGoods.prototype, 'sumRound', {
    get: function () {
      return this.sumRound_y71hmz$_0;
    },
    set: function (sumRound) {
      this.sumRound_y71hmz$_0 = sumRound;
    }
  });
  Object.defineProperty(BaseGoods.prototype, 'name', {
    get: function () {
      return this.name_dx74sj$_0;
    },
    set: function (name) {
      this.name_dx74sj$_0 = name;
    }
  });
  Object.defineProperty(BaseGoods.prototype, 'buyPrice', {
    get: function () {
      return this.buyPrice_79wyzv$_0;
    },
    set: function (buyPrice) {
      this.buyPrice_79wyzv$_0 = buyPrice;
    }
  });
  Object.defineProperty(BaseGoods.prototype, 'sellPrice', {
    get: function () {
      return this.sellPrice_tlp66n$_0;
    },
    set: function (sellPrice) {
      this.sellPrice_tlp66n$_0 = sellPrice;
    }
  });
  Object.defineProperty(BaseGoods.prototype, 'description', {
    get: function () {
      return this.description_yw9j1y$_0;
    },
    set: function (description) {
      this.description_yw9j1y$_0 = description;
    }
  });
  Object.defineProperty(BaseGoods.prototype, 'eventId', {
    get: function () {
      return this.eventId_jwk35b$_0;
    },
    set: function (eventId) {
      this.eventId_jwk35b$_0 = eventId;
    }
  });
  BaseGoods.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.mEnable_799bpm$_0 = buf[offset + 3 | 0] & 255;
    this.sumRound = buf[offset + 4 | 0] & 255;
    this.mImage_na5mo$_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GDP_getInstance(), this.type, buf[offset + 5 | 0] & 255), ResImage) ? tmp$ : throwCCE();
    this.name = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 6 | 0);
    this.buyPrice = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 18 | 0);
    this.sellPrice = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 20 | 0);
    this.description = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 30 | 0);
    this.eventId = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 132 | 0);
    this.setOtherData_ir89t6$(buf, offset);
  };
  BaseGoods.prototype.canPlayerUse_za3lpa$ = function (playId) {
    var tmp$;
    if ((new IntRange(1, 4)).contains_mef7kx$(playId)) {
      tmp$ = (this.mEnable_799bpm$_0 & 1 << playId - 1) !== 0;
    }
     else
      tmp$ = false;
    return tmp$;
  };
  BaseGoods.prototype.draw_2g4tob$ = function (canvas, x, y) {
    ensureNotNull(this.mImage_na5mo$_0).draw_tj1hu5$(canvas, 1, x, y);
  };
  BaseGoods.prototype.addGoodsNum_za3lpa$ = function (d) {
    this.goodsNum = this.goodsNum + d | 0;
  };
  BaseGoods.prototype.effectAll = function () {
    return false;
  };
  BaseGoods.prototype.equals = function (other) {
    var tmp$;
    return this.type === (Kotlin.isType(tmp$ = other, BaseGoods) ? tmp$ : throwCCE()).type && this.index === other.index;
  };
  BaseGoods.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BaseGoods',
    interfaces: [ResBase]
  };
  function GoodsDecorations() {
    GoodsEquipment.call(this);
    this.mMp_0 = 0;
    this.mHp_0 = 0;
    this.mMagic_0 = 0;
  }
  Object.defineProperty(GoodsDecorations.prototype, 'coopMagic', {
    get: function () {
      var tmp$;
      return Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MRS_getInstance(), 1, this.mMagic_0), MagicAttack) ? tmp$ : throwCCE();
    }
  });
  GoodsDecorations.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mMp_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 22 | 0);
    this.mHp_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 23 | 0);
    this.mdf = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 24 | 0);
    this.mat = buf[offset + 25 | 0] & 255;
    this.mlingli = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 26 | 0);
    this.mSpeed = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 27 | 0);
    this.mMagic_0 = buf[offset + 28 | 0] & 255;
    this.mLuck = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 29 | 0);
  };
  GoodsDecorations.prototype.putOn_xa4yhy$ = function (p) {
    GoodsEquipment.prototype.putOn_xa4yhy$.call(this, p);
  };
  GoodsDecorations.prototype.takeOff_xa4yhy$ = function (p) {
    GoodsEquipment.prototype.takeOff_xa4yhy$.call(this, p);
  };
  GoodsDecorations.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsDecorations',
    interfaces: [GoodsEquipment]
  };
  function GoodsDrama() {
    BaseGoods.call(this);
  }
  GoodsDrama.prototype.setOtherData_ir89t6$ = function (buf, offset) {
  };
  GoodsDrama.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsDrama',
    interfaces: [BaseGoods]
  };
  function GoodsEquipment() {
    BaseGoods.call(this);
    this.mMpMax = 0;
    this.mHpMax = 0;
    this.mdf = 0;
    this.mat = 0;
    this.mlingli = 0;
    this.mSpeed = 0;
    this.mBitEffect = 0;
    this.mLuck = 0;
  }
  GoodsEquipment.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mMpMax = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 22 | 0);
    this.mHpMax = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 23 | 0);
    this.mdf = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 24 | 0);
    this.mat = buf[offset + 25 | 0] & 255;
    this.mlingli = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 26 | 0);
    this.mSpeed = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 27 | 0);
    this.mBitEffect = buf[offset + 28 | 0] & 255;
    this.mLuck = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 29 | 0);
  };
  GoodsEquipment.prototype.putOn_xa4yhy$ = function (p) {
    if (this.canPlayerUse_za3lpa$(p.index)) {
      p.maxMP = p.maxMP + this.mMpMax | 0;
      p.maxHP = p.maxHP + this.mHpMax | 0;
      p.defend = p.defend + this.mdf | 0;
      p.attack = p.attack + this.mat | 0;
      p.lingli = p.lingli + this.mlingli | 0;
      p.speed = p.speed + this.mSpeed | 0;
      if (!Kotlin.isType(this, GoodsWeapon)) {
        p.addBuff_vux9f0$(this.mBitEffect);
      }
      p.luck = p.luck + this.mLuck | 0;
      if (this.eventId !== 0) {
        ScriptResources_getInstance().setEvent_za3lpa$(this.eventId);
      }
    }
  };
  GoodsEquipment.prototype.takeOff_xa4yhy$ = function (p) {
    p.maxMP = p.maxMP - this.mMpMax | 0;
    p.maxHP = p.maxHP - this.mHpMax | 0;
    p.defend = p.defend - this.mdf | 0;
    p.attack = p.attack - this.mat | 0;
    p.lingli = p.lingli - this.mlingli | 0;
    p.speed = p.speed - this.mSpeed | 0;
    if (!Kotlin.isType(this, GoodsWeapon)) {
      p.delBuff_za3lpa$(this.mBitEffect);
    }
    p.luck = p.luck - this.mLuck | 0;
    if (this.eventId !== 0) {
      ScriptResources_getInstance().clearEvent_za3lpa$(this.eventId);
    }
  };
  GoodsEquipment.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsEquipment',
    interfaces: [BaseGoods]
  };
  function GoodsHiddenWeapon() {
    BaseGoods.call(this);
    this.affectHp_adfuug$_0 = 0;
    this.affectMp_adfyjx$_0 = 0;
    this.ani_o36ozx$_0 = null;
    this.mBitMask_0 = 0;
  }
  Object.defineProperty(GoodsHiddenWeapon.prototype, 'affectHp', {
    get: function () {
      return this.affectHp_adfuug$_0;
    },
    set: function (affectHp) {
      this.affectHp_adfuug$_0 = affectHp;
    }
  });
  Object.defineProperty(GoodsHiddenWeapon.prototype, 'affectMp', {
    get: function () {
      return this.affectMp_adfyjx$_0;
    },
    set: function (affectMp) {
      this.affectMp_adfyjx$_0 = affectMp;
    }
  });
  Object.defineProperty(GoodsHiddenWeapon.prototype, 'ani', {
    get: function () {
      return this.ani_o36ozx$_0;
    },
    set: function (ani) {
      this.ani_o36ozx$_0 = ani;
    }
  });
  GoodsHiddenWeapon.prototype.get2ByteSint_0 = function (buf, start) {
    var tmp$;
    var i = buf[start] & 255 | buf[start + 1 | 0] << 8 & 32512;
    if ((buf[start + 1 | 0] & 128) !== 0) {
      tmp$ = -i;
    }
     else
      tmp$ = i;
    return tmp$;
  };
  GoodsHiddenWeapon.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.affectHp = this.get2ByteSint_0(buf, offset + 22 | 0);
    this.affectMp = this.get2ByteSint_0(buf, offset + 24 | 0);
    var type = buf[offset + 27 | 0] & 255;
    var index = buf[offset + 26 | 0] & 255;
    if (type > 0 && index > 0) {
      this.ani = (tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), type, index)) == null || Kotlin.isType(tmp$, ResSrs) ? tmp$ : throwCCE();
    }
    this.mBitMask_0 = buf[offset + 28 | 0] & 255;
  };
  GoodsHiddenWeapon.prototype.effectAll = function () {
    return (this.mBitMask_0 & 16) !== 0;
  };
  GoodsHiddenWeapon.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsHiddenWeapon',
    interfaces: [BaseGoods]
  };
  function GoodsManage() {
    this.mGoodsList_8be2vx$ = ArrayList_init();
    this.mEquipList_8be2vx$ = ArrayList_init();
  }
  Object.defineProperty(GoodsManage.prototype, 'goodsList', {
    get: function () {
      return this.mGoodsList_8be2vx$;
    }
  });
  Object.defineProperty(GoodsManage.prototype, 'equipList', {
    get: function () {
      return this.mEquipList_8be2vx$;
    }
  });
  Object.defineProperty(GoodsManage.prototype, 'equitTypeNum', {
    get: function () {
      return this.mEquipList_8be2vx$.size;
    }
  });
  Object.defineProperty(GoodsManage.prototype, 'goodsTypeNum', {
    get: function () {
      return this.mGoodsList_8be2vx$.size;
    }
  });
  GoodsManage.prototype.getEquip_za3lpa$ = function (i) {
    var tmp$;
    if (i >= 0 && i < this.mEquipList_8be2vx$.size) {
      tmp$ = this.mEquipList_8be2vx$.get_za3lpa$(i);
    }
     else
      tmp$ = null;
    return tmp$;
  };
  GoodsManage.prototype.getGoods_za3lpa$ = function (i) {
    var tmp$;
    if (i >= 0 && i < this.mGoodsList_8be2vx$.size) {
      tmp$ = this.mGoodsList_8be2vx$.get_za3lpa$(i);
    }
     else
      tmp$ = null;
    return tmp$;
  };
  GoodsManage.prototype.getGoods_vux9f0$ = function (type, index) {
    if (type >= 1 && type <= 7) {
      return this.getGoods_0(this.mEquipList_8be2vx$, type, index);
    }
     else if (type >= 8 && type <= 14) {
      return this.getGoods_0(this.mGoodsList_8be2vx$, type, index);
    }
    return null;
  };
  GoodsManage.prototype.getGoods_0 = function (list, type, index) {
    var firstOrNull$result;
    firstOrNull$break: do {
      var tmp$;
      tmp$ = list.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        if (element.type === type && element.index === index) {
          firstOrNull$result = element;
          break firstOrNull$break;
        }
      }
      firstOrNull$result = null;
    }
     while (false);
    return firstOrNull$result;
  };
  GoodsManage.prototype.getGoodsNum_vux9f0$ = function (type, index) {
    var num = 0;
    if ((new IntRange(1, 7)).contains_mef7kx$(type)) {
      num = this.getGoodsNum_0(this.mEquipList_8be2vx$, type, index);
    }
     else if ((new IntRange(8, 14)).contains_mef7kx$(type)) {
      num = this.getGoodsNum_0(this.mGoodsList_8be2vx$, type, index);
    }
    return num;
  };
  GoodsManage.prototype.getGoodsNum_0 = function (list, type, index) {
    var tmp$, tmp$_0;
    var firstOrNull$result;
    firstOrNull$break: do {
      var tmp$_1;
      tmp$_1 = list.iterator();
      while (tmp$_1.hasNext()) {
        var element = tmp$_1.next();
        if (element.type === type && element.index === index) {
          firstOrNull$result = element;
          break firstOrNull$break;
        }
      }
      firstOrNull$result = null;
    }
     while (false);
    return (tmp$_0 = (tmp$ = firstOrNull$result) != null ? tmp$.goodsNum : null) != null ? tmp$_0 : 0;
  };
  GoodsManage.prototype.addGoods_qt1dr2$ = function (type, index, num) {
    if ((new IntRange(1, 7)).contains_mef7kx$(type)) {
      this.addGoods_0(this.mEquipList_8be2vx$, type, index, num);
    }
     else if ((new IntRange(8, 14)).contains_mef7kx$(type)) {
      this.addGoods_0(this.mGoodsList_8be2vx$, type, index, num);
    }
  };
  GoodsManage.prototype.addGoods_vux9f0$ = function (type, index) {
    if ((new IntRange(1, 7)).contains_mef7kx$(type)) {
      this.addGoods_0(this.mEquipList_8be2vx$, type, index, 1);
    }
     else if ((new IntRange(8, 14)).contains_mef7kx$(type)) {
      this.addGoods_0(this.mGoodsList_8be2vx$, type, index, 1);
    }
  };
  GoodsManage.prototype.addGoods_0 = function (list, type, index, num) {
    var tmp$;
    var tmp$_0;
    tmp$_0 = list.iterator();
    while (tmp$_0.hasNext()) {
      var element = tmp$_0.next();
      if (element.type === type && element.index === index) {
        element.addGoodsNum_za3lpa$(num);
        return;
      }
    }
    var item = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), type, index), BaseGoods) ? tmp$ : throwCCE();
    item.goodsNum = num;
    list.add_11rb$(item);
  };
  GoodsManage.prototype.deleteGoods_vux9f0$ = function (type, index) {
    return this.useGoodsNum_qt1dr2$(type, index, 1);
  };
  GoodsManage.prototype.useGoodsNum_qt1dr2$ = function (type, index, num) {
    if ((new IntRange(1, 7)).contains_mef7kx$(type)) {
      return this.deleteGoods_0(this.mEquipList_8be2vx$, type, index, num);
    }
     else if ((new IntRange(8, 14)).contains_mef7kx$(type)) {
      return this.deleteGoods_0(this.mGoodsList_8be2vx$, type, index, num);
    }
    return false;
  };
  GoodsManage.prototype.deleteGoods_0 = function (list, type, index, num) {
    var iter = list.iterator();
    while (iter.hasNext()) {
      var i = iter.next();
      if (i.type === type && i.index === index) {
        if (i.goodsNum < num) {
          return false;
        }
        i.addGoodsNum_za3lpa$(-num);
        if (i.goodsNum <= 0) {
          iter.remove();
        }
        return true;
      }
    }
    return false;
  };
  GoodsManage.prototype.clear = function () {
    this.mEquipList_8be2vx$.clear();
    this.mGoodsList_8be2vx$.clear();
  };
  GoodsManage.prototype.read_setnfj$ = function (coder) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    this.clear();
    var size = coder.readInt();
    tmp$ = size;
    for (var i = 0; i < tmp$; i++) {
      var g = Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), coder.readInt(), coder.readInt()), BaseGoods) ? tmp$_0 : throwCCE();
      g.goodsNum = coder.readInt();
      this.mEquipList_8be2vx$.add_11rb$(g);
    }
    size = coder.readInt();
    tmp$_1 = size;
    for (var i_0 = 0; i_0 < tmp$_1; i_0++) {
      var g_0 = Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), coder.readInt(), coder.readInt()), BaseGoods) ? tmp$_2 : throwCCE();
      g_0.goodsNum = coder.readInt();
      this.mGoodsList_8be2vx$.add_11rb$(g_0);
    }
  };
  GoodsManage.prototype.write_vcd9jg$ = function (out) {
    var tmp$, tmp$_0;
    var size = this.mEquipList_8be2vx$.size;
    out.writeInt_za3lpa$(size);
    tmp$ = size;
    for (var i = 0; i < tmp$; i++) {
      var g = this.mEquipList_8be2vx$.get_za3lpa$(i);
      out.writeInt_za3lpa$(g.type);
      out.writeInt_za3lpa$(g.index);
      out.writeInt_za3lpa$(g.goodsNum);
    }
    size = this.mGoodsList_8be2vx$.size;
    out.writeInt_za3lpa$(size);
    tmp$_0 = size;
    for (var i_0 = 0; i_0 < tmp$_0; i_0++) {
      var g_0 = this.mGoodsList_8be2vx$.get_za3lpa$(i_0);
      out.writeInt_za3lpa$(g_0.type);
      out.writeInt_za3lpa$(g_0.index);
      out.writeInt_za3lpa$(g_0.goodsNum);
    }
  };
  GoodsManage.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsManage',
    interfaces: []
  };
  function GoodsMedicine() {
    BaseGoods.call(this);
    this.mHp_0 = 0;
    this.mMp_0 = 0;
    this.ani_3cznkv$_0 = null;
    this.mBitMask_0 = 0;
  }
  Object.defineProperty(GoodsMedicine.prototype, 'ani', {
    get: function () {
      return this.ani_3cznkv$_0;
    },
    set: function (ani) {
      this.ani_3cznkv$_0 = ani;
    }
  });
  GoodsMedicine.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.mHp_0 = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 22 | 0);
    this.mMp_0 = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 24 | 0);
    var index = buf[offset + 26 | 0] & 255;
    if (index > 0) {
      this.ani = (tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 2, index)) == null || Kotlin.isType(tmp$, ResSrs) ? tmp$ : throwCCE();
    }
    this.mBitMask_0 = buf[offset + 28 | 0] & 255;
  };
  GoodsMedicine.prototype.eat_xa4yhy$ = function (player) {
    player.hp = player.hp + this.mHp_0 | 0;
    if (player.hp > player.maxHP) {
      player.hp = player.maxHP;
    }
    player.mp = player.mp + this.mMp_0 | 0;
    if (player.mp > player.maxMP) {
      player.mp = player.maxMP;
    }
    player.delDebuff_za3lpa$(this.mBitMask_0);
    Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(this.type, this.index);
  };
  GoodsMedicine.prototype.effectAll = function () {
    return (this.mBitMask_0 & 16) !== 0;
  };
  GoodsMedicine.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsMedicine',
    interfaces: [IEatMedicine, BaseGoods]
  };
  function GoodsMedicineChg4Ever() {
    BaseGoods.call(this);
    this.mMpMax_0 = 0;
    this.mHpMax_0 = 0;
    this.mdf_0 = 0;
    this.mat_0 = 0;
    this.mling_0 = 0;
    this.mSpeed_0 = 0;
    this.mLuck_0 = 0;
  }
  GoodsMedicineChg4Ever.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mMpMax_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 22 | 0);
    this.mHpMax_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 23 | 0);
    this.mdf_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 24 | 0);
    this.mat_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 25 | 0);
    this.mling_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 26 | 0);
    this.mSpeed_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 27 | 0);
    this.mLuck_0 = ResBase$Companion_getInstance().get1ByteSInt_ir89t6$(buf, offset + 29 | 0);
  };
  GoodsMedicineChg4Ever.prototype.eat_xa4yhy$ = function (player) {
    player.maxMP = player.maxMP + this.mMpMax_0 | 0;
    player.maxHP = player.maxHP + this.mHpMax_0 | 0;
    player.defend = player.defend + this.mdf_0 | 0;
    player.attack = player.attack + this.mat_0 | 0;
    player.lingli = player.lingli + this.mling_0 | 0;
    player.speed = player.speed + this.mSpeed_0 | 0;
    player.luck = player.luck + this.mLuck_0 | 0;
    Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(this.type, this.index);
  };
  GoodsMedicineChg4Ever.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsMedicineChg4Ever',
    interfaces: [IEatMedicine, BaseGoods]
  };
  function GoodsMedicineLife() {
    BaseGoods.call(this);
    this.mPercent_0 = 0;
  }
  GoodsMedicineLife.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mPercent_0 = buf[offset + 23 | 0] & 255;
    if (this.mPercent_0 > 100) {
      this.mPercent_0 = 100;
    }
  };
  GoodsMedicineLife.prototype.eat_xa4yhy$ = function (player) {
    player.mp = player.mp + (Kotlin.imul(player.maxMP, this.mPercent_0) / 100 | 0) | 0;
    if (player.mp > player.maxMP) {
      player.mp = player.maxMP;
    }
    Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(this.type, this.index);
  };
  GoodsMedicineLife.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsMedicineLife',
    interfaces: [IEatMedicine, BaseGoods]
  };
  function GoodsStimulant() {
    BaseGoods.call(this);
    this.mdfPercent_0 = 0;
    this.matPercent_0 = 0;
    this.mSpeedPercent_0 = 0;
    this.mForAll_0 = false;
  }
  GoodsStimulant.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mdfPercent_0 = buf[offset + 24 | 0] & 255;
    this.matPercent_0 = buf[offset + 25 | 0] & 255;
    this.mSpeedPercent_0 = buf[offset + 27 | 0] & 255;
    this.mForAll_0 = (buf[offset + 28 | 0] & 16) !== 0;
  };
  GoodsStimulant.prototype.effectAll = function () {
    return this.mForAll_0;
  };
  GoodsStimulant.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsStimulant',
    interfaces: [BaseGoods]
  };
  function GoodsTudun() {
    BaseGoods.call(this);
  }
  GoodsTudun.prototype.setOtherData_ir89t6$ = function (buf, offset) {
  };
  GoodsTudun.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsTudun',
    interfaces: [BaseGoods]
  };
  function GoodsWeapon() {
    GoodsEquipment.call(this);
  }
  GoodsWeapon.prototype.putOn_xa4yhy$ = function (p) {
    GoodsEquipment.prototype.putOn_xa4yhy$.call(this, p);
    p.addAtbuff_vux9f0$(this.mBitEffect, this.sumRound);
  };
  GoodsWeapon.prototype.takeOff_xa4yhy$ = function (p) {
    GoodsEquipment.prototype.takeOff_xa4yhy$.call(this, p);
    p.delAtbuff_za3lpa$(this.mBitEffect);
  };
  GoodsWeapon.prototype.attackAll = function () {
    return (this.mBitEffect & 16) !== 0;
  };
  GoodsWeapon.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GoodsWeapon',
    interfaces: [GoodsEquipment]
  };
  function IEatMedicine() {
  }
  IEatMedicine.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'IEatMedicine',
    interfaces: []
  };
  function TextRender() {
    TextRender_instance = this;
    this.mHZKBuf_0 = File$Companion_getInstance().contentsOf_61zpoe$('HZK16');
    this.mASCBuf_0 = File$Companion_getInstance().contentsOf_61zpoe$('ASC16');
    var array = Array_0(16 * 16 | 0);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = Color$Companion_getInstance().WHITE;
    }
    this.mPixels_0 = array;
    this.mBmpHzk_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(16, 16);
    this.mBmpAsc_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(8, 16);
  }
  TextRender.prototype.drawText_kkuqvh$ = function (canvas, text, x, y) {
    this.drawText_pbrmiz$(canvas, gbkBytes(text), x, y);
  };
  TextRender.prototype.drawSelText_kkuqvh$ = function (canvas, text, x, y) {
    this.drawSelText_pbrmiz$(canvas, gbkBytes(text), x, y);
  };
  TextRender.prototype.drawSelText_pbrmiz$ = function (canvas, text, x, y) {
    Global_getInstance().fgColor = Global_getInstance().COLOR_WHITE;
    Global_getInstance().bgColor = Global_getInstance().COLOR_BLACK;
    this.drawText_pbrmiz$(canvas, text, x, y);
    Global_getInstance().fgColor = Global_getInstance().COLOR_BLACK;
    Global_getInstance().bgColor = Global_getInstance().COLOR_WHITE;
  };
  TextRender.prototype.drawText_pbrmiz$ = function (canvas, text, x, y) {
    var x0 = x;
    var i = 0;
    while (i < text.length && text[i] !== 0) {
      var t = text[i] & 255;
      if (t >= 161) {
        i = i + 1 | 0;
        var offset = ((94 * (t - 161 | 0) | 0) + (text[i] & 255) - 161 | 0) * 32 | 0;
        canvas.drawBitmap_t8cslu$(this.getHzk_0(offset), x0, y);
        x0 = x0 + 16 | 0;
      }
       else if (t < 128) {
        var offset_0 = t * 16 | 0;
        canvas.drawBitmap_t8cslu$(this.getAsc_0(offset_0), x0, y);
        x0 = x0 + 8 | 0;
      }
       else {
        x0 = x0 + 8 | 0;
      }
      i = i + 1 | 0;
    }
  };
  TextRender.prototype.drawText_3oojmu$ = function (canvas, text, r, y) {
    return this.drawText_sfexxe$(canvas, gbkBytes(text), r, y);
  };
  TextRender.prototype.drawText_sfexxe$ = function (canvas, buf, r, y) {
    var tmp$;
    var tmpY = y;
    var i = 0;
    while (tmpY <= (r.top - 16 | 0) && i < buf.length) {
      var tmpX = 0;
      while (tmpX < 160 && i < buf.length) {
        var t = buf[i] & 255;
        if (t >= 161) {
          i = i + 2 | 0;
          tmpX = tmpX + 16 | 0;
        }
         else {
          i = i + 1 | 0;
          tmpX = tmpX + 8 | 0;
        }
      }
      tmpY = tmpY + 16 | 0;
    }
    if (i >= buf.length) {
      return 0;
    }
    while (tmpY < r.bottom && i < buf.length) {
      var tmpX_0 = 0;
      while (tmpX_0 < 160 && i < buf.length) {
        var t_0 = buf[i] & 255;
        if (t_0 >= 161) {
          i = i + 1 | 0;
          var offset = ((94 * (t_0 - 161 | 0) | 0) + (buf[i] & 255) - 161 | 0) * 32 | 0;
          canvas.drawBitmap_t8cslu$(this.getHzk_0(offset), tmpX_0, tmpY);
          tmpX_0 = tmpX_0 + 16 | 0;
        }
         else if (t_0 < 128) {
          var offset_0 = t_0 * 16 | 0;
          canvas.drawBitmap_t8cslu$(this.getAsc_0(offset_0), tmpX_0, tmpY);
          tmpX_0 = tmpX_0 + 8 | 0;
        }
         else {
          tmpX_0 = tmpX_0 + 8 | 0;
        }
        i = i + 1 | 0;
      }
      tmpY = tmpY + 16 | 0;
    }
    var tmp$_0 = i === 0;
    if (tmp$_0) {
      tmp$_0 = !(buf.length === 0);
    }
    if (tmp$_0) {
      tmp$ = 2;
    }
     else
      tmp$ = 1;
    return tmp$;
  };
  TextRender.prototype.drawText_8xt01w$ = function (canvas, text, start, r) {
    return this.drawText_tz7kd0$(canvas, gbkBytes(text), start, r);
  };
  TextRender.prototype.drawText_tz7kd0$ = function (canvas, buf, start, r) {
    var i = start;
    var y = r.top;
    while (y <= (r.bottom - 16 | 0) && i < buf.length) {
      var x = r.left;
      while (x <= (r.right - 16 | 0) && i < buf.length) {
        var t = buf[i] & 255;
        if (t >= 161) {
          i = i + 1 | 0;
          var offset = ((94 * (t - 161 | 0) | 0) + (buf[i] & 255) - 161 | 0) * 32 | 0;
          canvas.drawBitmap_t8cslu$(this.getHzk_0(offset), x, y);
          x = x + 16 | 0;
        }
         else if (t < 128) {
          var offset_0 = t * 16 | 0;
          canvas.drawBitmap_t8cslu$(this.getAsc_0(offset_0), x, y);
          x = x + 8 | 0;
        }
         else {
          x = x + 8 | 0;
        }
        i = i + 1 | 0;
      }
      y = y + 16 | 0;
    }
    return i;
  };
  TextRender.prototype.getHzk_0 = function (offset) {
    for (var i = 0; i <= 31; i++) {
      var t = this.mHZKBuf_0[offset + i | 0];
      var k = i << 3;
      this.mPixels_0[k] = (t & 128) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 1] = (t & 64) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 2] = (t & 32) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 3] = (t & 16) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 4] = (t & 8) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 5] = (t & 4) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 6] = (t & 2) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 7] = (t & 1) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
    }
    this.mBmpHzk_0.setPixels_khhbfk$(this.mPixels_0, 0, 0, 0, 16, 16);
    return this.mBmpHzk_0;
  };
  TextRender.prototype.getAsc_0 = function (offset) {
    for (var i = 0; i <= 15; i++) {
      var t = this.mASCBuf_0[offset + i | 0];
      var k = i << 3;
      this.mPixels_0[k] = (t & 128) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 1] = (t & 64) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 2] = (t & 32) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 3] = (t & 16) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 4] = (t & 8) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 5] = (t & 4) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 6] = (t & 2) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
      this.mPixels_0[k | 7] = (t & 1) !== 0 ? Global_getInstance().fgColor : Global_getInstance().bgColor;
    }
    this.mBmpAsc_0.setPixels_khhbfk$(this.mPixels_0, 0, 0, 0, 8, 16);
    return this.mBmpAsc_0;
  };
  TextRender.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'TextRender',
    interfaces: []
  };
  var TextRender_instance = null;
  function TextRender_getInstance() {
    if (TextRender_instance === null) {
      new TextRender();
    }
    return TextRender_instance;
  }
  function Tiles(index) {
    Tiles$Companion_getInstance();
    var tmp$;
    this.mTileRes_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$TIL_getInstance(), 1, index), ResImage) ? tmp$ : throwCCE();
  }
  Tiles.prototype.draw_tj1hu5$ = function (canvas, x, y, i) {
    this.mTileRes_0.draw_tj1hu5$(canvas, i + 1 | 0, x, y);
  };
  function Tiles$Companion() {
    Tiles$Companion_instance = this;
    this.WIDTH = 16;
    this.HEIGHT = 16;
  }
  Tiles$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Tiles$Companion_instance = null;
  function Tiles$Companion_getInstance() {
    if (Tiles$Companion_instance === null) {
      new Tiles$Companion();
    }
    return Tiles$Companion_instance;
  }
  Tiles.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Tiles',
    interfaces: []
  };
  function Util() {
    Util_instance = this;
    this.bmpInformationBg_0 = null;
    this.bmpSideFrame_0 = null;
    this.drawFramePaint_0 = new Paint();
    this.bmpTriangleCursor_0 = null;
    this.imgSmallNum_0 = null;
    this.bmpChuandai = null;
    this.sBlackPaint = null;
    var tmp$;
    var canvas = Canvas_init();
    var paint = new Paint();
    paint.color = Global_getInstance().COLOR_WHITE;
    paint.style = Paint$Style$FILL_AND_STROKE_getInstance();
    var ind = {v: 0};
    var array = Array_0(5);
    var tmp$_0;
    tmp$_0 = array.length - 1 | 0;
    for (var i = 0; i <= tmp$_0; i++) {
      var bmp = Bitmap$Companion_getInstance().createBitmap_vux9f0$(138, 23 + (16 * ind.v | 0) | 0);
      canvas.setBitmap_963ehe$(bmp);
      canvas.drawColor_we4i00$(Global_getInstance().COLOR_BLACK);
      canvas.drawRect_x3aj6j$(1, 1, 135, 20 + (16 * ind.v | 0) | 0, paint);
      canvas.drawRect_x3aj6j$(136, 0, 138, 3, paint);
      canvas.drawLine_x3aj6j$(0, 21 + (16 * ind.v | 0) | 0, 3, 21 + (16 * ind.v | 0) | 0, paint);
      canvas.drawLine_x3aj6j$(0, 22 + (16 * ind.v | 0) | 0, 3, 22 + (16 * ind.v | 0) | 0, paint);
      ind.v = ind.v + 1 | 0;
      array[i] = bmp;
    }
    this.bmpInformationBg_0 = array;
    this.bmpSideFrame_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(8, 96);
    canvas.setBitmap_963ehe$(this.bmpSideFrame_0);
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    paint.color = Global_getInstance().COLOR_BLACK;
    for (var i_0 = 0; i_0 <= 3; i_0++) {
      canvas.drawLine_x3aj6j$(i_0 * 2 | 0, 0, i_0 * 2 | 0, 96, paint);
    }
    this.bmpTriangleCursor_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(7, 13);
    canvas.setBitmap_963ehe$(this.bmpTriangleCursor_0);
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    for (var i_1 = 0; i_1 <= 6; i_1++) {
      canvas.drawLine_x3aj6j$(i_1, i_1, i_1, 13 - i_1 | 0, paint);
    }
    this.imgSmallNum_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 5), ResImage) ? tmp$ : throwCCE();
    this.bmpChuandai = Bitmap$Companion_getInstance().createBitmap_vux9f0$(22, 39);
    var b = Global_getInstance().COLOR_BLACK;
    var w = Global_getInstance().COLOR_WHITE;
    var pixels = [w, w, w, w, w, w, w, w, w, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, b, b, b, b, b, w, w, w, w, w, w, w, w, b, b, b, b, b, w, w, w, w, b, b, w, w, w, b, b, b, w, w, b, b, b, w, w, w, b, b, w, w, w, w, w, b, w, w, b, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w, w, w, w, b, w, w, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, b, b, w, w, w, b, b, b, b, w, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, w, w, w, b, b, b, w, w, w, w, w, w, w, b, b, b, b, w, w, b, b, b, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, w, w, w, w, b, b, b, w, w, w, w, w, w, w, w, b, b, b, b, w, w, w, w, w, w, w, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, b, b, w, w, w, b, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, b, b, w, w, b, b, w, w, w, w, w, w, w, b, b, w, w, b, w, w, w, w, b, b, w, b, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, b, b, w, b, b, w, w, w, w, w, w, w, w, w, b, b, w, b, b, b, b, w, b, b, w, b, b, w, w, w, w, w, w, w, w, w, b, w, b, b, w, w, b, w, w, b, w, b, b, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, w, b, b, b, b, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, w, b, b, b, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, w, w, w, w, b, b, b, w, w, w, w, w, w, b, b, b, b, b, w, w, w, w, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, b, b, b, w, w, b, b, w, b, b, w, w, b, b, b, b, b, b, b, w, w, b, b, w, w, w, w, w, w, b, w, w, w, w, w, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w];
    this.bmpChuandai.setPixels_khhbfk$(pixels, 0, 0, 0, 22, 39);
    this.drawFramePaint_0.color = Global_getInstance().COLOR_BLACK;
    this.drawFramePaint_0.style = Paint$Style$STROKE_getInstance();
    this.sBlackPaint = new Paint();
    this.sBlackPaint.color = Global_getInstance().COLOR_BLACK;
    this.sBlackPaint.style = Paint$Style$STROKE_getInstance();
    this.sBlackPaint.strokeWidth = 1;
  }
  Util.prototype.showInformation_g6cl4j$ = function (canvas, msg) {
    canvas.drawBitmap_t8cslu$(this.bmpInformationBg_0[0], 11, 37);
    TextRender_getInstance().drawText_kkuqvh$(canvas, msg, 16, 39);
  };
  Util.prototype.showMessage_g6cl4j$ = function (canvas, msg) {
    this.showMessage_2yb3jp$(canvas, gbkBytes(msg));
  };
  Util.prototype.showMessage_2yb3jp$ = function (canvas, msg) {
    var lineNum = msg.length / 16 | 0;
    if (lineNum >= 5)
      lineNum = 4;
    var textY = 39 - (lineNum * 8 | 0) | 0;
    canvas.drawBitmap_t8cslu$(this.bmpInformationBg_0[lineNum], 11, textY - 2 | 0);
    TextRender_getInstance().drawText_tz7kd0$(canvas, msg, 0, new Rect(16, textY, 16 + (16 * 8 | 0) | 0, textY + (16 * lineNum | 0) + 16 | 0));
  };
  Util.prototype.drawSideFrame_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.bmpSideFrame_0, 0, 0);
    canvas.drawBitmap_t8cslu$(this.bmpSideFrame_0, 152, 0);
  };
  Util.prototype.getFrameBitmap_vux9f0$ = function (w, h) {
    var bmp = Bitmap$Companion_getInstance().createBitmap_vux9f0$(w, h);
    var tmpC = new Canvas(bmp);
    tmpC.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    tmpC.drawRect_x3aj6j$(1, 1, w - 2 | 0, h - 2 | 0, this.drawFramePaint_0);
    return bmp;
  };
  Util.prototype.drawTriangleCursor_2g4tob$ = function (canvas, x, y) {
    canvas.drawBitmap_t8cslu$(this.bmpTriangleCursor_0, x, y);
  };
  Util.prototype.drawSmallNum_tj1hu5$ = function (canvas, num, x, y) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var num_0 = num;
    var x_0 = x;
    if (num_0 < 0)
      num_0 = -num_0;
    var digits = num_0.toString();
    tmp$ = get_indices_2(digits);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      this.imgSmallNum_0.draw_tj1hu5$(canvas, digits.charCodeAt(i) - 48 + 1 | 0, x_0, y);
      x_0 = x_0 + (this.imgSmallNum_0.width + 1) | 0;
    }
    return Kotlin.imul(digits.length, this.imgSmallNum_0.width);
  };
  Util.prototype.getSmallSignedNumBitmap_za3lpa$ = function (num) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    var digits = (num > 0 ? num : -num).toString();
    var sign = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, num > 0 ? 6 : 7), ResImage) ? tmp$ : throwCCE();
    var bmp = Bitmap$Companion_getInstance().createBitmap_vux9f0$(sign.width + Kotlin.imul(digits.length, this.imgSmallNum_0.width) + 1 + digits.length | 0, this.imgSmallNum_0.height);
    var c = new Canvas(bmp);
    sign.draw_tj1hu5$(c, 1, 0, 0);
    var x = sign.width + 1 | 0;
    tmp$_0 = get_indices_2(digits);
    tmp$_1 = tmp$_0.first;
    tmp$_2 = tmp$_0.last;
    tmp$_3 = tmp$_0.step;
    for (var i = tmp$_1; i <= tmp$_2; i += tmp$_3) {
      this.imgSmallNum_0.draw_tj1hu5$(c, digits.charCodeAt(i) - 48 + 1 | 0, x, 0);
      x = x + (this.imgSmallNum_0.width + 1) | 0;
    }
    return bmp;
  };
  Util.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Util',
    interfaces: []
  };
  var Util_instance = null;
  function Util_getInstance() {
    if (Util_instance === null) {
      new Util();
    }
    return Util_instance;
  }
  function DatLib(buffer) {
    DatLib$Companion_getInstance();
    this.mBuffer_0 = buffer;
    this.mDataOffset_0 = HashMap_init(2048);
    this.getAllResOffset_0();
  }
  DatLib.prototype.getAllResOffset_0 = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    var i = 16;
    var j = 8192;
    while (i < this.mBuffer_0.length && this.mBuffer_0[i] !== -1) {
      var key = this.getKey_0(this.mBuffer_0[tmp$ = i, i = tmp$ + 1 | 0, tmp$], this.mBuffer_0[tmp$_0 = i, i = tmp$_0 + 1 | 0, tmp$_0], this.mBuffer_0[tmp$_1 = i, i = tmp$_1 + 1 | 0, tmp$_1] & 255);
      var block = this.mBuffer_0[tmp$_2 = j, j = tmp$_2 + 1 | 0, tmp$_2] & 255;
      var low = this.mBuffer_0[tmp$_3 = j, j = tmp$_3 + 1 | 0, tmp$_3] & 255;
      var high = this.mBuffer_0[tmp$_4 = j, j = tmp$_4 + 1 | 0, tmp$_4] & 255;
      var value = block * 16384 | 0 | (high << 8 | low);
      this.mDataOffset_0.put_xwzc9p$(key, value);
    }
  };
  DatLib.prototype.getRes_2et8c9$ = function (resType, type, index, allowNull) {
    if (allowNull === void 0)
      allowNull = false;
    var tmp$, tmp$_0;
    var offset = this.getDataOffset_0(resType, type, index);
    if (offset !== -1) {
      if (equals(resType, DatLib$ResType$GUT_getInstance()))
        tmp$ = new ResGut();
      else if (equals(resType, DatLib$ResType$MAP_getInstance()))
        tmp$ = new ResMap();
      else if (equals(resType, DatLib$ResType$ARS_getInstance())) {
        if (type === 1)
          tmp$ = new Player();
        else if (type === 2)
          tmp$ = new NPC();
        else if (type === 3)
          tmp$ = new Monster();
        else if (type === 4)
          tmp$ = new SceneObj();
        else
          tmp$ = null;
      }
       else if (equals(resType, DatLib$ResType$MRS_getInstance()))
        tmp$ = this.getMagic_0(type);
      else if (equals(resType, DatLib$ResType$SRS_getInstance()))
        tmp$ = new ResSrs();
      else if (equals(resType, DatLib$ResType$GRS_getInstance()))
        tmp$ = this.getGoods_0(type);
      else if (equals(resType, DatLib$ResType$TIL_getInstance()) || equals(resType, DatLib$ResType$ACP_getInstance()) || equals(resType, DatLib$ResType$GDP_getInstance()) || equals(resType, DatLib$ResType$GGJ_getInstance()) || equals(resType, DatLib$ResType$PIC_getInstance()))
        tmp$ = new ResImage();
      else if (equals(resType, DatLib$ResType$MLR_getInstance())) {
        if (type === 1)
          tmp$ = new ResMagicChain();
        else if (type === 2)
          tmp$ = new ResLevelupChain();
        else
          tmp$ = null;
      }
       else
        tmp$ = Kotlin.noWhenBranchMatched();
      var res = tmp$;
      res != null ? (res.setData_ir89t6$(this.mBuffer_0, offset), Unit) : null;
      tmp$_0 = res;
    }
     else {
      tmp$_0 = null;
    }
    var res_0 = tmp$_0;
    if (allowNull)
      return res_0;
    if (res_0 == null) {
      throw new Error_0('res not found:resType=' + resType + ',type=' + type + ',index=' + index);
    }
    return res_0;
  };
  DatLib.prototype.getGoods_0 = function (type) {
    if ((new IntRange(1, 5)).contains_mef7kx$(type)) {
      return new GoodsEquipment();
    }
    var rtn = null;
    if (type === 6)
      rtn = new GoodsDecorations();
    else if (type === 7)
      rtn = new GoodsWeapon();
    else if (type === 8)
      rtn = new GoodsHiddenWeapon();
    else if (type === 9)
      rtn = new GoodsMedicine();
    else if (type === 10)
      rtn = new GoodsMedicineLife();
    else if (type === 11)
      rtn = new GoodsMedicineChg4Ever();
    else if (type === 12)
      rtn = new GoodsStimulant();
    else if (type === 13)
      rtn = new GoodsTudun();
    else if (type === 14)
      rtn = new GoodsDrama();
    return rtn;
  };
  DatLib.prototype.getMagic_0 = function (type) {
    if (type === 1)
      return new MagicAttack();
    else if (type === 2)
      return new MagicEnhance();
    else if (type === 3)
      return new MagicRestore();
    else if (type === 4)
      return new MagicAuxiliary();
    else if (type === 5)
      return new MagicSpecial();
    return null;
  };
  DatLib.prototype.getDataOffset_0 = function (resType, type, index) {
    var tmp$;
    return (tmp$ = this.mDataOffset_0.get_11rb$(this.getKey_0(resType.v, type, index))) != null ? tmp$ : -1;
  };
  DatLib.prototype.getKey_0 = function (resType, type, index) {
    return resType << 16 | type << 8 | index;
  };
  function DatLib$ResType(name, ordinal, v) {
    Enum.call(this);
    this.v = v;
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function DatLib$ResType_initFields() {
    DatLib$ResType_initFields = function () {
    };
    DatLib$ResType$GUT_instance = new DatLib$ResType('GUT', 0, 1);
    DatLib$ResType$MAP_instance = new DatLib$ResType('MAP', 1, 2);
    DatLib$ResType$ARS_instance = new DatLib$ResType('ARS', 2, 3);
    DatLib$ResType$MRS_instance = new DatLib$ResType('MRS', 3, 4);
    DatLib$ResType$SRS_instance = new DatLib$ResType('SRS', 4, 5);
    DatLib$ResType$GRS_instance = new DatLib$ResType('GRS', 5, 6);
    DatLib$ResType$TIL_instance = new DatLib$ResType('TIL', 6, 7);
    DatLib$ResType$ACP_instance = new DatLib$ResType('ACP', 7, 8);
    DatLib$ResType$GDP_instance = new DatLib$ResType('GDP', 8, 9);
    DatLib$ResType$GGJ_instance = new DatLib$ResType('GGJ', 9, 10);
    DatLib$ResType$PIC_instance = new DatLib$ResType('PIC', 10, 11);
    DatLib$ResType$MLR_instance = new DatLib$ResType('MLR', 11, 12);
  }
  var DatLib$ResType$GUT_instance;
  function DatLib$ResType$GUT_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$GUT_instance;
  }
  var DatLib$ResType$MAP_instance;
  function DatLib$ResType$MAP_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$MAP_instance;
  }
  var DatLib$ResType$ARS_instance;
  function DatLib$ResType$ARS_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$ARS_instance;
  }
  var DatLib$ResType$MRS_instance;
  function DatLib$ResType$MRS_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$MRS_instance;
  }
  var DatLib$ResType$SRS_instance;
  function DatLib$ResType$SRS_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$SRS_instance;
  }
  var DatLib$ResType$GRS_instance;
  function DatLib$ResType$GRS_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$GRS_instance;
  }
  var DatLib$ResType$TIL_instance;
  function DatLib$ResType$TIL_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$TIL_instance;
  }
  var DatLib$ResType$ACP_instance;
  function DatLib$ResType$ACP_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$ACP_instance;
  }
  var DatLib$ResType$GDP_instance;
  function DatLib$ResType$GDP_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$GDP_instance;
  }
  var DatLib$ResType$GGJ_instance;
  function DatLib$ResType$GGJ_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$GGJ_instance;
  }
  var DatLib$ResType$PIC_instance;
  function DatLib$ResType$PIC_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$PIC_instance;
  }
  var DatLib$ResType$MLR_instance;
  function DatLib$ResType$MLR_getInstance() {
    DatLib$ResType_initFields();
    return DatLib$ResType$MLR_instance;
  }
  DatLib$ResType.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResType',
    interfaces: [Enum]
  };
  function DatLib$ResType$values() {
    return [DatLib$ResType$GUT_getInstance(), DatLib$ResType$MAP_getInstance(), DatLib$ResType$ARS_getInstance(), DatLib$ResType$MRS_getInstance(), DatLib$ResType$SRS_getInstance(), DatLib$ResType$GRS_getInstance(), DatLib$ResType$TIL_getInstance(), DatLib$ResType$ACP_getInstance(), DatLib$ResType$GDP_getInstance(), DatLib$ResType$GGJ_getInstance(), DatLib$ResType$PIC_getInstance(), DatLib$ResType$MLR_getInstance()];
  }
  DatLib$ResType.values = DatLib$ResType$values;
  function DatLib$ResType$valueOf(name) {
    switch (name) {
      case 'GUT':
        return DatLib$ResType$GUT_getInstance();
      case 'MAP':
        return DatLib$ResType$MAP_getInstance();
      case 'ARS':
        return DatLib$ResType$ARS_getInstance();
      case 'MRS':
        return DatLib$ResType$MRS_getInstance();
      case 'SRS':
        return DatLib$ResType$SRS_getInstance();
      case 'GRS':
        return DatLib$ResType$GRS_getInstance();
      case 'TIL':
        return DatLib$ResType$TIL_getInstance();
      case 'ACP':
        return DatLib$ResType$ACP_getInstance();
      case 'GDP':
        return DatLib$ResType$GDP_getInstance();
      case 'GGJ':
        return DatLib$ResType$GGJ_getInstance();
      case 'PIC':
        return DatLib$ResType$PIC_getInstance();
      case 'MLR':
        return DatLib$ResType$MLR_getInstance();
      default:throwISE('No enum constant fmj.lib.DatLib.ResType.' + name);
    }
  }
  DatLib$ResType.valueOf_61zpoe$ = DatLib$ResType$valueOf;
  function DatLib$Companion() {
    DatLib$Companion_instance = this;
    this.instance_vl9d13$_0 = lazy(DatLib$Companion$instance$lambda);
  }
  Object.defineProperty(DatLib$Companion.prototype, 'instance', {
    get: function () {
      var $receiver = this.instance_vl9d13$_0;
      new PropertyMetadata('instance');
      return $receiver.value;
    }
  });
  DatLib$Companion.prototype.getRes_2et8c9$ = function (resType, type, index, allowNull) {
    if (allowNull === void 0)
      allowNull = false;
    return this.instance.getRes_2et8c9$(resType, type, index, allowNull);
  };
  DatLib$Companion.prototype.getPic_ydzd23$ = function (type, index, allowNull) {
    if (allowNull === void 0)
      allowNull = false;
    var tmp$;
    return (tmp$ = this.getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), type, index, allowNull)) == null || Kotlin.isType(tmp$, ResImage) ? tmp$ : throwCCE();
  };
  DatLib$Companion.prototype.getMlr_ydzd23$ = function (type, index, allowNull) {
    if (allowNull === void 0)
      allowNull = false;
    var tmp$, tmp$_0;
    return (tmp$_0 = (tmp$ = this.getRes_2et8c9$(DatLib$ResType$MLR_getInstance(), type, index, allowNull)) == null || Kotlin.isType(tmp$, ResMagicChain) ? tmp$ : throwCCE()) != null ? tmp$_0 : ResMagicChain$Companion_getInstance().empty;
  };
  function DatLib$Companion$instance$lambda() {
    return new DatLib(File$Companion_getInstance().contentsOf_61zpoe$('DAT.LIB'));
  }
  DatLib$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var DatLib$Companion_instance = null;
  function DatLib$Companion_getInstance() {
    if (DatLib$Companion_instance === null) {
      new DatLib$Companion();
    }
    return DatLib$Companion_instance;
  }
  DatLib.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'DatLib',
    interfaces: []
  };
  function ResBase() {
    ResBase$Companion_getInstance();
    this.type_6ym9b$_0 = 0;
    this.index_m1yu1$_0 = 0;
  }
  Object.defineProperty(ResBase.prototype, 'type', {
    get: function () {
      return this.type_6ym9b$_0;
    },
    set: function (type) {
      this.type_6ym9b$_0 = type;
    }
  });
  Object.defineProperty(ResBase.prototype, 'index', {
    get: function () {
      return this.index_m1yu1$_0;
    },
    set: function (index) {
      this.index_m1yu1$_0 = index;
    }
  });
  function ResBase$Companion() {
    ResBase$Companion_instance = this;
  }
  ResBase$Companion.prototype.getString_ir89t6$ = function (buf, start) {
    var i = 0;
    while (buf[start + i | 0] !== 0)
      i = i + 1 | 0;
    return gbkString(buf, start, i);
  };
  ResBase$Companion.prototype.get2BytesInt_ir89t6$ = function (buf, start) {
    return buf[start] & 255 | buf[start + 1 | 0] << 8 & 65280;
  };
  ResBase$Companion.prototype.get2BytesSInt_ir89t6$ = function (buf, start) {
    var tmp$;
    var i = buf[start] & 255 | buf[start + 1 | 0] << 8 & 32512;
    if ((buf[start + 1 | 0] & 128) !== 0) {
      tmp$ = -i;
    }
     else
      tmp$ = i;
    return tmp$;
  };
  ResBase$Companion.prototype.get1ByteSInt_ir89t6$ = function (buf, start) {
    var tmp$;
    var i = buf[start] & 127;
    if ((buf[start] & 128) !== 0) {
      tmp$ = -i;
    }
     else
      tmp$ = i;
    return tmp$;
  };
  ResBase$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ResBase$Companion_instance = null;
  function ResBase$Companion_getInstance() {
    if (ResBase$Companion_instance === null) {
      new ResBase$Companion();
    }
    return ResBase$Companion_instance;
  }
  ResBase.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResBase',
    interfaces: []
  };
  function ResGut() {
    ResBase.call(this);
    this.description_ropvta$_0 = '';
    this.mLength_0 = 0;
    this.mNumSceneEvent_0 = 0;
    this.sceneEvent_d7sl48$_0 = null;
    this.scriptData_7tvrfz$_0 = null;
  }
  Object.defineProperty(ResGut.prototype, 'description', {
    get: function () {
      return this.description_ropvta$_0;
    },
    set: function (description) {
      this.description_ropvta$_0 = description;
    }
  });
  Object.defineProperty(ResGut.prototype, 'sceneEvent', {
    get: function () {
      return this.sceneEvent_d7sl48$_0;
    },
    set: function (sceneEvent) {
      this.sceneEvent_d7sl48$_0 = sceneEvent;
    }
  });
  Object.defineProperty(ResGut.prototype, 'scriptData', {
    get: function () {
      return this.scriptData_7tvrfz$_0;
    },
    set: function (scriptData) {
      this.scriptData_7tvrfz$_0 = scriptData;
    }
  });
  ResGut.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.type = buf[offset];
    this.index = buf[offset + 1 | 0];
    this.description = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 2 | 0);
    this.mLength_0 = (buf[offset + 25 | 0] & 255) << 8 | buf[offset + 24 | 0] & 255;
    this.mNumSceneEvent_0 = buf[offset + 26 | 0] & 255;
    this.sceneEvent = new Int32Array(this.mNumSceneEvent_0);
    tmp$ = this.mNumSceneEvent_0;
    for (var i = 0; i < tmp$; i++) {
      ensureNotNull(this.sceneEvent)[i] = (buf[offset + (i << 1) + 28 | 0] & 255) << 8 | buf[offset + (i << 1) + 27 | 0] & 255;
    }
    var len = this.mLength_0 - (this.mNumSceneEvent_0 * 2 | 0) - 3 | 0;
    this.scriptData = new Int8Array(len);
    System_getInstance().arraycopy_nlwz52$(buf, offset + 27 + (this.mNumSceneEvent_0 * 2 | 0) | 0, ensureNotNull(this.scriptData), 0, len);
  };
  ResGut.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResGut',
    interfaces: [ResBase]
  };
  function ResImage() {
    ResBase.call(this);
    this.width_qjhswd$_0 = 0;
    this.height_koqwes$_0 = 0;
    this.number_po9mq6$_0 = 0;
    this.mTransparent_0 = false;
    this.mData_0 = null;
    this.mBitmaps_0 = null;
  }
  Object.defineProperty(ResImage.prototype, 'width', {
    get: function () {
      return this.width_qjhswd$_0;
    },
    set: function (width) {
      this.width_qjhswd$_0 = width;
    }
  });
  Object.defineProperty(ResImage.prototype, 'height', {
    get: function () {
      return this.height_koqwes$_0;
    },
    set: function (height) {
      this.height_koqwes$_0 = height;
    }
  });
  Object.defineProperty(ResImage.prototype, 'number', {
    get: function () {
      return this.number_po9mq6$_0;
    },
    set: function (number) {
      this.number_po9mq6$_0 = number;
    }
  });
  Object.defineProperty(ResImage.prototype, 'bytesCount', {
    get: function () {
      return ensureNotNull(this.mData_0).length + 6 | 0;
    }
  });
  ResImage.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.width = buf[offset + 2 | 0] & 255;
    this.height = buf[offset + 3 | 0] & 255;
    this.number = buf[offset + 4 | 0] & 255;
    this.mTransparent_0 = buf[offset + 5 | 0] === 2;
    var len = Kotlin.imul(Kotlin.imul(Kotlin.imul(this.number, (this.width / 8 | 0) + (this.width % 8 !== 0 ? 1 : 0) | 0), this.height), buf[offset + 5 | 0]);
    this.mData_0 = new Int8Array(len);
    System_getInstance().arraycopy_nlwz52$(buf, offset + 6 | 0, ensureNotNull(this.mData_0), 0, len);
    this.createBitmaps_0();
  };
  ResImage.prototype.createBitmaps_0 = function () {
    var iOfData = {v: 0};
    var array = Array_0(this.number);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      var init$result;
      var tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
      var array_0 = Array_0(Kotlin.imul(this.width, this.height));
      var tmp$_5;
      tmp$_5 = array_0.length - 1 | 0;
      for (var i_0 = 0; i_0 <= tmp$_5; i_0++) {
        array_0[i_0] = Color$Companion_getInstance().WHITE;
      }
      var tmp = array_0;
      if (this.mTransparent_0) {
        var cnt = 0;
        var iOfTmp = 0;
        tmp$_0 = this.height;
        for (var y = 0; y < tmp$_0; y++) {
          tmp$_1 = this.width;
          for (var x = 0; x < tmp$_1; x++) {
            if ((ensureNotNull(this.mData_0)[iOfData.v] << cnt & 128) !== 0) {
              tmp[iOfTmp] = Global_getInstance().COLOR_TRANSP;
            }
             else {
              tmp[iOfTmp] = (ensureNotNull(this.mData_0)[iOfData.v] << cnt << 1 & 128) !== 0 ? Global_getInstance().COLOR_BLACK : Global_getInstance().COLOR_WHITE;
            }
            iOfTmp = iOfTmp + 1 | 0;
            cnt = cnt + 2 | 0;
            if (cnt >= 8) {
              cnt = 0;
              iOfData.v = iOfData.v + 1 | 0;
            }
          }
          if ((new IntRange(1, 7)).contains_mef7kx$(cnt)) {
            cnt = 0;
            iOfData.v = iOfData.v + 1 | 0;
          }
          if (iOfData.v % 2 !== 0) {
            iOfData.v = iOfData.v + 1 | 0;
          }
        }
        init$result = Bitmap$Companion_getInstance().createBitmap_34aymq$(tmp, this.width, this.height);
      }
       else {
        var cnt_0 = 0;
        var iOfTmp_0 = 0;
        tmp$_2 = this.height;
        for (var y_0 = 0; y_0 < tmp$_2; y_0++) {
          tmp$_3 = this.width;
          for (var x_0 = 0; x_0 < tmp$_3; x_0++) {
            tmp[tmp$_4 = iOfTmp_0, iOfTmp_0 = tmp$_4 + 1 | 0, tmp$_4] = (ensureNotNull(this.mData_0)[iOfData.v] << cnt_0 & 128) !== 0 ? Global_getInstance().COLOR_BLACK : Global_getInstance().COLOR_WHITE;
            if ((cnt_0 = cnt_0 + 1 | 0, cnt_0) >= 8) {
              cnt_0 = 0;
              iOfData.v = iOfData.v + 1 | 0;
            }
          }
          if (cnt_0 !== 0) {
            cnt_0 = 0;
            iOfData.v = iOfData.v + 1 | 0;
          }
        }
        init$result = Bitmap$Companion_getInstance().createBitmap_34aymq$(tmp, this.width, this.height);
      }
      array[i] = init$result;
    }
    this.mBitmaps_0 = array;
  };
  ResImage.prototype.draw_tj1hu5$ = function (canvas, num, left, top) {
    if (num <= this.number) {
      canvas.drawBitmap_t8cslu$(ensureNotNull(this.mBitmaps_0)[num - 1 | 0], left, top);
    }
     else {
      if (this.number > 0) {
        canvas.drawBitmap_t8cslu$(ensureNotNull(this.mBitmaps_0)[0], left, top);
      }
       else {
        TextRender_getInstance().drawText_kkuqvh$(canvas, '\u70EB', left, top);
      }
    }
  };
  ResImage.prototype.getBitmap_za3lpa$ = function (index) {
    var tmp$;
    if (index >= this.number) {
      tmp$ = null;
    }
     else
      tmp$ = ensureNotNull(this.mBitmaps_0)[index].copy();
    return tmp$;
  };
  ResImage.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResImage',
    interfaces: [ResBase]
  };
  function ResMap() {
    ResMap$Companion_getInstance();
    ResBase.call(this);
    this.mTilIndex_0 = 0;
    this.mapName_e64bwv$_0 = null;
    this.mapWidth_8pedri$_0 = 0;
    this.mapHeight_5cshsl$_0 = 0;
    this.mData_0 = null;
    this.mTiles_0 = null;
  }
  Object.defineProperty(ResMap.prototype, 'mapName', {
    get: function () {
      return this.mapName_e64bwv$_0;
    },
    set: function (mapName) {
      this.mapName_e64bwv$_0 = mapName;
    }
  });
  Object.defineProperty(ResMap.prototype, 'mapWidth', {
    get: function () {
      return this.mapWidth_8pedri$_0;
    },
    set: function (mapWidth) {
      this.mapWidth_8pedri$_0 = mapWidth;
    }
  });
  Object.defineProperty(ResMap.prototype, 'mapHeight', {
    get: function () {
      return this.mapHeight_5cshsl$_0;
    },
    set: function (mapHeight) {
      this.mapHeight_5cshsl$_0 = mapHeight;
    }
  });
  ResMap.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.mTilIndex_0 = buf[offset + 2 | 0];
    var i = 0;
    while (buf[offset + 3 + i | 0] !== 0)
      i = i + 1 | 0;
    this.mapName = gbkString(buf, offset + 3 | 0, i);
    this.mapWidth = buf[offset + 16 | 0];
    this.mapHeight = buf[offset + 17 | 0];
    var len = Kotlin.imul(this.mapWidth, this.mapHeight) * 2 | 0;
    this.mData_0 = new Int8Array(len);
    System_getInstance().arraycopy_nlwz52$(buf, offset + 18 | 0, ensureNotNull(this.mData_0), 0, len);
  };
  ResMap.prototype.canWalk_vux9f0$ = function (x, y) {
    if (x < 0 || x >= this.mapWidth || y < 0 || y >= this.mapHeight) {
      return false;
    }
    var i = Kotlin.imul(y, this.mapWidth) + x | 0;
    return (ensureNotNull(this.mData_0)[i * 2 | 0] & 128) !== 0;
  };
  ResMap.prototype.canPlayerWalk_vux9f0$ = function (x, y) {
    return this.canWalk_vux9f0$(x, y) && x >= 4 && x < (this.mapWidth - 4 | 0) && y >= 3 && y < (this.mapHeight - 2 | 0);
  };
  ResMap.prototype.getEventNum_vux9f0$ = function (x, y) {
    if (x < 0 || x >= this.mapWidth || y < 0 || y >= this.mapHeight) {
      return -1;
    }
    var i = Kotlin.imul(y, this.mapWidth) + x | 0;
    return ensureNotNull(this.mData_0)[(i * 2 | 0) + 1 | 0] & 255;
  };
  ResMap.prototype.getTileIndex_0 = function (x, y) {
    var i = Kotlin.imul(y, this.mapWidth) + x | 0;
    return ensureNotNull(this.mData_0)[i * 2 | 0] & 127;
  };
  ResMap.prototype.drawMap_2g4tob$ = function (canvas, left, top) {
    if (this.mTiles_0 == null) {
      this.mTiles_0 = new Tiles(this.mTilIndex_0);
    }
    var a = ResMap$Companion_getInstance().HEIGHT;
    var b = this.mapHeight - top | 0;
    var minY = Math_0.min(a, b);
    var a_0 = ResMap$Companion_getInstance().WIDTH;
    var b_0 = this.mapWidth - left | 0;
    var minX = Math_0.min(a_0, b_0);
    for (var y = 0; y < minY; y++) {
      for (var x = 0; x < minX; x++) {
        ensureNotNull(this.mTiles_0).draw_tj1hu5$(canvas, Kotlin.imul(x, Tiles$Companion_getInstance().WIDTH) + Global_getInstance().MAP_LEFT_OFFSET | 0, Kotlin.imul(y, Tiles$Companion_getInstance().HEIGHT), this.getTileIndex_0(left + x | 0, top + y | 0));
      }
    }
  };
  ResMap.prototype.drawWholeMap_2g4tob$ = function (canvas, x, y) {
    var tmp$, tmp$_0;
    if (this.mTiles_0 == null) {
      this.mTiles_0 = new Tiles(this.mTilIndex_0);
    }
    tmp$ = this.mapHeight;
    for (var ty = 0; ty < tmp$; ty++) {
      tmp$_0 = this.mapWidth;
      for (var tx = 0; tx < tmp$_0; tx++) {
        var sx = Kotlin.imul(tx, Tiles$Companion_getInstance().WIDTH) + x | 0;
        var sy = Kotlin.imul(ty, Tiles$Companion_getInstance().HEIGHT) + y | 0;
        ensureNotNull(this.mTiles_0).draw_tj1hu5$(canvas, sx, sy, this.getTileIndex_0(tx, ty));
        var event = this.getEventNum_vux9f0$(tx, ty);
        if (event !== 0) {
          Global_getInstance().bgColor = Color$Companion_getInstance().RED;
          TextRender_getInstance().drawText_kkuqvh$(canvas, event.toString(), sx, sy);
          Global_getInstance().bgColor = Global_getInstance().COLOR_WHITE;
        }
      }
    }
  };
  function ResMap$Companion() {
    ResMap$Companion_instance = this;
    this.WIDTH = (160 / 16 | 0) - 1 | 0;
    this.HEIGHT = 96 / 16 | 0;
  }
  ResMap$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ResMap$Companion_instance = null;
  function ResMap$Companion_getInstance() {
    if (ResMap$Companion_instance === null) {
      new ResMap$Companion();
    }
    return ResMap$Companion_instance;
  }
  ResMap.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResMap',
    interfaces: [ResBase]
  };
  function ResSrs() {
    ResBase.call(this);
    this.mFrameNum_0 = 0;
    this.mImageNum_0 = 0;
    this.mStartFrame_0 = 0;
    this.mEndFrame_0 = 0;
    this.mFrameHeader_0 = null;
    this.mImage_0 = null;
    this.ITERATOR_0 = 1;
    this.mShowList_0 = ArrayList_init();
  }
  ResSrs.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9;
    this.type = buf[offset];
    this.index = buf[offset + 1 | 0] & 255;
    this.mFrameNum_0 = buf[offset + 2 | 0] & 255;
    this.mImageNum_0 = buf[offset + 3 | 0] & 255;
    this.mStartFrame_0 = buf[offset + 4 | 0] & 255;
    this.mEndFrame_0 = buf[offset + 5 | 0] & 255;
    var ptr = {v: offset + 6 | 0};
    var array = Array_0(this.mFrameNum_0);
    var tmp$_10;
    tmp$_10 = array.length - 1 | 0;
    for (var i = 0; i <= tmp$_10; i++) {
      array[i] = new Int32Array(5);
    }
    this.mFrameHeader_0 = array;
    tmp$ = this.mFrameNum_0;
    for (var i_0 = 0; i_0 < tmp$; i_0++) {
      tmp$_1 = (tmp$_0 = ptr.v, ptr.v = tmp$_0 + 1 | 0, tmp$_0);
      ensureNotNull(this.mFrameHeader_0)[i_0][0] = buf[tmp$_1] & 255;
      tmp$_3 = (tmp$_2 = ptr.v, ptr.v = tmp$_2 + 1 | 0, tmp$_2);
      ensureNotNull(this.mFrameHeader_0)[i_0][1] = buf[tmp$_3] & 255;
      tmp$_5 = (tmp$_4 = ptr.v, ptr.v = tmp$_4 + 1 | 0, tmp$_4);
      ensureNotNull(this.mFrameHeader_0)[i_0][2] = buf[tmp$_5] & 255;
      tmp$_7 = (tmp$_6 = ptr.v, ptr.v = tmp$_6 + 1 | 0, tmp$_6);
      ensureNotNull(this.mFrameHeader_0)[i_0][3] = buf[tmp$_7] & 255;
      tmp$_9 = (tmp$_8 = ptr.v, ptr.v = tmp$_8 + 1 | 0, tmp$_8);
      ensureNotNull(this.mFrameHeader_0)[i_0][4] = buf[tmp$_9] & 255;
    }
    var array_0 = Array_0(this.mImageNum_0);
    var tmp$_11;
    tmp$_11 = array_0.length - 1 | 0;
    for (var i_1 = 0; i_1 <= tmp$_11; i_1++) {
      var img = new ResImage();
      img.setData_ir89t6$(buf, ptr.v);
      ptr.v = ptr.v + img.bytesCount | 0;
      array_0[i_1] = img;
    }
    this.mImage_0 = array_0;
  };
  function ResSrs$Key($outer, index) {
    this.$outer = $outer;
    this.index_8be2vx$ = index;
    this.show_8be2vx$ = 0;
    this.nshow_8be2vx$ = 0;
    this.show_8be2vx$ = ensureNotNull(this.$outer.mFrameHeader_0)[this.index_8be2vx$][2];
    this.nshow_8be2vx$ = ensureNotNull(this.$outer.mFrameHeader_0)[this.index_8be2vx$][3];
  }
  ResSrs$Key.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Key',
    interfaces: []
  };
  ResSrs.prototype.startAni = function () {
    this.mShowList_0.clear();
    this.mShowList_0.add_11rb$(new ResSrs$Key(this, 0));
  };
  ResSrs.prototype.update_s8cxhz$ = function (delta) {
    var tmp$;
    tmp$ = this.ITERATOR_0;
    for (var j = 0; j < tmp$; j++) {
      var iter = this.mShowList_0.listIterator();
      while (iter.hasNext()) {
        var i = iter.next();
        i.show_8be2vx$ = i.show_8be2vx$ - 1 | 0;
        i.nshow_8be2vx$ = i.nshow_8be2vx$ - 1 | 0;
        if (i.nshow_8be2vx$ === 0 && (i.index_8be2vx$ + 1 | 0) < this.mFrameNum_0) {
          iter.add_11rb$(new ResSrs$Key(this, i.index_8be2vx$ + 1 | 0));
        }
      }
      iter = this.mShowList_0.listIterator();
      while (iter.hasNext()) {
        var i_0 = iter.next();
        if (i_0.show_8be2vx$ <= 0) {
          iter.remove();
        }
      }
      if (this.mShowList_0.isEmpty())
        return false;
    }
    return true;
  };
  ResSrs.prototype.draw_2g4tob$ = function (canvas, dx, dy) {
    var tmp$;
    tmp$ = this.mShowList_0.iterator();
    while (tmp$.hasNext()) {
      var i = tmp$.next();
      ensureNotNull(this.mImage_0)[ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][4]].draw_tj1hu5$(canvas, 1, ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][0] + dx | 0, ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][1] + dy | 0);
    }
  };
  ResSrs.prototype.drawAbsolutely_2g4tob$ = function (canvas, x, y) {
    var tmp$;
    tmp$ = this.mShowList_0.iterator();
    while (tmp$.hasNext()) {
      var i = tmp$.next();
      ensureNotNull(this.mImage_0)[ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][4]].draw_tj1hu5$(canvas, 1, ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][0] - ensureNotNull(this.mFrameHeader_0)[0][0] + x | 0, ensureNotNull(this.mFrameHeader_0)[i.index_8be2vx$][1] - ensureNotNull(this.mFrameHeader_0)[0][1] + y | 0);
    }
  };
  ResSrs.prototype.setIteratorNum_za3lpa$ = function (n) {
    this.ITERATOR_0 = n;
    if (this.ITERATOR_0 < 1) {
      this.ITERATOR_0 = 1;
    }
  };
  ResSrs.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResSrs',
    interfaces: [ResBase]
  };
  function BaseMagic() {
    ResBase.call(this);
    this.roundNum_kflf2c$_0 = 0;
    this.isForAll_alczq$_0 = false;
    this.costMp_a1afck$_0 = 0;
    this.magicAni_fbh3sj$_0 = null;
    this.magicName_miz96o$_0 = '';
    this.magicDescription_3948lv$_0 = '';
  }
  Object.defineProperty(BaseMagic.prototype, 'roundNum', {
    get: function () {
      return this.roundNum_kflf2c$_0;
    },
    set: function (roundNum) {
      this.roundNum_kflf2c$_0 = roundNum;
    }
  });
  Object.defineProperty(BaseMagic.prototype, 'isForAll', {
    get: function () {
      return this.isForAll_alczq$_0;
    },
    set: function (isForAll) {
      this.isForAll_alczq$_0 = isForAll;
    }
  });
  Object.defineProperty(BaseMagic.prototype, 'costMp', {
    get: function () {
      return this.costMp_a1afck$_0;
    },
    set: function (costMp) {
      this.costMp_a1afck$_0 = costMp;
    }
  });
  Object.defineProperty(BaseMagic.prototype, 'magicAni', {
    get: function () {
      return this.magicAni_fbh3sj$_0;
    },
    set: function (magicAni) {
      this.magicAni_fbh3sj$_0 = magicAni;
    }
  });
  Object.defineProperty(BaseMagic.prototype, 'magicName', {
    get: function () {
      return this.magicName_miz96o$_0;
    },
    set: function (magicName) {
      this.magicName_miz96o$_0 = magicName;
    }
  });
  Object.defineProperty(BaseMagic.prototype, 'magicDescription', {
    get: function () {
      return this.magicDescription_3948lv$_0;
    },
    set: function (magicDescription) {
      this.magicDescription_3948lv$_0 = magicDescription;
    }
  });
  BaseMagic.prototype.setData_ir89t6$ = function (buf, offset) {
    var tmp$;
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.roundNum = buf[offset + 3 | 0] & 127;
    this.isForAll = (buf[offset + 3 | 0] & 128) !== 0;
    this.costMp = buf[offset + 4 | 0];
    this.magicAni = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 2, buf[offset + 5 | 0] & 255), ResSrs) ? tmp$ : throwCCE();
    this.magicName = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 6 | 0);
    if ((buf[offset + 2 | 0] & 255) > 112) {
      buf[offset + 112 | 0] = 0;
    }
    this.magicDescription = ResBase$Companion_getInstance().getString_ir89t6$(buf, offset + 26 | 0);
    this.setOtherData_ir89t6$(buf, offset);
  };
  BaseMagic.prototype.use_qwqr58$ = function (src, dst) {
  };
  BaseMagic.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BaseMagic',
    interfaces: [ResBase]
  };
  function MagicAttack() {
    BaseMagic.call(this);
    this.mHp_0 = 0;
    this.mMp_0 = 0;
    this.mDf_0 = 0;
    this.mAt_0 = 0;
    this.mBuff_0 = 0;
    this.mDebuff_0 = 0;
  }
  MagicAttack.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mHp_0 = ResBase$Companion_getInstance().get2BytesSInt_ir89t6$(buf, offset + 18 | 0);
    this.mMp_0 = ResBase$Companion_getInstance().get2BytesSInt_ir89t6$(buf, offset + 20 | 0);
    this.mDf_0 = buf[offset + 22 | 0] & 255;
    this.mAt_0 = buf[offset + 23 | 0] & 255;
    this.mBuff_0 = buf[offset + 24 | 0] & 255;
    this.mDebuff_0 = buf[offset + 25 | 0] & 255;
  };
  MagicAttack.prototype.use_qwqr58$ = function (src, dst) {
    src.mp = src.mp - this.costMp | 0;
    dst.hp = dst.hp - this.mHp_0 | 0;
  };
  MagicAttack.prototype.use_h32lzv$ = function (src, dst) {
    var tmp$;
    src.mp = src.mp - this.costMp | 0;
    tmp$ = dst.iterator();
    while (tmp$.hasNext()) {
      var fc = tmp$.next();
      fc.hp = fc.hp - this.mHp_0 | 0;
    }
  };
  MagicAttack.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MagicAttack',
    interfaces: [BaseMagic]
  };
  function MagicAuxiliary() {
    BaseMagic.call(this);
    this.mHp_0 = 0;
  }
  MagicAuxiliary.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mHp_0 = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 18 | 0);
  };
  MagicAuxiliary.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MagicAuxiliary',
    interfaces: [BaseMagic]
  };
  function MagicEnhance() {
    BaseMagic.call(this);
    this.mDf_0 = 0;
    this.mAt_0 = 0;
    this.mRound_0 = 0;
    this.mBuff_0 = 0;
  }
  MagicEnhance.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mDf_0 = buf[offset + 22 | 0] & 255;
    this.mAt_0 = buf[offset + 23 | 0] & 255;
    this.mRound_0 = buf[offset + 24 | 0] >> 4 & 15;
    this.mBuff_0 = buf[offset + 25 | 0] & 255;
  };
  MagicEnhance.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MagicEnhance',
    interfaces: [BaseMagic]
  };
  function MagicRestore() {
    BaseMagic.call(this);
    this.mHp_0 = 0;
    this.mBuff_0 = 0;
  }
  MagicRestore.prototype.setOtherData_ir89t6$ = function (buf, offset) {
    this.mHp_0 = ResBase$Companion_getInstance().get2BytesInt_ir89t6$(buf, offset + 18 | 0);
    this.mBuff_0 = buf[offset + 24 | 0];
  };
  MagicRestore.prototype.use_wd4e0$ = function (src, dst) {
    if (src.mp < this.costMp)
      return;
    src.mp = src.mp - this.costMp | 0;
    dst.hp = dst.hp + this.mHp_0 | 0;
    if (dst.hp > dst.maxHP) {
      dst.hp = dst.maxHP;
    }
    dst.delDebuff_za3lpa$(this.mBuff_0);
  };
  MagicRestore.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MagicRestore',
    interfaces: [BaseMagic]
  };
  function MagicSpecial() {
    BaseMagic.call(this);
  }
  MagicSpecial.prototype.setOtherData_ir89t6$ = function (buf, offset) {
  };
  MagicSpecial.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MagicSpecial',
    interfaces: [BaseMagic]
  };
  function ResMagicChain() {
    ResMagicChain$Companion_getInstance();
    ResBase.call(this);
    this.magicSum_pn35oq$_0 = 0;
    this.mMagics_0 = null;
    this.learnNum = 0;
  }
  Object.defineProperty(ResMagicChain.prototype, 'magicSum', {
    get: function () {
      return this.magicSum_pn35oq$_0;
    },
    set: function (magicSum) {
      this.magicSum_pn35oq$_0 = magicSum;
    }
  });
  ResMagicChain.prototype.setData_ir89t6$ = function (buf, offset) {
    this.type = buf[offset] & 255;
    this.index = buf[offset + 1 | 0] & 255;
    this.magicSum = buf[offset + 2 | 0] & 255;
    var index = {v: offset + 3 | 0};
    var array = Array_0(this.magicSum);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      var tmp$_0, tmp$_1, tmp$_2;
      array[i] = Kotlin.isType(tmp$_2 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MRS_getInstance(), buf[tmp$_0 = index.v, index.v = tmp$_0 + 1 | 0, tmp$_0], buf[tmp$_1 = index.v, index.v = tmp$_1 + 1 | 0, tmp$_1]), BaseMagic) ? tmp$_2 : throwCCE();
    }
    this.mMagics_0 = array;
  };
  ResMagicChain.prototype.learnNextMagic = function () {
    this.learnNum = this.learnNum + 1 | 0;
  };
  ResMagicChain.prototype.getMagic_za3lpa$ = function (index) {
    return ensureNotNull(this.mMagics_0)[index];
  };
  function ResMagicChain$Companion() {
    ResMagicChain$Companion_instance = this;
    this.empty_gc7hrp$_0 = lazy(ResMagicChain$Companion$empty$lambda);
  }
  Object.defineProperty(ResMagicChain$Companion.prototype, 'empty', {
    get: function () {
      var $receiver = this.empty_gc7hrp$_0;
      new PropertyMetadata('empty');
      return $receiver.value;
    }
  });
  function ResMagicChain$Companion$empty$lambda() {
    var rv = new ResMagicChain();
    rv.magicSum = 0;
    rv.mMagics_0 = [];
    return rv;
  }
  ResMagicChain$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ResMagicChain$Companion_instance = null;
  function ResMagicChain$Companion_getInstance() {
    if (ResMagicChain$Companion_instance === null) {
      new ResMagicChain$Companion();
    }
    return ResMagicChain$Companion_instance;
  }
  ResMagicChain.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ResMagicChain',
    interfaces: [ResBase]
  };
  function ScreenMagic(mMagicChain, mOnItemSelectedListener) {
    ScreenMagic$Companion_getInstance();
    BaseScreen.call(this);
    this.mMagicChain_0 = mMagicChain;
    this.mOnItemSelectedListener_0 = mOnItemSelectedListener;
    this.mFirstItemIndex_0 = 0;
    this.mCurItemIndex_0 = 0;
    this.mBmpCursor_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(12, 11);
    this.mBmpMarker_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(5, 8);
    this.mBmpMarker2_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(5, 8);
    this.mRectTop_0 = new Rect(10, 4, 147, 39);
    this.mRectBtm_0 = new Rect(10, 41, 147, 76);
    this.mRectDsp_0 = new Rect(11, 42, 146, 75);
    this.mToDraw_0 = 0;
    this.mNextToDraw_0 = 0;
    this.mStackLastToDraw_0 = Stack$Companion_getInstance().create_287e2$();
    this.mTextPos_0 = new Point(10, 77);
    this.mFramePaint_0 = new Paint();
    this.mFramePaint_0.color = Global_getInstance().COLOR_BLACK;
    this.mFramePaint_0.style = Paint$Style$STROKE_getInstance();
    this.mFramePaint_0.strokeWidth = 1;
    this.createBmp_0();
  }
  function ScreenMagic$OnItemSelectedListener() {
  }
  ScreenMagic$OnItemSelectedListener.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'OnItemSelectedListener',
    interfaces: []
  };
  ScreenMagic.prototype.createBmp_0 = function () {
    var canvas = Canvas_init();
    var p = new Paint();
    p.color = Global_getInstance().COLOR_BLACK;
    p.strokeWidth = 1;
    p.style = Paint$Style$STROKE_getInstance();
    canvas.setBitmap_963ehe$(this.mBmpCursor_0);
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    canvas.drawLine_x3aj6j$(8, 0, 11, 0, p);
    canvas.drawLine_x3aj6j$(11, 1, 11, 4, p);
    canvas.drawRect_x3aj6j$(6, 1, 7, 4, p);
    canvas.drawRect_x3aj6j$(7, 4, 10, 5, p);
    canvas.drawLine_x3aj6j$(7, 4, 0, 11, p);
    canvas.drawLine_x3aj6j$(8, 5, 2, 11, p);
    canvas.setBitmap_963ehe$(this.mBmpMarker_0);
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    var pts = new Float32Array([2.0, 0.0, 4.0, 2.0, 4.0, 2.0, 4.0, 6.0, 4.0, 6.0, 2.0, 8.0, 2.0, 7.0, 0.0, 5.0, 0.0, 5.0, 0.0, 2.0, 0.0, 3.0, 3.0, 0.0, 2.0, 3.0, 2.0, 5.0]);
    canvas.drawLines_ffeagz$(pts, p);
    canvas.setBitmap_963ehe$(this.mBmpMarker2_0);
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    canvas.drawLines_ffeagz$(pts, p);
    var pts2 = new Float32Array([1.0, 1.0, 1.0, 6.0, 2.0, 0.0, 2.0, 8.0, 3.0, 2.0, 3.0, 6.0]);
    canvas.drawLines_ffeagz$(pts2, p);
  };
  ScreenMagic.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMagic.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    canvas.drawRect_ed5hcw$(this.mRectTop_0, this.mFramePaint_0);
    canvas.drawRect_ed5hcw$(this.mRectBtm_0, this.mFramePaint_0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mMagicChain_0.getMagic_za3lpa$(this.mFirstItemIndex_0).magicName, this.mRectTop_0.left + 1 | 0, this.mRectTop_0.top + 1 | 0);
    if ((this.mFirstItemIndex_0 + 1 | 0) < this.mMagicChain_0.learnNum) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, this.mMagicChain_0.getMagic_za3lpa$(this.mFirstItemIndex_0 + 1 | 0).magicName, this.mRectTop_0.left + 1 | 0, this.mRectTop_0.top + 1 + 16 | 0);
    }
    this.mNextToDraw_0 = TextRender_getInstance().drawText_8xt01w$(canvas, this.mMagicChain_0.getMagic_za3lpa$(this.mCurItemIndex_0).magicDescription, this.mToDraw_0, this.mRectDsp_0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u8017\u771F\u6C14:' + toString(this.mMagicChain_0.getMagic_za3lpa$(this.mCurItemIndex_0).costMp), this.mTextPos_0.x, this.mTextPos_0.y);
    canvas.drawBitmap_t8cslu$(this.mBmpCursor_0, 100, this.mFirstItemIndex_0 === this.mCurItemIndex_0 ? 10 : 26);
    canvas.drawBitmap_t8cslu$(this.mFirstItemIndex_0 === 0 ? this.mBmpMarker_0 : this.mBmpMarker2_0, 135, 6);
    canvas.drawBitmap_t8cslu$(this.mBmpMarker_0, 135, 6 + 8 | 0);
    canvas.drawBitmap_t8cslu$(this.mBmpMarker_0, 135, 6 + 16 | 0);
    canvas.drawBitmap_t8cslu$((this.mFirstItemIndex_0 + 2 | 0) < this.mMagicChain_0.learnNum ? this.mBmpMarker2_0 : this.mBmpMarker_0, 135, 6 + 24 | 0);
  };
  ScreenMagic.prototype.onKeyDown_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_UP && this.mCurItemIndex_0 > 0) {
      this.mCurItemIndex_0 = this.mCurItemIndex_0 - 1 | 0;
      if (this.mCurItemIndex_0 < this.mFirstItemIndex_0) {
        this.mFirstItemIndex_0 = this.mFirstItemIndex_0 - 1 | 0;
      }
      this.mNextToDraw_0 = 0;
      this.mToDraw_0 = this.mNextToDraw_0;
      this.mStackLastToDraw_0.clear();
    }
     else if (key === Global_getInstance().KEY_DOWN && (this.mCurItemIndex_0 + 1 | 0) < this.mMagicChain_0.learnNum) {
      this.mCurItemIndex_0 = this.mCurItemIndex_0 + 1 | 0;
      if (this.mCurItemIndex_0 >= (this.mFirstItemIndex_0 + ScreenMagic$Companion_getInstance().ITEM_NUM_0 | 0)) {
        this.mFirstItemIndex_0 = this.mFirstItemIndex_0 + 1 | 0;
      }
      this.mNextToDraw_0 = 0;
      this.mToDraw_0 = this.mNextToDraw_0;
      this.mStackLastToDraw_0.clear();
    }
     else if (key === Global_getInstance().KEY_PAGEDOWN) {
      var len = gbkBytes(this.mMagicChain_0.getMagic_za3lpa$(this.mCurItemIndex_0).magicDescription).length;
      if (this.mNextToDraw_0 < len) {
        this.mStackLastToDraw_0.push_11rb$(this.mToDraw_0);
        this.mToDraw_0 = this.mNextToDraw_0;
      }
    }
     else if (key === Global_getInstance().KEY_PAGEUP && this.mToDraw_0 !== 0) {
      if ((tmp$ = this.mStackLastToDraw_0.pop()) != null) {
        this.mToDraw_0 = tmp$;
      }
    }
  };
  ScreenMagic.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      this.mOnItemSelectedListener_0.onItemSelected_3fncnk$(this.mMagicChain_0.getMagic_za3lpa$(this.mCurItemIndex_0));
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  function ScreenMagic$Companion() {
    ScreenMagic$Companion_instance = this;
    this.ITEM_NUM_0 = 2;
  }
  ScreenMagic$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenMagic$Companion_instance = null;
  function ScreenMagic$Companion_getInstance() {
    if (ScreenMagic$Companion_instance === null) {
      new ScreenMagic$Companion();
    }
    return ScreenMagic$Companion_instance;
  }
  ScreenMagic.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMagic',
    interfaces: [BaseScreen]
  };
  function SaveLoadGame() {
    SaveLoadGame_instance = this;
    this.startNewGame = true;
    this.MapType = 1;
    this.MapIndex = 1;
    this.MapScreenX = 1;
    this.MapScreenY = 1;
    this.ScriptType = 1;
    this.ScriptIndex = 1;
    this.SceneName = '';
    this.NpcObjs = [];
  }
  function SaveLoadGame$write$lambda(io, obj) {
    if (obj.isEmpty) {
      io.writeByte_s8j3t7$(0);
    }
     else {
      if (Kotlin.isType(obj, SceneObj)) {
        io.writeByte_s8j3t7$(2);
      }
       else {
        io.writeByte_s8j3t7$(1);
      }
      obj.encode_vcd9jg$(io);
    }
    return Unit;
  }
  SaveLoadGame.prototype.write_vcd9jg$ = function (out) {
    var tmp$;
    out.writeString_61zpoe$(this.SceneName);
    var actorNum = ScreenMainGame$Companion_getInstance().sPlayerList.size;
    out.writeInt_za3lpa$(actorNum);
    for (var i = 0; i < actorNum; i++) {
      out.writeInt_za3lpa$(ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(i).index);
    }
    out.writeInt_za3lpa$(this.MapType);
    out.writeInt_za3lpa$(this.MapIndex);
    out.writeInt_za3lpa$(this.MapScreenX);
    out.writeInt_za3lpa$(this.MapScreenY);
    out.writeInt_za3lpa$(this.ScriptType);
    out.writeInt_za3lpa$(this.ScriptIndex);
    out.writeInt_za3lpa$(ScreenMainGame$Companion_getInstance().sPlayerList.size);
    tmp$ = ScreenMainGame$Companion_getInstance().sPlayerList.size;
    for (var i_0 = 0; i_0 < tmp$; i_0++) {
      ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(i_0).encode_vcd9jg$(out);
    }
    out.writeInt_za3lpa$(Player$Companion_getInstance().sMoney);
    Player$Companion_getInstance().sGoodsList.write_vcd9jg$(out);
    writeArray(out, this.NpcObjs, SaveLoadGame$write$lambda);
    Combat$Companion_getInstance().write_vcd9jg$(out);
  };
  SaveLoadGame.prototype.read_setnfj$ = function (coder) {
    var tmp$;
    this.SceneName = coder.readString();
    var actorNum = coder.readInt();
    while ((tmp$ = actorNum, actorNum = tmp$ - 1 | 0, tmp$) > 0)
      coder.readInt();
    this.MapType = coder.readInt();
    this.MapIndex = coder.readInt();
    this.MapScreenX = coder.readInt();
    this.MapScreenY = coder.readInt();
    this.ScriptType = coder.readInt();
    this.ScriptIndex = coder.readInt();
    var size = coder.readInt();
    ScreenMainGame$Companion_getInstance().sPlayerList.clear();
    for (var i = 0; i < size; i++) {
      var p = new Player();
      p.decode_setnfj$(coder);
      ScreenMainGame$Companion_getInstance().sPlayerList.add_11rb$(p);
    }
    Player$Companion_getInstance().sMoney = coder.readInt();
    Player$Companion_getInstance().sGoodsList.read_setnfj$(coder);
    var len = coder.readInt();
    var array = Array_0(len);
    var tmp$_0;
    tmp$_0 = array.length - 1 | 0;
    for (var i_0 = 0; i_0 <= tmp$_0; i_0++) {
      var tmp$_1;
      var type = coder.readByte();
      if (type === 0 || type === 1)
        tmp$_1 = new NPC();
      else if (type === 2)
        tmp$_1 = new SceneObj();
      else
        throw new Error_0('Bad obj type');
      var npc = tmp$_1;
      if (type !== 0) {
        npc.decode_setnfj$(coder);
      }
      array[i_0] = npc;
    }
    this.NpcObjs = array;
    Combat$Companion_getInstance().read_setnfj$(coder);
  };
  SaveLoadGame.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'SaveLoadGame',
    interfaces: []
  };
  var SaveLoadGame_instance = null;
  function SaveLoadGame_getInstance() {
    if (SaveLoadGame_instance === null) {
      new SaveLoadGame();
    }
    return SaveLoadGame_instance;
  }
  function ScreenMainGame() {
    ScreenMainGame$Companion_getInstance();
    BaseScreen.call(this);
    this.player = null;
    this.currentMap_say9n7$_0 = null;
    this.mMapScreenPos_0 = new Point();
    this.mScriptSys_0 = null;
    this.mScriptExecutor_0 = null;
    this.sceneName_v90cur$_0 = '';
    this.mRunScript_0 = true;
    var array = Array_0(41);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = NPC$Companion_getInstance().empty;
    }
    this.mNPCObj_0 = array;
    this.mCanWalk_0 = new ScreenMainGame$mCanWalk$ObjectLiteral(this);
    ScreenMainGame$Companion_getInstance().instance = this;
    this.mScriptSys_0 = ScriptProcess$Companion_getInstance().instance;
    this.mScriptSys_0.setScreenMainGame_pforfg$(this);
    if (SaveLoadGame_getInstance().startNewGame) {
      Combat$Companion_getInstance().FightDisable();
      ScriptResources_getInstance().initGlobalVar();
      ScriptResources_getInstance().initGlobalEvents();
      SaveLoadGame_getInstance().NpcObjs = this.mNPCObj_0;
      ScreenMainGame$Companion_getInstance().sPlayerList.clear();
      Player$Companion_getInstance().sGoodsList.clear();
      Player$Companion_getInstance().sMoney = 0;
      this.startChapter_vux9f0$(1, 1);
      ScriptExecutor$Companion_getInstance().goonExecute = true;
      this.mRunScript_0 = true;
    }
     else {
      this.loadMap_tjonv8$(SaveLoadGame_getInstance().MapType, SaveLoadGame_getInstance().MapIndex, SaveLoadGame_getInstance().MapScreenX, SaveLoadGame_getInstance().MapScreenY);
      this.mNPCObj_0 = SaveLoadGame_getInstance().NpcObjs;
      var $receiver = this.mNPCObj_0;
      var destination = ArrayList_init();
      var tmp$_0;
      for (tmp$_0 = 0; tmp$_0 !== $receiver.length; ++tmp$_0) {
        var element = $receiver[tmp$_0];
        if (!element.isEmpty)
          destination.add_11rb$(element);
      }
      var tmp$_1;
      tmp$_1 = destination.iterator();
      while (tmp$_1.hasNext()) {
        var element_0 = tmp$_1.next();
        element_0.setICanWalk_mkhjrq$(this.mCanWalk_0);
      }
      if (ScreenMainGame$Companion_getInstance().sPlayerList.size > 0) {
        this.player = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(0);
      }
       else {
        this.createActor_qt1dr2$(1, 4, 3);
      }
      this.mScriptSys_0.loadScript_vux9f0$(SaveLoadGame_getInstance().ScriptType, SaveLoadGame_getInstance().ScriptIndex);
      this.mScriptExecutor_0 = this.mScriptSys_0.scriptExecutor;
      ScriptExecutor$Companion_getInstance().goonExecute = true;
      this.mRunScript_0 = false;
    }
  }
  Object.defineProperty(ScreenMainGame.prototype, 'currentMap', {
    get: function () {
      return this.currentMap_say9n7$_0;
    },
    set: function (currentMap) {
      this.currentMap_say9n7$_0 = currentMap;
    }
  });
  Object.defineProperty(ScreenMainGame.prototype, 'sceneName', {
    get: function () {
      return this.sceneName_v90cur$_0;
    },
    set: function (name) {
      this.sceneName_v90cur$_0 = name;
      SaveLoadGame_getInstance().SceneName = name;
    }
  });
  function ScreenMainGame$get_ScreenMainGame$sortedNpcObjs$lambda(it) {
    return it.posInMap.y;
  }
  var sortedWith = Kotlin.kotlin.collections.sortedWith_eknfly$;
  var compareByDescending$lambda_0 = wrapFunction(function () {
    var compareValues = Kotlin.kotlin.comparisons.compareValues_s00gnj$;
    return function (closure$selector) {
      return function (a, b) {
        var selector = closure$selector;
        return compareValues(selector(b), selector(a));
      };
    };
  });
  function Comparator$ObjectLiteral_0(closure$comparison) {
    this.closure$comparison = closure$comparison;
  }
  Comparator$ObjectLiteral_0.prototype.compare = function (a, b) {
    return this.closure$comparison(a, b);
  };
  Comparator$ObjectLiteral_0.$metadata$ = {kind: Kind_CLASS, interfaces: [Comparator]};
  Object.defineProperty(ScreenMainGame.prototype, 'sortedNpcObjs_0', {
    get: function () {
      var $receiver = this.mNPCObj_0;
      var destination = ArrayList_init();
      var tmp$;
      for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
        var element = $receiver[tmp$];
        if (!element.isEmpty)
          destination.add_11rb$(element);
      }
      return copyToArray(sortedWith(destination, new Comparator$ObjectLiteral_0(compareByDescending$lambda_0(ScreenMainGame$get_ScreenMainGame$sortedNpcObjs$lambda))));
    }
  });
  Object.defineProperty(ScreenMainGame.prototype, 'playerList', {
    get: function () {
      return ScreenMainGame$Companion_getInstance().sPlayerList;
    }
  });
  ScreenMainGame.prototype.exitScript = function () {
    this.mRunScript_0 = false;
    ScriptExecutor$Companion_getInstance().goonExecute = false;
  };
  ScreenMainGame.prototype.startChapter_vux9f0$ = function (type, index) {
    this.mScriptSys_0.loadScript_vux9f0$(type, index);
    this.mScriptExecutor_0 = this.mScriptSys_0.scriptExecutor;
    ScriptExecutor$Companion_getInstance().goonExecute = false;
    for (var i = 1; i <= 40; i++) {
      this.mNPCObj_0[i] = NPC$Companion_getInstance().empty;
    }
    ScriptResources_getInstance().initLocalVar();
    SaveLoadGame_getInstance().ScriptType = type;
    SaveLoadGame_getInstance().ScriptIndex = index;
  };
  ScreenMainGame.prototype.update_s8cxhz$ = function (delta) {
    if (this.mRunScript_0 && this.mScriptExecutor_0 != null) {
      ensureNotNull(this.mScriptExecutor_0).process();
      ensureNotNull(this.mScriptExecutor_0).update_s8cxhz$(delta);
    }
     else if (Combat$Companion_getInstance().IsActive()) {
      Combat$Companion_getInstance().Update_s8cxhz$(delta);
    }
     else {
      var $receiver = new IntRange(1, 40);
      var destination = ArrayList_init();
      var tmp$;
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        if (!this.mNPCObj_0[element].isEmpty)
          destination.add_11rb$(element);
      }
      var tmp$_0;
      tmp$_0 = destination.iterator();
      while (tmp$_0.hasNext()) {
        var element_0 = tmp$_0.next();
        this.mNPCObj_0[element_0].update_s8cxhz$(delta);
      }
    }
  };
  ScreenMainGame.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mRunScript_0 && this.mScriptExecutor_0 != null) {
      if (Combat$Companion_getInstance().IsActive()) {
        Combat$Companion_getInstance().Draw_9in0vv$(canvas);
      }
      ensureNotNull(this.mScriptExecutor_0).draw_9in0vv$(canvas);
    }
     else if (Combat$Companion_getInstance().IsActive()) {
      Combat$Companion_getInstance().Draw_9in0vv$(canvas);
      return;
    }
     else {
      this.drawScene_9in0vv$(canvas);
    }
  };
  ScreenMainGame.prototype.drawScene_9in0vv$ = function (canvas) {
    var tmp$;
    if (this.currentMap != null) {
      ensureNotNull(this.currentMap).drawMap_2g4tob$(canvas, this.mMapScreenPos_0.x, this.mMapScreenPos_0.y);
    }
    var playY = 10000;
    var hasPlayerBeenDrawn = false;
    if (this.player != null) {
      playY = ensureNotNull(this.player).posInMap.y;
    }
    var npcs = this.sortedNpcObjs_0;
    tmp$ = reversed(get_indices_1(npcs)).iterator();
    while (tmp$.hasNext()) {
      var i = tmp$.next();
      if (!hasPlayerBeenDrawn && playY < npcs[i].posInMap.y) {
        ensureNotNull(this.player).drawWalkingSprite_hbb1nm$(canvas, this.mMapScreenPos_0);
        hasPlayerBeenDrawn = true;
      }
      npcs[i].drawWalkingSprite_hbb1nm$(canvas, this.mMapScreenPos_0);
    }
    if (this.player != null && !hasPlayerBeenDrawn) {
      ensureNotNull(this.player).drawWalkingSprite_hbb1nm$(canvas, this.mMapScreenPos_0);
    }
    Util_getInstance().drawSideFrame_9in0vv$(canvas);
  };
  ScreenMainGame.prototype.onKeyDown_za3lpa$ = function (key) {
    if (this.mRunScript_0 && this.mScriptExecutor_0 != null) {
      ensureNotNull(this.mScriptExecutor_0).keyDown_za3lpa$(key);
    }
     else if (Combat$Companion_getInstance().IsActive()) {
      Combat$Companion_getInstance().KeyDown_za3lpa$(key);
      return;
    }
     else if (this.player != null) {
      if (key === Global_getInstance().KEY_LEFT)
        this.walkLeft_0();
      else if (key === Global_getInstance().KEY_RIGHT)
        this.walkRight_0();
      else if (key === Global_getInstance().KEY_UP)
        this.walkUp_0();
      else if (key === Global_getInstance().KEY_DOWN)
        this.walkDown_0();
      else if (key === Global_getInstance().KEY_ENTER)
        this.triggerSceneObjEvent_0();
    }
  };
  ScreenMainGame.prototype.onKeyUp_za3lpa$ = function (key) {
    if (this.mRunScript_0 && this.mScriptExecutor_0 != null) {
      ensureNotNull(this.mScriptExecutor_0).keyUp_za3lpa$(key);
    }
     else if (Combat$Companion_getInstance().IsActive()) {
      Combat$Companion_getInstance().KeyUp_za3lpa$(key);
      return;
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.pushScreen_2o7n0o$(new ScreenGameMainMenu());
    }
  };
  ScreenMainGame.prototype.gotoAddress_za3lpa$ = function (address) {
    ensureNotNull(this.mScriptExecutor_0).gotoAddress_za3lpa$(address);
    this.mRunScript_0 = true;
  };
  ScreenMainGame.prototype.triggerEvent_za3lpa$ = function (eventId) {
    if (this.mScriptExecutor_0 != null) {
      this.mRunScript_0 = ensureNotNull(this.mScriptExecutor_0).triggerEvent_za3lpa$(eventId);
    }
  };
  ScreenMainGame.prototype.triggerSceneObjEvent_0 = function () {
    var tmp$;
    var p = this.player;
    var x = ensureNotNull(p).posInMap.x;
    var y = p.posInMap.y;
    tmp$ = p.direction;
    if (equals(tmp$, Direction$East_getInstance()))
      x = x + 1 | 0;
    else if (equals(tmp$, Direction$North_getInstance()))
      y = y - 1 | 0;
    else if (equals(tmp$, Direction$South_getInstance()))
      y = y + 1 | 0;
    else if (equals(tmp$, Direction$West_getInstance()))
      x = x - 1 | 0;
    else
      Kotlin.noWhenBranchMatched();
    var npcId = this.getNpcIdFromPosInMap_0(x, y);
    if (npcId !== 0) {
      this.mRunScript_0 = ensureNotNull(this.mScriptExecutor_0).triggerEvent_za3lpa$(npcId);
      return;
    }
     else
      this.triggerMapEvent_0(x, y);
  };
  ScreenMainGame.prototype.triggerMapEvent_0 = function (x, y) {
    if (this.currentMap != null && this.mScriptExecutor_0 != null) {
      var id = ensureNotNull(this.currentMap).getEventNum_vux9f0$(x, y);
      if (id !== 0) {
        ensureNotNull(this.mScriptExecutor_0).triggerEvent_za3lpa$(id + 40 | 0);
        this.mRunScript_0 = true;
        return true;
      }
    }
    Combat$Companion_getInstance().StartNewRandomCombat();
    return false;
  };
  ScreenMainGame.prototype.canPlayerWalk_0 = function (x, y) {
    return this.currentMap == null ? false : ensureNotNull(this.currentMap).canPlayerWalk_vux9f0$(x, y) && this.getNpcFromPosInMap_vux9f0$(x, y).isEmpty;
  };
  ScreenMainGame.prototype.walkLeft_0 = function () {
    var tmp$ = ensureNotNull(this.player).posInMap;
    var x = tmp$.component1()
    , y = tmp$.component2();
    this.triggerMapEvent_0(x - 1 | 0, y);
    if (this.canPlayerWalk_0(x - 1 | 0, y)) {
      ensureNotNull(this.player).walk_rtfsey$(Direction$West_getInstance());
      var tmp$_0;
      tmp$_0 = this.mMapScreenPos_0;
      tmp$_0.x = tmp$_0.x - 1 | 0;
      SaveLoadGame_getInstance().MapScreenX = this.mMapScreenPos_0.x;
    }
     else {
      ensureNotNull(this.player).walkStay_rtfsey$(Direction$West_getInstance());
    }
  };
  ScreenMainGame.prototype.walkUp_0 = function () {
    var tmp$ = ensureNotNull(this.player).posInMap;
    var x = tmp$.component1()
    , y = tmp$.component2();
    this.triggerMapEvent_0(x, y - 1 | 0);
    if (this.canPlayerWalk_0(x, y - 1 | 0)) {
      ensureNotNull(this.player).walk_rtfsey$(Direction$North_getInstance());
      var tmp$_0;
      tmp$_0 = this.mMapScreenPos_0;
      tmp$_0.y = tmp$_0.y - 1 | 0;
      SaveLoadGame_getInstance().MapScreenY = this.mMapScreenPos_0.y;
    }
     else {
      ensureNotNull(this.player).walkStay_rtfsey$(Direction$North_getInstance());
    }
  };
  ScreenMainGame.prototype.walkRight_0 = function () {
    var tmp$ = ensureNotNull(this.player).posInMap;
    var x = tmp$.component1()
    , y = tmp$.component2();
    this.triggerMapEvent_0(x + 1 | 0, y);
    if (this.canPlayerWalk_0(x + 1 | 0, y)) {
      var tmp$_0;
      tmp$_0 = this.mMapScreenPos_0;
      tmp$_0.x = tmp$_0.x + 1 | 0;
      SaveLoadGame_getInstance().MapScreenX = this.mMapScreenPos_0.x;
      ensureNotNull(this.player).walk_rtfsey$(Direction$East_getInstance());
    }
     else {
      ensureNotNull(this.player).walkStay_rtfsey$(Direction$East_getInstance());
    }
  };
  ScreenMainGame.prototype.walkDown_0 = function () {
    var tmp$ = ensureNotNull(this.player).posInMap;
    var x = tmp$.component1()
    , y = tmp$.component2();
    this.triggerMapEvent_0(x, y + 1 | 0);
    if (this.canPlayerWalk_0(x, y + 1 | 0)) {
      var tmp$_0;
      tmp$_0 = this.mMapScreenPos_0;
      tmp$_0.y = tmp$_0.y + 1 | 0;
      SaveLoadGame_getInstance().MapScreenY = this.mMapScreenPos_0.y;
      ensureNotNull(this.player).walk_rtfsey$(Direction$South_getInstance());
    }
     else {
      ensureNotNull(this.player).walkStay_rtfsey$(Direction$South_getInstance());
    }
  };
  ScreenMainGame.prototype.loadMap_tjonv8$ = function (type, index, x, y) {
    var tmp$;
    var tmpP = null;
    if (this.player != null && this.currentMap != null) {
      tmpP = ensureNotNull(this.player).getPosOnScreen_wl9rgt$(this.mMapScreenPos_0);
    }
    this.currentMap = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$MAP_getInstance(), type, index), ResMap) ? tmp$ : throwCCE();
    this.mMapScreenPos_0.set_vux9f0$(x, y);
    if (tmpP != null) {
      ensureNotNull(this.player).setPosOnScreen_av5i43$(tmpP.x, tmpP.y, this.mMapScreenPos_0);
    }
    SaveLoadGame_getInstance().MapType = type;
    SaveLoadGame_getInstance().MapIndex = index;
    SaveLoadGame_getInstance().MapScreenX = x;
    SaveLoadGame_getInstance().MapScreenY = y;
  };
  ScreenMainGame.prototype.setMapScreenPos_vux9f0$ = function (x, y) {
    this.mMapScreenPos_0.set_vux9f0$(x, y);
  };
  ScreenMainGame.prototype.createActor_qt1dr2$ = function (actorId, x, y) {
    var tmp$;
    this.player = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), 1, actorId), Player) ? tmp$ : throwCCE();
    ensureNotNull(this.player).setPosOnScreen_av5i43$(x, y, this.mMapScreenPos_0);
    ScreenMainGame$Companion_getInstance().sPlayerList.add_11rb$(ensureNotNull(this.player));
    this.player = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(0);
  };
  ScreenMainGame.prototype.deleteActor_za3lpa$ = function (actorId) {
    var tmp$, tmp$_0;
    tmp$ = ScreenMainGame$Companion_getInstance().sPlayerList.size;
    for (var i = 0; i < tmp$; i++) {
      if (ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(i).index === actorId) {
        ScreenMainGame$Companion_getInstance().sPlayerList.removeAt_za3lpa$(i);
        break;
      }
    }
    if (ScreenMainGame$Companion_getInstance().sPlayerList.isEmpty()) {
      tmp$_0 = null;
    }
     else {
      tmp$_0 = ScreenMainGame$Companion_getInstance().sPlayerList.get_za3lpa$(0);
    }
    this.player = tmp$_0;
  };
  ScreenMainGame.prototype.getPlayer_za3lpa$ = function (actorId) {
    var $receiver = ScreenMainGame$Companion_getInstance().sPlayerList;
    var firstOrNull$result;
    firstOrNull$break: do {
      var tmp$;
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var element = tmp$.next();
        if (element.index === actorId) {
          firstOrNull$result = element;
          break firstOrNull$break;
        }
      }
      firstOrNull$result = null;
    }
     while (false);
    return firstOrNull$result;
  };
  ScreenMainGame.prototype.createNpc_tjonv8$ = function (id, npc, x, y) {
    var tmp$;
    var npcobj = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), 2, npc), NPC) ? tmp$ : throwCCE();
    npcobj.setPosInMap_vux9f0$(x, y);
    npcobj.setICanWalk_mkhjrq$(this.mCanWalk_0);
    this.mNPCObj_0[id] = npcobj;
    return npcobj;
  };
  ScreenMainGame.prototype.deleteNpc_za3lpa$ = function (id) {
    this.mNPCObj_0[id] = NPC$Companion_getInstance().empty;
  };
  ScreenMainGame.prototype.deleteAllNpc = function () {
    for (var i = 0; i <= 40; i++) {
      this.mNPCObj_0[i] = NPC$Companion_getInstance().empty;
    }
  };
  ScreenMainGame.prototype.getNPC_za3lpa$ = function (id) {
    return this.mNPCObj_0[id];
  };
  ScreenMainGame.prototype.isNpcVisible_l8hog8$ = function (npc) {
    var tmp$ = npc.getPosOnScreen_wl9rgt$(this.mMapScreenPos_0);
    var x = tmp$.component1()
    , y = tmp$.component2();
    return x >= 0 && x < ResMap$Companion_getInstance().WIDTH && y >= 0 && y <= ResMap$Companion_getInstance().HEIGHT;
  };
  ScreenMainGame.prototype.getNpcFromPosInMap_vux9f0$ = function (x, y) {
    return this.mNPCObj_0[this.getNpcIdFromPosInMap_0(x, y)];
  };
  ScreenMainGame.prototype.getNpcIdFromPosInMap_0 = function (x, y) {
    var $receiver = this.mNPCObj_0;
    var indexOfFirst$result;
    indexOfFirst$break: do {
      var tmp$, tmp$_0, tmp$_1, tmp$_2;
      tmp$ = get_indices_1($receiver);
      tmp$_0 = tmp$.first;
      tmp$_1 = tmp$.last;
      tmp$_2 = tmp$.step;
      for (var index = tmp$_0; index <= tmp$_1; index += tmp$_2) {
        var it = $receiver[index];
        var tmp$_3;
        if (!it.isEmpty && ((tmp$_3 = it.posInMap) != null ? tmp$_3.equals(new Point(x, y)) : null)) {
          indexOfFirst$result = index;
          break indexOfFirst$break;
        }
      }
      indexOfFirst$result = -1;
    }
     while (false);
    var id = indexOfFirst$result;
    return id === -1 ? 0 : id;
  };
  ScreenMainGame.prototype.createBox_tjonv8$ = function (id, boxIndex, x, y) {
    var tmp$;
    var box = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$ARS_getInstance(), 4, boxIndex), SceneObj) ? tmp$ : throwCCE();
    box.setPosInMap_vux9f0$(x, y);
    this.mNPCObj_0[id] = box;
    return box;
  };
  ScreenMainGame.prototype.deleteBox_za3lpa$ = function (id) {
    this.mNPCObj_0[id] = NPC$Companion_getInstance().empty;
  };
  function ScreenMainGame$Companion() {
    ScreenMainGame$Companion_instance = this;
    this.instance_94bfgn$_0 = this.instance_94bfgn$_0;
    this.sPlayerList = ArrayList_init();
  }
  Object.defineProperty(ScreenMainGame$Companion.prototype, 'instance', {
    get: function () {
      if (this.instance_94bfgn$_0 == null)
        return throwUPAE('instance');
      return this.instance_94bfgn$_0;
    },
    set: function (instance) {
      this.instance_94bfgn$_0 = instance;
    }
  });
  ScreenMainGame$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenMainGame$Companion_instance = null;
  function ScreenMainGame$Companion_getInstance() {
    if (ScreenMainGame$Companion_instance === null) {
      new ScreenMainGame$Companion();
    }
    return ScreenMainGame$Companion_instance;
  }
  function ScreenMainGame$mCanWalk$ObjectLiteral(this$ScreenMainGame) {
    this.this$ScreenMainGame = this$ScreenMainGame;
  }
  ScreenMainGame$mCanWalk$ObjectLiteral.prototype.canWalk_vux9f0$ = function (x, y) {
    var tmp$;
    return ensureNotNull(this.this$ScreenMainGame.currentMap).canWalk_vux9f0$(x, y) && this.this$ScreenMainGame.getNpcFromPosInMap_vux9f0$(x, y).isEmpty && !((tmp$ = ensureNotNull(this.this$ScreenMainGame.player).posInMap) != null ? tmp$.equals(new Point(x, y)) : null);
  };
  ScreenMainGame$mCanWalk$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [NPC$ICanWalk]
  };
  ScreenMainGame.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMainGame',
    interfaces: [BaseScreen]
  };
  function Operate() {
    this.delagete_lwej9p$_0 = this.delagete_lwej9p$_0;
  }
  Object.defineProperty(Operate.prototype, 'delagete', {
    get: function () {
      if (this.delagete_lwej9p$_0 == null)
        return throwUPAE('delagete');
      return this.delagete_lwej9p$_0;
    },
    set: function (delagete) {
      this.delagete_lwej9p$_0 = delagete;
    }
  });
  Object.defineProperty(Operate.prototype, 'isPopup', {
    get: function () {
      return false;
    }
  });
  Operate.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Operate',
    interfaces: []
  };
  function OperateNop() {
    OperateNop$Companion_getInstance();
    Operate.call(this);
  }
  OperateNop.prototype.process = function () {
    return false;
  };
  OperateNop.prototype.update_s8cxhz$ = function (delta) {
    return true;
  };
  OperateNop.prototype.draw_9in0vv$ = function (canvas) {
  };
  OperateNop.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  OperateNop.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  function OperateNop$Companion() {
    OperateNop$Companion_instance = this;
    this.nop = new OperateNop();
  }
  OperateNop$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var OperateNop$Companion_instance = null;
  function OperateNop$Companion_getInstance() {
    if (OperateNop$Companion_instance === null) {
      new OperateNop$Companion();
    }
    return OperateNop$Companion_instance;
  }
  OperateNop.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OperateNop',
    interfaces: [Operate]
  };
  function OperateAdapter() {
    Operate.call(this);
  }
  OperateAdapter.prototype.update_s8cxhz$ = function (delta) {
    return false;
  };
  OperateAdapter.prototype.draw_9in0vv$ = function (canvas) {
  };
  OperateAdapter.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  OperateAdapter.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  OperateAdapter.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OperateAdapter',
    interfaces: [Operate]
  };
  function OperateBuy(data, start) {
    Operate.call(this);
    this.data_8be2vx$ = data;
    this.start_8be2vx$ = start;
    this.goodsList_0 = ArrayList_init();
    this.mBuyScreen_0 = new OperateBuy$BuyGoodsScreen();
  }
  OperateBuy.prototype.process = function () {
    var tmp$;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_buy');
    this.goodsList_0.clear();
    var i = this.start_8be2vx$;
    while (this.data_8be2vx$[i] !== 0) {
      var g = Player$Companion_getInstance().sGoodsList.getGoods_vux9f0$(this.data_8be2vx$[i + 1 | 0] & 255, this.data_8be2vx$[i] & 255);
      if (g == null) {
        g = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), this.data_8be2vx$[i + 1 | 0] & 255, this.data_8be2vx$[i] & 255), BaseGoods) ? tmp$ : throwCCE();
        g.goodsNum = 0;
      }
      this.goodsList_0.add_11rb$(g);
      i = i + 2 | 0;
    }
    this.delagete.pushScreen_2o7n0o$(new ScreenGoodsList(this.goodsList_0, this, ScreenGoodsList$Mode$Buy_getInstance()));
    return true;
  };
  OperateBuy.prototype.update_s8cxhz$ = function (delta) {
    return false;
  };
  OperateBuy.prototype.draw_9in0vv$ = function (canvas) {
    ScreenMainGame$Companion_getInstance().instance.drawScene_9in0vv$(canvas);
  };
  OperateBuy.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  OperateBuy.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  OperateBuy.prototype.onItemSelected_6xxg66$ = function (goods) {
    if (Player$Companion_getInstance().sMoney < goods.buyPrice) {
      this.delagete.showMessage_4wgjuj$('\u91D1\u94B1\u4E0D\u8DB3!', Kotlin.Long.fromInt(1000));
    }
     else {
      this.mBuyScreen_0.init_6xxg66$(goods);
      this.delagete.pushScreen_2o7n0o$(this.mBuyScreen_0);
    }
  };
  function OperateBuy$BuyGoodsScreen() {
    BaseScreen.call(this);
    this.goods_0 = null;
    this.buyCnt_0 = 0;
    this.money_0 = 0;
    this.bmpBg_dn5wdu$_0 = lazy(OperateBuy$BuyGoodsScreen$bmpBg$lambda);
  }
  Object.defineProperty(OperateBuy$BuyGoodsScreen.prototype, 'bmpBg_0', {
    get: function () {
      var $receiver = this.bmpBg_dn5wdu$_0;
      new PropertyMetadata('bmpBg');
      return $receiver.value;
    }
  });
  Object.defineProperty(OperateBuy$BuyGoodsScreen.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  OperateBuy$BuyGoodsScreen.prototype.init_6xxg66$ = function (goods) {
    this.goods_0 = goods;
    this.buyCnt_0 = 0;
    this.money_0 = Player$Companion_getInstance().sMoney;
  };
  OperateBuy$BuyGoodsScreen.prototype.update_s8cxhz$ = function (delta) {
  };
  OperateBuy$BuyGoodsScreen.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.bmpBg_0, 12, 21);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u91D1\u94B1\uFF1A' + toString(this.money_0), 15, 24);
    TextRender_getInstance().drawText_kkuqvh$(canvas, ensureNotNull(this.goods_0).name, 15, 40);
    TextRender_getInstance().drawText_kkuqvh$(canvas, ': ' + toString(ensureNotNull(this.goods_0).goodsNum), 93, 40);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u4E70\u5165\u4E2A\u6570\u3000\uFF1A' + toString(this.buyCnt_0), 15, 56);
  };
  OperateBuy$BuyGoodsScreen.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      Player$Companion_getInstance().sMoney = this.money_0;
      if (this.buyCnt_0 === ensureNotNull(this.goods_0).goodsNum && this.buyCnt_0 > 0) {
        Player$Companion_getInstance().sGoodsList.addGoods_qt1dr2$(ensureNotNull(this.goods_0).type, ensureNotNull(this.goods_0).index, this.buyCnt_0);
      }
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      ensureNotNull(this.goods_0).addGoodsNum_za3lpa$(-this.buyCnt_0);
      this.delegate.popScreen();
    }
  };
  OperateBuy$BuyGoodsScreen.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP && ensureNotNull(this.goods_0).goodsNum < 99) {
      if (this.money_0 >= ensureNotNull(this.goods_0).buyPrice) {
        this.buyCnt_0 = this.buyCnt_0 + 1 | 0;
        ensureNotNull(this.goods_0).addGoodsNum_za3lpa$(1);
        this.money_0 = this.money_0 - ensureNotNull(this.goods_0).buyPrice | 0;
      }
       else {
        this.delegate.showMessage_4wgjuj$('\u91D1\u94B1\u4E0D\u8DB3!', Kotlin.Long.fromInt(1000));
      }
    }
     else if (key === Global_getInstance().KEY_DOWN && this.buyCnt_0 > 0) {
      this.buyCnt_0 = this.buyCnt_0 - 1 | 0;
      ensureNotNull(this.goods_0).addGoodsNum_za3lpa$(-1);
      this.money_0 = this.money_0 + ensureNotNull(this.goods_0).buyPrice | 0;
    }
  };
  function OperateBuy$BuyGoodsScreen$bmpBg$lambda() {
    return Util_getInstance().getFrameBitmap_vux9f0$(136, 55);
  }
  OperateBuy$BuyGoodsScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BuyGoodsScreen',
    interfaces: [BaseScreen]
  };
  OperateBuy.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OperateBuy',
    interfaces: [ScreenGoodsList$OnItemSelectedListener, Operate]
  };
  function OperateDrawOnce() {
    Operate.call(this);
    this.drawCnt_jhho4u$_0 = 0;
  }
  OperateDrawOnce.prototype.update_s8cxhz$ = function (delta) {
    if (this.drawCnt_jhho4u$_0 >= 3) {
      this.drawCnt_jhho4u$_0 = 0;
      return false;
    }
    return true;
  };
  OperateDrawOnce.prototype.draw_9in0vv$ = function (canvas) {
    this.drawOnce_9in0vv$(canvas);
    this.drawCnt_jhho4u$_0 = this.drawCnt_jhho4u$_0 + 1 | 0;
  };
  OperateDrawOnce.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  OperateDrawOnce.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  OperateDrawOnce.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OperateDrawOnce',
    interfaces: [Operate]
  };
  function OperateSale() {
    Operate.call(this);
    this.mSaleScreen_0 = new OperateSale$SaleGoodsScreen(this);
  }
  OperateSale.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_sale');
    var list = ArrayList_init();
    list.addAll_brywnq$(Player$Companion_getInstance().sGoodsList.goodsList);
    list.addAll_brywnq$(Player$Companion_getInstance().sGoodsList.equipList);
    this.delagete.pushScreen_2o7n0o$(new ScreenGoodsList(list, this, ScreenGoodsList$Mode$Sale_getInstance()));
    return true;
  };
  OperateSale.prototype.update_s8cxhz$ = function (delta) {
    return false;
  };
  OperateSale.prototype.draw_9in0vv$ = function (canvas) {
  };
  OperateSale.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  OperateSale.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  OperateSale.prototype.onItemSelected_6xxg66$ = function (goods) {
    if (Kotlin.isType(goods, GoodsDrama)) {
      this.delagete.showMessage_4wgjuj$('\u4EFB\u52A1\u7269\u54C1!', Kotlin.Long.fromInt(1000));
    }
     else {
      this.mSaleScreen_0.init_6xxg66$(goods);
      this.delagete.pushScreen_2o7n0o$(this.mSaleScreen_0);
    }
  };
  function OperateSale$SaleGoodsScreen($outer) {
    this.$outer = $outer;
    BaseScreen.call(this);
    this.goods_0 = null;
    this.saleCnt_0 = 0;
    this.money_0 = 0;
    this.bmpBg_4u37ni$_0 = lazy(OperateSale$SaleGoodsScreen$bmpBg$lambda);
  }
  Object.defineProperty(OperateSale$SaleGoodsScreen.prototype, 'bmpBg_0', {
    get: function () {
      var $receiver = this.bmpBg_4u37ni$_0;
      new PropertyMetadata('bmpBg');
      return $receiver.value;
    }
  });
  Object.defineProperty(OperateSale$SaleGoodsScreen.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  OperateSale$SaleGoodsScreen.prototype.init_6xxg66$ = function (goods) {
    this.goods_0 = goods;
    this.saleCnt_0 = 0;
    this.money_0 = Player$Companion_getInstance().sMoney;
  };
  OperateSale$SaleGoodsScreen.prototype.update_s8cxhz$ = function (delta) {
  };
  OperateSale$SaleGoodsScreen.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(this.bmpBg_0, 12, 21);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u91D1\u94B1\uFF1A' + toString(this.money_0), 15, 24);
    TextRender_getInstance().drawText_kkuqvh$(canvas, ensureNotNull(this.goods_0).name, 15, 40);
    TextRender_getInstance().drawText_kkuqvh$(canvas, ': ' + toString(ensureNotNull(this.goods_0).goodsNum - this.saleCnt_0 | 0), 93, 40);
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u5356\u51FA\u4E2A\u6570\u3000\uFF1A' + toString(this.saleCnt_0), 15, 56);
  };
  OperateSale$SaleGoodsScreen.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      Player$Companion_getInstance().sMoney = this.money_0;
      if (this.saleCnt_0 > 0) {
        Player$Companion_getInstance().sGoodsList.useGoodsNum_qt1dr2$(ensureNotNull(this.goods_0).type, ensureNotNull(this.goods_0).index, this.saleCnt_0);
      }
      this.delegate.popScreen();
      this.delegate.popScreen();
      var list = ArrayList_init();
      list.addAll_brywnq$(Player$Companion_getInstance().sGoodsList.goodsList);
      list.addAll_brywnq$(Player$Companion_getInstance().sGoodsList.equipList);
      this.delegate.pushScreen_2o7n0o$(new ScreenGoodsList(list, this.$outer, ScreenGoodsList$Mode$Sale_getInstance()));
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
  };
  OperateSale$SaleGoodsScreen.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP && this.saleCnt_0 > 0) {
      this.saleCnt_0 = this.saleCnt_0 - 1 | 0;
      this.money_0 = this.money_0 - ensureNotNull(this.goods_0).sellPrice | 0;
    }
     else if (key === Global_getInstance().KEY_DOWN && ensureNotNull(this.goods_0).goodsNum > this.saleCnt_0) {
      this.saleCnt_0 = this.saleCnt_0 + 1 | 0;
      this.money_0 = this.money_0 + ensureNotNull(this.goods_0).sellPrice | 0;
      if (this.money_0 > 99999) {
        this.money_0 = 99999;
      }
    }
  };
  function OperateSale$SaleGoodsScreen$bmpBg$lambda() {
    return Util_getInstance().getFrameBitmap_vux9f0$(136, 55);
  }
  OperateSale$SaleGoodsScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'SaleGoodsScreen',
    interfaces: [BaseScreen]
  };
  OperateSale.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OperateSale',
    interfaces: [ScreenGoodsList$OnItemSelectedListener, Operate]
  };
  function ScriptExecutor(mOperateList, mEventIndex, mMapAddrOffsetIndex, mHeaderCnt) {
    ScriptExecutor$Companion_getInstance();
    this.mOperateList_0 = mOperateList;
    this.mEventIndex_0 = mEventIndex;
    this.mMapAddrOffsetIndex_0 = mMapAddrOffsetIndex;
    this.mHeaderCnt_0 = mHeaderCnt;
    this.mCurExeOperateIndex_0 = 0;
    this.mIsExeUpdateDraw_0 = false;
    this.mCurExeOperateIndex_0 = 0;
    this.mIsExeUpdateDraw_0 = false;
  }
  ScriptExecutor.prototype.triggerEvent_za3lpa$ = function (eventId) {
    if (eventId > this.mEventIndex_0.length) {
      return false;
    }
    var index = this.mEventIndex_0[eventId - 1 | 0];
    if (index !== -1) {
      this.mCurExeOperateIndex_0 = index;
      this.mIsExeUpdateDraw_0 = false;
      return true;
    }
    return false;
  };
  ScriptExecutor.prototype.gotoAddress_za3lpa$ = function (address) {
    this.mCurExeOperateIndex_0 = ensureNotNull(this.mMapAddrOffsetIndex_0.get_11rb$(address - this.mHeaderCnt_0 | 0));
    if (this.mIsExeUpdateDraw_0) {
      this.mIsExeUpdateDraw_0 = false;
      this.mCurExeOperateIndex_0 = this.mCurExeOperateIndex_0 - 1 | 0;
    }
     else {
      ScriptExecutor$Companion_getInstance().goonExecute = false;
    }
  };
  ScriptExecutor.prototype.process = function () {
    if (!this.mIsExeUpdateDraw_0) {
      while (this.mCurExeOperateIndex_0 < this.mOperateList_0.size && ScriptExecutor$Companion_getInstance().goonExecute) {
        var oper = this.mOperateList_0.get_za3lpa$(this.mCurExeOperateIndex_0);
        if (oper.process()) {
          this.mIsExeUpdateDraw_0 = true;
          return;
        }
        if (!ScriptExecutor$Companion_getInstance().goonExecute) {
          ScriptExecutor$Companion_getInstance().goonExecute = true;
          return;
        }
        this.mCurExeOperateIndex_0 = this.mCurExeOperateIndex_0 + 1 | 0;
      }
    }
  };
  ScriptExecutor.prototype.update_s8cxhz$ = function (delta) {
    if (this.mIsExeUpdateDraw_0) {
      if (!this.mOperateList_0.get_za3lpa$(this.mCurExeOperateIndex_0).update_s8cxhz$(delta)) {
        this.mIsExeUpdateDraw_0 = false;
        this.mCurExeOperateIndex_0 = this.mCurExeOperateIndex_0 + 1 | 0;
      }
    }
  };
  ScriptExecutor.prototype.draw_9in0vv$ = function (canvas) {
    if (this.mIsExeUpdateDraw_0) {
      this.mOperateList_0.get_za3lpa$(this.mCurExeOperateIndex_0).draw_9in0vv$(canvas);
    }
  };
  ScriptExecutor.prototype.keyDown_za3lpa$ = function (key) {
    if (this.mIsExeUpdateDraw_0) {
      this.mOperateList_0.get_za3lpa$(this.mCurExeOperateIndex_0).onKeyDown_za3lpa$(key);
    }
  };
  ScriptExecutor.prototype.keyUp_za3lpa$ = function (key) {
    if (this.mIsExeUpdateDraw_0) {
      this.mOperateList_0.get_za3lpa$(this.mCurExeOperateIndex_0).onKeyUp_za3lpa$(key);
    }
  };
  function ScriptExecutor$Companion() {
    ScriptExecutor$Companion_instance = this;
    this.goonExecute = true;
  }
  ScriptExecutor$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScriptExecutor$Companion_instance = null;
  function ScriptExecutor$Companion_getInstance() {
    if (ScriptExecutor$Companion_instance === null) {
      new ScriptExecutor$Companion();
    }
    return ScriptExecutor$Companion_instance;
  }
  ScriptExecutor.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScriptExecutor',
    interfaces: []
  };
  function ScriptProcess() {
    ScriptProcess$Companion_getInstance();
    this.delegate_41i177$_0 = this.delegate_41i177$_0;
    this.mScript_0 = null;
    this.mCmds_0 = null;
    this.mScreenMainGame_0 = null;
    this.cmd_music_0 = new ScriptProcess$cmd_music$ObjectLiteral();
    this.cmd_loadmap_0 = new ScriptProcess$cmd_loadmap$ObjectLiteral(this);
    this.cmd_createactor_0 = new ScriptProcess$cmd_createactor$ObjectLiteral(this);
    this.cmd_deletenpc_0 = new ScriptProcess$cmd_deletenpc$ObjectLiteral(this);
    this.cmd_move_0 = new ScriptProcess$cmd_move$ObjectLiteral(this);
    this.cmd_callback_0 = new ScriptProcess$cmd_callback$ObjectLiteral(this);
    this.cmd_goto_0 = new ScriptProcess$cmd_goto$ObjectLiteral(this);
    this.cmd_if_0 = new ScriptProcess$cmd_if$ObjectLiteral(this);
    this.cmd_set_0 = new ScriptProcess$cmd_set$ObjectLiteral();
    this.cmd_say_0 = new ScriptProcess$cmd_say$ObjectLiteral(this);
    this.cmd_startchapter_0 = new ScriptProcess$cmd_startchapter$ObjectLiteral(this);
    this.cmd_screens_0 = new ScriptProcess$cmd_screens$ObjectLiteral(this);
    this.cmd_gameover_0 = new ScriptProcess$cmd_gameover$ObjectLiteral(this);
    this.cmd_ifcmp_0 = new ScriptProcess$cmd_ifcmp$ObjectLiteral(this);
    this.cmd_add_0 = new ScriptProcess$cmd_add$ObjectLiteral();
    this.cmd_sub_0 = new ScriptProcess$cmd_sub$ObjectLiteral();
    this.cmd_setcontrolid_0 = new ScriptProcess$cmd_setcontrolid$ObjectLiteral();
    this.cmd_setevent_0 = new ScriptProcess$cmd_setevent$ObjectLiteral();
    this.cmd_clrevent_0 = new ScriptProcess$cmd_clrevent$ObjectLiteral();
    this.cmd_buy_0 = new ScriptProcess$cmd_buy$ObjectLiteral();
    this.cmd_facetoface_0 = new ScriptProcess$cmd_facetoface$ObjectLiteral(this);
    this.cmd_movie_0 = new ScriptProcess$cmd_movie$ObjectLiteral(this);
    this.cmd_choice_0 = new ScriptProcess$cmd_choice$ObjectLiteral(this);
    this.cmd_createbox_0 = new ScriptProcess$cmd_createbox$ObjectLiteral(this);
    this.cmd_deletebox_0 = new ScriptProcess$cmd_deletebox$ObjectLiteral(this);
    this.cmd_gaingoods_0 = new ScriptProcess$cmd_gaingoods$ObjectLiteral();
    this.cmd_initfight_0 = new ScriptProcess$cmd_initfight$ObjectLiteral(this);
    this.cmd_fightenable_0 = new ScriptProcess$cmd_fightenable$ObjectLiteral();
    this.cmd_fightdisenable_0 = new ScriptProcess$cmd_fightdisenable$ObjectLiteral();
    this.cmd_createnpc_0 = new ScriptProcess$cmd_createnpc$ObjectLiteral(this);
    this.cmd_enterfight_0 = new ScriptProcess$cmd_enterfight$ObjectLiteral(this);
    this.cmd_deleteactor_0 = new ScriptProcess$cmd_deleteactor$ObjectLiteral(this);
    this.cmd_gainmoney_0 = new ScriptProcess$cmd_gainmoney$ObjectLiteral();
    this.cmd_usemoney_0 = new ScriptProcess$cmd_usemoney$ObjectLiteral();
    this.cmd_setmoney_0 = new ScriptProcess$cmd_setmoney$ObjectLiteral();
    this.cmd_learnmagic_0 = new ScriptProcess$cmd_learnmagic$ObjectLiteral();
    this.cmd_sale_0 = new ScriptProcess$cmd_sale$ObjectLiteral();
    this.cmd_npcmovemod_0 = new ScriptProcess$cmd_npcmovemod$ObjectLiteral(this);
    this.cmd_message_0 = new ScriptProcess$cmd_message$ObjectLiteral();
    this.cmd_deletegoods_0 = new ScriptProcess$cmd_deletegoods$ObjectLiteral(this);
    this.cmd_resumeactorhp_0 = new ScriptProcess$cmd_resumeactorhp$ObjectLiteral(this);
    this.cmd_actorlayerup_0 = new ScriptProcess$cmd_actorlayerup$ObjectLiteral();
    this.cmd_boxopen_0 = new ScriptProcess$cmd_boxopen$ObjectLiteral(this);
    this.cmd_delallnpc_0 = new ScriptProcess$cmd_delallnpc$ObjectLiteral(this);
    this.cmd_npcstep_0 = new ScriptProcess$cmd_npcstep$ObjectLiteral(this);
    this.cmd_setscenename_0 = new ScriptProcess$cmd_setscenename$ObjectLiteral(this);
    this.cmd_showscenename_0 = new ScriptProcess$cmd_showscenename$ObjectLiteral(this);
    this.cmd_showscreen_0 = new ScriptProcess$cmd_showscreen$ObjectLiteral(this);
    this.cmd_usegoods_0 = new ScriptProcess$cmd_usegoods$ObjectLiteral(this);
    this.cmd_attribtest_0 = new ScriptProcess$cmd_attribtest$ObjectLiteral(this);
    this.cmd_attribset_0 = new ScriptProcess$cmd_attribset$ObjectLiteral(this);
    this.cmd_attribadd_0 = new ScriptProcess$cmd_attribadd$ObjectLiteral(this);
    this.cmd_showgut_0 = new ScriptProcess$cmd_showgut$ObjectLiteral();
    this.cmd_usegoodsnum_0 = new ScriptProcess$cmd_usegoodsnum$ObjectLiteral(this);
    this.cmd_randrade_0 = new ScriptProcess$cmd_randrade$ObjectLiteral(this);
    this.cmd_menu_0 = new ScriptProcess$cmd_menu$ObjectLiteral();
    this.cmd_testmoney_0 = new ScriptProcess$cmd_testmoney$ObjectLiteral(this);
    this.cmd_callchapter_0 = new ScriptProcess$cmd_callchapter$ObjectLiteral();
    this.cmd_discmp_0 = new ScriptProcess$cmd_discmp$ObjectLiteral(this);
    this.cmd_return_0 = new ScriptProcess$cmd_return$ObjectLiteral();
    this.cmd_timemsg_0 = new ScriptProcess$cmd_timemsg$ObjectLiteral();
    this.cmd_disablesave_0 = new ScriptProcess$cmd_disablesave$ObjectLiteral();
    this.cmd_enablesave_0 = new ScriptProcess$cmd_enablesave$ObjectLiteral();
    this.cmd_gamesave_0 = new ScriptProcess$cmd_gamesave$ObjectLiteral();
    this.cmd_seteventtimer_0 = new ScriptProcess$cmd_seteventtimer$ObjectLiteral();
    this.cmd_enableshowpos_0 = new ScriptProcess$cmd_enableshowpos$ObjectLiteral();
    this.cmd_disableshowpos_0 = new ScriptProcess$cmd_disableshowpos$ObjectLiteral();
    this.cmd_setto_0 = new ScriptProcess$cmd_setto$ObjectLiteral();
    this.cmd_testgoodsnum_0 = new ScriptProcess$cmd_testgoodsnum$ObjectLiteral(this);
    this.cmd_setarmstoss_0 = new ScriptProcess$cmd_setarmstoss$ObjectLiteral();
    this.cmd_setfightmiss_0 = new ScriptProcess$cmd_setfightmiss$ObjectLiteral();
    this.mCmds_0 = [this.cmd_music_0, this.cmd_loadmap_0, this.cmd_createactor_0, this.cmd_deletenpc_0, null, null, this.cmd_move_0, null, null, this.cmd_callback_0, this.cmd_goto_0, this.cmd_if_0, this.cmd_set_0, this.cmd_say_0, this.cmd_startchapter_0, null, this.cmd_screens_0, null, null, null, this.cmd_gameover_0, this.cmd_ifcmp_0, this.cmd_add_0, this.cmd_sub_0, this.cmd_setcontrolid_0, null, this.cmd_setevent_0, this.cmd_clrevent_0, this.cmd_buy_0, this.cmd_facetoface_0, this.cmd_movie_0, this.cmd_choice_0, this.cmd_createbox_0, this.cmd_deletebox_0, this.cmd_gaingoods_0, this.cmd_initfight_0, this.cmd_fightenable_0, this.cmd_fightdisenable_0, this.cmd_createnpc_0, this.cmd_enterfight_0, this.cmd_deleteactor_0, this.cmd_gainmoney_0, this.cmd_usemoney_0, this.cmd_setmoney_0, this.cmd_learnmagic_0, this.cmd_sale_0, this.cmd_npcmovemod_0, this.cmd_message_0, this.cmd_deletegoods_0, this.cmd_resumeactorhp_0, this.cmd_actorlayerup_0, this.cmd_boxopen_0, this.cmd_delallnpc_0, this.cmd_npcstep_0, this.cmd_setscenename_0, this.cmd_showscenename_0, this.cmd_showscreen_0, this.cmd_usegoods_0, this.cmd_attribtest_0, this.cmd_attribset_0, this.cmd_attribadd_0, this.cmd_showgut_0, this.cmd_usegoodsnum_0, this.cmd_randrade_0, this.cmd_menu_0, this.cmd_testmoney_0, this.cmd_callchapter_0, this.cmd_discmp_0, this.cmd_return_0, this.cmd_timemsg_0, this.cmd_disablesave_0, this.cmd_enablesave_0, this.cmd_gamesave_0, this.cmd_seteventtimer_0, this.cmd_enableshowpos_0, this.cmd_disableshowpos_0, this.cmd_setto_0, this.cmd_testgoodsnum_0, this.cmd_setfightmiss_0, this.cmd_setarmstoss_0];
  }
  Object.defineProperty(ScriptProcess.prototype, 'delegate', {
    get: function () {
      if (this.delegate_41i177$_0 == null)
        return throwUPAE('delegate');
      return this.delegate_41i177$_0;
    },
    set: function (delegate) {
      this.delegate_41i177$_0 = delegate;
    }
  });
  Object.defineProperty(ScriptProcess.prototype, 'scriptExecutor', {
    get: function () {
      var tmp$, tmp$_0, tmp$_1, tmp$_2;
      if (this.mScript_0 == null)
        return null;
      var code = ensureNotNull(ensureNotNull(this.mScript_0).scriptData);
      var pointer = 0;
      var map = HashMap_init(128);
      var iOfOper = 0;
      var operateList = ArrayList_init();
      while (pointer < code.length) {
        map.put_xwzc9p$(pointer, iOfOper);
        iOfOper = iOfOper + 1 | 0;
        var cmdCode = code[pointer] & 255;
        var cmd = this.mCmds_0[cmdCode];
        if (cmd != null) {
          var operate = cmd.getOperate_ir89t6$(code, pointer + 1 | 0);
          operate.delagete = this.delegate;
          operateList.add_11rb$(operate);
          pointer = cmd.getNextPos_ir89t6$(code, pointer + 1 | 0);
        }
         else {
          throw new Error_0('ECMD: ' + cmdCode);
        }
      }
      var events = ensureNotNull(ensureNotNull(this.mScript_0).sceneEvent);
      var eventIndex = new Int32Array(events.length);
      tmp$ = get_indices_0(events);
      tmp$_0 = tmp$.first;
      tmp$_1 = tmp$.last;
      tmp$_2 = tmp$.step;
      for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
        if (events[i] === 0) {
          eventIndex[i] = -1;
        }
         else {
          eventIndex[i] = ensureNotNull(map.get_11rb$(events[i] - (events.length * 2 | 0) - 3 | 0));
        }
      }
      return new ScriptExecutor(operateList, eventIndex, map, (events.length * 2 | 0) + 3 | 0);
    }
  });
  ScriptProcess.prototype.setScreenMainGame_pforfg$ = function (screenMainGame) {
    this.mScreenMainGame_0 = screenMainGame;
  };
  ScriptProcess.prototype.loadScript_vux9f0$ = function (type, index) {
    var tmp$;
    this.mScript_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GUT_getInstance(), type, index), ResGut) ? tmp$ : throwCCE();
    return this.mScript_0 != null;
  };
  function ScriptProcess$Command() {
  }
  ScriptProcess$Command.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Command',
    interfaces: []
  };
  function ScriptProcess$Companion() {
    ScriptProcess$Companion_instance = this;
    this.instance_kj8x35$_0 = lazy(ScriptProcess$Companion$instance$lambda);
    this.cmdDebug = true;
  }
  Object.defineProperty(ScriptProcess$Companion.prototype, 'instance', {
    get: function () {
      var $receiver = this.instance_kj8x35$_0;
      new PropertyMetadata('instance');
      return $receiver.value;
    }
  });
  ScriptProcess$Companion.prototype.get2ByteInt_ir89t6$ = function (data, start) {
    return data[start] & 255 | data[start + 1 | 0] << 8 & 65280;
  };
  ScriptProcess$Companion.prototype.get4BytesInt_ir89t6$ = function (data, start) {
    return data[start] & 255 | data[start + 1 | 0] << 8 & 65280 | data[start + 2 | 0] << 16 & 16711680 | data[start + 3 | 0] << 24;
  };
  ScriptProcess$Companion.prototype.getStringBytes_ir89t6$ = function (data, start) {
    var i = 0;
    while (data[start + i | 0] !== 0)
      i = i + 1 | 0;
    var rlt = new Int8Array((i = i + 1 | 0, i));
    System_getInstance().arraycopy_nlwz52$(data, start, rlt, 0, i);
    return rlt;
  };
  ScriptProcess$Companion.prototype.cmdPrint_61zpoe$ = function (msg) {
    if (this.cmdDebug)
      println(msg);
  };
  function ScriptProcess$Companion$instance$lambda() {
    return new ScriptProcess();
  }
  ScriptProcess$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScriptProcess$Companion_instance = null;
  function ScriptProcess$Companion_getInstance() {
    if (ScriptProcess$Companion_instance === null) {
      new ScriptProcess$Companion();
    }
    return ScriptProcess$Companion_instance;
  }
  function ScriptProcess$cmd_music$ObjectLiteral() {
  }
  ScriptProcess$cmd_music$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  ScriptProcess$cmd_music$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    println('cmd_music not implemented');
    return OperateNop$Companion_getInstance().nop;
  };
  ScriptProcess$cmd_music$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_loadmap$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_loadmap$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 8 | 0;
  };
  function ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateDrawOnce.call(this);
    this.type_8be2vx$ = closure$code[closure$start] & 255 | closure$code[closure$start + 1 | 0] << 8 & 65280;
    this.index_8be2vx$ = closure$code[closure$start + 2 | 0] & 255 | closure$code[closure$start + 3 | 0] << 8 & 65280;
    this.x_8be2vx$ = closure$code[closure$start + 4 | 0] & 255 | closure$code[closure$start + 5 | 0] << 8 & 65280;
    this.y_8be2vx$ = closure$code[closure$start + 6 | 0] & 255 | closure$code[closure$start + 7 | 0] << 8 & 65280;
  }
  ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_loadmap type=' + this.type_8be2vx$ + ' index=' + this.index_8be2vx$ + ' x=' + this.x_8be2vx$ + ' y=' + this.y_8be2vx$);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).loadMap_tjonv8$(this.type_8be2vx$, this.index_8be2vx$, this.x_8be2vx$, this.y_8be2vx$);
    return true;
  };
  ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral.prototype.drawOnce_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateDrawOnce]
  };
  ScriptProcess$cmd_loadmap$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_loadmap$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_loadmap$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_createactor$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_createactor$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateDrawOnce.call(this);
  }
  ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var actor = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var x = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    var y = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_createactor ' + actor + ' at (' + x + ', ' + y + ')');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).createActor_qt1dr2$(actor, x, y);
    return true;
  };
  ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral.prototype.drawOnce_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateDrawOnce]
  };
  ScriptProcess$cmd_createactor$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_createactor$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_createactor$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_deletenpc$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_deletenpc$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var npc = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_deletenpc ' + npc);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).deleteNpc_za3lpa$(npc);
    return false;
  };
  ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_deletenpc$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_deletenpc$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_deletenpc$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_move$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_move$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    Operate.call(this);
    this.time_8be2vx$ = Kotlin.Long.fromInt(400);
    this.npc_8be2vx$_bw8x1b$_0 = this.npc_8be2vx$_bw8x1b$_0;
    this.dstX_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 2 | 0);
    this.dstY_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 4 | 0);
  }
  Object.defineProperty(ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype, 'npc_8be2vx$', {
    get: function () {
      if (this.npc_8be2vx$_bw8x1b$_0 == null)
        return throwUPAE('npc');
      return this.npc_8be2vx$_bw8x1b$_0;
    },
    set: function (npc) {
      this.npc_8be2vx$_bw8x1b$_0 = npc;
    }
  });
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.time_8be2vx$ = this.time_8be2vx$.add(delta);
    if (this.time_8be2vx$.compareTo_11rb$(Kotlin.Long.fromInt(100)) > 0) {
      var p = this.npc_8be2vx$.posInMap;
      if (this.dstX_8be2vx$ < p.x)
        this.npc_8be2vx$.walk_rtfsey$(Direction$West_getInstance());
      else if (this.dstX_8be2vx$ > p.x)
        this.npc_8be2vx$.walk_rtfsey$(Direction$East_getInstance());
      else if (this.dstY_8be2vx$ < p.y)
        this.npc_8be2vx$.walk_rtfsey$(Direction$North_getInstance());
      else if (this.dstY_8be2vx$ > p.y)
        this.npc_8be2vx$.walk_rtfsey$(Direction$South_getInstance());
      else
        return false;
      this.time_8be2vx$ = Kotlin.Long.ZERO;
    }
    return true;
  };
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    this.npc_8be2vx$ = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getNPC_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_move ' + this.npc_8be2vx$.name + ' to (' + this.dstX_8be2vx$ + ', ' + this.dstY_8be2vx$ + ')');
    return true;
  };
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_move$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_move$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_move$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_callback$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_callback$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_callback');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).exitScript();
    return false;
  };
  ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_callback$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_callback$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess);
  };
  ScriptProcess$cmd_callback$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_goto$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_goto$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var toAddr = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(toAddr);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_goto from ' + this.closure$start + ' to ' + toAddr);
    return false;
  };
  ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_goto$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_goto$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_goto$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_if$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_if$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var va = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var addr = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    var value = ScriptResources_getInstance().globalEvents[va];
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_if ' + va + '(=' + value + ') goto ' + addr);
    if (value) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(addr);
    }
    return false;
  };
  ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_if$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_if$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_if$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_set$ObjectLiteral() {
  }
  ScriptProcess$cmd_set$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var id = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var value = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_set ' + id + ' = ' + value);
    ScriptResources_getInstance().variables[id] = value;
    return false;
  };
  ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_set$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_set$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_set$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_say$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_say$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 2;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  function ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    Operate.call(this);
    this.picNum_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start);
    this.headImg_8be2vx$ = null;
    this.text_8be2vx$ = ScriptProcess$Companion_getInstance().getStringBytes_ir89t6$(closure$code, closure$start + 2 | 0);
    this.iOfText_8be2vx$ = 0;
    this.iOfNext_8be2vx$ = 0;
    this.isAnyKeyDown_8be2vx$ = false;
    this.rWithPic_8be2vx$ = new RectF(9.0, 50.0, 151.0, 96 - 0.5);
    this.rWithTextT_8be2vx$ = new Rect(44, 58, 145, 75);
    this.rWithTextB_8be2vx$ = new Rect(14, 76, 145, 93);
    this.rWithoutPic_8be2vx$ = new RectF(9.0, 55.0, 151.0, 96 - 0.5);
    this.rWithoutTextT_8be2vx$ = new Rect(14, 58, 145, 75);
    this.rWithoutTextB_8be2vx$ = new Rect(14, 76, 145, 93);
    this.paint_8be2vx$ = new Paint();
    var tmp$, tmp$_0;
    if (this.picNum_8be2vx$ !== 0) {
      tmp$_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, this.picNum_8be2vx$), ResImage) ? tmp$ : throwCCE();
    }
     else {
      tmp$_0 = null;
    }
    this.headImg_8be2vx$ = tmp$_0;
    this.paint_8be2vx$.color = Global_getInstance().COLOR_BLACK;
    this.paint_8be2vx$.style = Paint$Style$FILL_AND_STROKE_getInstance();
  }
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    if (this.isAnyKeyDown_8be2vx$) {
      if (this.iOfNext_8be2vx$ >= (this.text_8be2vx$.length - 1 | 0)) {
        return false;
      }
       else {
        this.iOfText_8be2vx$ = this.iOfNext_8be2vx$;
        this.isAnyKeyDown_8be2vx$ = false;
      }
    }
    return true;
  };
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_say ' + gbkString_0(this.text_8be2vx$));
    this.iOfText_8be2vx$ = 0;
    this.iOfNext_8be2vx$ = 0;
    return true;
  };
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.isAnyKeyDown_8be2vx$ = true;
  };
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    if (!Combat$Companion_getInstance().IsActive()) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
    }
    if (this.headImg_8be2vx$ == null) {
      this.paint_8be2vx$.color = Global_getInstance().COLOR_WHITE;
      this.paint_8be2vx$.style = Paint$Style$FILL_getInstance();
      canvas.drawRect_mw38p4$(this.rWithoutPic_8be2vx$, this.paint_8be2vx$);
      this.paint_8be2vx$.color = Global_getInstance().COLOR_BLACK;
      this.paint_8be2vx$.style = Paint$Style$STROKE_getInstance();
      this.paint_8be2vx$.strokeWidth = 1;
      canvas.drawRect_mw38p4$(this.rWithoutPic_8be2vx$, this.paint_8be2vx$);
      this.iOfNext_8be2vx$ = TextRender_getInstance().drawText_tz7kd0$(canvas, this.text_8be2vx$, this.iOfText_8be2vx$, this.rWithoutTextT_8be2vx$);
      this.iOfNext_8be2vx$ = TextRender_getInstance().drawText_tz7kd0$(canvas, this.text_8be2vx$, this.iOfNext_8be2vx$, this.rWithoutTextB_8be2vx$);
    }
     else {
      this.paint_8be2vx$.color = Global_getInstance().COLOR_WHITE;
      this.paint_8be2vx$.style = Paint$Style$FILL_getInstance();
      canvas.drawRect_mw38p4$(this.rWithPic_8be2vx$, this.paint_8be2vx$);
      this.paint_8be2vx$.color = Global_getInstance().COLOR_BLACK;
      this.paint_8be2vx$.style = Paint$Style$STROKE_getInstance();
      this.paint_8be2vx$.strokeWidth = 1;
      canvas.drawRect_mw38p4$(this.rWithPic_8be2vx$, this.paint_8be2vx$);
      canvas.drawLine_x3aj6j$(38, 50, 44, 56, this.paint_8be2vx$);
      canvas.drawLine_gwdwo5$(43.5, 56.0, 151.0, 56.0, this.paint_8be2vx$);
      this.headImg_8be2vx$.draw_tj1hu5$(canvas, 1, 13, 46);
      this.iOfNext_8be2vx$ = TextRender_getInstance().drawText_tz7kd0$(canvas, this.text_8be2vx$, this.iOfText_8be2vx$, this.rWithTextT_8be2vx$);
      this.iOfNext_8be2vx$ = TextRender_getInstance().drawText_tz7kd0$(canvas, this.text_8be2vx$, this.iOfNext_8be2vx$, this.rWithTextB_8be2vx$);
    }
  };
  ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_say$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_say$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_say$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_startchapter$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_startchapter$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
    this.type_8be2vx$ = 0;
    this.index_8be2vx$ = 0;
    this.type_8be2vx$ = closure$code[closure$start] & 255 | closure$code[closure$start + 1 | 0] << 8 & 255;
    this.index_8be2vx$ = closure$code[closure$start + 2 | 0] & 255 | closure$code[closure$start + 3 | 0] << 8 & 255;
  }
  ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_startchapter ' + this.type_8be2vx$ + ' ' + this.index_8be2vx$);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).startChapter_vux9f0$(this.type_8be2vx$, this.index_8be2vx$);
    return false;
  };
  ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_startchapter$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_startchapter$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_startchapter$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_screens$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_screens$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var x = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var y = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_screens (' + x + ',' + y + ')');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).setMapScreenPos_vux9f0$(x, y);
    return false;
  };
  ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_screens$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_screens$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_screens$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_gameover$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_gameover$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_gameover');
    this.this$ScriptProcess.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
    return false;
  };
  ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_gameover$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_gameover$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess);
  };
  ScriptProcess$cmd_gameover$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_ifcmp$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_ifcmp$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var id = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var other = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    var value = ScriptResources_getInstance().variables[id];
    var addr = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_ifcmp ' + id + '(=' + value + ') vs ' + other + ' goto ' + addr);
    if (value === other) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(addr);
    }
    return false;
  };
  ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_ifcmp$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_ifcmp$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_ifcmp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_add$ObjectLiteral() {
  }
  ScriptProcess$cmd_add$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_add');
    tmp$ = ScriptResources_getInstance().variables;
    tmp$_0 = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    tmp$[tmp$_0] = tmp$[tmp$_0] + ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0) | 0;
    return false;
  };
  ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_add$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_add$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_add$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_sub$ObjectLiteral() {
  }
  ScriptProcess$cmd_sub$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_sub');
    tmp$ = ScriptResources_getInstance().variables;
    tmp$_0 = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    tmp$[tmp$_0] = tmp$[tmp$_0] - ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0) | 0;
    return false;
  };
  ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_sub$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_sub$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_sub$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setcontrolid$ObjectLiteral() {
  }
  ScriptProcess$cmd_setcontrolid$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  ScriptProcess$cmd_setcontrolid$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    throw new NotImplementedError('cmd_setcontrolid');
  };
  ScriptProcess$cmd_setcontrolid$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setevent$ObjectLiteral() {
  }
  ScriptProcess$cmd_setevent$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var event = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_setevent ' + event);
    ScriptResources_getInstance().setEvent_za3lpa$(event);
    return false;
  };
  ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_setevent$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_setevent$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_setevent$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_clrevent$ObjectLiteral() {
  }
  ScriptProcess$cmd_clrevent$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var event = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_clrevent ' + event);
    ScriptResources_getInstance().clearEvent_za3lpa$(event);
    return false;
  };
  ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_clrevent$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_clrevent$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_clrevent$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_buy$ObjectLiteral() {
  }
  ScriptProcess$cmd_buy$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 0;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  ScriptProcess$cmd_buy$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new OperateBuy(code, start);
  };
  ScriptProcess$cmd_buy$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_facetoface$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_facetoface$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateDrawOnce.call(this);
  }
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.prototype.getCharacter_0 = function (id) {
    var tmp$;
    if (id === 0) {
      tmp$ = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).player;
    }
     else
      tmp$ = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getNPC_za3lpa$(id);
    return tmp$;
  };
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_facetoface');
    var c1 = this.getCharacter_0(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    var c2 = this.getCharacter_0(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    var p1 = ensureNotNull(c1).posInMap;
    var p2 = ensureNotNull(c2).posInMap;
    if (p1.x > p2.x) {
      c2.direction = Direction$East_getInstance();
    }
     else if (p1.x < p2.x) {
      c2.direction = Direction$West_getInstance();
    }
     else {
      if (p1.y > p2.y) {
        c2.direction = Direction$South_getInstance();
      }
       else if (p1.y < p2.y) {
        c2.direction = Direction$North_getInstance();
      }
    }
    return true;
  };
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.prototype.drawOnce_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateDrawOnce]
  };
  ScriptProcess$cmd_facetoface$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_facetoface$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_facetoface$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_movie$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_movie$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 10 | 0;
  };
  function ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    Operate.call(this);
    this.type_8be2vx$ = 0;
    this.index_8be2vx$ = 0;
    this.x_8be2vx$ = 0;
    this.y_8be2vx$ = 0;
    this.ctl_8be2vx$ = 0;
    this.downKey_8be2vx$ = 0;
    this.isAnyKeyPressed_8be2vx$ = false;
    this.movie_8be2vx$_9btww3$_0 = this.movie_8be2vx$_9btww3$_0;
    this.type_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start);
    this.index_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 2 | 0);
    this.x_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 4 | 0);
    this.y_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 6 | 0);
    this.ctl_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 8 | 0);
  }
  Object.defineProperty(ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype, 'movie_8be2vx$', {
    get: function () {
      if (this.movie_8be2vx$_9btww3$_0 == null)
        return throwUPAE('movie');
      return this.movie_8be2vx$_9btww3$_0;
    },
    set: function (movie) {
      this.movie_8be2vx$_9btww3$_0 = movie;
    }
  });
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    var tmp$;
    if ((this.ctl_8be2vx$ === 1 || this.ctl_8be2vx$ === 3) && this.isAnyKeyPressed_8be2vx$) {
      tmp$ = false;
    }
     else
      tmp$ = this.movie_8be2vx$.update_s8cxhz$(delta);
    return tmp$;
  };
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_movie');
    this.movie_8be2vx$ = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), this.type_8be2vx$, this.index_8be2vx$), ResSrs) ? tmp$ : throwCCE();
    this.movie_8be2vx$.setIteratorNum_za3lpa$(5);
    this.movie_8be2vx$.startAni();
    return true;
  };
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === this.downKey_8be2vx$) {
      this.isAnyKeyPressed_8be2vx$ = true;
    }
  };
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.downKey_8be2vx$ = key;
  };
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    if (this.ctl_8be2vx$ === 2 || this.ctl_8be2vx$ === 3) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
    }
    this.movie_8be2vx$.draw_2g4tob$(canvas, this.x_8be2vx$, this.y_8be2vx$);
  };
  ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_movie$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_movie$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_movie$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_choice$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_choice$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 0;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    i = i + 1 | 0;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 3 | 0;
  };
  function ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    Operate.call(this);
    this.choice1_8be2vx$ = ScriptProcess$Companion_getInstance().getStringBytes_ir89t6$(closure$code, closure$start);
    this.choice2_8be2vx$ = ScriptProcess$Companion_getInstance().getStringBytes_ir89t6$(closure$code, closure$start + this.choice1_8be2vx$.length | 0);
    this.bg_8be2vx$ = null;
    this.bgx_8be2vx$ = 0;
    this.bgy_8be2vx$ = 0;
    this.curChoice_8be2vx$ = 0;
    this.addrOffset_8be2vx$ = this.choice1_8be2vx$.length + this.choice2_8be2vx$.length | 0;
    this.hasSelect_8be2vx$ = false;
    this.mLastDownKey_0 = -1;
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var w;
    var tmp;
    if (this.choice1_8be2vx$.length > this.choice2_8be2vx$.length) {
      w = (this.choice1_8be2vx$.length * 8 | 0) - 8 + 6 | 0;
      tmp = new Int8Array(this.choice1_8be2vx$.length);
      System_getInstance().arraycopy_nlwz52$(this.choice2_8be2vx$, 0, tmp, 0, this.choice2_8be2vx$.length);
      tmp$ = this.choice2_8be2vx$.length - 1 | 0;
      tmp$_0 = tmp.length;
      for (var i = tmp$; i < tmp$_0; i++) {
        tmp[i] = toByte(32 | 0);
      }
      tmp[tmp.length - 1 | 0] = 0;
      this.choice2_8be2vx$ = tmp;
    }
     else {
      w = (this.choice2_8be2vx$.length * 8 | 0) - 8 + 6 | 0;
      tmp = new Int8Array(this.choice2_8be2vx$.length);
      System_getInstance().arraycopy_nlwz52$(this.choice1_8be2vx$, 0, tmp, 0, this.choice1_8be2vx$.length);
      tmp$_1 = this.choice1_8be2vx$.length - 1 | 0;
      tmp$_2 = tmp.length;
      for (var i_0 = tmp$_1; i_0 < tmp$_2; i_0++) {
        tmp[i_0] = toByte(32 | 0);
      }
      tmp[tmp.length - 1 | 0] = 0;
      this.choice1_8be2vx$ = tmp;
    }
    this.bg_8be2vx$ = Util_getInstance().getFrameBitmap_vux9f0$(w, (16 * 2 | 0) + 6 | 0);
    this.bgx_8be2vx$ = (160 - this.bg_8be2vx$.width | 0) / 2 | 0;
    this.bgy_8be2vx$ = (96 - this.bg_8be2vx$.height | 0) / 2 | 0;
  }
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_choice');
    this.curChoice_8be2vx$ = 0;
    this.hasSelect_8be2vx$ = false;
    return true;
  };
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    if (this.hasSelect_8be2vx$) {
      if (this.curChoice_8be2vx$ === 1) {
        ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + this.addrOffset_8be2vx$ | 0));
      }
      return false;
    }
    return true;
  };
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER && this.mLastDownKey_0 === key) {
      this.hasSelect_8be2vx$ = true;
    }
  };
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_DOWN || key === Global_getInstance().KEY_UP || key === Global_getInstance().KEY_LEFT || key === Global_getInstance().KEY_RIGHT)
      this.curChoice_8be2vx$ = 1 - this.curChoice_8be2vx$ | 0;
    this.mLastDownKey_0 = key;
  };
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
    canvas.drawBitmap_t8cslu$(this.bg_8be2vx$, this.bgx_8be2vx$, this.bgy_8be2vx$);
    if (this.curChoice_8be2vx$ === 0) {
      TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.choice1_8be2vx$, this.bgx_8be2vx$ + 3 | 0, this.bgy_8be2vx$ + 3 | 0);
      TextRender_getInstance().drawText_pbrmiz$(canvas, this.choice2_8be2vx$, this.bgx_8be2vx$ + 3 | 0, this.bgy_8be2vx$ + 3 + 16 | 0);
    }
     else {
      TextRender_getInstance().drawText_pbrmiz$(canvas, this.choice1_8be2vx$, this.bgx_8be2vx$ + 3 | 0, this.bgy_8be2vx$ + 3 | 0);
      TextRender_getInstance().drawSelText_pbrmiz$(canvas, this.choice2_8be2vx$, this.bgx_8be2vx$ + 3 | 0, this.bgy_8be2vx$ + 3 + 16 | 0);
    }
  };
  ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_choice$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_choice$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_choice$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_createbox$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_createbox$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 8 | 0;
  };
  function ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var id = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start);
    var boxId = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    var x = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0);
    var y = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0);
    var box = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).createBox_tjonv8$(id, boxId, x, y);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_createbox ' + box.name + ' at (' + x + ',' + y + ')');
    return false;
  };
  ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_createbox$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_createbox$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_createbox$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_deletebox$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_deletebox$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_deletebox');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).deleteBox_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    return false;
  };
  ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_deletebox$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_deletebox$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_deletebox$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_gaingoods$ObjectLiteral() {
  }
  ScriptProcess$cmd_gaingoods$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    Operate.call(this);
    var tmp$;
    this.goods_8be2vx$ = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$GRS_getInstance(), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 2 | 0)), BaseGoods) ? tmp$ : throwCCE();
    this.msg_8be2vx$ = '\u83B7\u5F97:' + this.goods_8be2vx$.name;
    this.time_8be2vx$ = Kotlin.Long.ZERO;
    this.isAnyKeyPressed_8be2vx$ = false;
    this.downKey_8be2vx$ = 0;
  }
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_gaingoods ' + this.goods_8be2vx$.name);
    this.goods_8be2vx$.goodsNum = 1;
    Player$Companion_getInstance().sGoodsList.addGoods_vux9f0$(this.goods_8be2vx$.type, this.goods_8be2vx$.index);
    this.time_8be2vx$ = Kotlin.Long.ZERO;
    this.isAnyKeyPressed_8be2vx$ = false;
    this.downKey_8be2vx$ = -1;
    return true;
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.time_8be2vx$ = this.time_8be2vx$.add(delta);
    return !(this.time_8be2vx$.compareTo_11rb$(Kotlin.Long.fromInt(1000)) > 0 || this.isAnyKeyPressed_8be2vx$);
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === this.downKey_8be2vx$) {
      this.isAnyKeyPressed_8be2vx$ = true;
    }
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.downKey_8be2vx$ = key;
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    Util_getInstance().showMessage_g6cl4j$(canvas, this.msg_8be2vx$);
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_gaingoods$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_gaingoods$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_initfight$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_initfight$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 22 | 0;
  };
  function ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_initfight');
    var arr = new Int32Array(8);
    for (var i = 0; i <= 7; i++) {
      arr[i] = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + (i * 2 | 0) | 0);
    }
    Combat$Companion_getInstance().InitFight_7lcbvb$(arr, ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 16 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 18 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 20 | 0));
    Combat$Companion_getInstance().SetDelegate_wle2mc$(this.this$ScriptProcess.delegate);
    return false;
  };
  ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_initfight$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_initfight$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_initfight$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_fightenable$ObjectLiteral() {
  }
  ScriptProcess$cmd_fightenable$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral() {
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_fightenable');
    Combat$Companion_getInstance().FightEnable();
    return false;
  };
  ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_fightenable$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_fightenable$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_fightenable$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_fightdisenable$ObjectLiteral() {
  }
  ScriptProcess$cmd_fightdisenable$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral() {
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_fightdisable');
    Combat$Companion_getInstance().FightDisable();
    return false;
  };
  ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_fightdisenable$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_fightdisenable$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_fightdisenable$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_createnpc$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_createnpc$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 8 | 0;
  };
  function ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var npc = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).createNpc_tjonv8$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0));
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_createnpc ' + npc.name + ' at ' + npc.posInMap);
    return false;
  };
  ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_createnpc$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_createnpc$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_createnpc$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_enterfight$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_enterfight$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 30 | 0;
  };
  function ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_enterfight');
    var monstersType = new Int32Array([ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0)]);
    var scr = new Int32Array([ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 8 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 10 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 12 | 0)]);
    var evtRnds = new Int32Array([ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 14 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 16 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 18 | 0)]);
    var evts = new Int32Array([ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 20 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 22 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 24 | 0)]);
    var lossto = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 26 | 0);
    var winto = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 28 | 0);
    Combat$Companion_getInstance().EnterFight_hkogy6$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), monstersType, scr, evtRnds, evts, lossto, winto);
    Combat$Companion_getInstance().SetDelegate_wle2mc$(this.this$ScriptProcess.delegate);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).exitScript();
    return false;
  };
  ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_enterfight$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_enterfight$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_enterfight$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_deleteactor$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_deleteactor$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_deleteactor');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).deleteActor_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    return false;
  };
  ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_deleteactor$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_deleteactor$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_deleteactor$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_gainmoney$ObjectLiteral() {
  }
  ScriptProcess$cmd_gainmoney$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_gainmoney');
    tmp$ = Player$Companion_getInstance();
    tmp$.sMoney = tmp$.sMoney + ScriptProcess$Companion_getInstance().get4BytesInt_ir89t6$(this.closure$code, this.closure$start) | 0;
    return false;
  };
  ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_gainmoney$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_gainmoney$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_gainmoney$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_usemoney$ObjectLiteral() {
  }
  ScriptProcess$cmd_usemoney$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_usemoney');
    tmp$ = Player$Companion_getInstance();
    tmp$.sMoney = tmp$.sMoney - ScriptProcess$Companion_getInstance().get4BytesInt_ir89t6$(this.closure$code, this.closure$start) | 0;
    return false;
  };
  ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_usemoney$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_usemoney$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_usemoney$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setmoney$ObjectLiteral() {
  }
  ScriptProcess$cmd_setmoney$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    Player$Companion_getInstance().sMoney = ScriptProcess$Companion_getInstance().get4BytesInt_ir89t6$(this.closure$code, this.closure$start);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_setmoney ' + Player$Companion_getInstance().sMoney);
    return false;
  };
  ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_setmoney$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_setmoney$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_setmoney$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_learnmagic$ObjectLiteral() {
  }
  ScriptProcess$cmd_learnmagic$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    Operate.call(this);
    this.isAnyKeyDown_8be2vx$ = false;
    this.timeCnt_8be2vx$ = Kotlin.Long.ZERO;
  }
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.timeCnt_8be2vx$ = this.timeCnt_8be2vx$.add(delta);
    return this.timeCnt_8be2vx$.compareTo_11rb$(Kotlin.Long.fromInt(1000)) < 0 && !this.isAnyKeyDown_8be2vx$;
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_learmagic');
    this.isAnyKeyDown_8be2vx$ = false;
    this.timeCnt_8be2vx$ = Kotlin.Long.ZERO;
    return true;
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    TextRender_getInstance().drawText_kkuqvh$(canvas, '\u5B66\u4F1A\u4E86\u9B54\u6CD5:', 0, 0);
    TextRender_getInstance().drawText_kkuqvh$(canvas, 'actorId:' + toString(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start)) + 't' + toString(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0)) + 'i' + toString(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0)), 0, 16);
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_learnmagic$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_learnmagic$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_sale$ObjectLiteral() {
  }
  ScriptProcess$cmd_sale$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  ScriptProcess$cmd_sale$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new OperateSale();
  };
  ScriptProcess$cmd_sale$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_npcmovemod$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_npcmovemod$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_npcmovemod');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getNPC_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start)).state = Character$State$Companion_getInstance().fromInt_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    return false;
  };
  ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_npcmovemod$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_npcmovemod$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_npcmovemod$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_message$ObjectLiteral() {
  }
  ScriptProcess$cmd_message$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 0;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  function ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    Operate.call(this);
    this.msg_8be2vx$ = ScriptProcess$Companion_getInstance().getStringBytes_ir89t6$(closure$code, closure$start);
    this.downKey_8be2vx$ = 0;
    this.isAnyKeyDown_8be2vx$ = false;
  }
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_message ' + gbkString_0(this.msg_8be2vx$));
    this.downKey_8be2vx$ = -1;
    this.isAnyKeyDown_8be2vx$ = false;
    return true;
  };
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    return !this.isAnyKeyDown_8be2vx$;
  };
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (this.downKey_8be2vx$ === key) {
      this.isAnyKeyDown_8be2vx$ = true;
    }
  };
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.downKey_8be2vx$ = key;
  };
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    Util_getInstance().showMessage_2yb3jp$(canvas, this.msg_8be2vx$);
  };
  ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_message$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_message$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_message$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_deletegoods$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_deletegoods$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_deletegoods');
    var r = Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    if (!r) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_deletegoods$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_deletegoods$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_deletegoods$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_resumeactorhp$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_resumeactorhp');
    var p = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getPlayer_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    if (p != null) {
      p.hp = Kotlin.imul(p.maxHP, ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0)) / 100 | 0;
    }
    return false;
  };
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_resumeactorhp$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_resumeactorhp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_actorlayerup$ObjectLiteral() {
  }
  ScriptProcess$cmd_actorlayerup$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral() {
    Operate.call(this);
    this.exit_8be2vx$ = false;
  }
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    return !this.exit_8be2vx$;
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_actorlayerup TODO');
    return true;
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.exit_8be2vx$ = true;
    }
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    TextRender_getInstance().drawText_kkuqvh$(canvas, 'cmd_actorlayerup', 10, 20);
    TextRender_getInstance().drawText_kkuqvh$(canvas, 'press cancel to continue', 0, 40);
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_actorlayerup$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_actorlayerup$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_boxopen$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_boxopen$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  function ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_boxopen');
    var box = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getNPC_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start));
    box.step = 1;
    return false;
  };
  ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_boxopen$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_boxopen$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_boxopen$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_delallnpc$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_delallnpc$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_delallnpc');
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).deleteAllNpc();
    return false;
  };
  ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_delallnpc$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_delallnpc$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess);
  };
  ScriptProcess$cmd_delallnpc$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_npcstep$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_npcstep$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess, closure$code, closure$start) {
    this.this$ScriptProcess = this$ScriptProcess;
    Operate.call(this);
    this.time_8be2vx$ = Kotlin.Long.ZERO;
    this.interval_8be2vx$ = Kotlin.Long.ZERO;
    this.id_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start);
    this.faceto_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 2 | 0);
    this.step_8be2vx$ = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start + 4 | 0);
  }
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.time_8be2vx$ = this.time_8be2vx$.add(delta);
    return this.time_8be2vx$.compareTo_11rb$(this.interval_8be2vx$) < 0;
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0, tmp$_1;
    this.time_8be2vx$ = Kotlin.Long.ZERO;
    tmp$ = this.faceto_8be2vx$;
    if (tmp$ === 0)
      tmp$_0 = Direction$North_getInstance();
    else if (tmp$ === 1)
      tmp$_0 = Direction$East_getInstance();
    else if (tmp$ === 2)
      tmp$_0 = Direction$South_getInstance();
    else if (tmp$ === 3)
      tmp$_0 = Direction$West_getInstance();
    else
      tmp$_0 = Direction$South_getInstance();
    var d = tmp$_0;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_npcstep ' + this.id_8be2vx$ + ' ' + d + ' step=' + this.step_8be2vx$);
    if (this.id_8be2vx$ === 0) {
      var p = ensureNotNull(ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).player);
      p.direction = d;
      p.step = this.step_8be2vx$;
      this.interval_8be2vx$ = Kotlin.Long.fromInt(300);
    }
     else {
      var npc = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).getNPC_za3lpa$(this.id_8be2vx$);
      npc.direction = d;
      npc.step = this.step_8be2vx$;
      if (ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).isNpcVisible_l8hog8$(npc)) {
        tmp$_1 = Kotlin.Long.fromInt(300);
      }
       else {
        tmp$_1 = Kotlin.Long.ZERO;
      }
      this.interval_8be2vx$ = tmp$_1;
    }
    return true;
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_npcstep$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess, code, start);
  };
  ScriptProcess$cmd_npcstep$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setscenename$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_setscenename$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 0;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  function ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var name = ResBase$Companion_getInstance().getString_ir89t6$(this.closure$code, this.closure$start);
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_setscenname ' + name);
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).sceneName = name;
    return false;
  };
  ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_setscenename$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_setscenename$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_setscenename$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_showscenename$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_showscenename$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
    Operate.call(this);
    this.time_8be2vx$ = Kotlin.Long.ZERO;
    this.text_8be2vx$ = '';
    this.isAnyKeyDown_8be2vx$ = false;
  }
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.time_8be2vx$ = this.time_8be2vx$.add(delta);
    if (this.time_8be2vx$.compareTo_11rb$(Kotlin.Long.fromInt(100)) > 0 && this.isAnyKeyDown_8be2vx$) {
      this.isAnyKeyDown_8be2vx$ = false;
      return false;
    }
    return this.time_8be2vx$.compareTo_11rb$(Kotlin.Long.fromInt(1000)) < 0;
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_showscenename');
    this.text_8be2vx$ = ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).sceneName;
    return true;
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.isAnyKeyDown_8be2vx$ = true;
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
    Util_getInstance().showInformation_g6cl4j$(canvas, this.text_8be2vx$);
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_showscenename$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess);
  };
  ScriptProcess$cmd_showscenename$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_showscreen$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_showscreen$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
    OperateDrawOnce.call(this);
  }
  ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_showscreen');
    return true;
  };
  ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral.prototype.drawOnce_9in0vv$ = function (canvas) {
    ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).drawScene_9in0vv$(canvas);
  };
  ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateDrawOnce]
  };
  ScriptProcess$cmd_showscreen$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_showscreen$ObjectLiteral$getOperate$ObjectLiteral(this.this$ScriptProcess);
  };
  ScriptProcess$cmd_showscreen$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_usegoods$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_usegoods$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_usegoods');
    var b = Player$Companion_getInstance().sGoodsList.deleteGoods_vux9f0$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    if (!b) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_usegoods$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_usegoods$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_usegoods$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_attribtest$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_attribtest$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 10 | 0;
  };
  function ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral(closure$actor, closure$type, closure$value, this$ScriptProcess, closure$addr1, closure$addr2) {
    this.closure$actor = closure$actor;
    this.closure$type = closure$type;
    this.closure$value = closure$value;
    this.this$ScriptProcess = this$ScriptProcess;
    this.closure$addr1 = closure$addr1;
    this.closure$addr2 = closure$addr2;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9, tmp$_10, tmp$_11, tmp$_12, tmp$_13, tmp$_14, tmp$_15, tmp$_16, tmp$_17, tmp$_18;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_attribtest ' + this.closure$actor + ' ' + this.closure$type + ' ' + this.closure$value);
    tmp$_0 = (tmp$ = this.this$ScriptProcess.mScreenMainGame_0) != null ? tmp$.getPlayer_za3lpa$(this.closure$actor) : null;
    if (tmp$_0 == null) {
      return false;
    }
    var player = tmp$_0;
    tmp$_1 = this.closure$type;
    if (tmp$_1 === 0)
      tmp$_18 = player.level;
    else if (tmp$_1 === 1)
      tmp$_18 = player.attack;
    else if (tmp$_1 === 2)
      tmp$_18 = player.defend;
    else if (tmp$_1 === 3)
      tmp$_18 = player.speed;
    else if (tmp$_1 === 4)
      tmp$_18 = player.hp;
    else if (tmp$_1 === 5)
      tmp$_18 = player.mp;
    else if (tmp$_1 === 6)
      tmp$_18 = player.currentExp;
    else if (tmp$_1 === 7)
      tmp$_18 = player.lingli;
    else if (tmp$_1 === 8)
      tmp$_18 = player.luck;
    else if (tmp$_1 === 15)
      tmp$_18 = (tmp$_3 = (tmp$_2 = player.equipmentsArray[7]) != null ? tmp$_2.index : null) != null ? tmp$_3 : 0;
    else if (tmp$_1 === 16)
      tmp$_18 = (tmp$_5 = (tmp$_4 = player.equipmentsArray[5]) != null ? tmp$_4.index : null) != null ? tmp$_5 : 0;
    else if (tmp$_1 === 17)
      tmp$_18 = (tmp$_7 = (tmp$_6 = player.equipmentsArray[6]) != null ? tmp$_6.index : null) != null ? tmp$_7 : 0;
    else if (tmp$_1 === 18)
      tmp$_18 = (tmp$_9 = (tmp$_8 = player.equipmentsArray[2]) != null ? tmp$_8.index : null) != null ? tmp$_9 : 0;
    else if (tmp$_1 === 19)
      tmp$_18 = (tmp$_11 = (tmp$_10 = player.equipmentsArray[4]) != null ? tmp$_10.index : null) != null ? tmp$_11 : 0;
    else if (tmp$_1 === 20)
      tmp$_18 = (tmp$_13 = (tmp$_12 = player.equipmentsArray[3]) != null ? tmp$_12.index : null) != null ? tmp$_13 : 0;
    else if (tmp$_1 === 21)
      tmp$_18 = (tmp$_15 = (tmp$_14 = player.equipmentsArray[0]) != null ? tmp$_14.index : null) != null ? tmp$_15 : 0;
    else if (tmp$_1 === 22)
      tmp$_18 = (tmp$_17 = (tmp$_16 = player.equipmentsArray[1]) != null ? tmp$_16.index : null) != null ? tmp$_17 : 0;
    else if (tmp$_1 === 23)
      tmp$_18 = player.maxHP;
    else if (tmp$_1 === 24)
      tmp$_18 = player.maxMP;
    else
      throw new NotImplementedError('ATTRIBTEST ' + this.closure$type);
    var currentValue = tmp$_18;
    if (currentValue < this.closure$value)
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(this.closure$addr1);
    else if (currentValue > this.closure$value)
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(this.closure$addr2);
    return false;
  };
  ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_attribtest$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    var actor = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start);
    var type = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 2 | 0);
    var value = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 4 | 0);
    var addr1 = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 6 | 0);
    var addr2 = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 8 | 0);
    return new ScriptProcess$cmd_attribtest$ObjectLiteral$getOperate$ObjectLiteral(actor, type, value, this.this$ScriptProcess, addr1, addr2);
  };
  ScriptProcess$cmd_attribtest$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_attribset$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_attribset$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral(closure$actor, closure$type, closure$value, this$ScriptProcess) {
    this.closure$actor = closure$actor;
    this.closure$type = closure$type;
    this.closure$value = closure$value;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0, tmp$_1;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_attribset ' + this.closure$actor + ' ' + this.closure$type + ' ' + this.closure$value);
    tmp$_0 = (tmp$ = this.this$ScriptProcess.mScreenMainGame_0) != null ? tmp$.getPlayer_za3lpa$(this.closure$actor) : null;
    if (tmp$_0 == null) {
      return false;
    }
    var player = tmp$_0;
    tmp$_1 = this.closure$type;
    if (tmp$_1 === 0)
      player.setLevel_za3lpa$(this.closure$value);
    else if (tmp$_1 === 1)
      player.attack = this.closure$value;
    else if (tmp$_1 === 2)
      player.defend = this.closure$value;
    else if (tmp$_1 === 3)
      player.speed = this.closure$value;
    else if (tmp$_1 === 4)
      player.hp = this.closure$value;
    else if (tmp$_1 === 5)
      player.mp = this.closure$value;
    else if (tmp$_1 === 6)
      player.currentExp = this.closure$value;
    else if (tmp$_1 === 7)
      player.lingli = this.closure$value;
    else if (tmp$_1 === 8)
      player.luck = this.closure$value;
    else if (tmp$_1 === 15)
      player.maxHP = this.closure$value;
    else if (tmp$_1 === 16)
      player.maxMP = this.closure$value;
    else
      throw new NotImplementedError('ATTRIBSET ' + this.closure$type);
    return false;
  };
  ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_attribset$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    var actor = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start);
    var type = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 2 | 0);
    var value = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 4 | 0);
    return new ScriptProcess$cmd_attribset$ObjectLiteral$getOperate$ObjectLiteral(actor, type, value, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_attribset$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_attribadd$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_attribadd$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral(closure$actor, closure$type, closure$value, this$ScriptProcess) {
    this.closure$actor = closure$actor;
    this.closure$type = closure$type;
    this.closure$value = closure$value;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    var tmp$, tmp$_0, tmp$_1;
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_attribadd ' + this.closure$actor + ' ' + this.closure$type + ' ' + this.closure$value);
    tmp$_0 = (tmp$ = this.this$ScriptProcess.mScreenMainGame_0) != null ? tmp$.getPlayer_za3lpa$(this.closure$actor) : null;
    if (tmp$_0 == null) {
      return false;
    }
    var player = tmp$_0;
    tmp$_1 = this.closure$type;
    if (tmp$_1 === 0)
      player.setLevel_za3lpa$(player.level + this.closure$value | 0);
    else if (tmp$_1 === 1)
      player.attack = player.attack + this.closure$value | 0;
    else if (tmp$_1 === 2)
      player.defend = player.defend + this.closure$value | 0;
    else if (tmp$_1 === 3)
      player.speed = player.speed + this.closure$value | 0;
    else if (tmp$_1 === 4)
      player.hp = player.hp + this.closure$value | 0;
    else if (tmp$_1 === 5)
      player.mp = player.mp + this.closure$value | 0;
    else if (tmp$_1 === 6)
      player.currentExp = player.currentExp + this.closure$value | 0;
    else if (tmp$_1 === 7)
      player.lingli = player.lingli + this.closure$value | 0;
    else if (tmp$_1 === 8)
      player.luck = player.luck + this.closure$value | 0;
    else if (tmp$_1 === 10)
      player.maxHP = player.maxHP + this.closure$value | 0;
    else if (tmp$_1 === 11)
      player.maxMP = player.maxMP + this.closure$value | 0;
    else
      throw new NotImplementedError('ATTRIBADD ' + this.closure$type);
    return false;
  };
  ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_attribadd$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    var actor = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start);
    var type = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 2 | 0);
    var value = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(code, start + 4 | 0);
    return new ScriptProcess$cmd_attribadd$ObjectLiteral$getOperate$ObjectLiteral(actor, type, value, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_attribadd$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_showgut$ObjectLiteral() {
  }
  ScriptProcess$cmd_showgut$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 4;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  function ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    Operate.call(this);
    this.imgTop_8be2vx$ = null;
    this.imgBottom_8be2vx$ = null;
    this.text_8be2vx$ = null;
    this.goon_8be2vx$ = true;
    this.interval_8be2vx$ = Kotlin.Long.fromInt(50);
    this.timeCnt_8be2vx$ = Kotlin.Long.ZERO;
    this.step_8be2vx$ = 1;
    this.curY_8be2vx$ = 0;
    this.rect_8be2vx$ = null;
    this.top_0 = closure$code[closure$start] & 255 | closure$code[closure$start + 1 | 0] << 8 & 65280;
    this.btm_0 = closure$code[closure$start + 2 | 0] & 255 | closure$code[closure$start + 3 | 0] << 8 & 65280;
    var tmp$, tmp$_0;
    this.imgTop_8be2vx$ = DatLib$Companion_getInstance().getPic_ydzd23$(5, this.top_0, true);
    this.imgBottom_8be2vx$ = DatLib$Companion_getInstance().getPic_ydzd23$(5, this.btm_0, true);
    this.text_8be2vx$ = gbkBytes(ResBase$Companion_getInstance().getString_ir89t6$(closure$code, closure$start + 4 | 0));
    this.curY_8be2vx$ = this.imgBottom_8be2vx$ != null ? 96 - this.imgBottom_8be2vx$.height | 0 : 96;
    this.rect_8be2vx$ = new Rect(0, (tmp$_0 = (tmp$ = this.imgTop_8be2vx$) != null ? tmp$.height : null) != null ? tmp$_0 : 0, 160, this.curY_8be2vx$);
  }
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_showgut topimg = ' + this.top_0 + ', btmimg = ' + this.btm_0);
    this.goon_8be2vx$ = true;
    this.interval_8be2vx$ = Kotlin.Long.fromInt(50);
    this.timeCnt_8be2vx$ = Kotlin.Long.ZERO;
    this.step_8be2vx$ = 1;
    this.curY_8be2vx$ = this.imgBottom_8be2vx$ != null ? 96 - this.imgBottom_8be2vx$.height | 0 : 96;
    return true;
  };
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    if (!this.goon_8be2vx$)
      return false;
    this.timeCnt_8be2vx$ = this.timeCnt_8be2vx$.add(delta);
    if (this.timeCnt_8be2vx$.compareTo_11rb$(this.interval_8be2vx$) >= 0) {
      this.timeCnt_8be2vx$ = Kotlin.Long.ZERO;
      this.curY_8be2vx$ = this.curY_8be2vx$ - this.step_8be2vx$ | 0;
    }
    return true;
  };
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL) {
      this.goon_8be2vx$ = false;
    }
    this.step_8be2vx$ = 1;
    this.interval_8be2vx$ = Kotlin.Long.fromInt(50);
  };
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.step_8be2vx$ = 3;
    this.interval_8be2vx$ = Kotlin.Long.fromInt(20);
  };
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0;
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    var e = TextRender_getInstance().drawText_sfexxe$(canvas, this.text_8be2vx$, this.rect_8be2vx$, this.curY_8be2vx$);
    if (e !== 1 && e !== 2) {
      this.goon_8be2vx$ = false;
    }
    (tmp$ = this.imgTop_8be2vx$) != null ? (tmp$.draw_tj1hu5$(canvas, 1, 0, 0), Unit) : null;
    (tmp$_0 = this.imgBottom_8be2vx$) != null ? (tmp$_0.draw_tj1hu5$(canvas, 1, 0, 96 - this.imgBottom_8be2vx$.height | 0), Unit) : null;
  };
  ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [Operate]
  };
  ScriptProcess$cmd_showgut$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_showgut$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_showgut$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_usegoodsnum$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 8 | 0;
  };
  function ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_usegoodsnum');
    var b = Player$Companion_getInstance().sGoodsList.useGoodsNum_qt1dr2$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0));
    if (!b) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_usegoodsnum$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_usegoodsnum$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_randrade$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_randrade$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_randrade');
    if (numberToInt(random() * 1000) <= ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start)) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_randrade$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_randrade$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_randrade$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_menu$ObjectLiteral() {
  }
  ScriptProcess$cmd_menu$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 2;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  function ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    OperateAdapter.call(this);
    this.finished = false;
    this.rvAddr = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(closure$code, closure$start);
    this.items_ocvzno$_0 = lazy(ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral$items$lambda(closure$code, closure$start));
    this.menu = new ScreenCommonMenu(this.items, ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral$menu$lambda(this));
  }
  Object.defineProperty(ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype, 'items', {
    get: function () {
      var $receiver = this.items_ocvzno$_0;
      new PropertyMetadata('items');
      return $receiver.value;
    }
  });
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_menu');
    this.menu.reset();
    this.finished = false;
    this.delagete.pushScreen_2o7n0o$(this.menu);
    return true;
  };
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    this.menu.draw_9in0vv$(canvas);
  };
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.menu.onKeyDown_za3lpa$(key);
  };
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
    this.menu.onKeyUp_za3lpa$(key);
  };
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    if (this.finished)
      return false;
    this.menu.update_s8cxhz$(delta);
    return true;
  };
  function ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral$items$lambda(closure$code, closure$start) {
    return function () {
      var stringItems = split(gbkString_0(getCString(closure$code, closure$start + 2 | 0)), Kotlin.charArrayOf(32));
      println(stringItems);
      return copyToArray(stringItems);
    };
  }
  function ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral$menu$lambda(this$) {
    return function (it) {
      ScriptResources_getInstance().variables[this$.rvAddr] = it;
      this$.finished = true;
      return Unit;
    };
  }
  ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_menu$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_menu$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_menu$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_testmoney$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_testmoney$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 6 | 0;
  };
  function ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_testmoney');
    if (Player$Companion_getInstance().sMoney < ScriptProcess$Companion_getInstance().get4BytesInt_ir89t6$(this.closure$code, this.closure$start)) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_testmoney$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_testmoney$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_testmoney$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_callchapter$ObjectLiteral() {
  }
  ScriptProcess$cmd_callchapter$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  ScriptProcess$cmd_callchapter$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    throw new NotImplementedError('cmd_callchapter');
  };
  ScriptProcess$cmd_callchapter$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_discmp$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_discmp$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 8 | 0;
  };
  function ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_discmp');
    var var_0 = ScriptResources_getInstance().variables[ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start)];
    var num = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0);
    if (var_0 < num) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0));
    }
     else if (var_0 > num) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_discmp$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_discmp$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_discmp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_return$ObjectLiteral() {
  }
  ScriptProcess$cmd_return$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  ScriptProcess$cmd_return$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    throw new NotImplementedError('cmd_return');
  };
  ScriptProcess$cmd_return$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_timemsg$ObjectLiteral() {
  }
  ScriptProcess$cmd_timemsg$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    var i = 2;
    while (code[start + i | 0] !== 0)
      i = i + 1 | 0;
    return start + i + 1 | 0;
  };
  ScriptProcess$cmd_timemsg$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    throw new NotImplementedError('cmd_timemsg');
  };
  ScriptProcess$cmd_timemsg$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_disablesave$ObjectLiteral() {
  }
  ScriptProcess$cmd_disablesave$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral() {
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_disablesave');
    Global_getInstance().disableSave = true;
    return false;
  };
  ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_disablesave$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_disablesave$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_disablesave$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_enablesave$ObjectLiteral() {
  }
  ScriptProcess$cmd_enablesave$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral() {
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_enablesave');
    Global_getInstance().disableSave = false;
    return false;
  };
  ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_enablesave$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_enablesave$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_enablesave$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_gamesave$ObjectLiteral() {
  }
  ScriptProcess$cmd_gamesave$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  function ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral() {
    OperateAdapter.call(this);
    this.end_0 = false;
  }
  function ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral$process$lambda(this$) {
    return function () {
      this$.end_0 = true;
      return Unit;
    };
  }
  ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_gamesave');
    var view = new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$SAVE_getInstance());
    view.callback = ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral$process$lambda(this);
    this.delagete.pushScreen_2o7n0o$(view);
    this.end_0 = false;
    return true;
  };
  ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    return !this.end_0;
  };
  ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_gamesave$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_gamesave$ObjectLiteral$getOperate$ObjectLiteral();
  };
  ScriptProcess$cmd_gamesave$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_seteventtimer$ObjectLiteral() {
  }
  ScriptProcess$cmd_seteventtimer$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  ScriptProcess$cmd_seteventtimer$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    throw new NotImplementedError('cmd_seteventtimer');
  };
  ScriptProcess$cmd_seteventtimer$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_enableshowpos$ObjectLiteral() {
  }
  ScriptProcess$cmd_enableshowpos$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  ScriptProcess$cmd_enableshowpos$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return OperateNop$Companion_getInstance().nop;
  };
  ScriptProcess$cmd_enableshowpos$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_disableshowpos$ObjectLiteral() {
  }
  ScriptProcess$cmd_disableshowpos$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start;
  };
  ScriptProcess$cmd_disableshowpos$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return OperateNop$Companion_getInstance().nop;
  };
  ScriptProcess$cmd_disableshowpos$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setto$ObjectLiteral() {
  }
  ScriptProcess$cmd_setto$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 4 | 0;
  };
  function ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_setto');
    ScriptResources_getInstance().variables[ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0)] = ScriptResources_getInstance().variables[ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start)];
    return false;
  };
  ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_setto$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_setto$ObjectLiteral$getOperate$ObjectLiteral(code, start);
  };
  ScriptProcess$cmd_setto$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_testgoodsnum$ObjectLiteral(this$ScriptProcess) {
    this.this$ScriptProcess = this$ScriptProcess;
  }
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 10 | 0;
  };
  function ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral(closure$code, closure$start, this$ScriptProcess) {
    this.closure$code = closure$code;
    this.closure$start = closure$start;
    this.this$ScriptProcess = this$ScriptProcess;
    OperateAdapter.call(this);
  }
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral.prototype.process = function () {
    ScriptProcess$Companion_getInstance().cmdPrint_61zpoe$('cmd_testgoodsnum');
    var goodsnum = Player$Companion_getInstance().sGoodsList.getGoodsNum_vux9f0$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start), ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 2 | 0));
    var num = ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 4 | 0);
    if (goodsnum === num) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 6 | 0));
    }
     else if (goodsnum > num) {
      ensureNotNull(this.this$ScriptProcess.mScreenMainGame_0).gotoAddress_za3lpa$(ScriptProcess$Companion_getInstance().get2ByteInt_ir89t6$(this.closure$code, this.closure$start + 8 | 0));
    }
    return false;
  };
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [OperateAdapter]
  };
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return new ScriptProcess$cmd_testgoodsnum$ObjectLiteral$getOperate$ObjectLiteral(code, start, this.this$ScriptProcess);
  };
  ScriptProcess$cmd_testgoodsnum$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setarmstoss$ObjectLiteral() {
  }
  ScriptProcess$cmd_setarmstoss$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  ScriptProcess$cmd_setarmstoss$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return OperateNop$Companion_getInstance().nop;
  };
  ScriptProcess$cmd_setarmstoss$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  function ScriptProcess$cmd_setfightmiss$ObjectLiteral() {
  }
  ScriptProcess$cmd_setfightmiss$ObjectLiteral.prototype.getNextPos_ir89t6$ = function (code, start) {
    return start + 2 | 0;
  };
  ScriptProcess$cmd_setfightmiss$ObjectLiteral.prototype.getOperate_ir89t6$ = function (code, start) {
    return OperateNop$Companion_getInstance().nop;
  };
  ScriptProcess$cmd_setfightmiss$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScriptProcess$Command]
  };
  ScriptProcess.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScriptProcess',
    interfaces: []
  };
  function ScriptResources() {
    ScriptResources_instance = this;
    this.globalEvents = Kotlin.booleanArray(2401);
    this.variables = new Int32Array(240);
  }
  ScriptResources.prototype.initLocalVar = function () {
    for (var i = 200; i <= 239; i++) {
      this.variables[i] = 0;
    }
  };
  ScriptResources.prototype.initGlobalVar = function () {
    for (var i = 0; i <= 199; i++) {
      this.variables[i] = 0;
    }
  };
  ScriptResources.prototype.initGlobalEvents = function () {
    for (var i = 1; i <= 2400; i++) {
      this.globalEvents[i] = false;
    }
  };
  ScriptResources.prototype.setEvent_za3lpa$ = function (num) {
    this.globalEvents[num] = true;
  };
  ScriptResources.prototype.clearEvent_za3lpa$ = function (num) {
    this.globalEvents[num] = false;
  };
  ScriptResources.prototype.write_vcd9jg$ = function (out) {
    for (var i = 1; i <= 2400; i++) {
      out.writeBoolean_6taknv$(this.globalEvents[i]);
    }
    for (var i_0 = 0; i_0 <= 239; i_0++) {
      out.writeInt_za3lpa$(this.variables[i_0]);
    }
  };
  ScriptResources.prototype.read_setnfj$ = function (coder) {
    for (var i = 1; i <= 2400; i++) {
      this.globalEvents[i] = coder.readBoolean();
    }
    for (var i_0 = 0; i_0 <= 239; i_0++) {
      this.variables[i_0] = coder.readInt();
    }
  };
  ScriptResources.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'ScriptResources',
    interfaces: []
  };
  var ScriptResources_instance = null;
  function ScriptResources_getInstance() {
    if (ScriptResources_instance === null) {
      new ScriptResources();
    }
    return ScriptResources_instance;
  }
  function ScreenDelegate() {
  }
  ScreenDelegate.prototype.showMessage_61zpoe$ = function (msg) {
    this.showMessage_4wgjuj$(msg, Kotlin.Long.fromInt(1000));
  };
  ScreenDelegate.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ScreenDelegate',
    interfaces: []
  };
  function BaseScreen() {
    this.delegate_kstz2p$_0 = this.delegate_kstz2p$_0;
  }
  Object.defineProperty(BaseScreen.prototype, 'delegate', {
    get: function () {
      if (this.delegate_kstz2p$_0 == null)
        return throwUPAE('delegate');
      return this.delegate_kstz2p$_0;
    },
    set: function (delegate) {
      this.delegate_kstz2p$_0 = delegate;
    }
  });
  Object.defineProperty(BaseScreen.prototype, 'msgDelegate', {
    get: function () {
      return this.delegate;
    }
  });
  Object.defineProperty(BaseScreen.prototype, 'isPopup', {
    get: function () {
      return false;
    }
  });
  BaseScreen.prototype.willAppear = function () {
  };
  BaseScreen.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'BaseScreen',
    interfaces: []
  };
  function ScreenAnimation(index) {
    BaseScreen.call(this);
    this.index_0 = index;
    this.mResSrs_0 = null;
    var tmp$;
    if (this.index_0 !== 247 && this.index_0 !== 248 && this.index_0 !== 249) {
      throw new IllegalArgumentException('\u53EA\u80FD\u662F247,248,249');
    }
    this.mResSrs_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, this.index_0), ResSrs) ? tmp$ : throwCCE();
    this.mResSrs_0.setIteratorNum_za3lpa$(4);
    this.mResSrs_0.startAni();
  }
  ScreenAnimation.prototype.update_s8cxhz$ = function (delta) {
    if (!this.mResSrs_0.update_s8cxhz$(delta)) {
      if (this.index_0 === 247) {
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_GAME_LOGO_getInstance());
      }
       else if (this.index_0 === 248) {
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
      }
       else if (this.index_0 === 249) {
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
      }
    }
  };
  ScreenAnimation.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    this.mResSrs_0.draw_2g4tob$(canvas, 0, 0);
  };
  ScreenAnimation.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_CANCEL && (this.index_0 === 247 || this.index_0 === 248)) {
      this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MENU_getInstance());
    }
  };
  ScreenAnimation.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScreenAnimation.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenAnimation',
    interfaces: [BaseScreen]
  };
  function ScreenMenu() {
    BaseScreen.call(this);
    this.mImgMenu_0 = null;
    this.mLeft_0 = 0;
    this.mTop_0 = 0;
    this.mSrsSelector_0 = null;
    this.mCurSelect_0 = 0;
    this.isCancelKeyDown_0 = false;
    var tmp$, tmp$_0, tmp$_1;
    this.mImgMenu_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, 14), ResImage) ? tmp$ : throwCCE();
    this.mSrsSelector_0 = [Kotlin.isType(tmp$_0 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 250), ResSrs) ? tmp$_0 : throwCCE(), Kotlin.isType(tmp$_1 = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$SRS_getInstance(), 1, 251), ResSrs) ? tmp$_1 : throwCCE()];
    this.mSrsSelector_0[0].startAni();
    this.mSrsSelector_0[1].startAni();
    this.mLeft_0 = (160 - this.mImgMenu_0.width | 0) / 2 | 0;
    this.mTop_0 = (96 - this.mImgMenu_0.height | 0) / 2 | 0;
  }
  ScreenMenu.prototype.update_s8cxhz$ = function (delta) {
    if (!this.mSrsSelector_0[this.mCurSelect_0].update_s8cxhz$(delta)) {
      this.mSrsSelector_0[this.mCurSelect_0].startAni();
    }
  };
  ScreenMenu.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    this.mImgMenu_0.draw_tj1hu5$(canvas, 1, this.mLeft_0, this.mTop_0);
    this.mSrsSelector_0[this.mCurSelect_0].draw_2g4tob$(canvas, 0, 0);
  };
  ScreenMenu.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP || key === Global_getInstance().KEY_DOWN)
      this.mCurSelect_0 = 1 - this.mCurSelect_0 | 0;
    else if (key === Global_getInstance().KEY_CANCEL)
      this.isCancelKeyDown_0 = true;
  };
  ScreenMenu.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      if (this.mCurSelect_0 === 0) {
        SaveLoadGame_getInstance().startNewGame = true;
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MAIN_GAME_getInstance());
      }
       else if (this.mCurSelect_0 === 1) {
        this.delegate.pushScreen_2o7n0o$(new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$LOAD_getInstance()));
      }
    }
     else if (key === Global_getInstance().KEY_CANCEL && this.isCancelKeyDown_0) {
      println('\u9000\u51FA\u6E38\u620F');
    }
  };
  ScreenMenu.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMenu',
    interfaces: [BaseScreen]
  };
  function ScreenMessageBox(msg, mOnOkClickListener) {
    ScreenMessageBox$Companion_getInstance();
    BaseScreen.call(this);
    this.mOnOkClickListener_0 = mOnOkClickListener;
    this.index_0 = 0;
    this.mMsg_0 = '';
    if (msg != null) {
      this.mMsg_0 = msg;
    }
  }
  Object.defineProperty(ScreenMessageBox.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  function ScreenMessageBox$OnOKClickListener() {
  }
  ScreenMessageBox$OnOKClickListener.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'OnOKClickListener',
    interfaces: []
  };
  ScreenMessageBox.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenMessageBox.prototype.draw_9in0vv$ = function (canvas) {
    canvas.drawBitmap_t8cslu$(ScreenMessageBox$Companion_getInstance().bmpBg_0, 27, 15);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mMsg_0, 33, 23);
    if (this.index_0 === 0) {
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, '\u662F ', 45, 53);
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u5426 ', 93, 53);
    }
     else if (this.index_0 === 1) {
      TextRender_getInstance().drawText_kkuqvh$(canvas, '\u662F ', 45, 53);
      TextRender_getInstance().drawSelText_kkuqvh$(canvas, '\u5426 ', 93, 53);
    }
  };
  ScreenMessageBox.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_LEFT || key === Global_getInstance().KEY_RIGHT) {
      this.index_0 = 1 - this.index_0 | 0;
    }
  };
  ScreenMessageBox.prototype.onKeyUp_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_ENTER) {
      if (this.index_0 === 0 && this.mOnOkClickListener_0 != null) {
        this.mOnOkClickListener_0.onOKClick();
      }
      this.exit_0();
    }
     else if (key === Global_getInstance().KEY_CANCEL) {
      this.exit_0();
    }
  };
  ScreenMessageBox.prototype.exit_0 = function () {
    this.delegate.popScreen();
  };
  function ScreenMessageBox$Companion() {
    ScreenMessageBox$Companion_instance = this;
    this.bmpBg_0 = Bitmap$Companion_getInstance().createBitmap_vux9f0$(137 - 27 + 1 | 0, 81 - 15 + 1 | 0);
    var c = new Canvas(this.bmpBg_0);
    c.drawColor_we4i00$(Global_getInstance().COLOR_WHITE);
    var p = new Paint();
    p.color = Global_getInstance().COLOR_BLACK;
    p.style = Paint$Style$STROKE_getInstance();
    c.drawRect_x3aj6j$(1, 1, this.bmpBg_0.width - 5 | 0, this.bmpBg_0.height - 5 | 0, p);
    c.drawRect_x3aj6j$(43 - 27 | 0, 51 - 15 | 0, 70 - 27 | 0, 70 - 15 | 0, p);
    c.drawRect_x3aj6j$(91 - 27 | 0, 51 - 15 | 0, 118 - 27 | 0, 70 - 15 | 0, p);
    p.style = Paint$Style$FILL_AND_STROKE_getInstance();
    c.drawRect_x3aj6j$(32 - 27 | 0, 77 - 15 | 0, 137 - 27 | 0, 81 - 15 | 0, p);
    c.drawRect_x3aj6j$(133 - 27 | 0, 20 - 15 | 0, this.bmpBg_0.width - 1 | 0, this.bmpBg_0.height - 1 | 0, p);
  }
  ScreenMessageBox$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ScreenMessageBox$Companion_instance = null;
  function ScreenMessageBox$Companion_getInstance() {
    if (ScreenMessageBox$Companion_instance === null) {
      new ScreenMessageBox$Companion();
    }
    return ScreenMessageBox$Companion_instance;
  }
  ScreenMessageBox.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenMessageBox',
    interfaces: [BaseScreen]
  };
  function ScreenSaveLoadGame(mOperate) {
    BaseScreen.call(this);
    this.mOperate_0 = mOperate;
    this.mTextPos_0 = [new Int32Array([68, 28]), new Int32Array([68, 51]), new Int32Array([68, 74])];
    this.index_0 = 0;
    this.mEmpty_0 = '\u7A7A\u6863\u6848    ';
    this.mText_0 = [this.mEmpty_0, this.mEmpty_0, this.mEmpty_0];
    this.mHeadImgs_0 = ArrayList_init();
    this.mFileNames_0 = ['fmjsave0', 'fmjsave1', 'fmjsave2'];
    this.mImgBg_0 = null;
    this.callback = null;
    var tmp$;
    this.mImgBg_0 = Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 2, this.mOperate_0 === ScreenSaveLoadGame$Operate$LOAD_getInstance() ? 16 : 15), ResImage) ? tmp$ : throwCCE();
    this.mHeadImgs_0.add_11rb$(ArrayList_init());
    this.mHeadImgs_0.add_11rb$(ArrayList_init());
    this.mHeadImgs_0.add_11rb$(ArrayList_init());
    var file = new File('sav/' + this.mFileNames_0[0]);
    if (file.exists()) {
      this.mText_0[0] = this.format_0(this.getSceneNameAndHeads_0(file, this.mHeadImgs_0.get_za3lpa$(0)));
    }
    file = new File('sav/' + this.mFileNames_0[1]);
    if (file.exists()) {
      this.mText_0[1] = this.format_0(this.getSceneNameAndHeads_0(file, this.mHeadImgs_0.get_za3lpa$(1)));
    }
    file = new File('sav/' + this.mFileNames_0[2]);
    if (file.exists()) {
      this.mText_0[2] = this.format_0(this.getSceneNameAndHeads_0(file, this.mHeadImgs_0.get_za3lpa$(2)));
    }
  }
  function ScreenSaveLoadGame$Operate(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function ScreenSaveLoadGame$Operate_initFields() {
    ScreenSaveLoadGame$Operate_initFields = function () {
    };
    ScreenSaveLoadGame$Operate$SAVE_instance = new ScreenSaveLoadGame$Operate('SAVE', 0);
    ScreenSaveLoadGame$Operate$LOAD_instance = new ScreenSaveLoadGame$Operate('LOAD', 1);
  }
  var ScreenSaveLoadGame$Operate$SAVE_instance;
  function ScreenSaveLoadGame$Operate$SAVE_getInstance() {
    ScreenSaveLoadGame$Operate_initFields();
    return ScreenSaveLoadGame$Operate$SAVE_instance;
  }
  var ScreenSaveLoadGame$Operate$LOAD_instance;
  function ScreenSaveLoadGame$Operate$LOAD_getInstance() {
    ScreenSaveLoadGame$Operate_initFields();
    return ScreenSaveLoadGame$Operate$LOAD_instance;
  }
  ScreenSaveLoadGame$Operate.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Operate',
    interfaces: [Enum]
  };
  function ScreenSaveLoadGame$Operate$values() {
    return [ScreenSaveLoadGame$Operate$SAVE_getInstance(), ScreenSaveLoadGame$Operate$LOAD_getInstance()];
  }
  ScreenSaveLoadGame$Operate.values = ScreenSaveLoadGame$Operate$values;
  function ScreenSaveLoadGame$Operate$valueOf(name) {
    switch (name) {
      case 'SAVE':
        return ScreenSaveLoadGame$Operate$SAVE_getInstance();
      case 'LOAD':
        return ScreenSaveLoadGame$Operate$LOAD_getInstance();
      default:throwISE('No enum constant fmj.views.ScreenSaveLoadGame.Operate.' + name);
    }
  }
  ScreenSaveLoadGame$Operate.valueOf_61zpoe$ = ScreenSaveLoadGame$Operate$valueOf;
  ScreenSaveLoadGame.prototype.format_0 = function (s) {
    var s_0 = s;
    while (gbkBytes(s_0).length < gbkBytes(this.mEmpty_0).length)
      s_0 += ' ';
    return s_0;
  };
  ScreenSaveLoadGame.prototype.getSceneNameAndHeads_0 = function (f, heads) {
    var tmp$;
    var file = objectInputOf(f);
    var name = file.readString();
    var actorNum = file.readInt();
    for (var i = 0; i < actorNum; i++) {
      heads.add_11rb$(Kotlin.isType(tmp$ = DatLib$Companion_getInstance().getRes_2et8c9$(DatLib$ResType$PIC_getInstance(), 1, file.readInt()), ResImage) ? tmp$ : throwCCE());
    }
    file.close();
    return name;
  };
  ScreenSaveLoadGame.prototype.update_s8cxhz$ = function (delta) {
  };
  ScreenSaveLoadGame.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
    this.mImgBg_0.draw_tj1hu5$(canvas, 1, 0, 0);
    tmp$ = get_indices(this.mHeadImgs_0);
    tmp$_0 = tmp$.first;
    tmp$_1 = tmp$.last;
    tmp$_2 = tmp$.step;
    for (var i = tmp$_0; i <= tmp$_1; i += tmp$_2) {
      tmp$_3 = this.mHeadImgs_0.get_za3lpa$(i).size;
      for (var j = 0; j < tmp$_3; j++) {
        var img = this.mHeadImgs_0.get_za3lpa$(i).get_za3lpa$(j);
        img.draw_tj1hu5$(canvas, 7, 8 + (20 * j | 0) | 0, this.mTextPos_0[i][1] - 6 | 0);
      }
    }
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mText_0[0], this.mTextPos_0[0][0], this.mTextPos_0[0][1]);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mText_0[1], this.mTextPos_0[1][0], this.mTextPos_0[1][1]);
    TextRender_getInstance().drawText_kkuqvh$(canvas, this.mText_0[2], this.mTextPos_0[2][0], this.mTextPos_0[2][1]);
    TextRender_getInstance().drawSelText_kkuqvh$(canvas, this.mText_0[this.index_0], this.mTextPos_0[this.index_0][0], this.mTextPos_0[this.index_0][1]);
  };
  ScreenSaveLoadGame.prototype.onKeyDown_za3lpa$ = function (key) {
    if (key === Global_getInstance().KEY_UP) {
      if ((this.index_0 = this.index_0 - 1 | 0, this.index_0) < 0) {
        this.index_0 = 2;
      }
    }
     else if (key === Global_getInstance().KEY_DOWN) {
      if ((this.index_0 = this.index_0 + 1 | 0, this.index_0) > 2) {
        this.index_0 = 0;
      }
    }
  };
  function ScreenSaveLoadGame$onKeyUp$ObjectLiteral(closure$file, this$ScreenSaveLoadGame) {
    this.closure$file = closure$file;
    this.this$ScreenSaveLoadGame = this$ScreenSaveLoadGame;
  }
  ScreenSaveLoadGame$onKeyUp$ObjectLiteral.prototype.onOKClick = function () {
    var tmp$;
    this.this$ScreenSaveLoadGame.saveGame_epr4t4$(this.closure$file);
    this.this$ScreenSaveLoadGame.delegate.popScreen();
    this.this$ScreenSaveLoadGame.delegate.popScreen();
    this.this$ScreenSaveLoadGame.delegate.popScreen();
    (tmp$ = this.this$ScreenSaveLoadGame.callback) != null ? tmp$() : null;
  };
  ScreenSaveLoadGame$onKeyUp$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [ScreenMessageBox$OnOKClickListener]
  };
  ScreenSaveLoadGame.prototype.onKeyUp_za3lpa$ = function (key) {
    var tmp$;
    if (key === Global_getInstance().KEY_CANCEL) {
      this.delegate.popScreen();
    }
     else if (key === Global_getInstance().KEY_ENTER) {
      var file = new File('sav/' + this.mFileNames_0[this.index_0]);
      if (this.mOperate_0 === ScreenSaveLoadGame$Operate$LOAD_getInstance()) {
        if (!file.exists()) {
          return;
        }
        this.loadGame_epr4t4$(file);
        SaveLoadGame_getInstance().startNewGame = false;
        this.delegate.changeScreen_gacx6e$(ScreenViewType$SCREEN_MAIN_GAME_getInstance());
      }
       else {
        if (!file.exists()) {
          file.createNewFile();
          this.saveGame_epr4t4$(file);
          this.delegate.popScreen();
          this.delegate.popScreen();
          this.delegate.popScreen();
          (tmp$ = this.callback) != null ? tmp$() : null;
        }
         else {
          this.delegate.pushScreen_2o7n0o$(new ScreenMessageBox('\u8986\u76D6\u539F\u8FDB\u5EA6?', new ScreenSaveLoadGame$onKeyUp$ObjectLiteral(file, this)));
        }
      }
    }
  };
  ScreenSaveLoadGame.prototype.loadGame_epr4t4$ = function (file) {
    var ioIn = objectInputOf(file);
    SaveLoadGame_getInstance().read_setnfj$(ioIn);
    Combat$Companion_getInstance().SetDelegate_wle2mc$(this.delegate);
    ScriptResources_getInstance().read_setnfj$(ioIn);
    ioIn.close();
  };
  ScreenSaveLoadGame.prototype.saveGame_epr4t4$ = function (file) {
    var o = objectOutputOf(file);
    SaveLoadGame_getInstance().write_vcd9jg$(o);
    ScriptResources_getInstance().write_vcd9jg$(o);
    o.close();
  };
  ScreenSaveLoadGame.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenSaveLoadGame',
    interfaces: [BaseScreen]
  };
  function ScreenStack() {
    this.mScreenStack_0 = Stack$Companion_getInstance().create_287e2$();
  }
  ScreenStack.prototype.clear = function () {
    this.mScreenStack_0.clear();
  };
  ScreenStack.prototype.keyDown_za3lpa$ = function (key) {
    ensureNotNull(this.mScreenStack_0.peek()).onKeyDown_za3lpa$(key);
  };
  ScreenStack.prototype.keyUp_za3lpa$ = function (key) {
    ensureNotNull(this.mScreenStack_0.peek()).onKeyUp_za3lpa$(key);
  };
  ScreenStack.prototype.changeScreen_gacx6e$ = function (scr) {
    var tmp$;
    if (equals(scr, ScreenViewType$SCREEN_DEV_LOGO_getInstance()))
      tmp$ = new ScreenAnimation(247);
    else if (equals(scr, ScreenViewType$SCREEN_GAME_LOGO_getInstance()))
      tmp$ = new ScreenAnimation(248);
    else if (equals(scr, ScreenViewType$SCREEN_MENU_getInstance()))
      tmp$ = new ScreenMenu();
    else if (equals(scr, ScreenViewType$SCREEN_MAIN_GAME_getInstance()))
      tmp$ = new ScreenMainGame();
    else if (equals(scr, ScreenViewType$SCREEN_GAME_FAIL_getInstance()))
      tmp$ = new ScreenAnimation(249);
    else if (equals(scr, ScreenViewType$SCREEN_SAVE_GAME_getInstance()))
      tmp$ = new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$SAVE_getInstance());
    else if (equals(scr, ScreenViewType$SCREEN_LOAD_GAME_getInstance()))
      tmp$ = new ScreenSaveLoadGame(ScreenSaveLoadGame$Operate$LOAD_getInstance());
    else
      tmp$ = Kotlin.noWhenBranchMatched();
    var tmp = tmp$;
    this.mScreenStack_0.clear();
    this.mScreenStack_0.push_11rb$(tmp);
    tmp.willAppear();
    tmp.delegate = this;
  };
  ScreenStack.prototype.pushScreen_2o7n0o$ = function (scr) {
    this.mScreenStack_0.push_11rb$(scr);
    scr.willAppear();
    scr.delegate = this;
  };
  ScreenStack.prototype.popScreen = function () {
    var tmp$;
    this.mScreenStack_0.pop();
    (tmp$ = this.mScreenStack_0.peek()) != null ? (tmp$.willAppear(), Unit) : null;
  };
  ScreenStack.prototype.getCurScreen = function () {
    return ensureNotNull(this.mScreenStack_0.peek());
  };
  function ScreenStack$showMessage$ObjectLiteral(closure$delay, closure$msg) {
    this.closure$delay = closure$delay;
    this.closure$msg = closure$msg;
    BaseScreen.call(this);
    this.cnt_8be2vx$ = Kotlin.Long.ZERO;
  }
  Object.defineProperty(ScreenStack$showMessage$ObjectLiteral.prototype, 'isPopup', {
    get: function () {
      return true;
    }
  });
  ScreenStack$showMessage$ObjectLiteral.prototype.update_s8cxhz$ = function (delta) {
    this.cnt_8be2vx$ = this.cnt_8be2vx$.add(delta);
    if (this.cnt_8be2vx$.compareTo_11rb$(this.closure$delay) > 0) {
      this.delegate.popScreen();
    }
  };
  ScreenStack$showMessage$ObjectLiteral.prototype.draw_9in0vv$ = function (canvas) {
    Util_getInstance().showMessage_g6cl4j$(canvas, this.closure$msg);
  };
  ScreenStack$showMessage$ObjectLiteral.prototype.onKeyUp_za3lpa$ = function (key) {
  };
  ScreenStack$showMessage$ObjectLiteral.prototype.onKeyDown_za3lpa$ = function (key) {
    this.delegate.popScreen();
  };
  ScreenStack$showMessage$ObjectLiteral.$metadata$ = {
    kind: Kind_CLASS,
    interfaces: [BaseScreen]
  };
  ScreenStack.prototype.showMessage_4wgjuj$ = function (msg, delay) {
    this.pushScreen_2o7n0o$(new ScreenStack$showMessage$ObjectLiteral(delay, msg));
  };
  ScreenStack.prototype.draw_9in0vv$ = function (canvas) {
    var tmp$;
    tmp$ = this.mScreenStack_0.iterator();
    while (tmp$.hasNext()) {
      var scr = tmp$.next();
      scr.draw_9in0vv$(canvas);
    }
  };
  ScreenStack.prototype.update_s8cxhz$ = function (delta) {
    var tmp$;
    (tmp$ = this.mScreenStack_0.peek()) != null ? (tmp$.update_s8cxhz$(delta), Unit) : null;
  };
  ScreenStack.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ScreenStack',
    interfaces: [ScreenDelegate]
  };
  function Color(r, g, b, a) {
    Color$Companion_getInstance();
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }
  Object.defineProperty(Color.prototype, 'rgb', {
    get: function () {
      return (this.r >> 24 & this.g) >> 16 & this.b;
    }
  });
  function Color$Companion() {
    Color$Companion_instance = this;
    this.WHITE = new Color(180, 180, 180, 255);
    this.BLACK = new Color(0, 0, 0, 255);
    this.TRANSP = new Color(0, 0, 0, 0);
    this.RED = Color_init(255, 0, 0);
  }
  Color$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Color$Companion_instance = null;
  function Color$Companion_getInstance() {
    if (Color$Companion_instance === null) {
      new Color$Companion();
    }
    return Color$Companion_instance;
  }
  Color.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Color',
    interfaces: []
  };
  function Color_init(r, g, b, $this) {
    $this = $this || Object.create(Color.prototype);
    Color.call($this, r, g, b, 255);
    return $this;
  }
  function Paint() {
    this.style = Paint$Style$FILL_getInstance();
    this.color = Color$Companion_getInstance().BLACK;
    this.strokeWidth = 1;
  }
  function Paint$Style(name, ordinal) {
    Enum.call(this);
    this.name$ = name;
    this.ordinal$ = ordinal;
  }
  function Paint$Style_initFields() {
    Paint$Style_initFields = function () {
    };
    Paint$Style$FILL_instance = new Paint$Style('FILL', 0);
    Paint$Style$STROKE_instance = new Paint$Style('STROKE', 1);
    Paint$Style$FILL_AND_STROKE_instance = new Paint$Style('FILL_AND_STROKE', 2);
  }
  var Paint$Style$FILL_instance;
  function Paint$Style$FILL_getInstance() {
    Paint$Style_initFields();
    return Paint$Style$FILL_instance;
  }
  var Paint$Style$STROKE_instance;
  function Paint$Style$STROKE_getInstance() {
    Paint$Style_initFields();
    return Paint$Style$STROKE_instance;
  }
  var Paint$Style$FILL_AND_STROKE_instance;
  function Paint$Style$FILL_AND_STROKE_getInstance() {
    Paint$Style_initFields();
    return Paint$Style$FILL_AND_STROKE_instance;
  }
  Paint$Style.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Style',
    interfaces: [Enum]
  };
  function Paint$Style$values() {
    return [Paint$Style$FILL_getInstance(), Paint$Style$STROKE_getInstance(), Paint$Style$FILL_AND_STROKE_getInstance()];
  }
  Paint$Style.values = Paint$Style$values;
  function Paint$Style$valueOf(name) {
    switch (name) {
      case 'FILL':
        return Paint$Style$FILL_getInstance();
      case 'STROKE':
        return Paint$Style$STROKE_getInstance();
      case 'FILL_AND_STROKE':
        return Paint$Style$FILL_AND_STROKE_getInstance();
      default:throwISE('No enum constant graphics.Paint.Style.' + name);
    }
  }
  Paint$Style.valueOf_61zpoe$ = Paint$Style$valueOf;
  Paint.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Paint',
    interfaces: []
  };
  function Bitmap(width, height, buffer) {
    Bitmap$Companion_getInstance();
    this.width = width;
    this.height = height;
    this.buffer = buffer;
    this.color = Color$Companion_getInstance().WHITE;
  }
  Bitmap.prototype.drawImage_t8cslu$ = function (src, x, y) {
    this.setPixels_khhbfk$(src.buffer, 0, x, y, src.width, src.height);
  };
  Bitmap.prototype.setPixel_0 = function (col, row, color) {
    this.buffer[Kotlin.imul(this.width, row) + col | 0] = color;
  };
  Bitmap.prototype.fillRect_tjonv8$ = function (x, y, w, h) {
    var tmp$, tmp$_0;
    tmp$ = x + w | 0;
    for (var col = x; col < tmp$; col++) {
      tmp$_0 = y + h | 0;
      for (var row = y; row < tmp$_0; row++) {
        this.setPixel_0(col, row, this.color);
      }
    }
  };
  Bitmap.prototype.drawLine_tjonv8$ = function (x1, y1, x2, y2) {
    var dx = x2 - x1 | 0;
    var dy = y2 - y1 | 0;
    var ux = dx > 0 ? 1 : -1;
    var uy = dy > 0 ? 1 : -1;
    var x = x1;
    var y = y1;
    var eps = 0;
    dx = abs(dx);
    dy = abs(dy);
    if (dx > dy) {
      x = x1;
      while (true) {
        this.setPixel_0(x, y, this.color);
        if (x === x2)
          break;
        eps = eps + dy | 0;
        if (eps << 1 >= dx) {
          y = y + uy | 0;
          eps = eps - dx | 0;
        }
        x = x + ux | 0;
      }
    }
     else {
      y = y1;
      while (true) {
        this.setPixel_0(x, y, this.color);
        if (y === y2)
          break;
        eps = eps + dx | 0;
        if (eps << 1 >= dy) {
          x = x + ux | 0;
          eps = eps - dy | 0;
        }
        y = y + uy | 0;
      }
    }
  };
  Bitmap.prototype.drawRect_tjonv8$ = function (x, y, w, h) {
    this.drawLine_tjonv8$(x, y, x + w | 0, y);
    this.drawLine_tjonv8$(x + w | 0, y, x + w | 0, y + h | 0);
    this.drawLine_tjonv8$(x, y + h | 0, x + w | 0, y + h | 0);
    this.drawLine_tjonv8$(x, y, x, y + h | 0);
  };
  Bitmap.prototype.setPixels_khhbfk$ = function (pixels, offset, x, y, w, h) {
    var a = this.width - x | 0;
    var xWidth = Math_0.min(a, w);
    var a_0 = this.height - y | 0;
    var xHeight = Math_0.min(a_0, h);
    for (var col = 0; col < xWidth; col++) {
      for (var row = 0; row < xHeight; row++) {
        var dOff = Kotlin.imul(this.width, y + row | 0) + x + col | 0;
        var sOff = Kotlin.imul(w, row) + col | 0;
        if (pixels[offset + sOff | 0].a > 0) {
          this.buffer[dOff] = pixels[offset + sOff | 0];
        }
      }
    }
  };
  Bitmap.prototype.copy = function () {
    return new Bitmap(this.width, this.height, this.buffer.slice());
  };
  function Bitmap$Companion() {
    Bitmap$Companion_instance = this;
  }
  Bitmap$Companion.prototype.createBitmap_vux9f0$ = function (w, h) {
    return Bitmap_init(w, h);
  };
  Bitmap$Companion.prototype.createBitmap_34aymq$ = function (pixels, w, h) {
    return new Bitmap(w, h, pixels);
  };
  Bitmap$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Bitmap$Companion_instance = null;
  function Bitmap$Companion_getInstance() {
    if (Bitmap$Companion_instance === null) {
      new Bitmap$Companion();
    }
    return Bitmap$Companion_instance;
  }
  Bitmap.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Bitmap',
    interfaces: []
  };
  function Bitmap_init(width, height, $this) {
    $this = $this || Object.create(Bitmap.prototype);
    var tmp$ = width;
    var tmp$_0 = height;
    var array = Array_0(Kotlin.imul(width, height));
    var tmp$_1;
    tmp$_1 = array.length - 1 | 0;
    for (var i = 0; i <= tmp$_1; i++) {
      array[i] = Color$Companion_getInstance().WHITE;
    }
    Bitmap.call($this, tmp$, tmp$_0, array);
    return $this;
  }
  function Rect(left, top, right, bottom) {
    this.left = left;
    this.top = top;
    this.right = right;
    this.bottom = bottom;
  }
  Rect.prototype.width = function () {
    return this.right - this.left + 1 | 0;
  };
  Rect.prototype.height = function () {
    return this.bottom - this.top + 1 | 0;
  };
  Rect.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Rect',
    interfaces: []
  };
  Rect.prototype.component1 = function () {
    return this.left;
  };
  Rect.prototype.component2 = function () {
    return this.top;
  };
  Rect.prototype.component3 = function () {
    return this.right;
  };
  Rect.prototype.component4 = function () {
    return this.bottom;
  };
  Rect.prototype.copy_tjonv8$ = function (left, top, right, bottom) {
    return new Rect(left === void 0 ? this.left : left, top === void 0 ? this.top : top, right === void 0 ? this.right : right, bottom === void 0 ? this.bottom : bottom);
  };
  Rect.prototype.toString = function () {
    return 'Rect(left=' + Kotlin.toString(this.left) + (', top=' + Kotlin.toString(this.top)) + (', right=' + Kotlin.toString(this.right)) + (', bottom=' + Kotlin.toString(this.bottom)) + ')';
  };
  Rect.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.left) | 0;
    result = result * 31 + Kotlin.hashCode(this.top) | 0;
    result = result * 31 + Kotlin.hashCode(this.right) | 0;
    result = result * 31 + Kotlin.hashCode(this.bottom) | 0;
    return result;
  };
  Rect.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.left, other.left) && Kotlin.equals(this.top, other.top) && Kotlin.equals(this.right, other.right) && Kotlin.equals(this.bottom, other.bottom)))));
  };
  function RectF(left, top, right, bottom) {
    this.left = left;
    this.top = top;
    this.right = right;
    this.bottom = bottom;
  }
  RectF.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RectF',
    interfaces: []
  };
  RectF.prototype.component1 = function () {
    return this.left;
  };
  RectF.prototype.component2 = function () {
    return this.top;
  };
  RectF.prototype.component3 = function () {
    return this.right;
  };
  RectF.prototype.component4 = function () {
    return this.bottom;
  };
  RectF.prototype.copy_7b5o5w$ = function (left, top, right, bottom) {
    return new RectF(left === void 0 ? this.left : left, top === void 0 ? this.top : top, right === void 0 ? this.right : right, bottom === void 0 ? this.bottom : bottom);
  };
  RectF.prototype.toString = function () {
    return 'RectF(left=' + Kotlin.toString(this.left) + (', top=' + Kotlin.toString(this.top)) + (', right=' + Kotlin.toString(this.right)) + (', bottom=' + Kotlin.toString(this.bottom)) + ')';
  };
  RectF.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.left) | 0;
    result = result * 31 + Kotlin.hashCode(this.top) | 0;
    result = result * 31 + Kotlin.hashCode(this.right) | 0;
    result = result * 31 + Kotlin.hashCode(this.bottom) | 0;
    return result;
  };
  RectF.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.left, other.left) && Kotlin.equals(this.top, other.top) && Kotlin.equals(this.right, other.right) && Kotlin.equals(this.bottom, other.bottom)))));
  };
  function Point(x, y) {
    if (x === void 0)
      x = 0;
    if (y === void 0)
      y = 0;
    this.x = x;
    this.y = y;
  }
  Point.prototype.set_vux9f0$ = function (x, y) {
    this.x = x;
    this.y = y;
  };
  Point.prototype.offset_vux9f0$ = function (dx, dy) {
    this.x = this.x + dx | 0;
    this.y = this.y + dy | 0;
  };
  Point.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Point',
    interfaces: []
  };
  Point.prototype.component1 = function () {
    return this.x;
  };
  Point.prototype.component2 = function () {
    return this.y;
  };
  Point.prototype.copy_vux9f0$ = function (x, y) {
    return new Point(x === void 0 ? this.x : x, y === void 0 ? this.y : y);
  };
  Point.prototype.toString = function () {
    return 'Point(x=' + Kotlin.toString(this.x) + (', y=' + Kotlin.toString(this.y)) + ')';
  };
  Point.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.x) | 0;
    result = result * 31 + Kotlin.hashCode(this.y) | 0;
    return result;
  };
  Point.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.x, other.x) && Kotlin.equals(this.y, other.y)))));
  };
  function Canvas(b) {
    this.bg_0 = b;
  }
  Object.defineProperty(Canvas.prototype, 'width', {
    get: function () {
      return this.bg_0.width;
    }
  });
  Object.defineProperty(Canvas.prototype, 'height', {
    get: function () {
      return this.bg_0.height;
    }
  });
  Object.defineProperty(Canvas.prototype, 'buffer', {
    get: function () {
      return this.bg_0.buffer;
    }
  });
  Canvas.prototype.drawBitmap_t8cslu$ = function (bitmap, left, top) {
    this.bg_0.drawImage_t8cslu$(bitmap, left, top);
  };
  Canvas.prototype.drawBitmap_2map2q$ = function (bitmap, left, top) {
    this.drawBitmap_t8cslu$(bitmap, numberToInt(left), numberToInt(top));
  };
  Canvas.prototype.drawColor_we4i00$ = function (color) {
    this.bg_0.color = color;
    this.bg_0.fillRect_tjonv8$(0, 0, this.bg_0.width, this.bg_0.height);
  };
  Canvas.prototype.drawLine_x3aj6j$ = function (startX, startY, stopX, stopY, paint) {
    this.drawLine_gwdwo5$(startX, startY, stopX, stopY, paint);
  };
  Canvas.prototype.drawLine_gwdwo5$ = function (startX, startY, stopX, stopY, paint) {
    this.bg_0.color = paint.color;
    this.bg_0.drawLine_tjonv8$(numberToInt(startX), numberToInt(startY), numberToInt(stopX), numberToInt(stopY));
  };
  Canvas.prototype.drawR_0 = function (x, y, i, j, paint, color) {
    var tmp$;
    this.bg_0.color = color;
    tmp$ = paint.style;
    if (equals(tmp$, Paint$Style$FILL_getInstance()))
      this.bg_0.fillRect_tjonv8$(x, y, i, j);
    else if (equals(tmp$, Paint$Style$STROKE_getInstance()))
      this.bg_0.drawRect_tjonv8$(x, y, i, j);
    else
      this.bg_0.fillRect_tjonv8$(x, y, i, j);
  };
  Canvas.prototype.drawRect_x3aj6j$ = function (x, y, i, j, paint) {
    this.drawR_0(x, y, i - x | 0, j - y | 0, paint, paint.color);
  };
  Canvas.prototype.drawRect_ed5hcw$ = function (rect, paint) {
    this.drawR_0(rect.left, rect.top, rect.right - rect.left | 0, rect.bottom - rect.top | 0, paint, paint.color);
  };
  Canvas.prototype.drawRect_mw38p4$ = function (rect, paint) {
    this.drawR_0(numberToInt(rect.left), numberToInt(rect.top), numberToInt(rect.right - rect.left), numberToInt(rect.bottom - rect.top), paint, paint.color);
  };
  Canvas.prototype.drawLines_ffeagz$ = function (dots, paint) {
    this.bg_0.color = paint.color;
    var size = dots.length / 4 | 0;
    for (var i = 0; i < size; i++) {
      this.bg_0.drawLine_tjonv8$(numberToInt(dots[i * 4 | 0]), numberToInt(dots[(i * 4 | 0) + 1 | 0]), numberToInt(dots[(i * 4 | 0) + 2 | 0]), numberToInt(dots[(i * 4 | 0) + 3 | 0]));
    }
  };
  Canvas.prototype.setBitmap_963ehe$ = function (bmp) {
    this.bg_0 = bmp;
  };
  Canvas.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Canvas',
    interfaces: []
  };
  function Canvas_init($this) {
    $this = $this || Object.create(Canvas.prototype);
    Canvas.call($this, Bitmap$Companion_getInstance().createBitmap_vux9f0$(Global_getInstance().SCREEN_WIDTH, Global_getInstance().SCREEN_HEIGHT));
    return $this;
  }
  function Canvas_init_0(width, height, $this) {
    $this = $this || Object.create(Canvas.prototype);
    Canvas.call($this, Bitmap$Companion_getInstance().createBitmap_vux9f0$(width, height));
    return $this;
  }
  function Stack(list) {
    Stack$Companion_getInstance();
    this.items_0 = list;
  }
  Object.defineProperty(Stack.prototype, 'size', {
    get: function () {
      return this.items_0.size;
    }
  });
  Stack.prototype.push_11rb$ = function (element) {
    var position = this.items_0.size;
    this.items_0.add_wxm5ur$(position, element);
  };
  Stack.prototype.toString = function () {
    return this.items_0.toString();
  };
  Stack.prototype.pop = function () {
    var tmp$;
    if (this.items_0.isEmpty()) {
      tmp$ = null;
    }
     else {
      tmp$ = this.items_0.removeAt_za3lpa$(this.items_0.size - 1 | 0);
    }
    return tmp$;
  };
  Stack.prototype.peek = function () {
    var tmp$;
    if (this.items_0.isEmpty()) {
      tmp$ = null;
    }
     else {
      tmp$ = last(this.items_0);
    }
    return tmp$;
  };
  Stack.prototype.clear = function () {
    this.items_0.clear();
  };
  Stack.prototype.iterator = function () {
    return this.items_0.iterator();
  };
  function Stack$Companion() {
    Stack$Companion_instance = this;
  }
  Stack$Companion.prototype.create_287e2$ = function () {
    return new Stack(ArrayList_init());
  };
  Stack$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Stack$Companion_instance = null;
  function Stack$Companion_getInstance() {
    if (Stack$Companion_instance === null) {
      new Stack$Companion();
    }
    return Stack$Companion_instance;
  }
  Stack.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Stack',
    interfaces: []
  };
  function Random() {
  }
  Random.prototype.nextInt_za3lpa$ = function (rng) {
    var x = random() * rng;
    return numberToInt(Math_0.floor(x));
  };
  Random.prototype.nextBoolean = function () {
    return this.nextInt_za3lpa$(2) === 1;
  };
  Random.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Random',
    interfaces: []
  };
  function Coder() {
  }
  Coder.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Coder',
    interfaces: []
  };
  function ObjectOutput() {
  }
  ObjectOutput.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ObjectOutput',
    interfaces: []
  };
  function ObjectInput() {
  }
  ObjectInput.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'ObjectInput',
    interfaces: []
  };
  function Runnable() {
  }
  Runnable.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Runnable',
    interfaces: []
  };
  function gbkString($receiver, offset, length) {
    var arr = toTypedArray(sliceArray($receiver, until(offset, offset + length | 0)));
    return sysGbkDecode(arr);
  }
  function gbkString_0($receiver) {
    return sysGbkDecode(toTypedArray($receiver));
  }
  function getCString($receiver, from) {
    if (from === void 0)
      from = 0;
    var tmp$;
    var src = from === 0 ? $receiver : sliceArray($receiver, until(from, $receiver.length));
    var ind = indexOf(src, 0);
    if (ind === -1) {
      tmp$ = src;
    }
     else {
      tmp$ = toByteArray(slice(src, until(0, ind)));
    }
    return tmp$;
  }
  function gbkBytes($receiver) {
    return toByteArray_0(sysGbkEncode($receiver));
  }
  function System() {
    System_instance = this;
  }
  System.prototype.arraycopy_nlwz52$ = function (src, srcPos, dest, destPos, length) {
    for (var i = 0; i < length; i++) {
      dest[destPos + i | 0] = src[srcPos + i | 0];
    }
  };
  System.prototype.arraycopy_vybhjg$ = function (src, srcPos, dest, destPos, length) {
    if ((dest.length - destPos | 0) < length) {
      throw new Error_0('array copy dst overflow');
    }
    if ((src.length - srcPos | 0) < length) {
      throw new Error_0('array copy src overflow');
    }
    for (var i = 0; i < length; i++) {
      dest[destPos + i | 0] = src[srcPos + i | 0];
    }
  };
  System.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'System',
    interfaces: []
  };
  var System_instance = null;
  function System_getInstance() {
    if (System_instance === null) {
      new System();
    }
    return System_instance;
  }
  function Queue() {
  }
  Queue.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Queue',
    interfaces: []
  };
  function ArrayQueue(base) {
    ArrayQueue$Companion_getInstance();
    this.base_0 = base;
  }
  ArrayQueue.prototype.push_11rb$ = function (item) {
    this.base_0.add_11rb$(item);
  };
  ArrayQueue.prototype.pop = function () {
    var tmp$;
    if (this.base_0.isEmpty()) {
      tmp$ = null;
    }
     else {
      var r = first(this.base_0);
      this.base_0.removeAt_za3lpa$(0);
      tmp$ = r;
    }
    return tmp$;
  };
  function ArrayQueue$Companion() {
    ArrayQueue$Companion_instance = this;
  }
  ArrayQueue$Companion.prototype.create_287e2$ = function () {
    return new ArrayQueue(ArrayList_init());
  };
  ArrayQueue$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var ArrayQueue$Companion_instance = null;
  function ArrayQueue$Companion_getInstance() {
    if (ArrayQueue$Companion_instance === null) {
      new ArrayQueue$Companion();
    }
    return ArrayQueue$Companion_instance;
  }
  Object.defineProperty(ArrayQueue.prototype, 'size', {
    get: function () {
      return this.base_0.size;
    }
  });
  ArrayQueue.prototype.add_11rb$ = function (element) {
    return this.base_0.add_11rb$(element);
  };
  ArrayQueue.prototype.add_wxm5ur$ = function (index, element) {
    return this.base_0.add_wxm5ur$(index, element);
  };
  ArrayQueue.prototype.addAll_u57x28$ = function (index, elements) {
    return this.base_0.addAll_u57x28$(index, elements);
  };
  ArrayQueue.prototype.addAll_brywnq$ = function (elements) {
    return this.base_0.addAll_brywnq$(elements);
  };
  ArrayQueue.prototype.clear = function () {
    return this.base_0.clear();
  };
  ArrayQueue.prototype.contains_11rb$ = function (element) {
    return this.base_0.contains_11rb$(element);
  };
  ArrayQueue.prototype.containsAll_brywnq$ = function (elements) {
    return this.base_0.containsAll_brywnq$(elements);
  };
  ArrayQueue.prototype.get_za3lpa$ = function (index) {
    return this.base_0.get_za3lpa$(index);
  };
  ArrayQueue.prototype.indexOf_11rb$ = function (element) {
    return this.base_0.indexOf_11rb$(element);
  };
  ArrayQueue.prototype.isEmpty = function () {
    return this.base_0.isEmpty();
  };
  ArrayQueue.prototype.iterator = function () {
    return this.base_0.iterator();
  };
  ArrayQueue.prototype.lastIndexOf_11rb$ = function (element) {
    return this.base_0.lastIndexOf_11rb$(element);
  };
  ArrayQueue.prototype.listIterator = function () {
    return this.base_0.listIterator();
  };
  ArrayQueue.prototype.listIterator_za3lpa$ = function (index) {
    return this.base_0.listIterator_za3lpa$(index);
  };
  ArrayQueue.prototype.remove_11rb$ = function (element) {
    return this.base_0.remove_11rb$(element);
  };
  ArrayQueue.prototype.removeAll_brywnq$ = function (elements) {
    return this.base_0.removeAll_brywnq$(elements);
  };
  ArrayQueue.prototype.removeAt_za3lpa$ = function (index) {
    return this.base_0.removeAt_za3lpa$(index);
  };
  ArrayQueue.prototype.retainAll_brywnq$ = function (elements) {
    return this.base_0.retainAll_brywnq$(elements);
  };
  ArrayQueue.prototype.set_wxm5ur$ = function (index, element) {
    return this.base_0.set_wxm5ur$(index, element);
  };
  ArrayQueue.prototype.subList_vux9f0$ = function (fromIndex, toIndex) {
    return this.base_0.subList_vux9f0$(fromIndex, toIndex);
  };
  ArrayQueue.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ArrayQueue',
    interfaces: [Queue, MutableList]
  };
  function hexByte(b) {
    var tbl = '0123456789ABCDEF';
    var l = tbl.charCodeAt(b & 15);
    var h = tbl.charCodeAt(b >> 4 & 15);
    return String.fromCharCode(h) + String.fromCharCode(l);
  }
  function hexByte$half(c) {
    var tmp$;
    if ((new CharRange(48, 57)).contains_mef7kx$(c))
      tmp$ = c - 48;
    else if ((new CharRange(97, 102)).contains_mef7kx$(c))
      tmp$ = c - 97 + 10 | 0;
    else if ((new CharRange(65, 70)).contains_mef7kx$(c))
      tmp$ = c - 65 + 10 | 0;
    else
      tmp$ = 0;
    return tmp$;
  }
  function hexByte_0(h, l) {
    var half = hexByte$half;
    return toByte(half(h) << 4 | half(l));
  }
  function hexDecode(s) {
    var $receiver = new IntRange(0, s.length / 2 | 0);
    var destination = ArrayList_init(collectionSizeOrDefault($receiver, 10));
    var tmp$;
    tmp$ = $receiver.iterator();
    while (tmp$.hasNext()) {
      var item = tmp$.next();
      destination.add_11rb$(hexByte_0(s.charCodeAt(item * 2 | 0), s.charCodeAt((item * 2 | 0) + 1 | 0)));
    }
    return toByteArray(destination);
  }
  function hexEncode$lambda(it) {
    return hexByte(it);
  }
  function hexEncode(arr) {
    return joinToString(arr, '', void 0, void 0, void 0, void 0, hexEncode$lambda);
  }
  function File(path) {
    File$Companion_getInstance();
    this.path_0 = path;
  }
  function File$Companion() {
    File$Companion_instance = this;
  }
  File$Companion.prototype.contentsOf_61zpoe$ = function (path) {
    var file = new File(path);
    var rv = file.readAll();
    file.close();
    return rv;
  };
  File$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var File$Companion_instance = null;
  function File$Companion_getInstance() {
    if (File$Companion_instance === null) {
      new File$Companion();
    }
    return File$Companion_instance;
  }
  File.prototype.exists = function () {
    return sysStorageHas(this.path_0);
  };
  File.prototype.createNewFile = function () {
  };
  File.prototype.readAll = function () {
    var tmp$;
    var s = sysStorageGet(this.path_0);
    if (s == null) {
      throw new Error_0('File not found: ' + this.path_0);
    }
     else {
      tmp$ = hexDecode(s);
    }
    return tmp$;
  };
  File.prototype.wholeWrite_fqrh44$ = function (buf) {
    sysStorageSet(this.path_0, hexEncode(buf));
  };
  File.prototype.close = function () {
  };
  File.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'File',
    interfaces: []
  };
  function toIntShl($receiver, sft) {
    return ($receiver & 255) << (sft * 8 | 0);
  }
  function byte($receiver, ind) {
    return toByte($receiver >> (ind * 8 | 0) & 255);
  }
  function ObjectInputStream(file) {
    this.file_0 = file;
    this.buffer_0 = this.file_0.readAll();
    this.cur_0 = 0;
  }
  ObjectInputStream.prototype.readByte = function () {
    var tmp$;
    return this.buffer_0[tmp$ = this.cur_0, this.cur_0 = tmp$ + 1 | 0, tmp$];
  };
  ObjectInputStream.prototype.readBytes_0 = function (n) {
    var c = this.cur_0;
    this.cur_0 = this.cur_0 + n | 0;
    return sliceArray(this.buffer_0, until(c, c + n | 0));
  };
  ObjectInputStream.prototype.readInt = function () {
    var bytes = this.readBytes_0(2);
    return toIntShl(bytes[0], 0) | toIntShl(bytes[1], 1);
  };
  ObjectInputStream.prototype.readLong = function () {
    var bytes = this.readBytes_0(4);
    return Kotlin.Long.fromInt(toIntShl(bytes[0], 0) | toIntShl(bytes[1], 1) | toIntShl(bytes[2], 2) | toIntShl(bytes[3], 3));
  };
  ObjectInputStream.prototype.readString = function () {
    var len = this.readInt();
    return gbkString(this.readBytes_0(len), 0, len);
  };
  ObjectInputStream.prototype.readBoolean = function () {
    return this.readByte() !== 0;
  };
  ObjectInputStream.prototype.readIntArray = function () {
    var len = this.readInt();
    var array = new Int32Array(len);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = this.readInt();
    }
    return array;
  };
  ObjectInputStream.prototype.close = function () {
    this.file_0.close();
  };
  ObjectInputStream.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ObjectInputStream',
    interfaces: [ObjectInput]
  };
  var readArray = defineInlineFunction('fmj.core.java.readArray_z86k8m$', wrapFunction(function () {
    var Array_0 = Array;
    return function (T_0, isT, io, read) {
      var len = io.readInt();
      var array = Array_0(len);
      var tmp$;
      tmp$ = array.length - 1 | 0;
      for (var i = 0; i <= tmp$; i++) {
        array[i] = read(io);
      }
      return array;
    };
  }));
  function writeArray(out, objs, write) {
    var tmp$;
    out.writeInt_za3lpa$(objs.length);
    for (tmp$ = 0; tmp$ !== objs.length; ++tmp$) {
      var obj = objs[tmp$];
      write(out, obj);
    }
  }
  function ObjectOutputStream(file) {
    this.file_0 = file;
    this.buffer_0 = ArrayList_init();
  }
  ObjectOutputStream.prototype.writeInt_za3lpa$ = function (v) {
    this.buffer_0.add_11rb$(byte(v, 0));
    this.buffer_0.add_11rb$(byte(v, 1));
  };
  ObjectOutputStream.prototype.writeByte_s8j3t7$ = function (v) {
    this.buffer_0.add_11rb$(v);
  };
  ObjectOutputStream.prototype.writeLong_s8cxhz$ = function (v) {
    var i = v.toInt();
    this.buffer_0.add_11rb$(byte(i, 0));
    this.buffer_0.add_11rb$(byte(i, 1));
    this.buffer_0.add_11rb$(byte(i, 2));
    this.buffer_0.add_11rb$(byte(i, 3));
  };
  ObjectOutputStream.prototype.writeString_61zpoe$ = function (v) {
    var bytes = gbkBytes(v);
    var len = bytes.length;
    this.writeInt_za3lpa$(len);
    this.buffer_0.addAll_brywnq$(toList(bytes));
  };
  ObjectOutputStream.prototype.writeBoolean_6taknv$ = function (v) {
    var i = v ? 1 : 0;
    this.writeByte_s8j3t7$(toByte(i));
  };
  ObjectOutputStream.prototype.writeIntArray_q5rwfd$ = function (v) {
    var tmp$;
    this.writeInt_za3lpa$(v.length);
    for (tmp$ = 0; tmp$ !== v.length; ++tmp$) {
      var i = v[tmp$];
      this.writeInt_za3lpa$(i);
    }
  };
  ObjectOutputStream.prototype.close = function () {
    this.file_0.wholeWrite_fqrh44$(toByteArray(this.buffer_0));
    this.file_0.close();
  };
  ObjectOutputStream.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'ObjectOutputStream',
    interfaces: [ObjectOutput]
  };
  function objectInputOf(f) {
    return new ObjectInputStream(f);
  }
  function objectOutputOf(f) {
    return new ObjectOutputStream(f);
  }
  function random() {
    return sysRandom();
  }
  var package$fmj = _.fmj || (_.fmj = {});
  package$fmj.GameView = GameView;
  package$fmj.main_kand9s$ = main;
  Object.defineProperty(package$fmj, 'Global', {
    get: Global_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_DEV_LOGO', {
    get: ScreenViewType$SCREEN_DEV_LOGO_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_GAME_LOGO', {
    get: ScreenViewType$SCREEN_GAME_LOGO_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_MENU', {
    get: ScreenViewType$SCREEN_MENU_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_MAIN_GAME', {
    get: ScreenViewType$SCREEN_MAIN_GAME_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_GAME_FAIL', {
    get: ScreenViewType$SCREEN_GAME_FAIL_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_SAVE_GAME', {
    get: ScreenViewType$SCREEN_SAVE_GAME_getInstance
  });
  Object.defineProperty(ScreenViewType, 'SCREEN_LOAD_GAME', {
    get: ScreenViewType$SCREEN_LOAD_GAME_getInstance
  });
  package$fmj.ScreenViewType = ScreenViewType;
  Object.defineProperty(Character$State, 'STOP', {
    get: Character$State$STOP_getInstance
  });
  Object.defineProperty(Character$State, 'FORCE_MOVE', {
    get: Character$State$FORCE_MOVE_getInstance
  });
  Object.defineProperty(Character$State, 'WALKING', {
    get: Character$State$WALKING_getInstance
  });
  Object.defineProperty(Character$State, 'PAUSE', {
    get: Character$State$PAUSE_getInstance
  });
  Object.defineProperty(Character$State, 'ACTIVE', {
    get: Character$State$ACTIVE_getInstance
  });
  Object.defineProperty(Character$State, 'Companion', {
    get: Character$State$Companion_getInstance
  });
  Character.State = Character$State;
  var package$characters = package$fmj.characters || (package$fmj.characters = {});
  package$characters.Character = Character;
  Object.defineProperty(Direction, 'North', {
    get: Direction$North_getInstance
  });
  Object.defineProperty(Direction, 'East', {
    get: Direction$East_getInstance
  });
  Object.defineProperty(Direction, 'South', {
    get: Direction$South_getInstance
  });
  Object.defineProperty(Direction, 'West', {
    get: Direction$West_getInstance
  });
  Object.defineProperty(Direction, 'Companion', {
    get: Direction$Companion_getInstance
  });
  package$characters.Direction = Direction;
  Object.defineProperty(FightingCharacter, 'Companion', {
    get: FightingCharacter$Companion_getInstance
  });
  package$characters.FightingCharacter = FightingCharacter;
  package$characters.FightingSprite = FightingSprite;
  Object.defineProperty(Monster, 'Companion', {
    get: Monster$Companion_getInstance
  });
  package$characters.Monster = Monster;
  NPC.ICanWalk = NPC$ICanWalk;
  Object.defineProperty(NPC, 'Companion', {
    get: NPC$Companion_getInstance
  });
  package$characters.NPC = NPC;
  Object.defineProperty(Player, 'Companion', {
    get: Player$Companion_getInstance
  });
  package$characters.Player = Player;
  Object.defineProperty(ResLevelupChain, 'Companion', {
    get: ResLevelupChain$Companion_getInstance
  });
  package$characters.ResLevelupChain = ResLevelupChain;
  package$characters.SceneObj = SceneObj;
  Object.defineProperty(WalkingSprite, 'Companion', {
    get: WalkingSprite$Companion_getInstance
  });
  package$characters.WalkingSprite = WalkingSprite;
  var package$combat = package$fmj.combat || (package$fmj.combat = {});
  package$combat.ActionExecutor = ActionExecutor;
  Object.defineProperty(Combat, 'Companion', {
    get: Combat$Companion_getInstance
  });
  package$combat.Combat = Combat;
  Object.defineProperty(Action, 'Companion', {
    get: Action$Companion_getInstance
  });
  var package$actions = package$combat.actions || (package$combat.actions = {});
  package$actions.Action = Action;
  Object.defineProperty(ActionCoopMagic, 'Companion', {
    get: ActionCoopMagic$Companion_getInstance
  });
  package$actions.ActionCoopMagic_init_3g2wtp$ = ActionCoopMagic_init;
  package$actions.ActionCoopMagic_init_zbpvge$ = ActionCoopMagic_init_0;
  package$actions.ActionCoopMagic = ActionCoopMagic;
  package$actions.ActionDefend = ActionDefend;
  package$actions.ActionFlee = ActionFlee;
  Object.defineProperty(ActionMagicAttackAll, 'Companion', {
    get: ActionMagicAttackAll$Companion_getInstance
  });
  package$actions.ActionMagicAttackAll = ActionMagicAttackAll;
  Object.defineProperty(ActionMagicAttackOne, 'Companion', {
    get: ActionMagicAttackOne$Companion_getInstance
  });
  package$actions.ActionMagicAttackOne = ActionMagicAttackOne;
  Object.defineProperty(ActionMagicHelpAll, 'Companion', {
    get: ActionMagicHelpAll$Companion_getInstance
  });
  package$actions.ActionMagicHelpAll = ActionMagicHelpAll;
  Object.defineProperty(ActionMagicHelpOne, 'Companion', {
    get: ActionMagicHelpOne$Companion_getInstance
  });
  package$actions.ActionMagicHelpOne = ActionMagicHelpOne;
  package$actions.ActionMultiTarget = ActionMultiTarget;
  package$actions.ActionPhysicalAttackAll = ActionPhysicalAttackAll;
  package$actions.ActionPhysicalAttackOne = ActionPhysicalAttackOne;
  package$actions.ActionSingleTarget = ActionSingleTarget;
  Object.defineProperty(ActionThrowItemAll, 'Companion', {
    get: ActionThrowItemAll$Companion_getInstance
  });
  package$actions.ActionThrowItemAll = ActionThrowItemAll;
  Object.defineProperty(ActionThrowItemOne, 'Companion', {
    get: ActionThrowItemOne$Companion_getInstance
  });
  package$actions.ActionThrowItemOne = ActionThrowItemOne;
  Object.defineProperty(ActionUseItemAll, 'Companion', {
    get: ActionUseItemAll$Companion_getInstance
  });
  package$actions.ActionUseItemAll = ActionUseItemAll;
  Object.defineProperty(ActionUseItemOne, 'Companion', {
    get: ActionUseItemOne$Companion_getInstance
  });
  package$actions.ActionUseItemOne = ActionUseItemOne;
  Object.defineProperty(package$actions, 'CalcDamage', {
    get: CalcDamage_getInstance
  });
  var package$anim = package$combat.anim || (package$combat.anim = {});
  package$anim.FrameAnimation = FrameAnimation;
  package$anim.RaiseAnimation = RaiseAnimation;
  var package$ui = package$combat.ui || (package$combat.ui = {});
  package$ui.CombatSuccess = CombatSuccess;
  CombatUI.CallBack = CombatUI$CallBack;
  Object.defineProperty(CombatUI, 'Companion', {
    get: CombatUI$Companion_getInstance
  });
  package$ui.CombatUI = CombatUI;
  var package$gamemenu = package$fmj.gamemenu || (package$fmj.gamemenu = {});
  package$gamemenu.ScreenActorState = ScreenActorState;
  Object.defineProperty(ScreenActorWearing, 'Companion', {
    get: ScreenActorWearing$Companion_getInstance
  });
  package$gamemenu.ScreenActorWearing = ScreenActorWearing;
  package$gamemenu.ScreenChgEquipment = ScreenChgEquipment;
  package$gamemenu.ScreenCommonMenu = ScreenCommonMenu;
  package$gamemenu.ScreenGameMainMenu = ScreenGameMainMenu;
  Object.defineProperty(ScreenGoodsList$Mode, 'Sale', {
    get: ScreenGoodsList$Mode$Sale_getInstance
  });
  Object.defineProperty(ScreenGoodsList$Mode, 'Buy', {
    get: ScreenGoodsList$Mode$Buy_getInstance
  });
  Object.defineProperty(ScreenGoodsList$Mode, 'Use', {
    get: ScreenGoodsList$Mode$Use_getInstance
  });
  ScreenGoodsList.Mode = ScreenGoodsList$Mode;
  ScreenGoodsList.OnItemSelectedListener = ScreenGoodsList$OnItemSelectedListener;
  Object.defineProperty(ScreenGoodsList, 'Companion', {
    get: ScreenGoodsList$Companion_getInstance
  });
  package$gamemenu.ScreenGoodsList = ScreenGoodsList;
  package$gamemenu.ScreenMenuGoods = ScreenMenuGoods;
  package$gamemenu.ScreenMenuProperties = ScreenMenuProperties;
  package$gamemenu.ScreenMenuSystem = ScreenMenuSystem;
  package$gamemenu.ScreenTakeMedicine = ScreenTakeMedicine;
  Object.defineProperty(ScreenUseMagic, 'Companion', {
    get: ScreenUseMagic$Companion_getInstance
  });
  package$gamemenu.ScreenUseMagic = ScreenUseMagic;
  var package$goods = package$fmj.goods || (package$fmj.goods = {});
  package$goods.BaseGoods = BaseGoods;
  package$goods.GoodsDecorations = GoodsDecorations;
  package$goods.GoodsDrama = GoodsDrama;
  package$goods.GoodsEquipment = GoodsEquipment;
  package$goods.GoodsHiddenWeapon = GoodsHiddenWeapon;
  package$goods.GoodsManage = GoodsManage;
  package$goods.GoodsMedicine = GoodsMedicine;
  package$goods.GoodsMedicineChg4Ever = GoodsMedicineChg4Ever;
  package$goods.GoodsMedicineLife = GoodsMedicineLife;
  package$goods.GoodsStimulant = GoodsStimulant;
  package$goods.GoodsTudun = GoodsTudun;
  package$goods.GoodsWeapon = GoodsWeapon;
  package$goods.IEatMedicine = IEatMedicine;
  var package$graphics = package$fmj.graphics || (package$fmj.graphics = {});
  Object.defineProperty(package$graphics, 'TextRender', {
    get: TextRender_getInstance
  });
  Object.defineProperty(Tiles, 'Companion', {
    get: Tiles$Companion_getInstance
  });
  package$graphics.Tiles = Tiles;
  Object.defineProperty(package$graphics, 'Util', {
    get: Util_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'GUT', {
    get: DatLib$ResType$GUT_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'MAP', {
    get: DatLib$ResType$MAP_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'ARS', {
    get: DatLib$ResType$ARS_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'MRS', {
    get: DatLib$ResType$MRS_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'SRS', {
    get: DatLib$ResType$SRS_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'GRS', {
    get: DatLib$ResType$GRS_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'TIL', {
    get: DatLib$ResType$TIL_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'ACP', {
    get: DatLib$ResType$ACP_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'GDP', {
    get: DatLib$ResType$GDP_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'GGJ', {
    get: DatLib$ResType$GGJ_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'PIC', {
    get: DatLib$ResType$PIC_getInstance
  });
  Object.defineProperty(DatLib$ResType, 'MLR', {
    get: DatLib$ResType$MLR_getInstance
  });
  DatLib.ResType = DatLib$ResType;
  Object.defineProperty(DatLib, 'Companion', {
    get: DatLib$Companion_getInstance
  });
  var package$lib = package$fmj.lib || (package$fmj.lib = {});
  package$lib.DatLib = DatLib;
  Object.defineProperty(ResBase, 'Companion', {
    get: ResBase$Companion_getInstance
  });
  package$lib.ResBase = ResBase;
  package$lib.ResGut = ResGut;
  package$lib.ResImage = ResImage;
  Object.defineProperty(ResMap, 'Companion', {
    get: ResMap$Companion_getInstance
  });
  package$lib.ResMap = ResMap;
  package$lib.ResSrs = ResSrs;
  var package$magic = package$fmj.magic || (package$fmj.magic = {});
  package$magic.BaseMagic = BaseMagic;
  package$magic.MagicAttack = MagicAttack;
  package$magic.MagicAuxiliary = MagicAuxiliary;
  package$magic.MagicEnhance = MagicEnhance;
  package$magic.MagicRestore = MagicRestore;
  package$magic.MagicSpecial = MagicSpecial;
  Object.defineProperty(ResMagicChain, 'Companion', {
    get: ResMagicChain$Companion_getInstance
  });
  package$magic.ResMagicChain = ResMagicChain;
  ScreenMagic.OnItemSelectedListener = ScreenMagic$OnItemSelectedListener;
  Object.defineProperty(ScreenMagic, 'Companion', {
    get: ScreenMagic$Companion_getInstance
  });
  package$magic.ScreenMagic = ScreenMagic;
  $$importsForInline$$['fmj.core'] = _;
  var package$scene = package$fmj.scene || (package$fmj.scene = {});
  Object.defineProperty(package$scene, 'SaveLoadGame', {
    get: SaveLoadGame_getInstance
  });
  Object.defineProperty(ScreenMainGame, 'Companion', {
    get: ScreenMainGame$Companion_getInstance
  });
  package$scene.ScreenMainGame = ScreenMainGame;
  var package$script = package$fmj.script || (package$fmj.script = {});
  package$script.Operate = Operate;
  Object.defineProperty(OperateNop, 'Companion', {
    get: OperateNop$Companion_getInstance
  });
  package$script.OperateNop = OperateNop;
  package$script.OperateAdapter = OperateAdapter;
  package$script.OperateBuy = OperateBuy;
  package$script.OperateDrawOnce = OperateDrawOnce;
  package$script.OperateSale = OperateSale;
  Object.defineProperty(ScriptExecutor, 'Companion', {
    get: ScriptExecutor$Companion_getInstance
  });
  package$script.ScriptExecutor = ScriptExecutor;
  Object.defineProperty(ScriptProcess, 'Companion', {
    get: ScriptProcess$Companion_getInstance
  });
  package$script.ScriptProcess = ScriptProcess;
  Object.defineProperty(package$script, 'ScriptResources', {
    get: ScriptResources_getInstance
  });
  var package$views = package$fmj.views || (package$fmj.views = {});
  package$views.ScreenDelegate = ScreenDelegate;
  package$views.BaseScreen = BaseScreen;
  package$views.ScreenAnimation = ScreenAnimation;
  package$views.ScreenMenu = ScreenMenu;
  ScreenMessageBox.OnOKClickListener = ScreenMessageBox$OnOKClickListener;
  Object.defineProperty(ScreenMessageBox, 'Companion', {
    get: ScreenMessageBox$Companion_getInstance
  });
  package$views.ScreenMessageBox = ScreenMessageBox;
  Object.defineProperty(ScreenSaveLoadGame$Operate, 'SAVE', {
    get: ScreenSaveLoadGame$Operate$SAVE_getInstance
  });
  Object.defineProperty(ScreenSaveLoadGame$Operate, 'LOAD', {
    get: ScreenSaveLoadGame$Operate$LOAD_getInstance
  });
  ScreenSaveLoadGame.Operate = ScreenSaveLoadGame$Operate;
  package$views.ScreenSaveLoadGame = ScreenSaveLoadGame;
  package$views.ScreenStack = ScreenStack;
  Object.defineProperty(Color, 'Companion', {
    get: Color$Companion_getInstance
  });
  var package$graphics_0 = _.graphics || (_.graphics = {});
  package$graphics_0.Color_init_qt1dr2$ = Color_init;
  package$graphics_0.Color = Color;
  Object.defineProperty(Paint$Style, 'FILL', {
    get: Paint$Style$FILL_getInstance
  });
  Object.defineProperty(Paint$Style, 'STROKE', {
    get: Paint$Style$STROKE_getInstance
  });
  Object.defineProperty(Paint$Style, 'FILL_AND_STROKE', {
    get: Paint$Style$FILL_AND_STROKE_getInstance
  });
  Paint.Style = Paint$Style;
  package$graphics_0.Paint = Paint;
  Object.defineProperty(Bitmap, 'Companion', {
    get: Bitmap$Companion_getInstance
  });
  package$graphics_0.Bitmap_init_vux9f0$ = Bitmap_init;
  package$graphics_0.Bitmap = Bitmap;
  package$graphics_0.Rect = Rect;
  package$graphics_0.RectF = RectF;
  package$graphics_0.Point = Point;
  package$graphics_0.Canvas_init = Canvas_init;
  package$graphics_0.Canvas_init_vux9f0$ = Canvas_init_0;
  package$graphics_0.Canvas = Canvas;
  Object.defineProperty(Stack, 'Companion', {
    get: Stack$Companion_getInstance
  });
  var package$java = _.java || (_.java = {});
  package$java.Stack = Stack;
  package$java.Random = Random;
  package$java.Coder = Coder;
  package$java.ObjectOutput = ObjectOutput;
  package$java.ObjectInput = ObjectInput;
  package$java.Runnable = Runnable;
  package$java.gbkString_ietg8x$ = gbkString;
  package$java.gbkString_964n91$ = gbkString_0;
  package$java.getCString_mrm5p$ = getCString;
  package$java.gbkBytes_pdl1vz$ = gbkBytes;
  Object.defineProperty(package$java, 'System', {
    get: System_getInstance
  });
  package$java.Queue = Queue;
  Object.defineProperty(ArrayQueue, 'Companion', {
    get: ArrayQueue$Companion_getInstance
  });
  package$java.ArrayQueue = ArrayQueue;
  Object.defineProperty(File, 'Companion', {
    get: File$Companion_getInstance
  });
  package$java.File = File;
  package$java.ObjectInputStream = ObjectInputStream;
  package$java.writeArray_3kjok1$ = writeArray;
  package$java.ObjectOutputStream = ObjectOutputStream;
  package$java.objectInputOf_epr4t4$ = objectInputOf;
  package$java.objectOutputOf_epr4t4$ = objectOutputOf;
  package$java.random = random;
  ScreenStack.prototype.showMessage_61zpoe$ = ScreenDelegate.prototype.showMessage_61zpoe$;
  main([]);
  Kotlin.defineModule('fmj.core', _);
  return _;
}(typeof this['fmj.core'] === 'undefined' ? {} : this['fmj.core'], kotlin);
